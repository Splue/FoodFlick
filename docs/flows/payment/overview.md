use stripe because industry standard.

use "stripe connect" to connect rests and customers. customers -> stripe connect -> customers

when you make a payemnt (charge) from customer to connected account, this is how stripe handles it.

1) the payment, P1, goes into foodflickco's stripe account. these payemnts are listened on the foodflickco's payment section in the sidebar. therefore this section will contain payments for ALL restaurants.

2) notice that charge need to specify a transfer destination. stripe uses this data to then take foodflickco money and "transfers" it to the specified restaurant which creates a "transfer" in the connected account's transfer tab. notice the connected account also has a "payments" tab. this is a "Copy" of foodflick's P1 payment. every "transfer" creats a copy of the original payment that went to ff and places that copy into the restaurant's payments. 
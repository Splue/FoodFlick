we store rest banking information in elastic

"banking": {
  "routingNumber": "110000000",
  "accountNumber": "000123456789",
  "stripeId": "acct_1Eiw8nLnIrhdwX9D"
}

We can store routing + account number without worry of any compliance issues. but maybe we should consider not storing
it anyway due to higher risks involved

most importantly, we store the stripeId. When a rest adds banking information to receive payment, FAS adds/updates a
stripe connected account identified by stripeId. Stripe has different connected account types. 

https://stripe.com/docs/connect
```
Standard accounts can be created in any country Stripe supports, except countries that are currently invite only.

Express accounts can only be created within the U.S. and Canada, but platforms in any supported country can create them. Contact us if you are interested in support for users outside the U.S. or Canada.

Custom accounts are supported only for platforms located in Australia, Austria, Belgium, Canada, Denmark, Finland, France, Germany, Hong Kong, Ireland, Italy, Japan, Luxembourg, the Netherlands, New Zealand, Norway, Portugal, Singapore, Spain, Sweden, Switzerland, the United Kingdom, or the United States.
```

We use "custom" connected accounts. but maybe we should be express...

when a rest receives a payment...
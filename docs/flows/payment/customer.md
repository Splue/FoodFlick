When a customer signs up, they have the option to add a card.

If they do, then we signup the user with a card token in user_metadata. Auth0 has a signup hook which detects this cardToken and follows up by signing up for a stripe account with that card token and adding that customerId to app_metadata. The hook DOES NOT add the cardToken to user_metadata, though maybe
it should? (nah, let's keep just reference the stripeId so we dont need to keep anything consistent if user stores mulitple cards. also makes it so we dont need to update any tokens stripe is source of truth)

The reason we do a hook is because if i dont, after signup, user signs in. but what if he signs in before i add the stripe data into app_data? Signup could call FAS to create stripeId and pass that to Auth0, but at the time of implementation we decided to use a hook to reduce network trips. Thinking back now I should have just did an auth0 signup with the cardToken. then immediately called FAS api to add into stripeId into
app_metadata, THEN signed in.

If the customer does not add a card then there's no cardToken in the signup and thus the stripe customer is created without a cardTok. Auth0 still stores the stripeId in app_metadata in both cases, but in the latter, there is no cardTok associated with that stripeId.

When updating the card FAS grabs the stripeId associated with the user and updates the stripe customers cardTok with the new cardTok
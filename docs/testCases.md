1. sign in
   1. bad login
   2. good login
2. sign up
   1. fill everything
   2. dupe email
   3. password too short
   4. skip address
   5. skip banking
3. sign out
4. add restaurant
5. menu
   1. add category
   2. add dupe category
   3. update category
   4. reorder category
   5. update item
      1. name
      2. dupe item name
      3. price
      4. addon
      5. printers
      6. option group add
      7. option group reorder
      8. option group remove
      9. choice add
      10. choice reorder
      11. choice remove
   6. add item
      1. name
      2. dupe item name
      3. price
      4. addon
      5. printers
      6. option group add
      7. option group reorder
      8. option group remove
      9. choice add
      10. choice reorder
      11. choice remove
   7. reorder item
      1. update item from this screen
6. update profile
7. update address
8. manager
   1. edit
   2. add
   3. remove
9. receiver update
10. update url
    1.  DUPE url
11. order managment udate
12. banking
    1.  update
    2.  show loading...
13. open orders
    1.  can update for x minutes. then goes to tip.
    3.  manager can close immediately
    4.  maanger returns order
14. pending tip orders
    1.  customer can update tip for 3 hours
15. completed orders
    1.  can return orders
16. change user email


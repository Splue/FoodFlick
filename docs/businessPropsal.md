# FoodFlick Overview

FoodFlick is an app for restaurants, used to accomplish 3 things.

1) Manage their menu with pictures
2) Promote their restaurant
3) Receive restauant feedback

### Customer perspective

A lot of eaters, myself included, choose restaruants on yelp / google before actually going there. Whether I am sitting at home or hanging out, I always take the time to look up where I want to eat before I go to a restaurant.

My deciding factors for choosing a restaurant are

1. price
2. overall rating
3. menu
4. pictures of food

Factors price and rating are easy enough to judge, but the menu and pictures are more inconvenient. Sometimes menus or *good* pictures don't even exist. When I finally find the menu, I pray it's in an easily readable format.

- Flashplayer menu? -- Well shit
- PDF -- I reluctantly download the damn file, only to find the PDF hard as hell to read. I gotta zoom in here, zoom out and rezoom there. Then I gotta swipe right crazy like I'm on Tinder just so I can see the right side of the menu. No.
- External website -- Damn this website ugly. How do I find the menu on this website? Ah there it is. 
- Yelp / Google menu renderer -- Pretty good, but where's the pictures? (Yelp actually started adding user-uploaded pictures directly to menu items but it's not everywhere, and the menu ui sucks).

FoodFlick provides a consistent and easily readable menu for all restaurant adopters. The menu is managed by the restaruant itself and is therefore guaranteed to be up to date. It's not uploaded once and forgotten. It's a living, breathing menu for the restaurant to be viewed by customers anytime -- inside or outside the restaurant. Daily specials, run-out-of items and other menu changes can all be seen in real-time. (code doesn't actually support real time currently, but we can and for now it's "most updated" when you first open the menu).

Whereas pictures directly next to menu items are non-existent in google and optional in yelp, they are *required* in FoodFlick. I know *exactly* what I'm ordering. No guessing. No vague answers from your server. No need to match menu item descriptions to user uploaded photos in yelp, only to find that the picture you stared at is an entirely different order when your food arrives.

 What's Rotelle pasta? How thin are the fries here? How big is this plate? I've never been to a Nigerian restaurant, what is this stuff? These *restaurant uploaded* pictures say it all.

Yeah, cool story bro. How will FoodFlick get restaurant buy-in in the first place? Where's the data going to come from? How is FoodFlick better than yelp / google?

### Restaurant perspective

Because our customer *is* the restaurant. The focus away from customers and towards the restaurants is FoodFlick's strategy for success. FoodFlick's ultimate goal is to enhance restaurant management, starting with menu maintenence, promotion pushing, and private feedback. Later it will add data analysis, custom profiles, table-ready alerts, and ordering through the app.

Every feature added must only *help* the restaurant and so features which may hurt the restaurant, such as ratings and comments are intentionally left out. Replacing ratings are "likes" and replacing comments is private feedback.

While the ability to provide convenient immediate menu updates (no more printing new menus, adjusting item prices with whiteout,memorizing specials) certainly allows restaurant flexibility, one of the main benefits of FoodFlick are its promotional opportunities. In addition to purchasing sponsorship ads on the  trending page, every restaurant can send promotions to users who have favorited the restaurant. This increases visibility, attracts new customers, and keeps them coming back. This is available for free!

The first focus is to make restaurants *want* to download FoodFlick. Usage provides only benefits and it's free, so there's no reason to NOT use it. The most inconvenient experience is creating the menu, which is why FoodFlick must make menu creation as easy as possible.

This will give us data and then hopefully lead to customer users.
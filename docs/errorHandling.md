if the error occurs as a result of rendering, ez. App.js's componentDidCatch will handle it.


if the error occurs as a result of event handling, then it's more interesting.

For now, we only focus on errors that happen due to our own event handlers, which we have 2 types of

1) regular actions
2) async actions that call some service


Both types of actions require doEvent from componentEventHandler because both events can error asyncly, since all event handlers are naturally async. see https://github.com/facebook/react/issues/11409.

If it's a regular action, then we simply want the action to throw an error up to the caller, which in this case is doEvent. doEvent then throws the error up to App.js for error handling, which logs the error (includes line #);

ex:

```
C:\Users\simon\Projects\Source\FoodFlick\node_modules\react-native\Libraries\ReactNative\YellowBox.…:80 APP.JS error info Error: Error: TESTING SUCKA

This error is located at:
    in RestProfileScreen (created by Connect(RestProfileScreen))
    in Connect(RestProfileScreen) (at SceneView.js:17)
    in SceneView (at CardStack.js:455)
    in RCTView (at View.js:60)
    in View (at CardStack.js:454)
    in RCTView (at View.js:60)
    in View (at CardStack.js:453)
    in RCTView (at View.js:60)
    in View (at createAnimatedComponent.js:154)
    in AnimatedComponent (at Card.js:12)
    in Card (at PointerEventsContainer.js:39)
    in Container (at CardStack.js:498)
    in RCTView (at View.js:60)
    in View (at CardStack.js:414)
    in RCTView (at View.js:60)
    in View (at CardStack.js:413)
    in CardStack (at CardStackTransitioner.js:67)
    in RCTView (at View.js:60)
    in View (at Transitioner.js:142)
    in Transitioner (at CardStackTransitioner.js:19)
    in CardStackTransitioner (at StackNavigator.js:7)
    in StackView (created by Connect(StackView))
    in Connect(StackView) (at createNavigator.js:13)
    in Navigator (at SceneView.js:17)
    in SceneView (at ResourceSavingSceneView.js:55)
    in RCTView (at View.js:60)
    in View (at ResourceSavingSceneView.js:48)
    in RCTView (at View.js:60)
    in View (at ResourceSavingSceneView.js:39)
    in ResourceSavingSceneView (at TabView.js:35)
    in RCTView (at View.js:60)
    in View (created by ViewPagerAndroid)
    in AndroidViewPager (at ViewPagerAndroid.android.js:236)
    in ViewPagerAndroid (at TabViewPagerAndroid.js:127)
    in TabViewPagerAndroid (at TabViewAnimated.js:71)
    in RCTView (at View.js:60)
    in View (at TabViewAnimated.js:194)
    in TabViewAnimated (at TabView.js:192)
    in TabView (at withCachedChildNavigation.js:69)
    in withCachedChildNavigation(TabView) (at TabNavigator.js:34)
    in Unknown (at createNavigator.js:13)
    in Navigator (at createNavigationContainer.js:226)
    in NavigationContainer (at AppNavigator.js:34)
    in RCTView (at View.js:60)
    in View (at Container.js:15)
    in Container (at connectStyle.js:384)
    in Styled(Container) (at AppNavigator.js:33)
    in AppNavigator (created by Connect(AppNavigator))
    in Connect(AppNavigator) (at App.js:55)
    in RCTView (at View.js:60)
    in View (at Root.js:13)
    in Root (at connectStyle.js:384)
    in Styled(Root) (at App.js:54)
    in StyleProvider (at App.js:52)
    in Provider (at App.js:51)
    in ApolloProvider (at App.js:50)
    in FoodFlick (at registerRootComponent.js:35)
    in RootErrorBoundary (at registerRootComponent.js:34)
    in ExpoRootComponent (at renderApplication.js:33)
    in RCTView (at View.js:60)
    in View (at AppContainer.js:102)
    in RCTView (at View.js:60)
    in View (at AppContainer.js:122)
    in AppContainer (at renderApplication.js:32)
    at C:\Users\simon\Projects\Source\FoodFlick\src\general\rest\restActions.ts:108
    at C:\Users\simon\Projects\Source\FoodFlick\node_modules\redux-thunk\lib\index.js:11
    at onConfirm (C:\Users\simon\Projects\Source\FoodFlick\src\account\myRest\RestProfileScreen.tsx:199)
    at doEvent (C:\Users\simon\Projects\Source\FoodFlick\src\general\utils\componentEventHandler.js:21)
    at Object.onConfirm [as onPress] (C:\Users\simon\Projects\Source\FoodFlick\src\account\myRest\RestProfileScreen.tsx:130)
    at Object.touchableHandlePress (C:\Users\simon\Projects\Source\FoodFlick\node_modules\react-native\Libraries\Components\Touchable\T…:175)
    at Object._performSideEffectsForTransition (C:\Users\simon\Projects\Source\FoodFlick\node_modules\react-native\Libraries\Components\Touchable\T…:744)
    at Object._receiveSignal (C:\Users\simon\Projects\Source\FoodFlick\node_modules\react-native\Libraries\Components\Touchable\T…:662)
    at Object.touchableHandleResponderRelease (C:\Users\simon\Projects\Source\FoodFlick\node_modules\react-native\Libraries\Components\Touchable\T…:431)
    at Object.invokeGuardedCallback (C:\Users\simon\Projects\Source\FoodFlick\node_modules\react-native\Libraries\Renderer\ReactNativeRe…:39) 
```


If it is an async action, then that's because it calls some service. If the service errors, then we will get a graphql error and graphql's errorLink will take care of that error by logging it (includes everything about graqphl). if we simply throw this error all the way up, then we get no userful line numbers unforunately (maybe because most recent calls are inside apollo library). we can work around this by catching the error int eh action and rethrowing. this gives us the action which produced the error (and errorLink takes care of apollo error). doHandle throws it up to App.js and so we also get the component which caused the error.

ex:

```


console.warn(`[GraphQL error]: Message: ${message}, Location: ${JSON.stringify(locations)}, Path: ${path}`),
C:\Users\simon\Projects\Source\FoodFlick\node_modules\react-native\Libraries\ReactNative\YellowBox.…:80 [GraphQL error]: Message: Variable "$newProfile" got invalid value {"name":"Alfredo's Italian Kitchen","description":"Family Italian food","phone":"","tagsQuery":"","tagSuggestions":[],"tags":["Cafe"]}.
In field "tagsQuery": Unknown field.
In field "tagSuggestions": Unknown field., Location: [{"line":1,"column":42}], Path: undefined


console.warn(`[Network error]: ${networkError}
C:\Users\simon\Projects\Source\FoodFlick\node_modules\react-native\Libraries\ReactNative\YellowBox.…:80 [Network error]: Error: Response not successful: Received status code 400



console.log(JSON.stringify(error));
 {"graphQLErrors":[],"networkError":{"response":{"type":"default","status":400,"ok":false,"headers":{"map":{"transfer-encoding":"chunked","connection":"keep-alive","date":"Sun, 18 Nov 2018 06:45:28 GMT","content-type":"application/json","cache-control":"public, max-age=0","access-control-allow-origin":"*","x-powered-by":"Express"}},"url":"http://192.168.0.13:3000/graphql","_bodyInit":{"_data":{"size":349,"offset":0,"blobId":"3c3b33cc-a1de-41f8-b860-b18d5e47adc0"}},"_bodyBlob":{"_data":{"size":349,"offset":0,"blobId":"3c3b33cc-a1de-41f8-b860-b18d5e47adc0"}},"bodyUsed":true},"statusCode":400,"result":{"errors":[{"message":"Variable \"$newProfile\" got invalid value {\"name\":\"Alfredo's Italian Kitchen\",\"description\":\"Family Italian food\",\"phone\":\"\",\"tagsQuery\":\"\",\"tagSuggestions\":[],\"tags\":[\"Cafe\"]}.\nIn field \"tagsQuery\": Unknown field.\nIn field \"tagSuggestions\": Unknown field.","locations":[{"line":1,"column":42}]}]}},"message":"Network error: Response not successful: Received status code 400"}

APP.JS error info Error: Error: Network error: Response not successful: Received status code 400

This error is located at:
    in RestProfileScreen (created by Connect(RestProfileScreen))
    in Connect(RestProfileScreen) (at SceneView.js:17)
    in SceneView (at CardStack.js:455)
    in RCTView (at View.js:60)
    in View (at CardStack.js:454)
    in RCTView (at View.js:60)
    in View (at CardStack.js:453)
    in RCTView (at View.js:60)
    in View (at createAnimatedComponent.js:154)
    in AnimatedComponent (at Card.js:12)
    in Card (at PointerEventsContainer.js:39)
    in Container (at CardStack.js:498)
    in RCTView (at View.js:60)
    in View (at CardStack.js:414)
    in RCTView (at View.js:60)
    in View (at CardStack.js:413)
    in CardStack (at CardStackTransitioner.js:67)
    in RCTView (at View.js:60)
    in View (at Transitioner.js:142)
    in Transitioner (at CardStackTransitioner.js:19)
    in CardStackTransitioner (at StackNavigator.js:7)
    in StackView (created by Connect(StackView))
    in Connect(StackView) (at createNavigator.js:13)
    in Navigator (at SceneView.js:17)
    in SceneView (at ResourceSavingSceneView.js:55)
    in RCTView (at View.js:60)
    in View (at ResourceSavingSceneView.js:48)
    in RCTView (at View.js:60)
    in View (at ResourceSavingSceneView.js:39)
    in ResourceSavingSceneView (at TabView.js:35)
    in RCTView (at View.js:60)
    in View (created by ViewPagerAndroid)
    in AndroidViewPager (at ViewPagerAndroid.android.js:236)
    in ViewPagerAndroid (at TabViewPagerAndroid.js:127)
    in TabViewPagerAndroid (at TabViewAnimated.js:71)
    in RCTView (at View.js:60)
    in View (at TabViewAnimated.js:194)
    in TabViewAnimated (at TabView.js:192)
    in TabView (at withCachedChildNavigation.js:69)
    in withCachedChildNavigation(TabView) (at TabNavigator.js:34)
    in Unknown (at createNavigator.js:13)
    in Navigator (at createNavigationContainer.js:226)
    in NavigationContainer (at AppNavigator.js:34)
    in RCTView (at View.js:60)
    in View (at Container.js:15)
    in Container (at connectStyle.js:384)
    in Styled(Container) (at AppNavigator.js:33)
    in AppNavigator (created by Connect(AppNavigator))
    in Connect(AppNavigator) (at App.js:55)
    in RCTView (at View.js:60)
    in View (at Root.js:13)
    in Root (at connectStyle.js:384)
    in Styled(Root) (at App.js:54)
    in StyleProvider (at App.js:52)
    in Provider (at App.js:51)
    in ApolloProvider (at App.js:50)
    in FoodFlick (at registerRootComponent.js:35)
    in RootErrorBoundary (at registerRootComponent.js:34)
    in ExpoRootComponent (at renderApplication.js:33)
    in RCTView (at View.js:60)
    in View (at AppContainer.js:102)
    in RCTView (at View.js:60)
    in View (at AppContainer.js:122)
    in AppContainer (at renderApplication.js:32)
_callee10$
    C:\Users\simon\Projects\Source\FoodFlick\src\general\rest\restActions.ts:111:10
tryCatch
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\regenerator-runtime\runtime.js:62:39
Generator.invoke [as _invoke]
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\regenerator-runtime\runtime.js:296:21
Generator.prototype.(anonymous
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\regenerator-runtime\runtime.js:114:20
tryCatch
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\regenerator-runtime\runtime.js:62:39
invoke
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\regenerator-runtime\runtime.js:152:19
<unknown>
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\regenerator-runtime\runtime.js:164:12
tryCallOne
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\promise\setimmediate\core.js:37:11
<unknown>
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\promise\setimmediate\core.js:123:14
<unknown>
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\react-native\Libraries\Core\Timers\JSTimers.js:295:17

```

if the async action itelf throws an error, we get
```
Error: Error: SUCKA

This error is located at:
    in RestProfileScreen (created by Connect(RestProfileScreen))
    in Connect(RestProfileScreen) (at SceneView.js:17)
    in SceneView (at CardStack.js:455)
    in RCTView (at View.js:60)
    in View (at CardStack.js:454)
    in RCTView (at View.js:60)
    in View (at CardStack.js:453)
    in RCTView (at View.js:60)
    in View (at createAnimatedComponent.js:154)
    in AnimatedComponent (at Card.js:12)
    in Card (at PointerEventsContainer.js:39)
    in Container (at CardStack.js:498)
    in RCTView (at View.js:60)
    in View (at CardStack.js:414)
    in RCTView (at View.js:60)
    in View (at CardStack.js:413)
    in CardStack (at CardStackTransitioner.js:67)
    in RCTView (at View.js:60)
    in View (at Transitioner.js:142)
    in Transitioner (at CardStackTransitioner.js:19)
    in CardStackTransitioner (at StackNavigator.js:7)
    in StackView (created by Connect(StackView))
    in Connect(StackView) (at createNavigator.js:13)
    in Navigator (at SceneView.js:17)
    in SceneView (at ResourceSavingSceneView.js:55)
    in RCTView (at View.js:60)
    in View (at ResourceSavingSceneView.js:48)
    in RCTView (at View.js:60)
    in View (at ResourceSavingSceneView.js:39)
    in ResourceSavingSceneView (at TabView.js:35)
    in RCTView (at View.js:60)
    in View (created by ViewPagerAndroid)
    in AndroidViewPager (at ViewPagerAndroid.android.js:236)
    in ViewPagerAndroid (at TabViewPagerAndroid.js:127)
    in TabViewPagerAndroid (at TabViewAnimated.js:71)
    in RCTView (at View.js:60)
    in View (at TabViewAnimated.js:194)
    in TabViewAnimated (at TabView.js:192)
    in TabView (at withCachedChildNavigation.js:69)
    in withCachedChildNavigation(TabView) (at TabNavigator.js:34)
    in Unknown (at createNavigator.js:13)
    in Navigator (at createNavigationContainer.js:226)
    in NavigationContainer (at AppNavigator.js:34)
    in RCTView (at View.js:60)
    in View (at Container.js:15)
    in Container (at connectStyle.js:384)
    in Styled(Container) (at AppNavigator.js:33)
    in AppNavigator (created by Connect(AppNavigator))
    in Connect(AppNavigator) (at App.js:55)
    in RCTView (at View.js:60)
    in View (at Root.js:13)
    in Root (at connectStyle.js:384)
    in Styled(Root) (at App.js:54)
    in StyleProvider (at App.js:52)
    in Provider (at App.js:51)
    in ApolloProvider (at App.js:50)
    in FoodFlick (at registerRootComponent.js:35)
    in RootErrorBoundary (at registerRootComponent.js:34)
    in ExpoRootComponent (at renderApplication.js:33)
    in RCTView (at View.js:60)
    in View (at AppContainer.js:102)
    in RCTView (at View.js:60)
    in View (at AppContainer.js:122)
    in AppContainer (at renderApplication.js:32)
_callee10$
    C:\Users\simon\Projects\Source\FoodFlick\src\general\rest\restActions.ts:112:10
tryCatch
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\regenerator-runtime\runtime.js:62:39
Generator.invoke [as _invoke]
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\regenerator-runtime\runtime.js:296:21
Generator.prototype.(anonymous
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\regenerator-runtime\runtime.js:114:20
tryCatch
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\regenerator-runtime\runtime.js:62:39
invoke
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\regenerator-runtime\runtime.js:152:19
<unknown>
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\regenerator-runtime\runtime.js:195:10
tryCallTwo
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\promise\setimmediate\core.js:45:4
doResolve
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\promise\setimmediate\core.js:200:12
new
    C:\Users\simon\Projects\Source\FoodFlick\node_modules\promise\setimmediate\core.js:66:2

```


sample files

// component
```
import { doEvent } from '../../general/utils/componentEventHandler';

type state = {
  name: string,
  description: string,
  phone: string,
  tagsQuery: string,
  tagSuggestions: Tag[],
  tags: Tag['name'][],
};

class RestProfileScreen extends React.Component<props, state> {
  render () {
    return (
      <FormScreenContainer navigation={this.props.navigation} onConfirm={() => doEvent(this, this.props.onConfirm, this.state)}>
      </FormScreenContainer>
    )
  }
}
})

const mapStateToProps = (state: RootState) => ({
  newRest: state.getAccount().getNewRest(),
  selectedRest: state.getAccount().getSelectedRest(),
  signedInUser: state.getSignedInUser(),
});

//need to use mergeProps so that 'Add' case has access to newRest. if not using mergeProps, then would have to repeat
//this switch logic in the component because comp would need to know when to pass in an entire newRest for add vs
//when to just pass profile. given this decision, we could also just always passa newRest, but then that would not be
//a good argument for the save condition.
const mergeProps = ({newRest, selectedRest, signedInUser}, {dispatch}, props) => {
  const {navigation} = props;
    return {
      ...props,
      selectedRest,     
      onConfirm: async newProfile => {
        await dispatch(updateRestProfileAction(selectedRest._id, newProfile));
        navigation.goBack();
      }
    }
  }
}

export default connect(mapStateToProps, null, mergeProps)(RestProfileScreen);

export const restProfileScreenName = 'restProfileScreen';
```

//component event handler
```
// todo 0: use this everywhere
// see https://github.com/facebook/react/issues/11409
/**
 * Calls the provided callback when the event fires. If cb result is a promise, throw an error in the component. This is
 * useful because otherwise cbs which return a rejected promise are uncaptured/unhandled and errors in component
 * event handlers (promise rejections or regular errors) will NOT trigger componentDidCatch.
 *
 * 
 * @param {self} the component.
 * @param {fn} cb the callback for the event.
 * @param {...args} the arguments for the cb.
 * @returns {void}
 */

export function doEvent () {
  const args = Array.from(arguments);
  const self = args.shift();
  const cb = args.shift();
  console.log('doing event');
  try {
    const res = cb(...args);
    // if res is a promise
    if (typeof res.then === 'function') {
      res.catch(e => {
        console.log(JSON.stringify(e));
        self.setState(() => { throw e })
      });
    }
  } catch (e) {
    console.log(JSON.stringify(e));
    self.setState(() => { throw e });
  }
};
```
//rest action
```
export const updateRestProfileAction = (restId, newProfile) => async dispatch => {
  try {
    throw new Error('SUCKA');
    const updatedRest = await RestService.updateRestProfile(restId, newProfile);
    dispatch(selectAccountRestAction(updatedRest));
  } catch (e) {
    throw new Error(e); // catch and rethrow so apollo errors get a line number
  }
};
```

//rest service
```
  updateRestProfile: async (restId, newProfile) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateRestProfile($restId: ID!, $newProfile: ProfileInput!) {
          updateRestProfile(restId: $restId, newProfile: $newProfile) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        newProfile
      },
      errorPolicy: 'all'
    });
    console.log('got update rest profile');
    return res.data.updateRestProfile;
  },
```
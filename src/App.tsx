import React from 'react';
import { ApolloProvider } from 'react-apollo';
import { AppLoading } from 'expo';
import { getItemAsync } from 'expo-secure-store';
import { loadAsync } from 'expo-font';
import { Root, StyleProvider } from 'native-base';
import getTheme from '../native-base-theme/components';
import commonColor from '../native-base-theme/variables/commonColor';
import { Provider } from 'react-redux';
import { initStore } from './general/redux/store';
import { initApolloClient } from './apolloClient';
import { getRootReducer } from './general/redux/rootReducer';
import AppNavigator from './AppNavigator';
import { signInWithRefreshAction } from './general/user/userActions';
import AnalyticsService from './general/analytics/AnalyticsService';
import { SignedInUser } from './general/user/SignedInUser';
import LoadingIndicator from './general/components/LoadingIndicator';

export default class FoodFlick extends React.Component {
  state = {
    isReady: false,
  };

  store;
  apolloClient;

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.setup().then(() => {
      this.setState({ isReady: true })
    })

  }

  async setup() {
    return new Promise(async res => {
      await AnalyticsService.init()
      await loadAsync({
        Roboto: require('../node_modules/native-base/Fonts/Roboto.ttf'),
        Roboto_medium: require('../node_modules/native-base/Fonts/Roboto_medium.ttf'),
        Ionicons: require('../node_modules/native-base/Fonts/Ionicons.ttf'),
        MaterialIcons: require('../node_modules/native-base/Fonts/MaterialIcons.ttf'),
      });
      this.store = initStore(getRootReducer());
      this.apolloClient = initApolloClient(this.store);
      try {
        const refreshToken = await getItemAsync('refreshToken')
        if (refreshToken) this.store.dispatch(signInWithRefreshAction(refreshToken));
      } catch (e) {
        console.error("couldn't read secure store for refreshtoken", e)
      }
      res();
    });
  }

  // componentDidCatch(error, info) {
  //   // in dev mode, even though we catch error, we will still get another. see
  //   // https://github.com/facebook/react/issues/11082
  //   console.warn('APP.JS error info', error, info);
  // }

  render() {
    if (!this.state.isReady) return <AppLoading />;

    //todo 1: might not need ApolloProvider anymore
    return (
      <ApolloProvider client={this.apolloClient}>
        <Provider store={this.store}>
          <StyleProvider style={getTheme(commonColor)}>
            {/* necessary for nativebase ActionSheet + Toast */}
            <Root>
              <AppNavigator />
              <LoadingIndicator />
            </Root>
          </StyleProvider>
        </Provider>
      </ApolloProvider>
    );
  }
}
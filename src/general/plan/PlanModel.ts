export enum PlanNames {
  Free = 'Free',
  Standard = 'Standard',
  Unlimited = 'Unlimited',
  Custom = 'Custom',
}

export interface Plan {
  stripePlanId: string
  name: string
  monthlyOrders: number
  monthlyRate: number,
  overagePercentageFee: number,
  proration: number,
}

export abstract class PlanSelector {
  static getId = (sub: Plan): string => sub ? sub.stripePlanId : undefined
  
  static getName = (sub: Plan): string => sub ? sub.name : undefined

  static getMonthlyOrders = (sub: Plan): number => sub ? sub.monthlyOrders : null
  
  static getMonthlyRate = (sub: Plan): number => sub ? sub.monthlyRate : null

  static getOveragePercentageFee = (sub: Plan): number => sub ? sub.overagePercentageFee : null

  static getProration = (sub: Plan): number => sub ? sub.proration : null

  static isEqual = (sub1: Plan, sub2: Plan): boolean => sub1.stripePlanId === sub2.stripePlanId;
}
import { ManagerRest, ManagerRestSelector } from './../rest/ManagerRest';
import { Plan } from './PlanModel';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import { planFragment } from './planFragment';

const activePlansQuery = gql`
  query activePlans($subscriptionId: ID!){
    activePlans(subscriptionId: $subscriptionId) {
      ...planFragment
    }
  }
  ${planFragment}
`;

type activePlansResponse = {
  activePlans: Plan[]
}

export default {
  getActivePlansInjector: graphql<any, activePlansResponse>(activePlansQuery, {
    options: ({ selectedRest }) => ({
      variables: {
        subscriptionId: ManagerRestSelector.getSubscriptionId(selectedRest)
      },
      fetchPolicy: 'no-cache',
    }),
    props: ({ data }) => ({
      data,
      plansLoading: data.loading,
      refetch: data.refetch,
      activePlans: data.activePlans,
      error: data.error,
    })
  }),
};

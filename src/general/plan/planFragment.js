import gql from 'graphql-tag';

export const planFragment = gql`
  fragment planFragment on Plan {
    stripePlanId
    name
    monthlyOrders
    monthlyRate
    overagePercentageFee
    proration
  }
`
import { ManagerItem } from './ManagerMenu';
import { PrinterType } from '../rest/ManagerRest';

export interface BaseLikes {
  count: number;
  hasLiked: boolean;
}

export interface Price {
  value: number
  label?: string
};

export interface option {
  name: string
  price?: number
};

export interface optionGroup {
  options: option[],
}

export interface ItemPrinter {
  name: string;
  itemName: string;
  ip: string;
  port: string;
  type: PrinterType;
}

export const clonePrice = ({ value, label }: Price) => ({ 
  value,
  label,
});

export const cloneOption = ({ name, price }: option) => ({
  name,
  price,
})

export const cloneOptionGroups = (optionGroups: optionGroup[]): optionGroup[] =>
  optionGroups.map(({ options }) => ({
    options: options.map(cloneOption)
  }));

/**
 * optional fields because they don't exist if we're adding items for MangerItem
 */
export interface BaseItem {
  name?: string;
  prices?: Price[];
  addons?: Price[];
  description?: string;
  printers?: ItemPrinter[];
  flick?: string;
  likes?: BaseLikes;
  optionGroups?: optionGroup[];
}

/**
 * Clean the provided input object by cleanInput on each property
 * Original argument is unmodifed.
 *
 * @param input - The object to be cleaned
 * @returns The newly cleaned object
 */
export const getCleanItems = (inputs: ManagerItem): ManagerItem => {
  const newInputs = { ...inputs };
  Object.keys(newInputs).forEach(key => {
    const value = newInputs[key];
    if (key === 'printers') {
      return;
    } else if (key === 'prices') {
      newInputs[key] = value.map(({ value, label }) => ({
        label: label ? label.trim() : null,
        value: parseFloat(value),
      }));
    } else if (key === 'addons') {
      newInputs[key] = value.map(({ value, label }) => ({
        label: label.trim(),
        value: parseFloat(value),
      }));
    } else if (key === 'optionGroups') {
      newInputs[key] = value.map(({ options }): optionGroup => ({
        options: options.map(({ name, price }) => ({
          name: name.trim(),
          price
        }))
      }));
    } else if (key === 'privateNames') {
      newInputs[key] = value.map((name: string): string => name.trim());
    }  else {
      newInputs[key] = value ? value.trim() : null;
    }
  });
  return newInputs;
}

export interface BaseCategory {
  name: string;
  description: string;
  items: BaseItem[];
}

export abstract class BaseCategorySelector {
  static getName = (category: BaseCategory): string => category ? category.name : undefined

  static getItems = (category: BaseCategory): BaseItem[] => category ? category.items : undefined

  static getItemByIndex = (category: BaseCategory, index: number): BaseItem => (
    BaseCategorySelector.getItems(category)[index]
  )

  static isEqual = (c1: BaseCategory, c2: BaseCategory): boolean => c1.name === c2.name;
}

export abstract class BaseItemSelector {
  static getName = (item: BaseItem): string => item ? item.name : undefined

  static getPrices = (item: BaseItem): Price[] => item ? item.prices : undefined

  static getAddons = (item: BaseItem): Price[] => item ? item.addons : undefined

  static getOptionGroups = (item: BaseItem): optionGroup[] => item ? item.optionGroups : undefined

  static getFlick = (item: BaseItem): string => item ? item.flick : undefined
}

export abstract class PriceSelector {
  static getValue = (price: Price): number => price ? price.value : undefined

  static getLabel = (price: Price): string => price ? price.label : undefined
}
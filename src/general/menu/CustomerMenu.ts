import { BaseCategory, BaseLikes, BaseItem, BaseCategorySelector, Price, option, cloneOptionGroups, BaseItemSelector, BasePriceSelector, clonePrice } from './BaseMenu';

interface CustomerLikes extends BaseLikes {
  hasLiked: boolean;
}

export interface CustomerItem extends BaseItem {
  name: string;
  prices: Price[];
  description?: string;
  flick: string;
  likes: CustomerLikes
}

export const cloneCustomerItem = ({ name, prices, description, flick, likes, optionGroups }: CustomerItem): CustomerItem => ({
  name,
  description,
  flick,
  likes: { ...likes },
  prices: prices.map(clonePrice),
  optionGroups: cloneOptionGroups(optionGroups),
});

export interface CustomerCategory extends BaseCategory {
  items: CustomerItem[];
}

export abstract class CustomerItemSelector extends BaseItemSelector {
}

export abstract class CustomerCategorySelector extends BaseCategorySelector {
}
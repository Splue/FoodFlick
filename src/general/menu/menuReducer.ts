import {
  SELECT_CATEGORY,
  UNSELECT_CATEGORY,
  ADD_SELECTED_ITEM_URIS,
  CLEAR_ITEMS,
  SELECT_ITEMS,
  UPDATE_ITEM_URI,
  ADD_EMPTY_ITEM,
  DELETE_ITEM_LOCALLY,
} from './menuActions';
import { action } from '../redux/actions';

interface selectedItemActions extends action {
  /**
   * items currently selected as a result of changing the viewed category
   */
  items?
  /**
   * locators for images in phone or from user's images corresponding to each newly added item
   */
  uris?: string[]
  /**
   * index of the item to be udpated
   */
  itemIndexInSelectedItems?: number
  /**
   * locator for image in phone or from user's images for a singley updated item
   */
  uri?: string
  /**
   * index in selectedItems to be deleted locally (redux only)
   */
  index?: number
}

export function selectedItems (state = [], action: selectedItemActions) {
  switch(action.type) {
    case ADD_SELECTED_ITEM_URIS:
      return [...state, ...action.uris.map(uri => ({ item: { uri }}))];
    case ADD_EMPTY_ITEM: 
      return [...state, { item: {} }]
    case DELETE_ITEM_LOCALLY:
      const target = action.index;
      const before = state.slice(0, target);
      const after = state.slice(target+1)
      return [...before, ...after];
    case CLEAR_ITEMS: 
      return [];
    case SELECT_ITEMS:
      return action.items;
    case UPDATE_ITEM_URI:
      const { itemIndexInSelectedItems, uri } = action;
      // map instead of [...state] because spread copies object refs
      const items = state.map(({ index, item }) => ({ 
        index,
        item: { ...item }
       }));
      items[itemIndexInSelectedItems].item.uri = uri;
      return items;
    default:
      return state;
  }
}

export function selectedCategory (state = null, action: any = {}) {
  switch(action.type) {
    case SELECT_CATEGORY:
      return action.category
    case UNSELECT_CATEGORY:
      return null;
    default:
      return state;
  }
}
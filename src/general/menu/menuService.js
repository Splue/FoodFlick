import getApolloClient from '../../apolloClient';
import { findIndex } from 'lodash';
import gql from 'graphql-tag';
import { managerRestFragment, customerRestFragment } from '../rest/restFragments';

export default MenuService = {
  addCategory: async (restId, newCategory) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation addCategory($restId: ID!, $newCategory: NewCategoryInput!) {
          addCategory(restId: $restId, newCategory: $newCategory) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        newCategory,
      }
    });
    return res.data.addCategory;
  },

  updateCategory: async (restId, categoryName, newCategory) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateCategory($restId: ID!, $categoryName: String!, $newCategory: NewCategoryInput!) {
          updateCategory(restId: $restId, categoryName: $categoryName, newCategory: $newCategory) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        categoryName,
        newCategory,
      }
    });
    return res.data.updateCategory;
  },

  updateCategoryOrder: async (restId, newOrder) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateCategoryOrder($restId: ID!, $newOrder: [Int!]!) {
          updateCategoryOrder(restId: $restId, newOrder: $newOrder) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        newOrder,
      }
    });
    return res.data.updateCategoryOrder;
  },

  deleteCategory: async (restId, categoryName) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation deleteCategory($restId: ID!, $categoryName: String!) {
          deleteCategory(restId: $restId, categoryName: $categoryName) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        categoryName,
      }
    });
    return res.data.deleteCategory;
  },

  addItems: async (restId, categoryName, items) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation addItems($restId: ID!, $categoryName: String!, $items: [NewItemInput!]!) {
          addItems(restId: $restId, categoryName: $categoryName, items: $items) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        categoryName,
        items,
      },
    });
    
    return res.data.addItems;
  },

  updateItems: async (restId, categoryName, items) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateItems($restId: ID!, $categoryName: String!, $items: [UpdateItemInput!]!) {
          updateItems(restId: $restId, categoryName: $categoryName, items: $items) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        categoryName,
        items,
      }
    });
    return res.data.updateItems;
  },

  updateItemOrder: async (restId, categoryName, newOrder) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateItemOrder($restId: ID!, $categoryName: String!, $newOrder: [Int!]!) {
          updateItemOrder(restId: $restId, categoryName: $categoryName, newOrder: $newOrder) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        categoryName,
        newOrder,
      }
    });
    return res.data.updateItemOrder;
  },

  deleteItem: async (restId, categoryName, itemName) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation deleteItem($restId: ID!, $categoryName: String!, $itemName: String!) {
          deleteItem(restId: $restId, categoryName: $categoryName, itemName: $itemName) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        categoryName,
        itemName,
      }
    });
    return res.data.deleteItem;
  },

  toggleItemLike: async (rest, categoryName, itemName, optimisticCb) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation toggleItemLike($restId: ID!, $categoryName: String!, $itemName: String!) {
          toggleItemLike(restId: $restId, categoryName: $categoryName, itemName: $itemName) {
            ...customerRestFragment
          }
        }
        ${customerRestFragment}
      `,
      variables: {
        restId: rest._id,
        categoryName,
        itemName,
      },
      optimisticResponse: ({ categoryName, itemName }) => {
        const categoryIndex = findIndex(rest.menu, { name: categoryName });
        const itemIndex = findIndex(rest.menu[categoryIndex].items, { name: itemName });
        const restCopy = JSON.parse(JSON.stringify(rest)); // copy since we can't mutate the arg
        const likes = restCopy.menu[categoryIndex].items[itemIndex].likes;
        likes.hasLiked ? likes.count !== 0 && likes.count-- : likes.count++;
        likes.hasLiked = !likes.hasLiked;
        optimisticCb(restCopy, restCopy.menu[categoryIndex]);
        return { toggleItemLike: restCopy };
      },
      // we dont need to do this but keeping this as a working example. if you uncomment below, you'll also
      // notice that on second update() execution (1st run for optimistic, 2nd run for real) that the cache is already
      // updated from the first run. i tried relying on the cache to update the ui, but it doesn't work. thus we have to
      // reselect with redux. the ui-update-on-cache could be because searchScreen isn't using cache properly or
      // because passing it as a navigation prop, somehow kills the link. i suspect that search screen results would
      // actually update, but MenuBrowseScreen doesn't even use the cache it uses redux's selected rest and updating
      // the cache wont directly update the redux state.
      
      // update: (cache, {data: { toggleItemLike: newRest }}) => {
      //

      //   console.log('newRest', newRest);
      //   const res = cache.readFragment({
      //     id: `${newRest.__typename}:${newRest._id}`,
      //     fragment: restFragment,
      //   });

      //   cache.writeFragment({
      //     id: `${newRest.__typename}:${newRest._id}`,
      //     fragment: restFragment,
      //     data: newRest,
      //   });

      //   /*
      //   when cache is printed, one data is 
      //     $Rest:sVnemmQB9nHU-Kj-cYDQ.location:Object
      //     $Rest:sVnemmQB9nHU-Kj-cYDQ.location.address:Object
      //     $Rest:sVnemmQB9nHU-Kj-cYDQ.profile:Object
      //     Manager:auth0|5a78c1ccebf64a46ecdd0d9c:Object
      //     ROOT_MUTATION:Object
      //     ROOT_QUERY:Object
      //     Rest:sVnemmQB9nHU-Kj-cYDQ:Object
      //       // if i were to expand this...
      //       .menu[0] : {
      //         generated: true
      //         id: "Rest:sVnemmQB9nHU-Kj-cYDQ.menu.0"
      //         type: "id"
      //         typename: "Category"
      //       }
      //     Rest:sVnemmQB9nHU-Kj-cYDQ.menu.0:Object
      //     Rest:sVnemmQB9nHU-Kj-cYDQ.menu.0.items.0:Object
      //     Rest:sVnemmQB9nHU-Kj-cYDQ.menu.0.items.1:Object
      //     Rest:sVnemmQB9nHU-Kj-cYDQ.menu.0.items.2:Object
      //     Rest:sVnemmQB9nHU-Kj-cYDQ.menu.1:Object
      //     Rest:sVnemmQB9nHU-Kj-cYDQ.menu.1.items.0:Object
      //     Rest:sVnemmQB9nHU-Kj-cYDQ.menu.1.items.1:Object
      //     Rest:sVnemmQB9nHU-Kj-cYDQ.menu.1.items.2:Object
      //     Rest:sVnemmQB9nHU-Kj-cYDQ.menu.2:Object
      //     Rest:sVnemmQB9nHU-Kj-cYDQ.menu.2.items.0:Object
      //     Rest:sVnemmQB9nHU-Kj-cYDQ.menu.2.items.1:Object
      //     Rest:sVnemmQB9nHU-Kj-cYDQ.menu.2.items.2:Object
      //     Rest:sVnemmQB9nHU-Kj-cYDQ.menu.3:Object
      //   this is the apollo cache. this update the above code manually grabs the Rest:sVnemmQB9nHU-Kj-cYDQ:Object
      //   and updates it to the value returned from the mutation. if you look above, rest.menu[0] doesn't actually have
      //   data. instead it references another key in the cache. so when i update the main rest, i am actually the main
      //   rest's "normalized" (pulled out into a list) cache loctions. this is how you can manually update the cache.
        
      //   but you dont even need to do this. mutation results are mapped against this cached based on dataIdFromObject
      //   from apolloClient and if a matching field is found, then the match is updated. you can verify that this 
      //   manual update does the same thing as the auto update by verifying the same cache result with the manual
      //   and manual update. compare the cache by uncommenting the console.log(getApolloClient()); below
      //   */
      // },
    });
    // inspect the cache in getApolloClient() by looking at the cache field
    // console.log(getApolloClient());
    return res.data.toggleItemLike;
  },
};

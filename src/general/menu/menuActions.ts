import 'react-native-console-time-polyfill';
import AnalyticsService from '../analytics/AnalyticsService';
import { ItemUpdateState, ManagerItem, ManagerCategorySelector } from './ManagerMenu';
import { ManagerRest, ManagerRestSelector } from './../rest/ManagerRest';
import { RootState } from './../redux/RootState';
import MenuService from './menuService';
import { find } from 'lodash';
import { selectAccountRestAction, selectTrendingRestAction, selectFavoritesRestAction } from '../rest/restActions';
import { ACCOUNT_TAB } from '../../account/redux/accountReducer';
import { TRENDING_TAB } from '../../trending/redux/trendingReducer';
import { FAVORITES_TAB } from '../../favorites/redux/favoritesReducer';
import { CustomerRestSelector, CustomerRest } from '../rest/CustomerRest';
import { CustomerCategorySelector, CustomerCategory } from './CustomerMenu';
import { BaseItemSelector } from './BaseMenu';

export const SELECT_CATEGORY = 'SELECT_CATEGORY';
export const UNSELECT_CATEGORY = 'UNSELECT_CATEGORY';
export const CLEAR_ITEMS = 'CLEAR_ITEMS';
export const ADD_SELECTED_ITEM_URIS = 'ADD_SELECTED_ITEM_URIS';
export const ADD_EMPTY_ITEM = 'ADD_EMPTY_ITEM';
export const DELETE_ITEM_LOCALLY = 'DELETE_ITEM_LOCALLY';
export const SELECT_ITEMS = 'SELECT_ITEMS';
export const UPDATE_ITEM_URI = 'UPDATE_ITEM_URI';
export const GO_FLICK_PICKER_AFTER_CATEGORY_ADDITION = 'GO_FLICK_PICKER_AFTER_CATEGORY_ADDITION';

export const addCategoryAction = (restId, newCategory) => async dispatch => {
  const newRest = await MenuService.addCategory(restId, newCategory);
  dispatch(selectAccountRestAction(newRest));
  dispatch(selectAccountCategoryAction(newCategory));
  AnalyticsService.trackEventWithProperties(AnalyticsService.events.ADD_CATEGORY, { 'restId': restId })
}

export const addItemsAction = (rest: ManagerRest, categoryName: string, items: ItemUpdateState[]) => async (dispatch, getState) => {
  try {
    const { firebaseStorageRef } = getState();
    await (setsFlickFirebaseUrlAndDeletesUri(firebaseStorageRef, rest, categoryName, items));
    const itemsWithFlicks = items.map(({ item }) => item);
    const newRest = await MenuService.addItems(ManagerRestSelector.getId(rest), categoryName, itemsWithFlicks);
    dispatch(selectAccountRestAction(newRest));
    dispatch(selectAccountCategoryAction(find(newRest.menu, { name: categoryName })));
    AnalyticsService.trackEventWithProperties(AnalyticsService.events.ADD_ITEMS, { 'restId': newRest._id, 'NumOfItems': itemsWithFlicks.length })
  } catch (e) {
    throw new Error(e); // catch and rethrow so apollo errors get a line number
  }
};

export const addEmtpyItemToSelectedAction = () => ({
  type: ADD_EMPTY_ITEM,
  tab: ACCOUNT_TAB,
});

export const deleteItemLocallyAction = (index) => ({
  type: DELETE_ITEM_LOCALLY,
  tab: ACCOUNT_TAB,
  index,
});

export const addSelectedItemUrisAction = (uris: string[]) => ({
  type: ADD_SELECTED_ITEM_URIS,
  tab: ACCOUNT_TAB,
  uris
});

export const deleteCategoryAction = (restId, categoryName) => async dispatch => {
  const newRest = await MenuService.deleteCategory(restId, categoryName);
  dispatch(selectAccountRestAction(newRest));
  AnalyticsService.trackEventWithProperties(AnalyticsService.events.DELETE_CATEGORY, { 'restId': restId, 'categoryName': categoryName });

  return newRest;
}

export const deleteItemAction = (restId, categoryName, item) => (async (dispatch, getState: () => RootState) => {
  const firebaseStorageRef = getState().getFirebaseStorageRef();
  const itemName = item.name;
  if (item.flick) {
    deleteFirebaseFlick(firebaseStorageRef, restId, categoryName, itemName).catch(e => {
      console.warn(`Could not delete old firebase image when deleting ${restId} ${categoryName} ${itemName}`, e);
    });
  }
  const newRest = await MenuService.deleteItem(restId, categoryName, itemName);
  dispatch(selectAccountRestAction(newRest));
  dispatch(selectAccountCategoryAction(find(newRest.menu, { name: categoryName })));
  AnalyticsService.trackEventWithProperties(AnalyticsService.events.DELETE_ITEM, { 'restId': restId, 'categoryName': categoryName, 'item': item });
  return newRest;
});

export const goFlickPickerScreenAfterCategoryAddition = () => ({
  type: GO_FLICK_PICKER_AFTER_CATEGORY_ADDITION,
});

export const selectAccountCategoryAction = category => selectCategoryAction(category, ACCOUNT_TAB);

export const selectFavoritesCategoryAction = category => selectCategoryAction(category, FAVORITES_TAB);

export const selectTrendingCategoryAction = category => selectCategoryAction(category, TRENDING_TAB);

export const selectItemsAction = (items: ItemUpdateState[]) => ({
  type: SELECT_ITEMS,
  tab: ACCOUNT_TAB,
  items,
});

export const updateCategoryAction = (restId, selectedCategory, newCategory) => async dispatch => {
  const newRest = await MenuService.updateCategory(restId, selectedCategory.name, newCategory);
  dispatch(selectAccountRestAction(newRest));
  dispatch(selectAccountCategoryAction({
    ...selectedCategory,
    ...newCategory,
  }));
  AnalyticsService.trackEventWithProperties(AnalyticsService.events.UPDATE_CATEGORY, { 'restId': restId, 'selectedCategory': selectedCategory, 'newCategory': newCategory })
}

export const updateCategoryOrderAction = (restId, newOrder) => async dispatch => {
  const newRest = await MenuService.updateCategoryOrder(restId, newOrder);
  dispatch(selectAccountRestAction(newRest));
  AnalyticsService.trackEventWithProperties(AnalyticsService.events.UPDATE_CATEGORY_ORDER, { 'restId': restId });
};

export const updateItemUriAction = (itemIndexInSelectedItems: number, uri: string) => ({
  type: UPDATE_ITEM_URI,
  tab: ACCOUNT_TAB,
  itemIndexInSelectedItems,
  uri,
});


//https://github.com/expo/firebase-storage-upload-example/issues/13#issuecomment-437597679
const urlToBlob = (url) => {
  return new Promise((resolve, reject) => {
    var xhr = new XMLHttpRequest();
    xhr.onerror = reject;
    xhr.onreadystatechange = () => {
      if (xhr.readyState === 4) {
        resolve(xhr.response);
      }
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob'; // convert type
    xhr.send();
  })
}

const deleteFirebaseFlick = (firebaseStorageRef, restId, categoryName, itemName) => {
  const ref = firebaseStorageRef.child(restId + '/' + categoryName + '/' + itemName);
  return ref.delete();
}

/**
 * Uploads the uris in each item into firebase and sets the new firebase image download url as to the flick property
 * and deletes the old uri property. If there are no uris, then nothing happens to that item.
 * @param firebaseStorageRef
 * @param rest 
 * @param categoryName 
 * @param items 
 */
const setsFlickFirebaseUrlAndDeletesUri = (
  firebaseStorageRef,
  rest: ManagerRest,
  categoryName: string,
  items: ItemUpdateState[]) => new Promise((resolve, reject) => {
    const restId = ManagerRestSelector.getId(rest);
    const categoryRef = firebaseStorageRef.child(restId + '/' + categoryName);
    let uploadCount = 0;
    items.forEach(({ item: { flick, uri, name } }, index) => {
      // items that are being updated without a new picture (info only) won't have a uri
      if (!uri) {
        if (++uploadCount === items.length) {
          resolve(items);
        }
        return;
      }
      const allowedWriters = ManagerRestSelector.getManagers(rest).concat(ManagerRestSelector.getOwner(rest));
      urlToBlob(uri).then(blob => {
        const pathRef = categoryRef.child('/' + name);
        return pathRef.put(
          blob,
          {
            customMetadata: {
              'managers': allowedWriters.map(user => user.userId).join(" ")
            },
          }
        );
      }).then(res => {
        uploadCount++;

        const oldItem: ManagerItem = ManagerCategorySelector.getItemByIndex(
          ManagerRestSelector.getCategoryByName(rest, categoryName),
          index,
        )

        // if this item had a previous flick, delete the previous one from firebase, but only if this is a new name
        // becuase if its the same name, then new image overwrites the firebase path of the old image. (but new image has
        // different download url). if we don't delete old firebase paths, then they will live forever
        const oldName = BaseItemSelector.getName(oldItem);
        if (oldName !== name && flick) {
          deleteFirebaseFlick(firebaseStorageRef, restId, categoryName, oldName).catch(e => {
            console.warn(`Could not delete old firebase image when renaming ${restId} ${categoryName} ${oldName}`, e);
          });
        }

        items[index].item.flick = res.downloadURL;
        // must delete uri field because items sent to graphql shouldn't have this field. plus this item now has a new
        // flick url from firebase
        delete items[index].item.uri;

        if (uploadCount === items.length) {
          resolve(items);
        }
      }).catch(err => {
        reject(err)
      });
    });
  });

export const updateItemsAction = (rest: ManagerRest, categoryName: string, items: ItemUpdateState[]) => async (dispatch, getState) => {
  const restId = ManagerRestSelector.getId(rest);
  const { firebaseStorageRef } = getState();
  await (setsFlickFirebaseUrlAndDeletesUri(firebaseStorageRef, rest, categoryName, items));
  const newRest = await MenuService.updateItems(restId, categoryName, items);
  dispatch(selectAccountRestAction(newRest));
  dispatch(selectAccountCategoryAction(find(newRest.menu, { name: categoryName })));
  AnalyticsService.trackEventWithProperties(AnalyticsService.events.UPDATE_ITEM, { 'restId': restId });

};

export const updateItemOrderAction = (restId, categoryName, newOrder) => (async (dispatch, getState) => {
  const newRest = await MenuService.updateItemOrder(restId, categoryName, newOrder);
  dispatch(selectAccountRestAction(newRest));
  dispatch(selectAccountCategoryAction(find(newRest.menu, { name: categoryName })));
  AnalyticsService.trackEventWithProperties(AnalyticsService.events.UPDATE_ITEM_ORDER, { 'restId': restId });
});

export const unselectAccountCategoryAction = () => unselectCategoryAction(ACCOUNT_TAB);

export const selectCategoryAction = (category, tab) => dispatch => {
  dispatch({
    type: SELECT_CATEGORY,
    tab,
    category
  });
  dispatch(clearItemsAction());
}

export const clearItemsAction = () => ({
  type: CLEAR_ITEMS,
  tab: ACCOUNT_TAB,
})

export const unselectCategoryAction = tab => ({
  type: UNSELECT_CATEGORY,
  tab,
});
export const toggleFavoritesItemLikeAction = (
  favoritesRest: CustomerRest,
  category: CustomerCategory,
  itemName: string) => async (dispatch, getState: () => RootState) => {

    const newRest = await dispatch(
      toggleItemLikeAction(favoritesRest, category, itemName, selectFavoritesRestAction, selectFavoritesCategoryAction));

    const trendingRest = getState().getTrending().getSelectedRest();
    if (CustomerRestSelector.isEqual(trendingRest, favoritesRest)) {
      /*
      * if the same rest is selected in the trending tab as is in the favorites tab, then we need to update the
      * selectedRest in the trending tab because otherwise the data on the screens wont match.
      * 1) we dont compare category because it even if the screens are showing different categories, the user can go to
      * the same category in trending, at which point we would need to show the same data in both tabs.
      * 2) we also need to reselect category because if the user IS on the same category, then he/she needs to see the new
      * category with data. if we just did reselectedRest, it would jump the user to the first category instead of the
      * one their currently viewing.
      */
      dispatch(selectTrendingRestAction(newRest));
      dispatch(
        selectTrendingCategoryAction(
          CustomerRestSelector.getCategoryByName(newRest, CustomerCategorySelector.getName(category))));
    }
  }

const toggleItemLikeAction = (
  rest: CustomerRest,
  category: CustomerCategory,
  itemName: string,
  selectRestAction: (rest: CustomerRest) => (dispatch: any) => void,
  selectCategoryAction: (category: any) => (dispatch: any) => void) => async dispatch => {

    let newRest;
    try {
      newRest = await MenuService.toggleItemLike(
        rest,
        CustomerCategorySelector.getName(category),
        itemName,
        (optimisticRest, optimisticCategory) => {
          dispatch(selectRestAction(optimisticRest));
          dispatch(selectCategoryAction(optimisticCategory));
        });
    } catch (e) {
      console.warn(e);
      return;
    }

    dispatch(selectRestAction(newRest));
    dispatch(selectCategoryAction(find(newRest.menu, { name: CustomerCategorySelector.getName(category) })));
    return newRest;
  }

export const toggleTrendingItemLikeAction = (
  trendingRest: CustomerRest,
  category: CustomerCategory,
  itemName: string) => async (dispatch, getState: () => RootState) => {

    const newRest = await dispatch(
      toggleItemLikeAction(trendingRest, category, itemName, selectTrendingRestAction, selectTrendingCategoryAction));

    const favoritesRest = getState().getFavorites().getSelectedRest();
    if (CustomerRestSelector.isEqual(favoritesRest, trendingRest)) {
      dispatch(selectFavoritesRestAction(newRest));
      dispatch(
        selectFavoritesCategoryAction(
          CustomerRestSelector.getCategoryByName(newRest, CustomerCategorySelector.getName(category))));
    }
  }
import { BaseCategory, BaseItem, BaseCategorySelector, BaseLikes } from './BaseMenu';

export interface ManagerCategory extends BaseCategory {
}

/*
* Items being updated or added
*/
export interface ItemUpdateState {
  /*
  * The category index of an item. For updated items, used to know which item is being updated. Undefined if being item
  * is being added. 
  */
  index?: number,
  item: ManagerItem,
};

export interface ManagerItem extends BaseItem {
  /*
  * Optional uri for an image locally stored in the phone. This exists when an item's "photo" comes locally from phone
  * rather than a firebase url. see updateItemUriAction and addSelectedItemUrisAction
  */
  uri?: string,
  privateNames: string[],
}


export abstract class ManagerCategorySelector extends BaseCategorySelector {
  static getItemByIndex = (category: BaseCategory, index: number): ManagerItem => (
    BaseCategorySelector.getItemByIndex(category, index) as ManagerItem
  )
}

export abstract class ManagerItemSelector {
  static getPrivateNames = (item: ManagerItem): string[] => item ? item.privateNames : undefined
}
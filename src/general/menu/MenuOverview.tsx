import React from 'react';
import { StyleSheet, Dimensions, Image, TouchableWithoutFeedback, View, Platform } from 'react-native';
import { Button, Icon, Text, Tab, Tabs, TabHeading, ScrollableTab, Card, CardItem, H3, Right, Content } from 'native-base';
import commonColor from '../../../native-base-theme/variables/commonColor';
import { BaseCategory, BaseItem } from './BaseMenu';
import { Profile, Location } from '../rest/BaseRest';
import { CustomerCategory, CustomerItem } from './CustomerMenu';
import { withNavigation, NavigationInjectedProps } from 'react-navigation';
import { giveFeedbackScreenName } from '../screens/GiveFeedbackScreen';

type props = {
  emptyCategoryView?: React.ReactNode,
  canLeaveFeedback?: boolean,
  location: Location,
  menu: BaseCategory[],
  onPressItem?: (item: BaseItem, index: number, categoryIndex: number) => void,
  onRenderInfo?: () => void,
  onRenderEmptyCategory?: () => void,
  onRenderCategory?: () => void,
  onChangeTab?: (height: number) => void,
  profile: Profile,
  renderCategory: (category: React.ReactNode) => React.ReactNode,
  renderActionBar?: (category: CustomerCategory, item: CustomerItem) => React.ReactNode,
  renderMenu: (tabs: React.ReactNode) => React.ReactNode,
  selectCategory: (category: BaseCategory) => void,
}

const ThinPaddedCard = ({ display }) => (
  <CardItem style={styles.thinPadding}>
    <Text>{display.toString()}</Text>
  </CardItem>
)

const CardHeader = ({ display }) => (
  <CardItem header>
    <Text>{display}</Text>
  </CardItem>
)

const isCategoryEmpty = category => !category.items || category.items.length === 0

class MenuOverview extends React.Component<props & NavigationInjectedProps, any> {
  tabs = null;

  state = {
    tabHeights: {},
  }

  componentWillMount () {
    const { menu, selectCategory, onRenderCategory, onRenderEmptyCategory } = this.props;
    if (menu.length > 0) selectCategory(menu[0]);
    if (onRenderEmptyCategory && onRenderCategory) isCategoryEmpty(menu[0]) ? onRenderEmptyCategory() : onRenderCategory();
  }

  leaveFeedback = () => this.props.navigation.navigate(giveFeedbackScreenName, {
    confirmText: 'Post',
  })

  onChangeTab = ({ i: tabIndex }) => {
    const { selectCategory, onRenderInfo, onRenderCategory, onRenderEmptyCategory, menu, onChangeTab } = this.props;
    selectCategory(menu[tabIndex - 1]); // -1 since 0th = info
    if (tabIndex === 0 && onRenderInfo) {
      onRenderInfo();
      return;
    }
    if (onRenderEmptyCategory && onRenderCategory) isCategoryEmpty(menu[tabIndex - 1]) ? onRenderEmptyCategory() : onRenderCategory();

    // this.state.tabHeights[tabIndex] is falsy on first render of that tab since onChangeTab fires before we can get the
    // content's height. in these scenarios, we call this.props.onChangeTab() from renderCategory
    if (onChangeTab && this.state.tabHeights[tabIndex]) onChangeTab(this.state.tabHeights[tabIndex]);
  }

  updateTabHeights = (tabIndex, h) => {
    if (this.props.onChangeTab) {
      this.setState(prev => {
        const newHeights = {
          ...prev.tabHeights,
          [tabIndex]: h,
        }
        this.props.onChangeTab(h);
        return { tabHeights: newHeights }
      });
    }
  }

  onPressItem = (item, itemIndex, categoryIndex) => {
    if (this.props.onPressItem) {
      this.props.onPressItem(item, itemIndex, categoryIndex);
    }
  }

  renderCategory = (category, categoryIndex, renderActionBar) => {
    const content =
      <>
        {/* using ternary instead of boolean expression because false && <text>""</text> throws yoganode error. see
        https://github.com/facebook/react-native/issues/13243*/}
        {!category.description ? null :
        <Card transparent>
          <CardItem>
            <Text>{category.description}</Text>
          </CardItem>
        </Card>}
        {/* i think i can remove this next line... */}
        {category.items.length < 1 && this.props.emptyCategoryView}
        {category.items.map((item, index) => (
          <TouchableWithoutFeedback key={index} onPress={() => this.onPressItem(item, index, categoryIndex)}>
            <Card>
              <CardItem cardBody>
                {!!item.flick ? 
                  <Image style={styles.image} source={{uri: item.flick}} />
                  :
                  <H3 style={styles.emptyFlick}>No image</H3>
                }
              </CardItem>
              {renderActionBar && renderActionBar(category, item)}
              <CardItem style={styles.thinPadding}>
                <Text style={styles.name}>{item.name}</Text>
              </CardItem>
              {item.prices.map(({ value, label }, index) => (
                <CardItem key={index} style={styles.thinPadding}>
                  <Text style={styles.price}>{value.toFixed(2)}</Text>
                  {label &&  <Text style={styles.label}>{label}</Text>}
                </CardItem>
              ))}
              {item.addons.map(({ value, label }, index) => (
                <CardItem key={index} style={styles.thinPadding}>
                  <Text style={styles.price}>{value.toFixed(2)}</Text>
                  <Text style={styles.label}>{label} (addon)</Text>
                </CardItem>
              ))}
              {item.description && <ThinPaddedCard display={item.description} />}
            </Card>
          </TouchableWithoutFeedback>
        ))}
      </>
    return (
      Platform.OS === 'ios' ?
        <View>
          {content}   
        </View>
      :
        <Content onContentSizeChange={(w ,h) => {
          // categoryIndex + 1, because tabIndex 0 is details tab
          this.updateTabHeights(categoryIndex + 1, h)
        }}>
          {content} 
        </Content>
    )
  }

  // this must be a function, otherwise rerenders of this.props.renderMenu won't cause
  // updates in renderTabs as it would be fixed if it was just renderTabs = <Tabs>
  renderTabs = () => {
    const { profile, location: { address }, canLeaveFeedback } = this.props;

    profile.tags.toString = function() {
      return this.join(', ');
    };

    const content = (
      <>
        {
          !!canLeaveFeedback &&
          <Button bordered iconLeft onPress={this.leaveFeedback}>
            <Icon type='MaterialIcons' name='insert-comment' />
            <Text>Leave feedback</Text>
          </Button>
        }
        <CardHeader display='Tags' />
        <ThinPaddedCard display={profile.tags} />
        {
          // !! to convert to boolean, otherwise if profile.description is valid, then it'll render raw description
          // and error
          !!profile.description &&
          <React.Fragment>
            <CardHeader display='Restaurant Description' />
            <ThinPaddedCard display={profile.description} />
          </React.Fragment>
        }
        {
          !!profile.phone &&
          <React.Fragment>
            <CardHeader display='Phone' />
            <ThinPaddedCard display={profile.phone} />
          </React.Fragment>
        }
        <CardHeader display='Address' />
        <ThinPaddedCard display={address.address1} />
        {address.address2 && <ThinPaddedCard display={address.address2} />}
        <ThinPaddedCard display={address.city} />
        <ThinPaddedCard display={address.state} />
        <ThinPaddedCard display={address.zip} />
      </>
    )

    return (
      <Tabs
        tabBarBackgroundColor={commonColor.brandCanvas}
        onChangeTab={this.onChangeTab}
        renderTabBar={() => <ScrollableTab />}
      >
        <Tab heading={<TabHeading><Icon name='md-information' /></TabHeading>}>
          <Card>
            {Platform.OS === 'ios' ?
              <View>
                {content}
              </View>
            :
              <Content onContentSizeChange={(w ,h) => this.updateTabHeights(0, h)}>
                {content}
              </Content>
            }
          </Card>
        </Tab>
        {this.props.menu.map((category, index) => {
          if (isCategoryEmpty(category)) {
            const { emptyCategoryView } = this.props;
            return emptyCategoryView ?
              <Tab key={index} heading={category.name}>{emptyCategoryView}</Tab>
              :
              null
          }

          return (
            <Tab key={index} heading={category.name}>
              {this.props.renderCategory(this.renderCategory(category, index, this.props.renderActionBar))}
            </Tab>
          )
        })}
      </Tabs>
    )
  }
  render () {
    return this.props.renderMenu(this.renderTabs());
  }
}

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  image: {
    height: width/1.5,
    width: width,
  },
  name: {
    fontWeight: '800',
  },
  priceView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
  },
  price: {
    fontWeight: '700',
  },
  thinPadding: {
    // must use paddingTop and bottom instead of vertical to override default styles
    paddingTop: commonColor.listItemThinPadding,
    paddingBottom: commonColor.listItemThinPadding
  },
  label: {
    paddingLeft: 10,
  },
  tabs: {
    backgroundColor: commonColor.brandCanvas,
    flex: 1
  },
  emptyFlick: {
    textAlign: 'center',
    textAlignVertical: 'center',
    padding: 20,
  }
});

export default withNavigation(MenuOverview);
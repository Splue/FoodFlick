import { activeConfig } from './../../config';
import AnalyticsService from './../analytics/AnalyticsService';
import { setItemAsync, deleteItemAsync } from 'expo-secure-store';
import 'firebase/auth';
import { KJUR } from 'jsrsasign';
import uuid from 'uuid/v1';
import * as firebase from 'firebase/app';
import { NavigationActions } from 'react-navigation';
import UserService from "./userService";
import { RootState } from '../redux/RootState';
import { SignedInUserSelector, SignedInUser, card } from './SignedInUser';
import getApolloClient from '../../apolloClient';
import { configureScope } from 'sentry-expo';

export const CLEAR_USER = 'CLEAR_USER';
export const SET_EMAIL = 'SET_EMAIL';
export const SET_NAME = 'SET_NAME';
export const SET_PASSWORD = 'SET_PASSWORD';
export const SET_CARD = 'STORE_CARD';
export const UPDATE_IS_REST_MANAGER = 'UPDATE_IS_REST_MANAGER';
export const SIGN_IN_ACTION = 'SIGN_IN';

const jwtUtil = KJUR.jws.JWS
const PUBLIC_KEY = activeConfig.auth.publicKey;

const client_id = activeConfig.auth.clientId;
const audience = activeConfig.auth.audience;
const namespace = 'https://foodflick.com'; // the namespace for each custom jwt field
export const auth0Domain = `${activeConfig.auth.domain}/`

//there is no firstName/lastName if isRestManager is true. default to strings, because auth0 only accepts strings
export const signUpAction = user => async dispatch => {

  const {
    email,
    firstName = '',
    lastName = '',
    isRestManager,
    password,
    cardToken,
  } = user;

  //creates a user with data, managed by a auth0 post-signup hook. sample profile
  // app_metadata: {
  //   isRestManager: true
  //   stripeId: the user's stripe customer id
  // },
  // user_metadata: {
  //   firstName: null if rest otherwise string,
  //   lastName: null if rest otherwise string,
  //   alternateEmails: []
  // },

  const res = await fetch(auth0Domain + 'dbconnections/signup', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      email,
      password,
      audience,
      connection: 'Username-Password-Authentication',
      client_id,
      user_metadata: {
        firstName,
        lastName,
        //'' for false string so field can be used as bool in auth0 hook. could not send json bool so converting to
        //string. this field only being used to populate app_metadata in auth0 and so we never use
        //user_metadata.isRestManager again
        isRestManager: isRestManager ? 'true' : '',
        // '' for false string. this field is only being used to set stripeId in app_metadata and so we never use
        // user_medatadata.cardToken again
        cardToken: cardToken ? cardToken : '',
      }
    }),
  });

  const json = await res.json();

  //todo 0 add error handling
  if (!res.ok) {
    dispatch(clearSignedInUser());
    console.warn('signup failed, gonna throw');
    throw json;
  }
  AnalyticsService.trackEvent(AnalyticsService.events.SIGN_UP)
  return json;
};

const getNewAccessTokenBefore = (dispatch, seconds) => {
  // const refreshToken = await Expo.SecureStore.getItemAsync('refreshToken');
  // setTimeout(() => dispatch(signInActionRefresh(refreshToken)), seconds * 1000 - 60000);
}

// todo 0: only sign in to firebase if youre a manager
const signInToFirebase = idToken => {
  const { [`${namespace}/firebaseToken`]: firebaseToken } = jwtUtil.parse(idToken).payloadObj;
  firebase.auth().signInWithCustomToken(firebaseToken)
    .catch(function (error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
    });
}

const getSignedInUser = (authJson): SignedInUser => {
  const {
    email,
    sub,
    [`${namespace}/firstName`]: firstName,
    [`${namespace}/lastName`]: lastName,
  } = jwtUtil.parse(authJson.id_token).payloadObj;

  configureScope(scope => {
    scope.setUser({
      id: sub,
      email,
    });
  });
  AnalyticsService.setUserId(sub);
  AnalyticsService.setUserProperties({
    perms: authJson.scope.includes('write:rests') ? ('write:rests') : null
  });
  return {
    email,
    _id: sub, //remove the identity provider
    accessToken: {
      token: authJson.access_token,
      type: authJson.token_type
    },
    firstName,
    lastName,
    //"openid profile email address phone write:rests offline_access"
    perms: authJson.scope.includes('write:rests') ? ('write:rests') : null
  } as SignedInUser
}

export const signInWithBasicAction = (email, password) => async dispatch => {
  const authRes = await fetch(auth0Domain + 'oauth/token', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      grant_type: 'password',
      username: email,
      password,
      scope: 'openid offline_access',
      client_id
    }),
  })

  const authJson = await authRes.json();

  if (!authRes.ok) throw (authJson);

  //todo 1: TURN THIS BACK ON!!!!!! BUT ITS VALIDATION IS INCONSISTENT. honestly, i dont even need to validate...
  //it's clienti side anyway... should use https://github.com/auth0/jwt-decode instead
  // const isValid = jwtUtil.verifyJWT(authJson.id_token, PUBLIC_KEY, {
  //   alg: ['RS256'],
  //   iss: ['https://allmenus.auth0.com/'], //token must be from auth0
  //   aud: ['bYqSrFjNtarq47IjNn3l7g0JRQjXQgz0'] //token must be for this react-native app
  // });

  // if (!isValid) throw new Error ('received invalid token ' + JSON.stringify(authJson.id_token));

  //todo 1: make it so i dont have to sign in every time by using the refresh token
  getNewAccessTokenBefore(dispatch, authJson.expires_int);
  // console.log('auth0 res', authJson);

  setItemAsync('refreshToken', authJson.refresh_token).catch(e => console.warn(e));

  // todo 1: need because it's possible to have rests which have specified this user as manager before this user signed
  // up, resulting in manager lists without _ids.
  // restActions.addManagerIdToManagedRests
  signInToFirebase(authJson.id_token);
  const signedInUser = getSignedInUser(authJson);

  dispatch({
    type: SIGN_IN_ACTION,
    signedInUser,
  });
  AnalyticsService.trackEvent(AnalyticsService.events.LOGIN_WITH_PASSWORD)
  return signedInUser;
};

export const signInWithRefreshAction = (refreshToken) => async dispatch => {
  const authRes = await fetch(auth0Domain + 'oauth/token', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      grant_type: 'refresh_token',
      refresh_token: refreshToken,
      client_id
    }),
  });

  const authJson = await authRes.json();

  if (!authRes.ok) throw (authJson);

  getNewAccessTokenBefore(dispatch, authJson.expires_int);

  signInToFirebase(authJson.id_token);

  const signedInUser = getSignedInUser(authJson);
  dispatch({
    type: SIGN_IN_ACTION,
    signedInUser,
  });
  console.log('auto logged in');
  AnalyticsService.trackEvent(AnalyticsService.events.LOGIN_WITH_REFRESH)
  return signedInUser;
};

const uploadFlicksAndGetUrls = (
  firebaseStorageRef,
  uris: string[],
  signedInUserId: string): Promise<string[]> => new Promise((resolve, reject) => {

    const userRef = firebaseStorageRef.child(signedInUserId + '/');
    let uploadCount = 0;
    const urls: string[] = new Array(uris.length);

    uris.forEach((uri, index) => {
      fetch(uri).then(res => res.blob()).then(blob => {
        const pathRef = userRef.child('/' + uuid());
        return pathRef.put(blob, { customMetadata: { 'owner': signedInUserId } });
      }).then(res => {
        uploadCount++;

        urls[index] = res.downloadURL;

        if (uploadCount === uris.length) {
          resolve(urls);
        }
      }).catch(err => reject(err));
    });
  });

export const addFlicksAction = (uris: string[]) => async (dispatch, getState: () => RootState) => {
  const firebaseStorageRef = getState().getFirebaseStorageRef();
  const signedInUser = getState().getSignedInUser();
  const urls: string[] = await uploadFlicksAndGetUrls(firebaseStorageRef, uris, SignedInUserSelector.getId(signedInUser));
  AnalyticsService.trackEventWithProperties(AnalyticsService.events.UPLOADED_PHOTOS_TO_ACCOUNT, { 'numOfPhotos': urls.length })
  return await UserService.addFlicks(urls);
}

export const navigateAndResetAccountNavAction = (nextScreen?) => {
  let state = {
    index: 0,
    actions: [
      NavigationActions.navigate({ routeName: 'accountScreen' })
    ],
    key: 'accountNavigator'
  };

  if (nextScreen) {
    state.actions.push(NavigationActions.navigate({ routeName: nextScreen }));
    state.index = 1;
  }

  return NavigationActions.reset(state);
}

export const doesUserExist = async (email: string): Promise<boolean> => await UserService.doesUserExist(email);

export const clearSignedInUser = () => {
  deleteItemAsync('refreshToken').catch(e => console.warn(e));
  getApolloClient().resetStore();
  AnalyticsService.trackEvent(AnalyticsService.events.SIGN_OUT);
  return {
    type: CLEAR_USER,
  }
}

export const setEmailAction = email => ({
  type: SET_EMAIL,
  email
});

export const setNameAction = ({ firstName, lastName }) => ({
  type: SET_NAME,
  firstName,
  lastName,
});

export const setPasswordAction = password => ({
  type: SET_PASSWORD,
  password
});

export const setNewCardTokenAction = cardToken => ({
  type: SET_CARD,
  cardToken,
});

export const updateCardAction = cardToken => async () => await UserService.updateCard(cardToken);

export const updateEmailAction = newEmail => (async dispatch => {
  const res = await UserService.updateUserEmail(newEmail);
  if (res) {
    dispatch(setEmailAction(newEmail));
    AnalyticsService.trackEvent(AnalyticsService.events.CHANGED_EMAIL)
  }
});

export const updateIsRestManagerAction = isRestManager => ({
  type: UPDATE_IS_REST_MANAGER,
  isRestManager
})
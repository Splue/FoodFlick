import gql from 'graphql-tag';

export const cardFragment = gql`
  fragment cardFragment on Card {
    last4,
    expMonth,
    expYear
  }
`

export interface Jwt {
  token: string,
  type: 'Bearer',
}

export interface card {
  number: string,
  expDate: string,
  cvv: string,
  zip: string,
}

export interface SignedInUser {
  accessToken: Jwt,
  card?: card,
  email: string,
  firstName: string,
  lastName: string,
  perms: 'write:rests' | null, // union type. either 'write:rests' or null
  _id: string,
}


export abstract class SignedInUserSelector {
  static getCard = (user: SignedInUser): card => user ? user.card : undefined;

  static getEmail = (user: SignedInUser): string => user ? user.email: undefined;

  static getId = (user: SignedInUser): string => user ? user._id : undefined;

  static getFirstName = (user: SignedInUser): string => user ? user.firstName : undefined;

  static getLastName = (user: SignedInUser): string => user ? user.lastName : undefined;

};
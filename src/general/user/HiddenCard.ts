export interface HiddenCard {
  cardTok: string;
  last4: string;
  expMonth: number;
  expYear: number;
}

import {
  SIGN_IN_ACTION,
  SET_CARD,
  SET_EMAIL, 
  SET_NAME,
  UPDATE_IS_REST_MANAGER,
  SET_PASSWORD,
  CLEAR_USER
} from "./userActions";

export function signedInUser (state = null, action = {}) {
  switch(action.type) {
    case SIGN_IN_ACTION:
      return action.signedInUser;
    case SET_CARD:
      return { ...state, cardToken: action.cardToken }
    case SET_EMAIL:
      return { ...state, email: action.email }
    case SET_NAME:
      return {
        ...state,
        firstName: action.firstName,
        lastName: action.lastName,
      }
    case UPDATE_IS_REST_MANAGER:
      return {...state, isRestManager: action.isRestManager }
    case SET_PASSWORD:
      return { ...state, password: action.password }
    case CLEAR_USER:
      return null;
    default:
      return state;
  }
}

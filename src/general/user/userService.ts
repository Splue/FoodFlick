import { cardFragment } from './cardFragment';
import { HiddenCard } from './HiddenCard';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import getApolloClient from '../../apolloClient';
import { Flick } from '../redux/Flick';

const myFlicksQuery = gql`
  query {
    myFlicks {
      flick
    }
  }
`;

const myCardQuery = gql`
  query myCard {
    myCard {
      ...cardFragment
    }
  }
  ${cardFragment}
`

type MyCard = {
  myCard: HiddenCard;
}

type MyFlicks = {
  myFlicks: Flick[];
}

export default {
  doesUserExist: async (email: string): Promise<boolean> => {
    type response = {
      doesUserExist: boolean;
    };
    const res = await getApolloClient().query<response>({
      query: gql`
        query doesUserExist($email: String!) {
          doesUserExist(email: $email)
        }
      `,
      fetchPolicy: 'no-cache',
      variables: { email },
    });
    return res.data.doesUserExist;
  },

  
  getMyCardInjector: graphql<any, MyCard>(myCardQuery, {
    props: ({ data }) => ({
      data,
      cardLoading: data.loading,
      refetchCard: data.refetch,
      myCard: data.myCard,
    })
  }),

  getMyFlicksInjector: graphql<any, MyFlicks>(myFlicksQuery, {
    props: ({ data }) => ({
      data,
      loading: data.loading,
      myFlicks: data.myFlicks,
      // errors: data.errors,
      error: data.error,
    })
  }),

  updateUserEmail: async newEmail => await getApolloClient().mutate({
    mutation: gql`
      mutation updateUserEmail($newEmail: String!) {
        updateUserEmail(newEmail: $newEmail)
      }
    `,
    variables: {
      newEmail
    }
  }),

  updateCard: async cardToken => await getApolloClient().mutate({
    mutation: gql`
      mutation updateUserCard($cardToken: ID!) {
        updateUserCard(cardToken: $cardToken) {
          ...cardFragment
        }
      }
      ${cardFragment}
    `,
    variables: {
      cardToken
    },
    update: (cache, {data: { updateUserCard }}) => {
      //try/catch because if we update card before ever querying my card, then we get error. see
      //https://github.com/apollographql/apollo-client/issues/1542
      try {
        cache.writeQuery({
          query: myCardQuery,
          data: { myCard: updateUserCard }
        });
      } catch(e) {}
    },
  }),

  addFlicks: async (urls: string[]) => {
    return await getApolloClient().mutate({
      mutation: gql`
        mutation addUserFlicks($urls: [String!]!) {
          addUserFlicks(urls: $urls)
        }
      `,
      variables: {
        urls,
      }
    });
  },
};
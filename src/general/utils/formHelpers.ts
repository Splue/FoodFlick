/**
 * Clean the provided input by removing trimming whitespace.
 * Original argument is unmodifed.
 *
 * @param input - The string to be cleaned
 * @returns The newly cleaned input
 */
export const getCleanInput = (inputs: string): string => inputs.trim();

/**
 * Clean the provided input object by cleanInput on each property
 * Original argument is unmodifed.
 *
 * @param input - The object to be cleaned
 * @returns The newly cleaned object
 */
export const getCleanInputs = (inputs: object): object => {
  const newInputs = { ...inputs };
  Object.keys(newInputs).forEach(key => {
    const value = newInputs[key];
    if (typeof value === 'string') {
      newInputs[key] = value.trim();
    }

    if (value instanceof Array) {
      newInputs[key] = value.map(arrayItem => {
        if (typeof arrayItem === 'string') return arrayItem.trim();
        if (typeof arrayItem === 'object') return getCleanInputs(arrayItem);
        throw new Error('unexpected cleaning');
      });
    }
  });
  return newInputs;
}
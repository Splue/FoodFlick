import { Keyboard } from 'react-native';

export function addKeyboardListeners (didShow, didHide) {
  this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', didShow);
  this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', didHide); 
}

export function removeKeyboardListeners () {
  this.keyboardDidShowSub.remove();
  this.keyboardDidHideSub.remove();
}
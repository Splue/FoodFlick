import PropTypes from 'prop-types';

export const signedInUserPropType = PropTypes.shape({
  accessToken: PropTypes.shape({
    token: PropTypes.string,
    type: PropTypes.string,
  }),
  email: PropTypes.string,
  firstName: PropTypes.string,
  fullName : PropTypes.string,
  perms: PropTypes.string,
  _id: PropTypes.string,
});

export const getReferencedUserPropType = (shape = {}) => PropTypes.shape({
  userId: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  ...shape
});
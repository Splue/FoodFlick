import PropTypes from 'prop-types';
import { getReferencedUserPropType } from './user';

export const selectedRestPropType = PropTypes.shape({
  location: PropTypes.shape({
    address: PropTypes.shape({
      address1: PropTypes.string.isRequired,
      address2: PropTypes.string,
      city: PropTypes.string.isRequired,
      state: PropTypes.string.isRequired,
      zip: PropTypes.string.isRequired,
    }),
  }),
  profile: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
  }),
  owner: getReferencedUserPropType(),
  managers: PropTypes.arrayOf(getReferencedUserPropType({ _id: PropTypes.string })),
  menu: PropTypes.array.isRequired,
});
import PropTypes from 'prop-types';

const navigationPropShape = {
  addListener: PropTypes.func,
  dispatch: PropTypes.func,
  goBack: PropTypes.func,
  navigate: PropTypes.func,
  pop: PropTypes.func,
  popToTop: PropTypes.func,
  push: PropTypes.func,
  setParams: PropTypes.func,
  state: PropTypes.object,
};

export const navigationPropType = PropTypes.shape(navigationPropShape);

export const getNavigationFormPropType = (params = {}) => (PropTypes.shape({
  ...navigationPropShape,
  state: PropTypes.shape({
    params: PropTypes.shape({
      confirmText: PropTypes.string.isRequired,
      ...params
    })
  })
}));
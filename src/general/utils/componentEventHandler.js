// todo 0: use this everywhere
// see https://github.com/facebook/react/issues/11409
/**
 * Calls the provided callback when the event fires. If cb result is a promise, throw an error in the component. This is
 * useful because otherwise cbs which return a rejected promise are uncaptured/unhandled and errors in component
 * event handlers (promise rejections or regular errors) will NOT trigger componentDidCatch.
 *
 * 
 * @param {self} the component.
 * @param {fn} cb the callback for the event.
 * @param {...args} the arguments for the cb.
 * @returns {void}
 */

export function doEvent (self, cb, ...args) {
  try {
    const res = cb(...args);
    // if res is a promise
    if (typeof res.then === 'function') {
      res.catch(e => {
        self.setState(() => { throw e })
      });
    }
  } catch (e) {
    self.setState(() => { throw e });
  }
};
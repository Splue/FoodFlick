import gql from 'graphql-tag';

export const managerRestFragment = gql`
  fragment managerRestFragment on Rest {
    _id
    favorites {
      count
    }
    feedback {
      feedback
      createdDate
    }
    profile {
      name
      phone
      description
      tags
    }
    location {
      address {
        address1
        address2
        city
        state
        zip
      }
      timezone {
        name
      }
    }
    banking {
      accountNumberLast4
      routingNumber
    }
    owner {
      userId
      email
    }
    managers {
      userId
      email
    }
    minsToUpdateCart
    menu {
      name
      description
      items {
        name
        privateNames
        prices {
          value
          label
        }
        addons {
          value
          label
        }
        description
        flick
        likes {
          count
        }
        optionGroups {
          options {
            name
            price
          }
        }
        printers {
          name
          itemName
          ip
          port
          type
        }
      }
    }
    receiver {
      receiverId
      printers {
        name
        ip
        port
        type
        isReceipt
      }
    }
    subscription {
      plan {
        stripeSubscriptionId
        stripePlanId
        name
        monthlyOrders
        monthlyRate
        overagePercentageFee
      }
      card {
        cardTok
        last4
        expMonth
        expYear
      }
    }
    servers {
      email
      userId
    }
    tables {
      _id
      userId
    }
    taxRate
    url
  }
`

export const customerRestFragment = gql`
  fragment customerRestFragment on Rest {
    _id
    banking {
      stripeId
    }
    favorites {
      count
      isFavorite
    }
    profile {
      name
      phone
      description
      tags
    }
    location {
      address {
        address1
        address2
        city
        state
        zip
      }
    }
    menu {
      name
      description
      items {
        name
        prices {
          value
          label
        }
        addons {
          value
          label
        }
        description
        flick
        likes {
          count
          hasLiked
        }
        optionGroups {
          options {
            name
            price
          }
        }
      }
    }
    url
  }
`
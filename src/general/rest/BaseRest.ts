import { BaseRest } from './BaseRest';
import { BaseCategory } from '../menu/BaseMenu';
import { Tag } from '../tags/Tag';
import { find } from 'lodash';

export interface FavoritesBase {
  count: number;
  // isFavorite: boolean;
}

export interface Profile {
  name: string;
  phone: string;
  description: string;
  tags: Tag['name'][];
}

export interface Address {
  address1: string;
  address2?: string;
  city: string;
  state: string; // todo 0: change to states enum
  zip: string;
}

export interface Timezone {
  name: string
}

export interface Location {
  address: Address;
  timezone: Timezone
}

export interface BaseRest {
  _id: string;
  favorites: FavoritesBase;
  profile: Profile;
  location: Location;
  menu: BaseCategory[];
  url: string;
  taxRate: number;
};

export abstract class BaseRestSelector {
  static getId = (rest: BaseRest): string => rest ? rest._id : undefined;

  static getCategoryByName = (rest: BaseRest, categoryName: string): BaseCategory => (
    // need to add 'as any' or TS complains
    // https://github.com/DefinitelyTyped/DefinitelyTyped/issues/25758
    find(BaseRestSelector.getMenu(rest), { name: categoryName }) as any
  )

  static getDescription = (rest: BaseRest) => rest ? rest.profile.description : undefined;

  static getMenu = (rest: BaseRest): BaseCategory[] => rest ? rest.menu : undefined;

  static getName = (rest: BaseRest): string => rest ? rest.profile.name : undefined;

  static getPhone = (rest: BaseRest) => rest ? rest.profile.phone : undefined;

  static getProfile = (rest: BaseRest): Profile => rest ? rest.profile : undefined;

  static getLocation = (rest: BaseRest): Location => rest ? rest.location : undefined;

  static getTags = (rest: BaseRest): Tag['name'][] => rest ? rest.profile.tags : undefined;

  static getTaxRate = (rest: BaseRest): number => rest ? rest.taxRate : undefined;

  static isEqual = (r1: BaseRest, r2: BaseRest): boolean => {
    if (typeof r1 === 'undefined') return typeof r2 === 'undefined' ? true : false;

    if (typeof r2 === 'undefined') return typeof r1 === 'undefined' ? true : false;

    if (r1 === null) return r2 === null;

    if (r2 === null) return r1 === null;
    
    return r1._id === r2._id;
  }
};
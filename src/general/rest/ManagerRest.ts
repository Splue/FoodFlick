import { SignedInUser } from './../user/SignedInUser';
import { HiddenCard } from './../user/HiddenCard';
import { Plan } from '../plan/PlanModel';
import { ManagerRest, Subscription, ActiveSubscription } from './ManagerRest';
import { ManagerCategory } from '../menu/ManagerMenu';
import { BaseRest, BaseRestSelector, Address } from './BaseRest';

export interface Feedback {
  feedback: string;
  createdDate: number;
}

export interface User {
  userId: string;
  email: string;
}

export interface Banking {
  accountNumberLast4?: string;
  routingNumber?: string;
  stripeId: string;
}

export enum PrinterType {
  EPSON = 'epson',
  STAR = 'star',
}

export interface Printer {
  name: string;
  ip: string;
  port: string;
  type: PrinterType;
  isReceipt: boolean;
}

export interface Table {
  _id: string;
  userId: string;
}

export interface UpdatePrinter {
  index: number,
  printer: Printer,
}

export interface Receiver {
  receiverId: string;
  printers: Printer[];
}

export interface ActiveSubscription extends Plan {
  stripeSubscriptionId: string,
  proation: never,
}

export interface Subscription {
  plan: ActiveSubscription,
  card: HiddenCard,
}

export interface ManagerRest extends BaseRest {
  feedback: Feedback[];
  banking: Banking,
  owner: User;
  managers: User[];
  menu: ManagerCategory[]
  receiver: Receiver
  minsToUpdateCart: number
  subscription: Subscription
  servers: User[];
  tables: Table[]
}

export abstract class ManagerRestSelector extends BaseRestSelector {
  static getAddress1 = (rest: ManagerRest): Address['address1'] => rest ? rest.location.address.address1 : undefined;

  static getTimezone = (rest: ManagerRest): string => rest ? rest.location.timezone.name : undefined;

  static getCategoryByName = (rest: ManagerRest, categoryName: string): ManagerCategory => (
    BaseRestSelector.getCategoryByName(rest, categoryName)
  )

  static getBankAccountNumber = (rest: ManagerRest): string => {
    if (!rest || !rest.banking || !rest.banking.accountNumberLast4) {
      return undefined;
    }
    return `**** **** **** ${rest.banking.accountNumberLast4}`
  }

  static getRoutingNumber = (rest: ManagerRest): string => rest ? (rest.banking || {} as any).routingNumber: undefined

  static getCity = (rest: ManagerRest): Address['city'] => rest ? rest.location.address.city : undefined;

  static getFeedback = (rest: ManagerRest): Feedback[] => rest ? rest.feedback : undefined;

  static getManagers = (rest: ManagerRest): User[] => rest ? rest.managers : undefined;
  
  static isManager = (rest: ManagerRest, user: SignedInUser) => {
    if (rest.owner.userId === user._id) return true;
    for (let i = 0; i < rest.managers.length; i++) {
      if (rest.managers[i].userId === user._id) return true;
    }
    return false;
  }

  static getMenu = (rest: ManagerRest): ManagerCategory[] => rest ? rest.menu : undefined;

  static getOwner = (rest: ManagerRest): User => rest ? rest.owner : undefined;

  static getState = (rest: ManagerRest): Address['state'] => rest ? rest.location.address.state : undefined;

  static getZip = (rest: ManagerRest): Address['zip'] => rest ? rest.location.address.zip : undefined;

  static getPrinters = (rest: ManagerRest): Printer[] => rest ? rest.receiver.printers : undefined;

  static getPrinter = (printers: Printer[], printerName: string): Printer => (
    printers.find(printer => printer.name === printerName)
  );

  static getReceiverId = (rest: ManagerRest): string => rest ? rest.receiver.receiverId : undefined;

  static getUrl = (rest: ManagerRest): string => rest ? rest.url : undefined;

  static getMinsToUpdateCart = (rest: ManagerRest): number => rest ? rest.minsToUpdateCart : undefined;

  static getActiveSubscription = (rest: ManagerRest): ActiveSubscription => rest ? rest.subscription.plan : undefined;

  static getSubscriptionId = (rest: ManagerRest): string => rest ? rest.subscription.plan.stripeSubscriptionId : undefined;

  static getSubscriptionCard = (rest: ManagerRest): string => {
    if (!rest || !rest.subscription.card) {
      return undefined;
    }
    return `**** **** **** ${rest.subscription.card.last4}`
  }

  static getServers = (rest: ManagerRest): User[] => rest ? rest.servers : undefined;

  static getTables = (rest: ManagerRest): Table[] => rest ? rest.tables : undefined;

}
import { LocationState } from './../redux/locationReducer';
import { graphql } from 'react-apollo';
import getApolloClient from '../../apolloClient';
import { managerRestFragment, customerRestFragment } from './restFragments';
import { reject } from 'lodash';
import gql from 'graphql-tag';

// const sponsoredRestsQuery = gql`
//   query {
//     sponsoredRestsQuery {
//       ...customerRestFragment
//     }
//   }
//   ${customerRestFragment}
// `;

const myRestsQuery = gql`
  query {
    myRests {
      ...managerRestFragment
    }
  }
  ${managerRestFragment}
`;

const myFavoriteRestsQuery = gql`
  query {
    myFavoriteRests {
      ...customerRestFragment
    }
  }
  ${customerRestFragment}
`;

const restBankingQuery = gql`
  query restBanking($restId: String!) {
    restBanking (restId: $restId) {
      accountNumberLast4
      routingNumber
      stripeId
    }
  }
`;

export const restSearchSuggestionsQuery = gql`
  query restSearchSuggestions($query: String!) {
    restSearchSuggestions(query: $query) {
      ...customerRestFragment
    }
  }
  ${customerRestFragment}
`;

export default {
  // getSponsoredRestsInjector: graphql(sponsoredRestsQuery, {
  //   props: ({ data, errors, error }) => ({
  //     data,m
  //     loading: data.loading,
  //     sponsoredRestsQuery: data.sponsoredRestsQuery,
  //     errors,
  //     error,
  //   })
  // }),

  getMyRestsInjector: graphql(myRestsQuery, {
    props: ({ data, errors, error }) => ({
      data,
      loading: data.loading,
      myRests: data.myRests,
      errors,
      error,
    })
  }),

  getMyFavoritesInjector: graphql(myFavoriteRestsQuery, {
    skip: ({ signedInUser }) => !Boolean(signedInUser),
    props: ({ data, errors, error }) => ({
      data,
      loading: data.loading,
      favoriteRests: data.myFavoriteRests,
      errors,
      error,
    })
  }),
  
  getRestBankingInjector: graphql(restBankingQuery, {
    options: ({ selectedRest }) => ({
      variables: {
        restId: selectedRest._id,
      },
    }),
    props: ({ data, errors, error }) => ({
      data,
      bankingLoading: data.loading,
      restBanking: data.restBanking,
      errors,
      error,
    })
  }),

  getRestSearchSuggestions: (query: string, location: LocationState) => {
    const res = getApolloClient().query({
      query: restSearchSuggestionsQuery,
      variables: { query, location },
    });
    return res.then(({ data }) => data.restSearchSuggestions);
  },

  giveRestFeedback: (restId, feedback) => {
    getApolloClient().mutate({
      mutation: gql`
        mutation giveRestFeedback($restId: ID!, $feedback: String!) {
          giveRestFeedback(restId: $restId, feedback: $feedback)
        }
      `,
      variables: {
        restId,
        feedback
      }
    });
  },

  addNewRest: async newRest => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation addRest($newRest: NewRestInput!) {
          addRest(newRest: $newRest) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        newRest
      },
      update: (cache, {data: {addRest}}) => {
        //try/catch because if we add rest before ever querying myRests, then we get error. see
        //https://github.com/apollographql/apollo-client/issues/1542
        try {
          const { myRests } = cache.readQuery({query: myRestsQuery});
          cache.writeQuery({
            query: myRestsQuery,
            data: { myRests: [...myRests, addRest] }
          });
        } catch(e) {}
      },
    });

    return res.data.addRest;
  },

  addRestPrinter: async (restId, newPrinter) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation addRestPrinter($restId: ID!, $newPrinter: PrinterInput!) {
          addRestPrinter(restId: $restId, newPrinter: $newPrinter) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        newPrinter
      },
      errorPolicy: 'all'
    });
    return res.data.addRestPrinter;
  },

  addRestReceiver: async (restId, receiverId) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation addRestReceiver($restId: ID!, $receiverId: ID!) {
          addRestReceiver(restId: $restId, receiverId: $receiverId) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        receiverId
      },
      errorPolicy: 'all'
    });
    return res.data.addRestReceiver;
  },

  addRestTaxRate: async (restId, taxRate) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation addRestTaxRate($restId: ID!, $taxRate: Float!) {
          addRestTaxRate(restId: $restId, taxRate: $taxRate) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        taxRate
      },
      errorPolicy: 'all'
    });
    return res.data.addRestTaxRate;
  },

  updateRestTaxRate: async (restId, taxRate) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateRestTaxRate($restId: ID!, $taxRate: Float!) {
          updateRestTaxRate(restId: $restId, taxRate: $taxRate) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        taxRate
      },
      errorPolicy: 'all'
    });
    return res.data.updateRestTaxRate;
  },


  addRestTable: async (restId, tableId) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation addRestTable($restId: ID!, $tableId: ID!) {
          addRestTable(restId: $restId, tableId: $tableId) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        tableId
      },
      errorPolicy: 'all'
    });
    return res.data.addRestTable;
  },

  deleteRestPrinter: async (restId, printerName) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation deleteRestPrinter($restId: ID!, $printerName: String!) {
          deleteRestPrinter(restId: $restId, printerName: $printerName) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        printerName
      },
      errorPolicy: 'all'
    });
    return res.data.deleteRestPrinter;
  },

  deleteRestTable: async (restId, tableId) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation deleteRestTable($restId: ID!, $tableId: ID!) {
          deleteRestTable(restId: $restId, tableId: $tableId) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        tableId
      },
      errorPolicy: 'all'
    });
    return res.data.deleteRestTable;
  },

  updateRestBanking: async (restId, newBanking) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateRestBanking($restId: ID!, $newBanking: BankingInput!) {
          updateRestBanking(restId: $restId, newBanking: $newBanking) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        newBanking
      }
    });
    return res.data.updateRestBanking;
  },

  updateRestLocation: async (restId, newLocation) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateRestLocation($restId: ID!, $newLocation: LocationInput!) {
          updateRestLocation(restId: $restId, newLocation: $newLocation) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        newLocation
      }
    });
    return res.data.updateRestLocation;
  },

  updateMinsToUpdateCart: async (restId, mins) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateRestMinsToUpdateCart($restId: ID!, $mins: Float!) {
          updateRestMinsToUpdateCart(restId: $restId, mins: $mins) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        mins
      },
      errorPolicy: 'all'
    });
    return res.data.updateRestMinsToUpdateCart;
  },

  updateRestPrinter: async (restId, newPrinter) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateRestPrinter($restId: ID!, $newPrinter: UpdatePrinterInput!) {
          updateRestPrinter(restId: $restId, newPrinter: $newPrinter) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        newPrinter
      },
      errorPolicy: 'all'
    });
    return res.data.updateRestPrinter;
  },

  updateRestProfile: async (restId, newProfile) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateRestProfile($restId: ID!, $newProfile: ProfileInput!) {
          updateRestProfile(restId: $restId, newProfile: $newProfile) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        newProfile
      },
      errorPolicy: 'all'
    });
    return res.data.updateRestProfile;
  },

  addRestManager: async (restId, managerEmail) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation addRestManager($restId: ID!, $managerEmail: String!) {
          addRestManager(restId: $restId, managerEmail: $managerEmail) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        managerEmail
      }
    });

    return res.data.addRestManager;
  },

  addRestServer: async (restId, serverEmail) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation addRestServer($restId: ID!, $serverEmail: String!) {
          addRestServer(restId: $restId, serverEmail: $serverEmail) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        serverEmail
      }
    });

    return res.data.addRestServer;
  },

  updateRestReceiver: async (restId, receiverId) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateRestReceiver($restId: ID!, $receiverId: ID!) {
          updateRestReceiver(restId: $restId, receiverId: $receiverId) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        receiverId
      },
      errorPolicy: 'all'
    });
    return res.data.updateRestReceiver;
  },

  updateRestSubscription: async (restId, planId) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateRestSubscription($restId: ID!, $planId: ID!) {
          updateRestSubscription(restId: $restId, planId: $planId) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        planId
      },
    });
    return res.data.updateRestSubscription;
  },

  updateRestSubscriptionCard: async (restId, cardTok) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateRestSubscriptionCard($restId: ID!, $cardTok: ID!) {
          updateRestSubscriptionCard(restId: $restId, cardTok: $cardTok) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        cardTok
      },
    });
    return res.data.updateRestSubscriptionCard;
  },

  updateRestTableCheckIn: async (restId: string, tableId: string) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateRestTableCheckIn($restId: ID!, $tableId: ID!) {
          updateRestTableCheckIn(restId: $restId, tableId: $tableId) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        tableId
      },
      errorPolicy: 'all'
    });
    return res.data.updateRestTableCheckIn;
  },

  updateRestTable: async (restId: string, prevId: string, newId: string) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateRestTable($restId: ID!, $prevId: ID!, $newId: ID!) {
          updateRestTable(restId: $restId, prevId: $prevId, newId: $newId) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        prevId,
        newId
      },
      errorPolicy: 'all'
    });
    return res.data.updateRestTable;
  },

  updateRestUrl: async (restId, url) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation updateRestUrl($restId: ID!, $url: String!) {
          updateRestUrl(restId: $restId, url: $url) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        url
      },
      errorPolicy: 'all'
    });
    return res.data.updateRestUrl;
  },

  deleteRestManager: async (restId, userId) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation deleteRestManager($restId: ID!, $userId: ID!) {
          deleteRestManager(restId: $restId, userId: $userId) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        userId
      }
    });

    return res.data.deleteRestManager;
  },

  deleteRestServer: async (restId, userId) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation deleteRestServer($restId: ID!, $userId: ID!) {
          deleteRestServer(restId: $restId, userId: $userId) {
            ...managerRestFragment
          }
        }
        ${managerRestFragment}
      `,
      variables: {
        restId,
        userId
      }
    });

    return res.data.deleteRestServer;
  },


  testPrinter: (restId, { ip, name, type, port }) => {
    getApolloClient().mutate({
      mutation: gql`
        mutation testPrinter($restId: ID!, $printer: TestPrinterInput!) {
          testPrinter(restId: $restId, printer: $printer)
        }
      `,
      variables: {
        restId,
        printer: {
          ip,
          name,
          type,
          port
        }
      }
    });
  },

  toggleRestFavorite: async (rest, optimisticCb) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation toggleRestFavorite($restId: ID!) {
          toggleRestFavorite(restId: $restId) {
            ...customerRestFragment
          }
        }
        ${customerRestFragment}
      `,
      variables: {
        restId: rest._id,
      },
      optimisticResponse: () => {
        const restCopy = JSON.parse(JSON.stringify(rest)); // copy since we can't mutate the arg
        const favorites = restCopy.favorites; 
        favorites.isFavorite ? favorites.count !== 0 && favorites.count-- : favorites.count++;
        favorites.isFavorite = !favorites.isFavorite;
        optimisticCb(restCopy);
        return { toggleRestFavorite: restCopy };
      },
      update: (cache, { data: { toggleRestFavorite: toggledRest }}) => {
        //try/catch because if we add favorite before ever querying for favorites, then we get error. see
        //https://github.com/apollographql/apollo-client/issues/1542

        const isFavorite = toggledRest.favorites.isFavorite;
        try {
          const { myFavoriteRests } = cache.readQuery({ query: myFavoriteRestsQuery });

          cache.writeQuery({
            query: myFavoriteRestsQuery,
            data: {
              myFavoriteRests: isFavorite ? 
                [...myFavoriteRests, toggledRest] 
                :
                reject(myFavoriteRests, { _id: toggledRest._id })
            }
          });
        } catch(e) {}
      }
    });
    return res.data.toggleRestFavorite;
  },
};

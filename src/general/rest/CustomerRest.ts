import { BaseRest, FavoritesBase, BaseRestSelector } from './BaseRest';
import { CustomerCategory } from "../menu/CustomerMenu";

export interface Banking {
  stripeId: string;
}
export interface CustomerFavorite extends FavoritesBase {
  isFavorite: boolean;
  count: number;
}

export interface CustomerRest extends BaseRest {
  favorites: CustomerFavorite;
  menu: CustomerCategory[];
  banking: Banking;
}

export abstract class CustomerRestSelector extends BaseRestSelector {
  static getCategoryByName = (rest: CustomerRest, categoryName: string): CustomerCategory => (
    BaseRestSelector.getCategoryByName(rest, categoryName) as CustomerCategory
  )
  
  static isFavorite = (rest: CustomerRest): boolean => rest ? rest.favorites.isFavorite : undefined;
}
import { SELECT_REST } from './restActions';

export function selectedRest(state = null, action = {}) {
  switch(action.type) {
    case SELECT_REST:
      return action.rest
    default:
      return state
  }
}

export default SelectedRestState = {
  getName: state => {
    return state.profile.name
  }
};
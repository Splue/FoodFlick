import { CustomerCategory } from '../menu/CustomerMenu';
import { selectFavoritesCategoryAction, selectTrendingCategoryAction } from './../menu/menuActions';
import { RootState } from './../redux/RootState';
import { CustomerRest, CustomerRestSelector } from './CustomerRest';
import { ACCOUNT_TAB } from '../../account/redux/accountReducer';
import { unselectCategoryAction } from '../menu/menuActions';
import { ManagerRest, Printer, UpdatePrinter, ManagerRestSelector } from './ManagerRest';
import RestService from './restService';
import { TRENDING_TAB } from '../../trending/redux/trendingReducer';
import { FAVORITES_TAB } from '../../favorites/redux/favoritesReducer';

export const SELECT_REST = 'SELECT_REST';
export const STORE_NEW_REST_BANKING = 'STORE_NEW_REST_BANKING';
export const STORE_NEW_REST_PROFILE = 'STORE_NEW_REST_PROFILE';
export const STORE_NEW_REST_LOCATION = 'STORE_NEW_REST_LOCATION';

export const addNewRestAction = newRest => async dispatch => {
  try {
    const addedRest = await RestService.addNewRest(newRest);
    dispatch(selectAccountRestAction(addedRest));
  } catch (e) {
    throw new Error(e);
  }
};

export const addRestPrinter = (restId: string, newPrinter: Printer) => async dispatch => {
  const updatedRest = await RestService.addRestPrinter(restId, newPrinter);
  dispatch(selectAccountRestAction(updatedRest));
}

export const addRestReceiver = (restId: string, receiverId: string) => async dispatch => {
  const updatedRest = await RestService.addRestReceiver(restId, receiverId);
  dispatch(selectAccountRestAction(updatedRest));
}

export const addRestTaxRate = (restId: string, taxRate: number) => async dispatch => {
  const updatedRest = await RestService.addRestTaxRate(restId, taxRate);
  dispatch(selectAccountRestAction(updatedRest));
}

export const updateRestTaxRate = (restId: string, taxRate: number) => async dispatch => {
  const updatedRest = await RestService.updateRestTaxRate(restId, taxRate);
  dispatch(selectAccountRestAction(updatedRest));
}

export const addRestManagerAction = (restId, managerEmail) => (async dispatch => {
  const updatedRest = await RestService.addRestManager(restId, managerEmail);
  dispatch(selectAccountRestAction(updatedRest));
});

export const addRestServerAction = (restId, serverEmail) => (async dispatch => {
  const updatedRest = await RestService.addRestServer(restId, serverEmail);
  dispatch(selectAccountRestAction(updatedRest));
});

export const addRestTableAction = (restId: string, receiverId: string) => async dispatch => {
  const updatedRest = await RestService.addRestTable(restId, receiverId);
  dispatch(selectAccountRestAction(updatedRest));
}

export const deleteRestManagerAction = (restId, userId) => (async dispatch => {
  const updatedRest = await RestService.deleteRestManager(restId, userId);
  dispatch(selectAccountRestAction(updatedRest));
});

export const deleteRestServerAction = (restId, userId) => (async dispatch => {
  const updatedRest = await RestService.deleteRestServer(restId, userId);
  dispatch(selectAccountRestAction(updatedRest));
});

export const deleteRestPrinter = (restId: string, printerName: string) => async dispatch => {
  const updatedRest = await RestService.deleteRestPrinter(restId, printerName);
  dispatch(selectAccountRestAction(updatedRest));
}

export const deleteRestTable = (tableId: string) => async (dispatch, getState) => {
  const updatedRest = await RestService.deleteRestTable(
    ManagerRestSelector.getId(getState().getAccount().getSelectedRest()),
    tableId
  );
  dispatch(selectAccountRestAction(updatedRest));
}

export const selectAccountRestAction = (rest: ManagerRest) => dispatch => {
  dispatch(selectRestAction(rest, ACCOUNT_TAB));
}

export const selectFavoritesRestAction = (rest: CustomerRest) => dispatch => {
  dispatch(selectRestAction(rest, FAVORITES_TAB));
}

export const selectTrendingRestAction = (rest: CustomerRest) => dispatch => {
  dispatch(selectRestAction(rest, TRENDING_TAB));
}

const selectRestAction = (rest, tab) => async dispatch => {
  dispatch({
    type: SELECT_REST,
    tab,
    rest
  });
  dispatch(unselectCategoryAction(tab));
};

export const storeNewRestLocationAction = location => ({
  type: STORE_NEW_REST_LOCATION,
  tab: ACCOUNT_TAB,
  location
});

export const storeNewRestProfileAction = profile => ({
  type: STORE_NEW_REST_PROFILE,
  tab: ACCOUNT_TAB,
  profile
});

export const storeNewRestBankingAction = banking => ({
  type: STORE_NEW_REST_BANKING,
  tab: ACCOUNT_TAB,
  banking
});

export const giveRestFeedbackAction = (restId, feedback) => async dispatch => {
  return await RestService.giveRestFeedback(restId, feedback);
};

export const testPrinterAction = (selectedRestId, printer) => () => {
  RestService.testPrinter(selectedRestId, printer)
};

export const toggleTrendingRestFavoriteAction = (selectedRest: CustomerRest) => async (dispatch, getState: () => RootState) => {
  const newRest = await dispatch(toggleRestFavoriteAction(selectedRest, selectTrendingRestAction));

  const favoritesRest = getState().getFavorites().getSelectedRest();

  if (CustomerRestSelector.isEqual(favoritesRest, selectedRest)) {
    const favoritesSelectedCategory: CustomerCategory = getState().getFavorites().getSelectedCategory();
    dispatch(selectFavoritesRestAction(newRest));
    dispatch(selectFavoritesCategoryAction(favoritesSelectedCategory));
  }
};

export const toggleFavoritesRestFavoriteAction = (selectedRest) => async (dispatch, getState: () => RootState) => {
  const newRest = await dispatch(toggleRestFavoriteAction(selectedRest, selectFavoritesRestAction));

  const trendingRest = getState().getTrending().getSelectedRest();

  if (CustomerRestSelector.isEqual(trendingRest, selectedRest)) {
    const trendingSelectedCategory: CustomerCategory = getState().getTrending().getSelectedCategory();
    dispatch(selectTrendingRestAction(newRest));
    dispatch(selectTrendingCategoryAction(trendingSelectedCategory));
  }
};

const toggleRestFavoriteAction = (selectedRest: CustomerRest, selectRestAction: (rest: CustomerRest) => void ) => async dispatch => {
  const newRest = await RestService.toggleRestFavorite(
    selectedRest,
    optimisticRest => dispatch(selectRestAction(optimisticRest)));

  dispatch(selectRestAction(newRest));
  return newRest;
};

export const updateRestLocationAction = (restId, newLocation) => async dispatch => {
  const updatedRest = await RestService.updateRestLocation(restId, newLocation);
  dispatch(selectAccountRestAction(updatedRest));
};

export const updateRestBankingAction = (restId, newBanking) => async dispatch => {
  const updatedRest = await RestService.updateRestBanking(restId, newBanking);
  dispatch(selectAccountRestAction(updatedRest));
};

export const updateRestPrinter = (restId: string, newPrinter: UpdatePrinter) => async dispatch => {
  const updatedRest = await RestService.updateRestPrinter(restId, newPrinter);
  dispatch(selectAccountRestAction(updatedRest));
}

export const updateMinsToUpdateCart = (restId: string, mins: number) => async dispatch => {
  const updatedRest = await RestService.updateMinsToUpdateCart(restId, mins);
  dispatch(selectAccountRestAction(updatedRest));
}

export const updateRestReceiver = (restId: string, receiverId: string) => async dispatch => {
  const updatedRest = await RestService.updateRestReceiver(restId, receiverId);
  dispatch(selectAccountRestAction(updatedRest));
}

export const updateRestSubscription = (planId: string) => async (dispatch, getState: () => RootState) => {
  const updatedRest = await RestService.updateRestSubscription(
    ManagerRestSelector.getId(getState().getAccount().getSelectedRest()),
    planId
  );
  dispatch(selectAccountRestAction(updatedRest));
}

export const updateRestSubscriptionCard = (cardTok: string) => async (dispatch, getState: () => RootState) => {
  const updatedRest = await RestService.updateRestSubscriptionCard(
    ManagerRestSelector.getId(getState().getAccount().getSelectedRest()),
    cardTok
  );
  dispatch(selectAccountRestAction(updatedRest));
}

export const updateRestUrl = (restId: string, url: string) => async dispatch => {
  const updatedRest = await RestService.updateRestUrl(restId, url);
  dispatch(selectAccountRestAction(updatedRest));
}

export const updateRestTableCheckInAction = tableId => async (dispatch, getState) => {
  const updatedRest = await RestService.updateRestTableCheckIn(
    ManagerRestSelector.getId(getState().getAccount().getSelectedRest()),
    tableId
  );
  dispatch(selectAccountRestAction(updatedRest));
}

export const updateRestTableAction = (prevId, newId) => async (dispatch, getState) => {
  const updatedRest = await RestService.updateRestTable(
    ManagerRestSelector.getId(getState().getAccount().getSelectedRest()),
    prevId,
    newId,
  );
  dispatch(selectAccountRestAction(updatedRest));
}

// TODO 0: FOLLOW ERROR HANDLING AS DONE HERE EVERHWERE ELSE
export const updateRestProfileAction = (restId, newProfile) => async dispatch => {
  try {
    const updatedRest = await RestService.updateRestProfile(restId, newProfile);
    dispatch(selectAccountRestAction(updatedRest));
  } catch (e) {
    throw new Error(e); // catch and rethrow so apollo errors get a line number
  }
};
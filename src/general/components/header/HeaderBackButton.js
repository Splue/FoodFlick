import React from 'react';
import { HeaderBackButton as BackButton } from "react-navigation";
import commonColors from '../../../../native-base-theme/variables/commonColor';

export default (props) => {
  const { hide, ...rest } = props;
  return (
    hide ? null : <BackButton {...rest} tintColor={commonColors.brandCanvas} />
  )
};
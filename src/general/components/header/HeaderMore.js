import React from 'react';
import { StyleSheet } from 'react-native';
import { Icon } from 'native-base';
import commonColor from '../../../../native-base-theme/variables/commonColor';

export default HeaderMore = () => <Icon name={'md-more'} style={styles.icon} /> 

const styles = StyleSheet.create({
  icon: {
    fontSize: 35,
    color: commonColor.brandCanvas,
  }
});
import React, { ReactElement } from 'react';
import { StyleSheet } from 'react-native';
import { Text, Button } from 'native-base';
import commonColor from '../../../../native-base-theme/variables/commonColor';

type props = {
  onConfirm: (o: object) => void;
  text: string;
  icon?: ReactElement<any>
  hide?: boolean
}

export default ({ onConfirm, text, icon, hide }: props) => hide ? 
  null
  :
  (
    <Button {...{testId: 'confirm'}} onPress={onConfirm} style={styles.button}>
      {text && <Text style={styles.textColor}>{text}</Text>}
      {icon && React.cloneElement(icon, { style: styles.iconColor })}
    </Button>
  )

const styles = StyleSheet.create({
  button: {
    height: '100%',
    borderWidth: 0,
    elevation: 0,
  },
  textColor: {
    color: commonColor.toolbarTextColor,
  },
  iconColor: {
    color: commonColor.toolbarTextColor,
  }
});
import React from 'react';
import { StyleSheet, View, TouchableWithoutFeedback } from 'react-native';
import { Text, Icon } from 'native-base';
import { connect } from 'react-redux';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import { RootState } from '../../redux/RootState';
import { NavigationInjectedProps, withNavigation } from 'react-navigation';
import { cartScreenName } from '../../cart/CartScreen';
import { CartSelector } from '../../cart/CartModel';

type props = {
  count: number,
  restName: string,
};

/**
 * how to handle 2 carts? 1 in favorites and 1 in trending?
 * 
 * 
 * i can only have 1 shopping cart at a time.
 * 
 * for any screen, if i try to add an item from a different rest then have warning
 *    - are you sure you want to add this? if you add it'll clear your cart because this is a different rest
 * 
 */

const HeaderCart = ({ count, restName, navigation }: props & Partial<NavigationInjectedProps>) => (
  <TouchableWithoutFeedback onPress={() => navigation.navigate(cartScreenName, { restName })}>
    <View style={styles.cart}>
      <Icon
        type='MaterialIcons'
        name='shopping-cart'
        style={styles.icon}
      />
      <Text style={styles.text}>{count}</Text>
    </View>
  </TouchableWithoutFeedback>
);

const styles = StyleSheet.create({
  text: {
    fontSize: commonColor.fontSizeH3,
    color: commonColor.brandCanvas,
  },
  icon: {
    color: commonColor.brandCanvas,
    marginRight: commonColor.contentPadding,
  },
  cart: {
    flexDirection: 'row',
    marginRight: commonColor.contentPadding,
    alignItems: 'center',
  },
});

const mapStateToProps = (state: RootState) => ({
  count: state.getCart() ? state.getCart().items.length : 0,
  restName: CartSelector.getRestName(state.getCart()),
});

export default connect(mapStateToProps)(withNavigation(HeaderCart));

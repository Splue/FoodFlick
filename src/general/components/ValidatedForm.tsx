import React, { RefObject } from 'react';
import { Form, Toast } from 'native-base';
import ValidatedInput, { ValidatedInputProps } from './ValidatedInput';
import { NavigationInjectedProps, withNavigation } from 'react-navigation'
import { difference } from 'lodash';

// map of ValidatedInput keys (same key as validatedInputMap) to their validation error, for forcing the display of
// an error in ValidatedInput
type state = {
  [key: string]: string,
}

type modifiedValidatedInput = {
  onChangeText: (text: string) => void,
  inputRef?: any, // optional because this is only set in render
  ref: RefObject<ValidatedInput>,
}

// a map of inputs' keys (same key as validatedInputMap) to their new onChangeTexts and internal Input elements.
// Map because given a ValidatedInput child in render, we need to find its modified onChangeText and grab the
// current value (text) of the input
type modifiedValidatedInputs = {
  [key: string]: modifiedValidatedInput
}

type validatedInputMap = {
  [key: string]: ValidatedInput // key is any key as specified by caller
}

type props = {
  // navigation: NavigationScreenProp<NavigationRoute>,
  onConfirm?: (...args: any[]) => void,
  onBack?: () => void,
  originalInputs: validatedInputMap,
  render: (input: validatedInputMap) => React.ReactNode,
}

/*
 * This component is to be used for each screen that is a form. It handles some boilerplate top-level wrapping components
 * and connects the onConfirm prop to react-navigation's header. It also adds onConfirm validation for each
 * ValidatedInput received in originalInputs by preventing the call of onConfirm if any one of ValidatedInputs are
 * invalid. It does this by modifying and injecting props into each received ValidatedInput. A map of modified
 * ValidatedInput is then passed as an argument to the render callback prop.
 */
 class ValidatedForm extends React.Component <props & NavigationInjectedProps, state> {
  modifiedValidatedInputs: modifiedValidatedInputs = {};

  state = {} // initialize state, otherwise it's null and render will break

  constructor(props) {
    super(props);
    this.setModifiedValidatedInputs();
    this.wrapOnConfirm();
  }

  componentDidUpdate(prevProps) {
    const prevInputs = Object.keys(prevProps.originalInputs);
    const currInputs = Object.keys(this.props.originalInputs);
    if (difference(prevInputs, currInputs).length === 0 && difference(currInputs, prevInputs).length === 0) return;
    this.wrapOnConfirm();
  }

  wrapOnConfirm = () => {
    const {
      navigation,
      onConfirm,
      originalInputs = {},
      ...remainingProps
    } = this.props;
    if (!onConfirm) return;
    // wrap onConfirm prop such that its called only if all the ValidatedInputs are valid
    const validatedOnConfirm = async (...args) => {
      let areAllInputsValid = true;
      for (const [key, originalInput] of Object.entries(originalInputs)) {
        const props: ValidatedInputProps = originalInput.props;
        // must use inputRef value prop for current text in the input because originalInput was evaluated at didMount
        // and therefore its value props start and stay as empty strings. However, using ._lastNativeText is a hack!
        // The other way to get the input text is via ...[key].inputRef.props.value but that only works if value prop
        // is set for ValidatedInput which it shoudln't have ot be. thus we resort to the hack
        const inputText = this.modifiedValidatedInputs[key].inputRef.wrappedInstance._lastNativeText;
        const validateRequired = this.modifiedValidatedInputs[key].ref.current.validateRequired;
        const { validateEndEdit, validateEndEditAsync } = props;
        let validationError;
        if (validateEndEditAsync) {
          validationError = await validateEndEditAsync(inputText);
        } else {
          validationError = validateEndEdit ? validateEndEdit(inputText) : validateRequired(inputText);
        }
        this.setState({
          [key]: validationError,
        });
        if (validationError) areAllInputsValid = false;
      }

      if (areAllInputsValid) {
        onConfirm(...args);
      } else {
        Toast.show({ text: 'Please fix errors', buttonText: 'Okay', position: 'bottom' });
      }
    } 
    
    // We can only set the function after the component has been initialized. Copy all component props and make them
    // available in the nav screens
    navigation.setParams({
      onConfirm: validatedOnConfirm,
      ...remainingProps
    });
  }
  
  setModifiedValidatedInputs = () => {
    const {
      originalInputs = {},
    } = this.props;

    Object.entries(originalInputs).forEach(([key, originalInput]: any) => {
      // modify the existing onChangeText so we can clear the forcedValitation error when a user starts to type
      // again. otherwise, the error will stay
      this.modifiedValidatedInputs[key] = {
        onChangeText: text => {
          this.setState({
            [key]: '',
          })
          originalInput.props.onChangeText(text);
        },
        ref: React.createRef(),
      }
    });
  }

  render () {
    this.setModifiedValidatedInputs();
    
    const newInputs = {};
    Object.entries(this.props.originalInputs).forEach(([key, originalInput]: any) => {
      newInputs[key] = (
        React.cloneElement(originalInput, {
          ...originalInput.props,
          key,
          forcedValidationError: this.state[key],
          onChangeText: this.modifiedValidatedInputs[key].onChangeText,
          setRef: input => {
            this.modifiedValidatedInputs[key].inputRef = input;
          },
          ref: this.modifiedValidatedInputs[key].ref
        })
      );
    });
    return (
      <Form>
        {this.props.render(newInputs)}
      </Form>
    )
  }
}

export default withNavigation(ValidatedForm);
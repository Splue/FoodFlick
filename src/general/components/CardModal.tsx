import React from 'react';
import { Text, Right, Icon, Card, CardItem } from 'native-base';
import Modal from "react-native-modal";
import { StyleSheet } from 'react-native';
import commonColor from '../../../native-base-theme/variables/commonColor';

type props = {
  isVisible: boolean,
  title: string,
  onClose: () => void,
}

const CardModal: React.FunctionComponent<props> = ({ isVisible, title, onClose: onPressClose, children }) => (
  <Modal isVisible={isVisible} onBackdropPress={onPressClose} style={styles.modal}>
    <Card style={styles.card}>
      <CardItem header bordered style={styles.cardHeader}>
        <Text style={styles.headerText}>{title}</Text>
        <Right style={styles.closeContainer}>
          <Icon name="close" style={styles.closeIcon} onPress={onPressClose} />
        </Right>
      </CardItem>
      {children}
    </Card>
  </Modal>
)

const styles = StyleSheet.create({
  modal: {
    marginLeft: 0,
    marginRight: 0,
  },
  closeContainer: {
    flex: 1,
  },
  closeIcon: {
    color: commonColor.brandCanvas,
  },
  cardHeader: {
    backgroundColor: commonColor.brandPrimary,
  },
  headerText: {
    color: commonColor.brandCanvas,
  },
  card: {
    flex: 0.65,
  },
});

export default CardModal;

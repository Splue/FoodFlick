import React from 'react';
import {View,UIManager,findNodeHandle, StyleSheet, Platform} from 'react-native';
import { Icon, ActionSheet } from 'native-base';
import commonColor from '../../../native-base-theme/variables/commonColor';

type props = {
  actions: string[],
  onPress: (i: number) => void,
};

//stolen from https://github.com/GeekyAnts/NativeBase/issues/814
class PopupMenu extends React.Component<props> {
  handleShowPopupError = () => {
    // show error here
  };

  handleMenuPress = () => {
    const { actions, onPress } = this.props;
    if (Platform.OS === 'android') {
      UIManager.showPopupMenu(
        findNodeHandle(this.refs.menu),
        actions,
        this.handleShowPopupError,
        (itemPressed, indexPressed) => onPress(indexPressed),
      );
    } else {
      const options = [...this.props.actions, 'Cancel'];
      ActionSheet.show(
        {
          options,
          cancelButtonIndex: actions.length
        },
        onPress
      );
    }
  };

  render() {
    return (
      <View>
        <Icon
          onPress={this.handleMenuPress}
          name='more'
          style={styles.icon}
          ref='menu'
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  icon: {
    color: commonColor.brandInfo,
    fontSize: 24,
    // to give the icon more area for pressing
    marginRight: -16,
    paddingLeft: 16,
    paddingRight: 16,
  },
});

export default PopupMenu;
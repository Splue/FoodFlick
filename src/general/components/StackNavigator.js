import React from 'react';
import { StyleSheet, Platform } from 'react-native';
import { connect } from 'react-redux';
import { StackRouter, createNavigator, CardStackTransitioner } from "react-navigation";
import commonColor from '../../../native-base-theme/variables/commonColor';

const StackView = ({ dispatch, nav, ...extraProps }) => <CardStackTransitioner {...extraProps} />

const StackNavigator = (initialRoute, mapStateToProps, screens) => {
  const StackViewWithRedux = connect(mapStateToProps)(StackView);

  const router = StackRouter(screens, {
    initialRouteName: initialRoute,
    screens,
    navigationOptions: {
      headerStyle: styles.header,
      headerTitleStyle: styles.title,
      headerTintColor: commonColor.toolbarTextColor
    }
  });

  return createNavigator(router)(StackViewWithRedux);
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: commonColor.brandBase,
    elevation: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -Expo.Constants.statusBarHeight
  },
  title: {
    fontSize: 18,
    fontWeight: '500',
    color: commonColor.toolbarTextColor,
  },
});

export default StackNavigator;
import React from 'react';
import { StyleSheet } from 'react-native';
import { H3 } from 'native-base';

type props = {
  text: string,
  style?: Object,
}

export default ({ text, style }: props) => (
  <H3 style={[styles.header, style]}>{text}</H3>
)

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: 10,
    paddingVertical: 30,
    textAlign: 'center'
  }
});
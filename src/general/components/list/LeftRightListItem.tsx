import { Text, ListItem, Left, Right } from 'native-base';
import React from 'react';
import { StyleSheet, View } from 'react-native';

type props = {
  leftText: string,
  leftNote?: string,
  right: React.ReactNode,
  onPress?: () => void,
};

const LeftRightListItem: React.FunctionComponent<props> = ({ leftText, leftNote, right, onPress }) => (
  <ListItem button onPress={onPress}>
    <Left>
      <View style={styles.column}>
        <Text style={styles.left}>{leftText}</Text>
        {!!leftNote && <Text note style={styles.left}>{leftNote}</Text>}
      </View>
    </Left>
    <Right style={styles.listRight}>
      {right}
    </Right>
  </ListItem>
);


const styles = StyleSheet.create({
  column: {
    flexDirection: 'column',
  },
  left: {
    marginLeft: 0,
    alignSelf: 'flex-start',
  },
  listRight: {
    flex: 1,
  },
});

export default LeftRightListItem;
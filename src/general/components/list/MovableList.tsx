import React from 'react';
import SortableList from 'react-native-sortable-list';

type props = {
  data?: object,
  list?: any[],
  listRef?: (ref: React.Ref<SortableList>) => void,
  onReleaseRow?: () => void
  onActivateRow?: () => void,
  renderHeader?: () => void,
  renderItem: (data: any, key: string, index: number) => React.ReactElement<any>,
  updateOrder: (newOrder: number[]) => void,
  scrollEnabled?: boolean,
}

class MovableList extends React.Component<props> {
  onReleaseRow = (key, changesToOriginalOrder) => {
    this.props.updateOrder(changesToOriginalOrder.map(i => Number(i)));
    if (this.props.onReleaseRow) this.props.onReleaseRow();
  }

  renderRow = ({ key, active, index, data}) => {
    // key = original index
    // index = new moved index
    const renderedItem = this.props.renderItem(data, key, index);
    return React.cloneElement(renderedItem, { data, active });
  }

  render () {
    const { list, data, listRef, ...rest } = this.props;
    return (
      <SortableList
        {...rest}
        manuallyActivateRows
        data={list || data}
        onReleaseRow={this.onReleaseRow}
        renderRow={this.renderRow}
        ref={listRef}
      />
    )
  }
}

export default MovableList;
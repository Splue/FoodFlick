import React from 'react';
import { StyleSheet } from 'react-native';
import { List, Text, Icon, ListItem, Left, Right, Body } from 'native-base';

export default ({items, ...remainingProps}) => (
  <List {...remainingProps}>
    {items.map((item, index) => (
      <ListItem icon last noBorder style={styles.item} {...item.props} key={index}>
        {item.leftIcon && <Left><Icon name={item.leftIcon} onPress={item.onIconPress}/></Left>}
        <Body>
          <Text>{item.text}</Text>
        </Body>
        {item.rightIcon && <Right><Icon name={item.rightIcon} onPress={item.onIconPress}/></Right>}
      </ListItem>
    ))}
  </List>
)

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'transparent'
  }
});
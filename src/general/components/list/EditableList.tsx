import { Text, Icon, Content, List, ListItem } from 'native-base';
import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity  } from 'react-native';
import commonColor from '../../../../native-base-theme/variables/commonColor';

type editableItem = {
  icon: string,
  text: string,
  onPress: (data, index, clearActiveItem: () => void) => void,
}

type props = {
  list: any[];
  editableItems: editableItem[],
  renderItem: (data: any, index: number, setActive: () => void) => React.ReactNode,
};

const EditableList: React.FunctionComponent<props> = ({ list, editableItems, renderItem }) => {
  const [activeIndex, setActiveIndex] = useState<number>(null);
  const clearActiveItem = () => setActiveIndex(null);
  return (
    <Content style={styles.content}>
      <List>
        {list.map((data, index) => activeIndex === index ?
          (
            <ListItem key={`edit-${index}`} style={styles.editableListItem}>
              <TouchableOpacity style={styles.editAction} onPress={clearActiveItem}>
                <Icon name='arrow-back' style={styles.editText} />
                <Text style={styles.editText}>Back</Text>
              </TouchableOpacity>
              {editableItems.map(({ icon, text, onPress }, editableItemIndex) => (
                <TouchableOpacity
                  key={editableItemIndex}
                  style={styles.editAction}
                  onPress={() => onPress(data, index, clearActiveItem)
                }>
                  <Icon name={icon} style={styles.editText} />
                  <Text style={styles.editText}>{text}</Text>
                </TouchableOpacity>
              ))}
            </ListItem>
          )
          : 
          renderItem(data, index, () => setActiveIndex(index))
        )}
      </List>
    </Content>
  )
}

const styles = StyleSheet.create({
  editableListItem: {
    marginLeft: 0,
    paddingRight: 0,
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  },
  editText: {
    textAlign: 'center',
  },
  editAction: {
    flex: 1,
  },
  column: {
    flexDirection: 'column',
  },
  left: {
    marginLeft: 0,
    alignSelf: 'flex-start',
  },
  content: {
    backgroundColor: commonColor.brandCanvas,
  },
  listRight: {
    flex: 1,
  },
});

export default EditableList;
import React from 'react';
import { Animated, Easing, TouchableOpacity, Platform, StyleSheet } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { ListItem, Left, Icon, Right, Button, Body } from 'native-base';
import commonColor from '../../../../native-base-theme/variables/commonColor';

class MovableRow extends React.Component {
  constructor(props) {
    super(props);
    this._active = new Animated.Value(0);
    this._style = {
      transform: [{
        scale: this._active.interpolate({
          inputRange: [0, 1],
          outputRange: [1, 1.09],
        }),
      }],
      opacity: this._active.interpolate({
        inputRange: [0, 1],
        outputRange: [1, .7],
      }),
      ...Platform.select({
        ios: {
          shadowRadius: this._active.interpolate({
            inputRange: [0, 1],
            outputRange: [2, 10],
          }),
        },
        android: {
          elevation: this._active.interpolate({
            inputRange: [0, 1],
            outputRange: [4, 8],
          }),
        },
      })
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.active !== nextProps.active) {
      Animated.timing(this._active, {
        duration: 300,
        easing: Easing.bounce,
        toValue: Number(nextProps.active),
      }).start();
    }
  }

  render() {
    // toggleRowActive comes magically from MovableList
    const { data, toggleRowActive, onPressData, onDeleteData, noRightBottomBorder, bodyStyle } = this.props;
    const rightStyle = {};
    if (noRightBottomBorder) rightStyle.borderBottomWidth = 0;

    return (
      <Animated.View style={this._style}>
        <ListItem icon>
          {/* need touchable feedback because Icon and Body do not accept on press */}
          <Left>
            <TouchableOpacity onPressIn={toggleRowActive}>
              <MaterialIcons name='swap-vert' color={commonColor.brandInfo} size={30} />
            </TouchableOpacity>
          </Left>
          <Body style={bodyStyle}>
            <TouchableOpacity onPress={() => onPressData(data)}>
              {this.props.children}
            </TouchableOpacity>
          </Body>
          <Right style={rightStyle}>
            <TouchableOpacity style={styles.trash} onPressIn={() => onDeleteData(data)}>
              <Icon name='trash' />
            </TouchableOpacity>
          </Right>
        </ListItem>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  trash: {
    padding: 5,
  }
})

export default MovableRow;
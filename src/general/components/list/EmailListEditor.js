import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Button, Container, Content, Text, H3 } from 'native-base';
import UndividedList from './UndividedList';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import CenteredH3 from '../CenteredH3';

//we have this component in case we allow users to have primary and other (secondary) emails. if that time comes, then
//we need to use this component to edit rest managers and user accunt emails
const EmailListEditor = ({users, addButtonText, onDelete, onAdd, emptyEmailsText}) => (
  <Container style={styles.container}>
    <Content>
      <View style={styles.header}>
        <H3>Emails</H3>
        <Button small onPress={onAdd}>
          <Text>{addButtonText}</Text>
        </Button>    
      </View>
      <UndividedList items={users.map(({ userId, email }) => ({
        text: email,
        rightIcon:'trash',
        onIconPress: () => onDelete(userId)
      }))} />
      {users.length === 0 && emptyEmailsText && <CenteredH3 text={emptyEmailsText} />}
      {/* <Divider leftText='Primary email' color={commonColor.brandFaintAccent} />
      <Divider leftText='Alternate emails' color={commonColor.brandFaintAccent} /> */}
    </Content>
  </Container>
);

const styles = StyleSheet.create({
  container: {
    backgroundColor: commonColor.brandCanvas,
    padding: commonColor.contentPadding,
  },
  header: {
    paddingBottom: commonColor.contentPadding + 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  }
})

export default EmailListEditor;

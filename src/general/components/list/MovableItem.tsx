import React from 'react';
import { Animated, Easing, TouchableWithoutFeedback, Platform } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import commonColor from '../../../../native-base-theme/variables/commonColor';

type props = {
  renderItem: (swapper: React.ReactNode) => React.ReactNode,
  toggleRowActive?: () => void // magically comes from SortableList
  active?: boolean, // comes from movable list
}

class MovableItem extends React.Component<props> {
  _active = null;
  _style = null;

  constructor(props) {
    super(props);
    this._active = new Animated.Value(0);
    this._style = {
      transform: [{
        scale: this._active.interpolate({
          inputRange: [0, 1],
          outputRange: [1, 1.09],
        }),
      }],
      opacity: this._active.interpolate({
        inputRange: [0, 1],
        outputRange: [1, .7],
      }),
      ...Platform.select({
        ios: {
          shadowRadius: this._active.interpolate({
            inputRange: [0, 1],
            outputRange: [2, 10],
          }),
        },
        android: {
          elevation: this._active.interpolate({
            inputRange: [0, 1],
            outputRange: [4, 8],
          }),
        },
      })
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.active !== nextProps.active) {
      Animated.timing(this._active, {
        duration: 300,
        easing: Easing.bounce,
        toValue: Number(nextProps.active),
      }).start();
    }
  }

  render() {
    const { toggleRowActive } = this.props;
    return (
      <Animated.View style={this._style}>
        {this.props.renderItem(
          <TouchableWithoutFeedback onPressIn={toggleRowActive}>
            <MaterialIcons name='swap-vert' color={commonColor.brandInfo} size={30} />
          </TouchableWithoutFeedback>
        )}
      </Animated.View>
    );
  }
}

export default MovableItem;
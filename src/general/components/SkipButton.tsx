import React from 'react';
import { StyleSheet, GestureResponderEvent } from 'react-native';
import { Text, Button, Form, Content } from 'native-base';

type props = {
  onPress: (event: GestureResponderEvent) => void;
  style: any
}

export default ({ onPress, style }: props) => (
  <Button style={{ ...styles.content, ...style }} transparent info onPress={onPress}>
    <Text>Skip</Text>
  </Button>
)

const styles = {
  content: {
    alignSelf: 'center',
    paddingTop: 30 
  }
};
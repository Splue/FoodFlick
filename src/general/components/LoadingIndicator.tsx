import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, Text } from 'react-native';
import commonColor from '../../../native-base-theme/variables/commonColor';
import { RootState } from '../redux/RootState';
import { Spinner } from 'native-base';
import CenteredH3 from './CenteredH3';

const LoadingIndicator = ({ loading }) => {
  return loading.isLoading ?
  (
    <View style={styles.loading}>
      {loading.msg && <CenteredH3 text={loading.msg} />}
      <Spinner color={commonColor.brandPrimary} />
    </View>
  )
  : null;
}

const styles = StyleSheet.create({
  loading: {
    position: 'absolute',
    backgroundColor: commonColor.brandCanvas,
    opacity: 0.75,
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  }
});

const mapStateToProps = (state: RootState) => ({
  loading: state.getUi().loading,
});

export default connect(mapStateToProps)(LoadingIndicator);

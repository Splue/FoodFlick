import React from 'react';
import { StyleSheet } from 'react-native';
import { Card, CardItem, Text, Icon } from 'native-base';

const renderItem = (item, index) => {
  return item ? <CardItem key={index}><Text>{item.toString()}</Text></CardItem> : null;
}

const renderItems = (items, emptyItemsText) => {
  const cardItems = items && items.length > 0 ? items.map(renderItem) : null;
  return cardItems ? cardItems : emptyItemsText && <CardItem><Text>{emptyItemsText}</Text></CardItem>;
}

export default ({ title, icon, items, onIconPress, emptyItemsText }) => (
  <Card>
    <CardItem header style={styles.header}>
      <Text>{title}</Text>
      <Icon name={icon} onPress={onIconPress} />
    </CardItem>
    {renderItems(items, emptyItemsText)}
  </Card>
)

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
})
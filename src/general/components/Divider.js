import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Text } from 'native-base';
import commonColor from '../../../native-base-theme/variables/commonColor';

export default Divider = ({leftText, centerText, rightText, color}) => {
  const textColor = {color: color || commonColor.brandFaint};
  const lineColor = {backgroundColor: color || commonColor.brandFaint};
  const position = leftText || centerText || rightText;
  const margin = 8;
  let margins;

  switch (position) {
    case leftText:
      margins = {marginRight: margin};
      break;
    case centerText:
      margins = {marginHorizontal: margin};
      break;
    case rightText:
      margins = {marginLeft: margin};
      break;
  }

  return (
    <View style={styles.view}>
      {leftText && <Text style={[margins, textColor]}>{leftText}</Text>}
      {(leftText || centerText) && <View style={[styles.line, lineColor]} />}
      {centerText && <Text style={[margins, textColor]}>{centerText}</Text>}  
      {(rightText || centerText) && <View style={[styles.line, lineColor]} />}
      {rightText && <Text style={[margins, textColor]}>{rightText}</Text>}    
    </View>
  )
}

const styles = StyleSheet.create({
  view: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8
  },
  line: {
    flex: 1,
    height: 1,
    backgroundColor: commonColor.brandFaint
  },
})

import React from 'react';
import { FlatList, Dimensions } from 'react-native';
import { Text } from 'native-base';
import FlickTile from './FlickTile';
import CenteredH3 from '../CenteredH3';
import { manipulateAsync, SaveFormat } from 'expo-image-manipulator';
const MAX_WIDTH = 600;

type state = {
  flickSize: number;
}

type flickSelection = {
  // key is the index of the photo in the camera roll and value is the photo's uri
  [key: number]: string
}

type image = {
  uri: string;
  height?: number; // undefined is image is a firebase image
  width?: number; // undefined if image is a firebase iamge
}

type props = {
  selected: flickSelection;
  updateSelected: (newSelected: flickSelection) => void;
  canSelectMultiple: boolean;
  numColumns: number;
  flicks: image[];
  loading: boolean;
  listEmptyText: string;
  onEndReached: () => Promise<void>;
}

class FlickPicker extends React.Component<props, state> {

  state = {
    flickSize: undefined, // set in getDerivedStateFromProps
  }

  static getDerivedStateFromProps(props, state) {
    return {
      flickSize: Dimensions.get('window').width / props.numColumns,
    }
  }

  selectImage = async (index: number, { uri, height, width }: image) => {
    let newUri = uri;
    const prevSelected = this.props.selected[index];
    const newSelected = this.props.canSelectMultiple ? { ...this.props.selected } : {};
    if (prevSelected) {
      delete newSelected[index]
    } else {
      newSelected[index] = newUri;
      if (width && width > MAX_WIDTH) {
        manipulateAsync(
          uri,
          [{ resize: { width: MAX_WIDTH }}],
          { format: SaveFormat.PNG }
        ).then(res => {
          newUri = res.uri;
          const newSelected = this.props.canSelectMultiple ? { ...this.props.selected } : {};
          newSelected[index] = newUri;
          this.props.updateSelected(newSelected);
        })
      }
    }
    this.props.updateSelected(newSelected);    
  }

  getItemLayout = (data, index) => ({ 
    length: this.state.flickSize, //height of your row 
    offset: this.state.flickSize * index, //the distance from the top of the first row to the top of the row at index index; 
    index //index of row
  })

  //use FlickTile instead of touchableHighlight directly because we need to render a pure component as required by flatlist,
  //and because pure component is faster. item = element in this.props.flicks (url / uri) and index is position in flatlist
  renderFlickTile = ({ item, index }) => (
    <FlickTile image={item} index={index} selected={this.props.selected[index]} onPress={this.selectImage}
    size={this.state.flickSize} />
  )

  render() {

    if (this.props.loading) return <Text>Loading...</Text>

    return (
      <FlatList
        onEndReachedThreshold={0.75}
        initialNumToRender={24} 
        data={this.props.flicks}
        numColumns={this.props.numColumns}
        renderItem={this.renderFlickTile}
        keyExtractor={(uri, index) => index.toString()}
        onEndReached={this.props.onEndReached}
        ListEmptyComponent={<CenteredH3 text={this.props.listEmptyText} />}
        getItemLayout={this.getItemLayout}
      />
    );
  }
}

export default FlickPicker;
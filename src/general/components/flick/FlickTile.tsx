import React from 'react';
import { Image, TouchableHighlight } from 'react-native';

type flickSelection = {
  // key is the index of the photo in the camera roll and value is the photo's uri
  [key: number]: string
}

type image = {
  uri: string;
  height: number;
  width: number;
}

type props = {
  image: image;
  index: number;
  selected: flickSelection;
  onPress: (index: number, image: image) => void;
  size: number;
}

// use pure component to prevent rerenders when passed the same image uri
export default class FlickTile extends React.PureComponent<props, any> {
  render() {
    const { image, index, selected, onPress, size } = this.props;
    if (!image) return null;
    return (
      <TouchableHighlight style={{opacity: selected ? 0.30 : 1}} onPress={() => onPress(index, image)}>
        <Image style={{width: size, height: size}} source={{uri: image.uri}} />
      </TouchableHighlight>
    )
  }
}
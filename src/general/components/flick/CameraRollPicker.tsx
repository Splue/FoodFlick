import React from 'react';
import { askAsync, CAMERA_ROLL } from 'expo-permissions';
import { CameraRoll, Platform } from 'react-native';
import SkipButton from '../SkipButton';
import FlickPicker from './FlickPicker';

type image = {
  uri: string;
  height: number;
  width: number;
}

type state = {
  // flicks: string[]; // string of photo uris in loaded by the camera roll
  flicks: image[];
  // the camera roll's last photo moving pointer. advances to the "next last" when user reaches page end
  lastEndCursor: string;
  hasNextPage: boolean;
  pendingPermission: boolean;
  isLoading: boolean;
}

type flickSelection = {
  // key is the index of the photo in the camera roll and value is the photo's uri
  [key: number]: string
}

type props = {
  onSkip?: () => void;
  selected: flickSelection;
  updateSelected: (newSelected: flickSelection) => void;
  onCameraPermissionGranted: () => void;
  canSelectMultiple: boolean;
  numColumns: number;
}

export default class CameraRollPicker extends React.Component<props, state> {

  state = {
    flicks: [],
    lastEndCursor: null,
    hasNextPage: true,
    pendingPermission: true,
    isLoading: true,
  }

  async componentDidMount() {
    const { status } = await askAsync(CAMERA_ROLL);
    if (status === 'granted') {
      this.setState({ pendingPermission: false })
      this.getPhotos();
      this.props.onCameraPermissionGranted()
    }
  }

  getPhotos = async () => {
    let params: any = { first: 50, assetType: 'Photos' };
    if (Platform.OS === 'ios') params.groupTypes = 'All'
    if (this.state.lastEndCursor) params.after = this.state.lastEndCursor
    if (!this.state.hasNextPage) return

    const res = await CameraRoll.getPhotos(params);

    //not sure why this would happen. but keeping here because the dude i stole it from had it
    // if (this.state.lastEndCursor === res.page_info.end_cursor) {
    //   console.log('lastEndCursur === pageinfo end cursor')
    //   return;
    // }

    const flicks = res.edges.map(item => item.node).map(item => item.image).map(image => image);

    this.setState({
      flicks: [...this.state.flicks, ...flicks],
      lastEndCursor: res.page_info.end_cursor,
      hasNextPage: res.page_info.has_next_page,
      isLoading: false,
    });
  }

  render() {
    const { pendingPermission, isLoading } = this.state;
    return (
      <React.Fragment>
        <FlickPicker
          {...this.props}
          loading={pendingPermission || isLoading}
          listEmptyText='No flicks in your camera'
          flicks={this.state.flicks}
          onEndReached={this.getPhotos}
        />
        {this.props.onSkip && <SkipButton {...{ testId: 'skipButton' }} onPress={this.props.onSkip} />}
      </React.Fragment>
    );
  }
}
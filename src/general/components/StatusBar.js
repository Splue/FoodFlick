import React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';

export default () => {
  return Platform.OS === 'ios' ? <StatusBar barStyle="default" /> : <View style={styles.statusBarUnderlay} />
}

const styles = StyleSheet.create({
  statusBarUnderlay: {
    height: 24,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
});

import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View } from 'react-native';
import { Container, Content, CardItem, Icon, Text, Toast, Card, Right, Button } from 'native-base';
import MenuOverview from '../menu/MenuOverview';
import commonColor from '../../../native-base-theme/variables/commonColor';
import { NavScreenOptions } from '../navModels/NavScreenOptions';
import { CustomerRestSelector, CustomerRest } from '../rest/CustomerRest';
import { NavigationScreenProp, NavigationRoute } from 'react-navigation';
import { CustomerCategory, CustomerItem } from '../menu/CustomerMenu';
import HeaderCart from './header/HeaderCart';
import { addItemToCartAction, resetCartAction } from '../cart/redux/cartActions';
import { optionGroup, Price, option } from '../menu/BaseMenu';
import { RootState } from '../redux/RootState';
import { CartSelector } from '../cart/CartModel';
import CardModal from './CardModal';
import CartItemPriceAndOptionsEditor from '../cart/components/CartItemPriceAndOptionsEditor';

type props = {
  navigation: NavigationScreenProp<NavigationRoute>;
  selectCategory: (category: CustomerCategory) => any;
  selectedRest: CustomerRest
  toggleItemLike: (selectedRest: CustomerRest, category: CustomerCategory, itemName: string) => void;
  toggleRestFavorite: (selectedRest: CustomerRest) => void;
  // redux
  cartRestId?: string,
  cartRestName?: string,
  resetCart?: () => void,
  addItemToCart?: (item: CustomerItem, itemIndex: number, categoryIndex: number, price?: Price, option?: option[]) => void,
}

type state = {
  isModalVisible: boolean,
  optionGroups: optionGroup[],
  prices: Price[],
  mightReplaceCart: boolean,
}

const defaultState = {
  isModalVisible: false,
  currOptionGroupIndex: null  as number,
  optionGroups: null as optionGroup[],
  prices: null as Price[],
  mightReplaceCart: false,
}

class MenuBrowser extends React.Component<props, state> {
  static navigationOptions = ({ navigation: { state: { params = {}}, navigate }}: NavScreenOptions) => {
    const selectedRest: CustomerRest = params.selectedRest;
    return {
      title: CustomerRestSelector.getName(selectedRest),
      headerRight: (
        <React.Fragment>
          <Icon
            type='MaterialIcons'
            style={styles.bookmarkIcon} 
            name={CustomerRestSelector.isFavorite(selectedRest) ? 'bookmark' : 'bookmark-border'}
            onPress={() => params.toggleRestFavorite(selectedRest)}
          />
          <HeaderCart />
        </React.Fragment>
      )
    }
  };

  state = defaultState;
  selectedItem: CustomerItem = null;
  selectedItemIndex: number = null;
  selectedCategoryIndex: number = null;

  componentDidMount () {
    this.props.navigation.setParams({ 
      toggleRestFavorite: this.props.toggleRestFavorite,
    });
  }

  onPressItem = (item: CustomerItem, itemIndex: number, categoryIndex: number) => {
    const optionGroups = item.optionGroups;
    const prices = item.prices;
    const currRestId = CustomerRestSelector.getId(this.props.selectedRest);
    const mightReplaceCart = !!this.props.cartRestId && this.props.cartRestId !== currRestId;
    if (!mightReplaceCart && optionGroups.length === 0 && prices.length === 1) {
      this.props.addItemToCart(item, itemIndex, categoryIndex);
      this.setState(prevState => {
        if (prevState.isModalVisible) {
          return {
            isModalVisible: false,
            mightReplaceCart: false,
          };
        }
        return null;
      });
      return;
    }
    this.selectedItem = item;
    this.selectedItemIndex = itemIndex;
    this.selectedCategoryIndex = categoryIndex;
    this.setState({
      isModalVisible: true,
      mightReplaceCart,
      optionGroups,
      prices,
    });
  };

  onFinalSelection = (price: Price, options: option[]) => {
    this.props.addItemToCart(this.selectedItem, this.selectedItemIndex, this.selectedCategoryIndex, price, options);
    this.resetLocalCart();
  }

  resetLocalCart = () => {
    this.setState(defaultState);
    this.selectedItem = null;
    this.selectedItemIndex = null;
    this.selectedCategoryIndex = null;
  }

  confirmReplaceCart = () => {
    this.props.resetCart();
    this.setState({
      mightReplaceCart: false
    }, () => this.onPressItem(this.selectedItem, this.selectedItemIndex, this.selectedCategoryIndex))
  }

  renderActionBar = (category: CustomerCategory, item) => (
    <CardItem style={styles.thinPadding}>
      <Icon type='MaterialIcons' name={item.likes.hasLiked ? 'favorite' : 'favorite-border'}
      onPress={() => this.props.toggleItemLike(this.props.selectedRest, category, item.name)} />
      <Text>{item.likes.count}</Text>
    </CardItem>
  )

  renderCategory = category => <Content>{category}</Content>

  renderMenu = tabs => <React.Fragment>{tabs}</React.Fragment>

  // assumes the menu already has items, otherwise we dont show this screen anyway
  render () {
    const { optionGroups, isModalVisible, prices, mightReplaceCart } = this.state;
    let headerMsg = 'Choose something';
    let mightReplaceCartConfirmation;
    if (mightReplaceCart) {
      headerMsg = 'Are you sure?';
      mightReplaceCartConfirmation = (
        <React.Fragment>
          <CardItem>
            <Text>
              You already have items in your cart from {this.props.cartRestName}.
               Adding items from {CustomerRestSelector.getName(this.props.selectedRest)} will reset your cart.
               Are you sure you want to
               reset your cart?
            </Text>
          </CardItem>
          <CardItem style={styles.clearCartButtons}>
            <Button block onPress={this.resetLocalCart}><Text>No, keep cart</Text></Button>
            <Button block bordered onPress={this.confirmReplaceCart}><Text>Yes, reset cart</Text></Button>
          </CardItem>
        </React.Fragment>
      );
    }
    return (
      <Container>
        <CardModal isVisible={isModalVisible} onClose={this.resetLocalCart} title={headerMsg}>
          {mightReplaceCartConfirmation ||
            <CartItemPriceAndOptionsEditor prices={prices} optionGroups={optionGroups} onFinalSelection={this.onFinalSelection}/>}
        </CardModal>
        {/* wrap renderCategory in fn otherwise, it ONLY gets called on mount and we need it called/rendered on scroll */}
        <MenuOverview
          canLeaveFeedback
          menu={this.props.selectedRest.menu}
          profile={this.props.selectedRest.profile}
          location={this.props.selectedRest.location}
          renderActionBar={this.renderActionBar}
          renderCategory={category => this.renderCategory(category)}
          renderMenu={this.renderMenu}
          onPressItem={this.onPressItem}
          selectCategory={this.props.selectCategory}
        />
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  right: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    flex: 1
  },
  bookmarkIcon: {
    color: commonColor.brandCanvas,
    marginRight: commonColor.contentPadding,
  },
  priceLabelContainer: {
    flex: 0.5,
    alignItems: 'center',
  },
  thinPadding: {
    // must use paddingTop and bottom instead of vertical to override default styles
    paddingTop: commonColor.listItemThinPadding,
    paddingBottom: commonColor.listItemThinPadding
  },
  clearCartButtons: {
    flexDirection: 'column',
  },
  choice: {
    fontSize: commonColor.fontSizeBase * 1.2,
  },
});

const mapStateToProps = (state: RootState) => ({
  cartRestId: CartSelector.getRestId(state.getCart()),
  cartRestName: CartSelector.getRestName(state.getCart()),
});

const mapDispatchToProps = (dispatch, { selectedRest }: props) => ({
  addItemToCart: (item: CustomerItem, itemIndex: number, categoryIndex: number, price: Price, options?: option[]) => {
    dispatch(addItemToCartAction(selectedRest, item, itemIndex, categoryIndex, price, options));
    Toast.show({ text: 'Added to cart', buttonText: 'Okay', position: 'bottom' });
  },
  resetCart: () => {
    dispatch(resetCartAction());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(MenuBrowser);
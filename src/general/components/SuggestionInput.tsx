import React from 'react';
import { debounce, throttle } from 'lodash';

type props = {
  /**
   * Render fn for customizing the render of SuggestionInput. Fn accepts as the first argument the controlled suggestion
   * fetch fn. The controlled suggestion fetch fn is the passed in updateSearchSuggestions that gets throttled or
   * debounced.
   * @param controlledSuggestionFetch the controlled updateSearchSuggestions
   */
  renderInput(
    controlledSuggestionFetch: (newQuery: string) => void
  ): React.ReactNode,

  /**
   * The fn that updates the suggestion texts, given a new query
   * @param newQuery 
   */
  updateSearchSuggestions(newQuery: string): void,
}

/**
 * A wrapper around native base inputs to throttle + debounce suggestion fetches on input change
 */
class SuggestionInput extends React.Component<props, any> {
  // todo 1: do the "next level stuff" from
  // https://www.peterbe.com/plog/how-to-throttle-and-debounce-an-autocomplete-input-in-react
  controlledSuggestionFetch = newQuery => {
    if (newQuery.length < 3) {
      this.throttledUpdateSearchSuggestions(newQuery);
    } else {
      this.debouncedUpdateSearchSuggestions(newQuery);
    }
  }

  throttledUpdateSearchSuggestions = throttle(this.props.updateSearchSuggestions, 500)
  debouncedUpdateSearchSuggestions = debounce(this.props.updateSearchSuggestions, 500)

  render () {
    return this.props.renderInput(this.controlledSuggestionFetch);
  }
}

export default SuggestionInput;
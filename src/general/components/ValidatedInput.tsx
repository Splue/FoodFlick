import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Item, Label, Input, Text, NativeBase } from 'native-base';
import commonColor from '../../../native-base-theme/variables/commonColor';

// todo 0: add unit tests for all the validators

export interface ValidatedInputProps extends NativeBase.Item {
  /**
   * Validate the input onTextChange and return the validation error message, empty string if there are no errors.
   *
   * @param text - The text to be validated. This is the raw input value, not related to this.props.value 
   * @returns Validation error message, empty string if there are no errors
   */
  validateTextChange?: (text: string) => string,
  /**
   * Validate the input onEndEditing and return the validation error message, empty string if there are no errors.
   *
   * @param text - The text to be validated
   * @returns Validation error message, empty string if there are no errors
   */
  validateEndEdit?: (text: string) => string,
  /**
   * Validate the input onEndEditing and return the promisified validation error message, empty string if there are no
   * errors. To be used over validateEndEdit when validation requires an async call.
   *
   * @param text - The text to be validated
   * @returns A promise containing Validation error message, empty string if there are no errors
   */
  validateEndEditAsync?: (text: string) => Promise<string>,
  /**
   * Props passed to the underlying NativeBase Input
   */
  inputProps?: NativeBase.Input,
  labelProps?: NativeBase.Label,
  onChangeText: (text: string) => void,
  /**
   * The input's label. Also used for required validation message if requiredLabel is not specified.
   */
  label?: string,
  /**
   * Boolean to determine if the input is allowed to be empty. True for required and false for optional where required
   * inputs cannot be empty. If true then an empty input onEndEdit will produce an error message
   * `${label} cannot be empty` where label is the value of the label prop. If this prop is specified along with 
   * validateEndEdit then validateEndEdit will take priority.
   */
  required?: boolean,
  /**
   * Label for the required validation message. If specified, then overrides the default use of label prop.
   */
  requiredLabel?: string,
  /**
   * The value displayed in this input. Setting this makes the input a "controlled component"
   */
  value?: string,
  /**
   * An error string to be forcefully displayed. Setting this prop will force ValidatedInput to display the error string
   * and ignore any internal state.
   */
  forcedValidationError?: string,
  /**
   * access to the input node
   */
  setRef?: any, // any instead of function because of nativebase typing errors

  style?: object,
  multiline?: boolean
  numberOfLines?: number
}

type state = {
  validationError: string,
}

/**
 * A wrapper around native base inputs to add styles and validation. Unnamed props are passed through to the item
 * wrapper.
 */
class ValidatedInput extends React.Component<ValidatedInputProps, state> {
  state = {
    validationError: '',
  }

  validatedOnChangeText = text => {
    const { validateTextChange, onChangeText } = this.props;

    this.setState({
      validationError: validateTextChange ? validateTextChange(text) : '',
    });

    onChangeText(text);
  }

  validateRequired = (
    text => this.props.required && (!text || text.length === 0)
    ? `${this.props.requiredLabel || this.props.label} cannot be empty`
    : ''
  )

  validatedOnEndEditing = async ({ nativeEvent: { text }}) => {
    const { validateEndEditAsync, validateEndEdit } = this.props;
    
    if (validateEndEditAsync) {
      const validationError = await validateEndEditAsync(text);
      this.setState({ validationError });
      return;
    }

    this.setState({
      validationError: validateEndEdit ? validateEndEdit(text) : this.validateRequired(text),
    });
  }

  renderError = () => {
    const error = this.props.forcedValidationError || this.state.validationError || null;
    return error ? <Text style={styles.error}>{error}</Text> : null
  }

  render () {
    const {
      value,
      label,
      onChangeText,
      forcedValidationError,
      setRef,
      inputProps,
      style,
      labelProps,
      multiline,
      numberOfLines,
      ...itemProps
    } = this.props;
    
    const isFloatingLabel = !!itemProps.floatingLabel;

    return (
      <View style={{ alignSelf: 'stretch', ...style }}>
        {/* View is necessary so that error always renders below Item, regardless of ValidatedInput's parent */}
        <Item {...itemProps} error={!!forcedValidationError || !!this.state.validationError}>
          <Label {...labelProps}>{label}</Label>
          <Input
            value={value}
            onChangeText={this.validatedOnChangeText}
            onEndEditing={this.validatedOnEndEditing}
            // getRef is only for floatingLabels https://github.com/GeekyAnts/NativeBase/issues/2591
            getRef={isFloatingLabel ? this.props.setRef : undefined}
            ref={isFloatingLabel ? undefined : this.props.setRef}
            multiline={multiline}
            numberOfLines={numberOfLines} {...inputProps}
          />
        </Item>
        {this.renderError()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  error: {
    marginLeft: commonColor.formInputMarginLeft,
    color: commonColor.brandDanger,
  },
});

export default ValidatedInput;
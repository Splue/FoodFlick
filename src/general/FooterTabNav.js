import React from 'react';
import { Footer, FooterTab, Button, Icon, Text } from 'native-base';
import { favoritesScreenName } from '../favorites/FavoritesScreen';

export default FooterTabNav = ({ navigation }) => (
  <Footer>
    <FooterTab>
      <Button vertical onPress={() => navigation.navigate('trendingNavigator')}>
        <Icon name='home' />
        <Text>Home</Text>
      </Button>
      <Button vertical onPress={() => navigation.navigate('favoritesNavigator')}>
        <Icon name='bookmarks' />
        <Text>Favorites</Text>
      </Button>
      <Button vertical onPress={() => navigation.navigate('accountNavigator')}>
        <Icon name='person' />
        <Text>Me</Text>
      </Button>
    </FooterTab>
  </Footer>
)

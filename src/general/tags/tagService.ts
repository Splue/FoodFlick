import getApolloClient from '../../apolloClient';
import gql from 'graphql-tag';
import { tagFragment } from './tagFragments';
import { Tag } from './Tag';

const tagSearchSuggestionsQuery = gql`
  query tagSearchSuggestions($query: String!) {
    tagSearchSuggestions(query: $query) {
      ...tagFragment
    }
  }
  ${tagFragment}
`;

export const TagService = {
  getTagSearchSuggestions: (query: string): Promise<[Tag]> => {
    const res = getApolloClient().query({
      query: tagSearchSuggestionsQuery,
      variables: { query },
    });
    return res.then(({ data }) => data.tagSearchSuggestions);
  },
};

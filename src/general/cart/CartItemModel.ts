import { clonePrice, cloneOption } from './../menu/BaseMenu';
import { CustomerItem, CustomerItemSelector, cloneCustomerItem } from '../menu/CustomerMenu';
import { option, Price } from '../menu/BaseMenu';

export interface CartItem extends CustomerItem {
  categoryIndex: number,
  itemIndex: number,
  selectedPrice: Price;
  selectedAddons: Price[];
  selectedOptions?: option[];
  quantity: number;
  specialRequests?: string;
}

export const cloneCartItem = (item: CartItem): CartItem => {
  const newSelectedPrice = clonePrice(item.selectedPrice);
  const newSelectedAddons = item.selectedAddons.map(addon => clonePrice(addon));
  const newSelectedOptions = !!item.selectedOptions ? item.selectedOptions.map(cloneOption) : null;
  const newItem = cloneCustomerItem(item);
  return {
    ...newItem,
    categoryIndex: item.categoryIndex,
    itemIndex: item.itemIndex,
    selectedAddons: newSelectedAddons,
    selectedPrice: newSelectedPrice,
    selectedOptions: newSelectedOptions,
    quantity: item.quantity,
    specialRequests: item.specialRequests,
  }
}

export abstract class CartItemSelector extends CustomerItemSelector {
  static getSelectedPrice = (item: CartItem): Price => item ? item.selectedPrice : undefined

  static getSelectedAddons = (item: CartItem): Price[] => item ? item.selectedAddons : undefined

  static getItemIndex = (item: CartItem): number => item ? item.itemIndex : undefined

  static getCategoryIndex = (item: CartItem): number => item ? item.categoryIndex : undefined

  static getQuantity = (item: CartItem): number => item ? item.quantity : undefined

  static getSelectedOptions = (item: CartItem): option[] => item ? item.selectedOptions : undefined

  static getSpecialRequests = (item: CartItem): string => item ? item.specialRequests : undefined

}

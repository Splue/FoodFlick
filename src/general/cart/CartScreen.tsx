import React from 'react';
import { connect } from 'react-redux';
import { Container, Text, Button, Toast } from 'native-base';
import { RootState } from '../redux/RootState';
import { Cart } from './CartModel';
import CenteredH3 from '../components/CenteredH3';
import { NavigationScreenComponent } from 'react-navigation';
import { reviewOrderScreenName } from './ReviewOrderScreen';
import CartList from './components/CartList';
import { SignedInUser } from '../user/SignedInUser';

type props = {
  cart: Cart,
  signedInUser: SignedInUser,
}

const CartScreen: NavigationScreenComponent<any, any, props> = ({ cart, navigation, signedInUser }) => (
  <Container>
    {cart ? 
      <React.Fragment>
        <CartList editable/>
        <Button block onPress={() => {
          if (!signedInUser) {
            Toast.show({
              text: 'Please sign in to place your order',
              type: 'warning',
              buttonText: 'Okay',
              position: 'bottom'
            });
            return;
          }
          navigation.navigate(reviewOrderScreenName)
        }}>
          <Text>Review your order</Text>
        </Button>
      </React.Fragment>
      :
      <CenteredH3 text={signedInUser ? "Your cart is empty. Fill it with some delicious food!" : "Please sign in so you can place an order"} />}
  </Container>
);

CartScreen.navigationOptions = ({ navigation }) => {
  const { params = {} } = navigation.state;
  const restName = params.restName ? ` - ${params.restName}` : '';
  return {
    title: `Your cart${restName}`,
    tabBarVisible: false,
  }
}

const mapStateToProps = (state: RootState) => ({
  signedInUser: state.getSignedInUser(),
  cart: state.getCart(),
});

export default connect(mapStateToProps)(CartScreen);

export const cartScreenName = 'cartScreen';
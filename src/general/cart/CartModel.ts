import { CustomerItem, cloneCustomerItem } from '../menu/CustomerMenu';
import { option, Price, PriceSelector } from '../menu/BaseMenu';
import { CartItem, CartItemSelector, cloneCartItem } from './CartItemModel';

export interface Cart {
  restId: string,
  restName: string,
  items: CartItem[],
  tableNumber: string,
}

export const getCartItem = (customerItem: CustomerItem, selectedPrice: Price, selectedOptions: option[]): CartItem => {
  const item  = cloneCustomerItem(customerItem) as CartItem;
  item.selectedPrice = selectedPrice || item.prices[0];
  if (selectedOptions) item.selectedOptions = selectedOptions;
  item.quantity = 1;
  return item;
}

export const getNewCartWithTableNumber = (cart: Cart, tableNumber: string) => ({
  restId: cart.restId,
  restName: cart.restName,
  items: cart.items.map(item => cloneCartItem(item)),
  tableNumber,
})

export abstract class CartSelector {
  static getRestId = (cart: Cart): string => cart ? cart.restId : undefined;

  static getTableNumber = (cart: Cart): string => cart? cart.tableNumber : undefined;

  static getRestName = (cart: Cart): string => cart ? cart.restName : undefined;

  static getItems = (cart: Cart): CartItem[] => cart ? cart.items : undefined;

  static getItemTotal = (cart: Cart): number => {
    if (!cart) return 0;
    return cart.items.reduce((sum, item: CartItem) => {
      const price = PriceSelector.getValue(item.selectedPrice);
      const quantity = CartItemSelector.getQuantity(item);
      return sum + price * quantity;
    }, 0)
  }
}

import { Text, ListItem, Left, Thumbnail, Body, Right, Picker } from 'native-base';
import { StyleSheet, View } from 'react-native';
import React from 'react';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import { OrderItem } from '../../order/OrderItem';
import { CartItem, CartItemSelector } from '../CartItemModel';
import { Price, option, PriceSelector } from '../../menu/BaseMenu';

type props = {
  item: CartItem | OrderItem
  onPress?: () => void
  onUpdateQuantity?: (quantity: number) => void
};

const CartListItem: React.FunctionComponent<props> = ({ item, onPress, onUpdateQuantity }) => {
  const selectedPrice: Price = CartItemSelector.getSelectedPrice(item);
  const selectedAddons: Price[] = CartItemSelector.getSelectedAddons(item);
  const selectedOptions: option[] = CartItemSelector.getSelectedOptions(item);
  const specialRequests = CartItemSelector.getSpecialRequests(item);
  const quantity = CartItemSelector.getQuantity(item);
  const uri = CartItemSelector.getFlick(item);
  return (
    <ListItem button onPress={onPress} style={styles.listItem} thumbnail>
      {uri && (
        <Left>
          <Thumbnail large square style={styles.thumbnail} source={{ uri: CartItemSelector.getFlick(item) }} />
        </Left>
      )}
      <Body style={styles.listBody}>
        <Text style={styles.item}>{CartItemSelector.getName(item)}</Text>
        <View style={styles.row}>
          <Text>{PriceSelector.getValue(selectedPrice).toFixed(2)}</Text>
          <Text note>{PriceSelector.getLabel(selectedPrice)}</Text>
        </View>
        {selectedAddons.length > 0 &&
        <View>
          <Text>Addons:</Text>
            {
              selectedAddons.map((addon, index) => (
                <View key={index} style={styles.row}>
                  <Text>{PriceSelector.getValue(addon).toFixed(2)}</Text>
                  <Text note>{PriceSelector.getLabel(addon)}</Text>
                </View>
              ))
            }
        </View>
        }
        {onUpdateQuantity ?
          <View style={styles.row}>
            <Text>Qty:</Text>
            <Picker prompt='Quantity' style={styles.picker} selectedValue={quantity}
            onValueChange={quantity => onUpdateQuantity(quantity)}>
              {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(count => <Picker.Item key={count} label={count.toString()} value={count} />)}
            </Picker>
          </View>
          :
          <Text note>Qty: {quantity}</Text>}
      </Body>
      <Right style={styles.listRight}>
        <Text note>
          {selectedOptions && selectedOptions.map((option, index) => index > 0 ? `, ${option.name}` : option.name)}
          {specialRequests && selectedOptions ? `, ${specialRequests}` : specialRequests}
        </Text>
      </Right>
    </ListItem>
  );
}

const styles = StyleSheet.create({
  item: {
    fontWeight: '600',
  },
  picker: {
    width: 82, // determined by inspection as the smallest possible number which displays 10
    height: 20,
    marginLeft: -8 // chosen by inspection to line up the picker
  },
  row: {
    flexDirection: 'row',
  },
  listItem: {
    marginLeft: 0,
    borderBottomWidth: commonColor.borderWidth,
    borderBottomColor: commonColor.listBorderColor,
  },
  listBody: {
    borderBottomWidth: 0,
  },
  listRight: {
    flexDirection: 'row',
    flex: 1,
    paddingRight: 0,
    borderBottomWidth: 0,
  },
  thumbnail: {
    height: 100,
    width: 100,
  },
});

export default CartListItem;

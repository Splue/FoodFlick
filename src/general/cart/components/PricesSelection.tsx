import React from 'react';
import { StyleSheet } from 'react-native';
import { CardItem, Text, Right } from 'native-base';
import { Price } from '../../menu/BaseMenu';
import commonColor from '../../../../native-base-theme/variables/commonColor';

type props = {
  prices: Price[],
  onPressChoice: (price: Price) => void,
}

const PricesSelection: React.FunctionComponent<props> = ({ prices, onPressChoice }) => (
  <React.Fragment>
    {prices.map(price => 
      <CardItem button key={price.label} onPress={() => onPressChoice(price)}>
        <Text style={styles.choice}>{price.value.toFixed(2)}</Text>
        <Right style={styles.priceLabelContainer}>
          <Text style={styles.choice}>{price.label}</Text>
        </Right>
      </CardItem>)}
  </React.Fragment>
)

const styles = StyleSheet.create({
  priceLabelContainer: {
    flex: 0.5,
    alignItems: 'center',
  },
  choice: {
    fontSize: commonColor.fontSizeBase * 1.2,
  },
});

export default PricesSelection;
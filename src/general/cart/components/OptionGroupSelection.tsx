import React from 'react';
import { StyleSheet } from 'react-native';
import { CardItem, Text } from 'native-base';
import { optionGroup, option } from '../../menu/BaseMenu';
import commonColor from '../../../../native-base-theme/variables/commonColor';

type props = {
  optionGroup: optionGroup
  onPressChoice: (option: option) => void,
}

const OptionGroupSelection: React.FunctionComponent<props> = ({ optionGroup, onPressChoice }) => (
  <React.Fragment>
    {optionGroup.options.map(option =>
      <CardItem button key={option.name} onPress={() => onPressChoice(option)}>
        <Text style={styles.choice}>{option.name}</Text>
      </CardItem>)}
  </React.Fragment>
)

const styles = StyleSheet.create({
  choice: {
    fontSize: commonColor.fontSizeBase * 1.2,
  },
});

export default OptionGroupSelection;
import React from 'react';
import { Price, optionGroup, option } from '../../menu/BaseMenu';
import PricesSelection from './PricesSelection';
import OptionGroupSelection from './OptionGroupSelection';

type props = {
  prices: Price[],
  optionGroups: optionGroup[],
  onFinalSelection: (price: Price, options: option[]) => void
}

type state = {
  optionGroupIndex: number,
  showPrices: boolean,
}

class CartItemPriceAndOptionsEditor extends React.Component<props, state> {

  selectedPrice: Price = null;
  selectedOptions: option[] = (this.props.optionGroups !== null && this.props.optionGroups.length > 0) ? [] : null;

  state = {
    optionGroupIndex: 0,
    showPrices: this.props.prices !== null && this.props.prices.length > 1,
  }

  onPressPrice = (price: Price) => {
    if (this.props.optionGroups.length === 0) {
      this.props.onFinalSelection(price, null);
    } else {
      this.selectedPrice = price;
    }
    this.setState({ showPrices: false });
  }

  onPressOption = (option: option) => {
    this.selectedOptions.push(option);
    this.setState(prevState => {
      const nextIndex = prevState.optionGroupIndex + 1;
      if (nextIndex >= this.props.optionGroups.length) {
        this.props.onFinalSelection(this.selectedPrice, this.selectedOptions);
        return null;
      }
      return { optionGroupIndex: nextIndex };
    });
  }

  render() {
    const { optionGroups, prices } = this.props;
    const { showPrices } = this.state;
    const priceChoices = prices && showPrices && <PricesSelection prices={prices} onPressChoice={this.onPressPrice} />;
    const optionGroupsChoices = (optionGroups && optionGroups.length) > 0 &&
      <OptionGroupSelection optionGroup={optionGroups[this.state.optionGroupIndex]} onPressChoice={this.onPressOption} />
    return priceChoices || optionGroupsChoices;
  }
}

export default CartItemPriceAndOptionsEditor;

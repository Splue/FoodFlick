import React from 'react';
import { connect } from 'react-redux';
import { Container, Content, Text, List, ListItem, Icon, Form, Textarea, Button } from 'native-base';
import { RootState } from '../../redux/RootState';
import { Cart, CartSelector } from '../CartModel';
import { CartItemSelector, CartItem } from '../CartItemModel';
import { Price, option, optionGroup } from '../../menu/BaseMenu';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { withNavigation, NavigationInjectedProps } from 'react-navigation';
import {
  updateQuantityAction,
  deleteCartItemAction,
  updateSpecialRequestAction,
  updateItemPriceAndOptionsAction
} from '../redux/cartActions';
import CardModal from '../../components/CardModal';
import CartItemPriceAndOptionsEditor from '../components/CartItemPriceAndOptionsEditor';
import CartListItem from './CartListItem';

type props = {
  editable?: boolean,
  // redux
  cart?: Cart,
  updateItemQuantity?: (index: number, quantity: number) => void
  deleteCartItem?: (index: number) => void,
  updateItemSpecialRequest?: (index, request) => void,
  updateItemPriceAndOptions?: (index: number, price: Price, options: option[]) => void,
}

const modal_request_title = 'Requests/Allergies';
const modal_options_title = 'Pick something';

type state = {
  activeIndex: number,
  isModalVisible: boolean,
  modalTitle: string,
  modalSpecialRequests: string,
  modalPrices: Price[] | null,
  modalOptionGroups: optionGroup[],
}

const defaultState = {
  activeIndex: null,
  isModalVisible: false,
  modalTitle: '',
  modalSpecialRequests: '',
  modalPrices: null,
  modalOptionGroups: null,
};

class CartList extends React.Component<props & NavigationInjectedProps, state> {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;
    const restName = params.restName ? ` - ${params.restName}` : '';
    return {
      title: `Your cart${restName}`,
      tabBarVisible: false,
    }
  };

  state = defaultState;

  modalItemIndex = null;

  onPressItem = index => {
    if (this.props.editable) this.setState({ activeIndex: index })
  }

  onPressSpecialRequest = (index: number, item: CartItem) => {
    this.setState({
      isModalVisible: true,
      modalTitle: modal_request_title,
      modalSpecialRequests: CartItemSelector.getSpecialRequests(item),
    });
    this.modalItemIndex = index;
  }

  onPressUpdateOptions = (index: number, item: CartItem) => {
    this.modalItemIndex = index;
    const prices: Price[] =  CartItemSelector.getPrices(item);
    const optionGroups: optionGroup[] = CartItemSelector.getOptionGroups(item);
    this.setState({
      isModalVisible: true,
      modalTitle: modal_options_title,
      modalPrices: prices,
      modalOptionGroups: optionGroups,
    });
  }

  onDeleteItem = index => {
    this.props.deleteCartItem(index);
    this.setState({ activeIndex: null });
  }

  onChangeSpecialRequest = newRequest => this.setState({ modalSpecialRequests: newRequest });

  onFinalSelection = (price: Price, options: option[]) => {
    this.props.updateItemPriceAndOptions(this.modalItemIndex, price, options);
    this.closeModal();
  }

  closeModal = () => {
    this.setState(defaultState);
    this.modalItemIndex = null;
  }

  saveSpecialRequest = () => {
    this.props.updateItemSpecialRequest(this.modalItemIndex, this.state.modalSpecialRequests);
    this.closeModal();
  }

  renderCartItem = (cartItem: CartItem, index) => {
    const { updateItemQuantity, editable } = this.props;
    const prices: Price[] = CartItemSelector.getPrices(cartItem);
    const selectedOptions: option[] = CartItemSelector.getSelectedOptions(cartItem);
    if (index === this.state.activeIndex) {
      return (
        <ListItem key={`edit-${index}`} style={styles.editableListItem}>
          <TouchableOpacity style={styles.editAction} onPress={() => this.setState({ activeIndex: null })}>
            <Icon name='arrow-back' style={styles.editText} />
            <Text style={styles.editText}>Back</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.editAction} onPress={() => this.onPressSpecialRequest(index, cartItem)}>
            <Icon name='text' style={styles.editText} />
            <Text style={styles.editText}>Requests/Allergies</Text>
          </TouchableOpacity>
          {(prices.length > 1 || selectedOptions) &&
          <TouchableOpacity style={styles.editAction} onPress={() => this.onPressUpdateOptions(index, cartItem)}>
            <Icon name='create' style={styles.editText} />
            <Text style={styles.editText}>Update options</Text>
          </TouchableOpacity>}
          <TouchableOpacity style={styles.editAction} onPress={this.onDeleteItem}>
            <Icon name='trash' style={styles.editText} />
            <Text style={styles.editText}>Delete</Text>
          </TouchableOpacity>
        </ListItem>
      )
    }
    return (
      <CartListItem
        key={`data-${index}`}
        item={cartItem}
        onPress={() => this.onPressItem(index)}
        onUpdateQuantity={editable ? quantity => updateItemQuantity(index, quantity) : undefined}
      />
    )
  }

  render() {
    const { modalPrices, modalOptionGroups } = this.state;
    return (
      <Container>
        <CardModal isVisible={this.state.isModalVisible} title={this.state.modalTitle} onClose={this.closeModal}>
          {this.state.modalTitle === modal_request_title && 
          <Form style={styles.form}>
            <Textarea bordered rowSpan={2} placeholder='How can we help?' value={this.state.modalSpecialRequests}
            onChangeText={text => this.onChangeSpecialRequest(text)} />
            <Button block style={styles.requestButton} onPress={this.saveSpecialRequest}><Text>Save</Text></Button>
          </Form>}
          {this.state.modalTitle === modal_options_title &&
            <CartItemPriceAndOptionsEditor
              prices={modalPrices}
              optionGroups={modalOptionGroups}
              onFinalSelection={this.onFinalSelection}
            />}
        </CardModal>
        <Content>
          <List>
            {/* necessary we dont get undefined.map() after ordering and resetting cart */}
            {(CartSelector.getItems(this.props.cart) || []).map(this.renderCartItem)}
          </List>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  form: {
    marginTop: 8,
  },
  requestButton: {
    paddingBottom: 0,
    marginBottom: 0,
  },
  editableListItem: {
    marginLeft: 0,
    paddingRight: 0,
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  },
  editText: {
    textAlign: 'center',
  },
  editAction: {
    flex: 1,
  },
});

const mapStateToProps = (state: RootState) => ({
  cart: state.getCart(),
});

const mapDispatchToProps = dispatch => ({
  updateItemQuantity: (index, quantity) => dispatch(updateQuantityAction(index, quantity)),
  updateItemSpecialRequest: (index, request) => dispatch(updateSpecialRequestAction(index, request)),
  updateItemPriceAndOptions: (index, price, options) => dispatch(updateItemPriceAndOptionsAction(index, price, options)),
  deleteCartItem: index => dispatch(deleteCartItemAction(index))
});

export default connect(mapStateToProps, mapDispatchToProps)(withNavigation(CartList));

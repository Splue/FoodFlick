import { CustomerRest, CustomerRestSelector } from './../../rest/CustomerRest';
import { getCartItem } from '../CartModel';
import { CartItem } from "../CartItemModel";
import { CustomerItem } from './../../menu/CustomerMenu';
import { Price, option } from '../../menu/BaseMenu';

export const ADD_ITEM_TO_CART = 'ADD_ITEM_TO_CART';
export const RESET_CART = 'RESET_CART';
export const UDPATE_ITEM_QUANTITY = 'UDPATE_ITEM_QUANTITY';
export const UPDATE_ITEM_SPECIAL_REQUEST = 'UPDATE_ITEM_SPECIAL_REQUEST';
export const UPDATE_ITEM_OPTIONS = 'UPDATE_ITEM_OPTIONS';
export const UPDATE_PRICE_AND_OPTIONS = 'UPDATE_PRICE_AND_OPTIONS';
export const UPDATE_ITEM_PRICE = 'UPDATE_ITEM_PRICE';
export const DELETE_CART_ITEM = 'DELETE_CART_ITEM';
export const GO_ORDER_CONFIRMATION_SCREEN = 'GO_ORDER_CONFIRMATION_SCREEN';

export const addItemToCartAction = (
rest: CustomerRest,
item: CustomerItem,
itemIndex: number,
categoryIndex: number,
selectedPrice?: Price,
selectedOptions?: option[]) => dispatch => {
  const cartItem: CartItem = getCartItem(item, selectedPrice, selectedOptions);
  dispatch({
    type: ADD_ITEM_TO_CART,
    item: cartItem,
    itemIndex,
    categoryIndex,
    restId: CustomerRestSelector.getId(rest),
    restName: CustomerRestSelector.getName(rest),
  });
};

/**
 * 
 * @param index the index of the item in the cart which is to receive a new quantity
 * @param quantity the new desired quantity for the item in the cart
 */
export const updateQuantityAction = (index: number, quantity: number) => ({
  type: UDPATE_ITEM_QUANTITY,
  index,
  quantity,
})

export const updateItemPriceAndOptionsAction = (index: number, price: Price, options: option[]) => ({
  type: UPDATE_PRICE_AND_OPTIONS,
  index,
  price,
  options,
})

export const updateSpecialRequestAction = (index: number, request: string) => ({
  type: UPDATE_ITEM_SPECIAL_REQUEST,
  index,
  request,
})

export const deleteCartItemAction = index => ({
  type: DELETE_CART_ITEM,
  index,
})

export const resetCartAction = () => ({
  type: RESET_CART,
});

export const goOrderConfirmationScreen = () => ({
  type: GO_ORDER_CONFIRMATION_SCREEN,
})
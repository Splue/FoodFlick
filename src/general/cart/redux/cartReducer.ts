import { Price, option } from './../../menu/BaseMenu';
import { Cart, CartSelector } from './../CartModel';
import { CartItem, cloneCartItem } from "./../CartItemModel";
import {
  ADD_ITEM_TO_CART,
  RESET_CART,
  UDPATE_ITEM_QUANTITY,
  UPDATE_ITEM_SPECIAL_REQUEST,
  DELETE_CART_ITEM,
  UPDATE_PRICE_AND_OPTIONS,
} from './cartActions';

interface cartAction {
  type: string,
  restId?: string, // ADD_ITEM_TO_CART
  restName?: string, // ADD_ITEM_TO_CART
  item?: CartItem, // ADD_ITEM_TO_CART
  itemIndex: number, // index of item in category. ADD_ITEM_TO_CART
  categoryIndex: number, // index of category in menu. ADD_ITEM_TO_CART
  // UDPATE_ITEM_QUANTITY, UPDATE_ITEM_SPECIAL_REQUEST, DELETE_CART_ITEM, UPDATE_PRICE_AND_OPTIONS
  index?: number, // the index of an item in the cart
  quantity?: number // UDPATE_ITEM_QUANTITY
  request?: string // UPDATE_ITEM_SPECIAL_REQUEST
  price?: Price // UPDATE_PRICE_AND_OPTIONS
  options?: option[] // UPDATE_PRICE_AND_OPTIONS
}

export const cart = (state: Cart = null, action: cartAction): Cart => {
  switch(action.type) {
    case ADD_ITEM_TO_CART: {
      const { restName, restId, item, itemIndex, categoryIndex } = action;
      item.itemIndex = itemIndex;
      item.categoryIndex = categoryIndex;
      return {
        restName,
        restId,
        items: state === null ? [item] : [...state.items, item],
      }
    }
    case UDPATE_ITEM_QUANTITY: {
      const { index, quantity } = action;
      const newItems = CartSelector.getItems(state).map(cloneCartItem);
      newItems[index].quantity = quantity;
      return {
        ...state,
        items: newItems,
      };
    }
    case DELETE_CART_ITEM: {
      const { index } = action;
      const newItems = CartSelector.getItems(state).map(cloneCartItem);
      newItems.splice(index, 1);
      if (newItems.length === 0) return null;
      return {
        ...state,
        items: newItems,
      };
    }
    case UPDATE_ITEM_SPECIAL_REQUEST: {
      const { index, request } = action;
      const newItems = CartSelector.getItems(state).map(cloneCartItem);
      newItems[index].specialRequests = request;
      return {
        ...state,
        items: newItems,
      };
    }
    case UPDATE_PRICE_AND_OPTIONS: {
      const { index, price, options } = action;
      const newItems = CartSelector.getItems(state).map(cloneCartItem);
      if (price) newItems[index].selectedPrice = price;
      if (options) newItems[index].selectedOptions = options;
      return {
        ...state,
        items: newItems,
      };
    }
    case RESET_CART:
      return null;
    default:
      return state;
  }
}
import React from 'react';
import { connect } from 'react-redux';
import { Container, Text, Button, Icon, Item, Label, Input, Toast } from 'native-base';
import { CartSelector } from './CartModel';
import { NavigationScreenProps } from 'react-navigation';
import CardModal from '../components/CardModal';
import CartList from './components/CartList';
import { View, StyleSheet } from 'react-native';
import commonColor from '../../../native-base-theme/variables/commonColor';
import { RootState } from '../redux/RootState';
import { placeOrderAction } from '../order/redux/orderActions';
import { goOrderConfirmationScreen, resetCartAction } from './redux/cartActions';
import { round2 } from '../utils/math';

type props = {
  itemTotal: number,
  placeOrder: (tableNumber: string) => void,
}

type state = {
  tableNumber: string,
  isModalVisible: boolean,
}

class ReviewOrderScreen extends React.Component<props & NavigationScreenProps, state> {
  static navigationOptions = {
    title: 'Review order',
    tabBarVisible: false,
  }

  state = {
    isModalVisible: false,
    tableNumber: '',
  }

  closeModal = () => {
    this.setState({ isModalVisible: false });
  }

  render() {
    const itemTotal = round2(this.props.itemTotal);
    const tax = round2(itemTotal * 0.0625);
    const tip = round2(itemTotal * 0.15);
    const total = round2(itemTotal + tax + tip);
    return (
      <Container>
        <CardModal isVisible={this.state.isModalVisible} title='How to tip?' onClose={this.closeModal}>
          <Text style={styles.taxHelp}>
            Foodflick cares about good service and good service deserves a good tip.
            <Text style={styles.bold}> That's why we automatically pay the tip you specify (%15) 6 hours after you place your order. </Text>
            Of course every experience is different, so feel free to change the tip amount before the given 6 hours
            and the Foodflick will tip according to your update.
          </Text>
          <Button block onPress={this.closeModal}><Text>Thanks, I got it.</Text></Button>
        </CardModal>
        <CartList />
        <View style={styles.costs}>
          <View>
            <Text>Items:</Text>
            <Text>Estimated tax:</Text>
            <View style={styles.label}>
              <Text>Auto tip:</Text>
              <Icon type='MaterialIcons' name='help-outline' style={styles.helpIcon}
              onPress={() => this.setState({ isModalVisible : true })}/>
            </View>
            <Text style={styles.bold}>Total:</Text>
          </View>
          <View>
            <Text>{itemTotal.toFixed(2)}</Text>
            <Text>{tax.toFixed(2)}</Text>
            <Text>{tip.toFixed(2)}</Text>
            <Text style={styles.totalPrice}>{total.toFixed(2)}</Text>
          </View>
        </View>
        <View style={styles.tableNumber}>
          <View style={styles.tableNumberInput}>
            <Item inlineLabel>
              <Label>Table #</Label>
              <Input value={this.state.tableNumber} onChangeText={tableNumber => this.setState({ tableNumber })} />
            </Item>
          </View>
          <View></View>
        </View>
        <Button block onPress={() => {
          if (!this.state.tableNumber) {
            Toast.show({
              text: 'Please add a table number',
              type: 'warning',
              buttonText: 'Okay',
              position: 'bottom'
            });
            return;
          }
          this.props.placeOrder(this.state.tableNumber)}
        }>
          <Text>Place order</Text>
        </Button>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  costs: {
    marginTop: commonColor.listItemPadding,
    paddingTop: commonColor.listItemThinPadding,
    borderTopWidth: commonColor.borderWidth,
    borderTopColor: commonColor.cardBorderColor,
    backgroundColor: commonColor.brandCanvas,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  tableNumber: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: commonColor.brandCanvas,
    paddingBottom: commonColor.listItemPadding,
    borderBottomWidth: commonColor.borderWidth,
    borderBottomColor:commonColor.cardBorderColor,
  },
  tableNumberInput: {
    flex: 0.5,
  },
  label: {
    flexDirection: 'row',
  },
  helpIcon: {
    fontSize: commonColor.iconFontSize * 0.80,
  },
  bold: {
    fontWeight: 'bold',
  },
  taxHelp: {
    margin: commonColor.contentPadding,
  },
  totalPrice: {
    fontWeight: 'bold',
    color: commonColor.brandInfo,
  },
});

const mapStateToProps = (state: RootState) => ({
  itemTotal: CartSelector.getItemTotal(state.getCart()),
});

const mapDispatchToProps = dispatch => ({
  placeOrder: async (tableNumber) => {
    await dispatch(placeOrderAction(tableNumber));
    dispatch(goOrderConfirmationScreen());
    dispatch(resetCartAction());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ReviewOrderScreen);

export const reviewOrderScreenName = 'reviewOrderScreen';
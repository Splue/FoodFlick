import React from 'react';
import { StyleSheet } from 'react-native';
import { NavigationScreenComponent } from 'react-navigation';
import { H2, Container, Text, Button } from 'native-base';
import commonColor from '../../../native-base-theme/variables/commonColor';

const OrderConfirmationScreen: NavigationScreenComponent = ({ navigation }) => (
  <Container style={styles.content}>
    <H2>Thank you, we have received your order</H2>
    <Text style={styles.text}>You will receive your email receipt shortly</Text>
    {/* need to wrap goBack in fn otherwise it doenst work. not sure why but moving on. */}
    <Button block onPress={() => navigation.goBack()}><Text>Order more</Text></Button>
  </Container>
)

OrderConfirmationScreen.navigationOptions = {
  headerLeft: <Text style={{ color: commonColor.brandCanvas }}>foodflick</Text>,
};

const styles = StyleSheet.create({
  content: {
    padding: commonColor.contentPadding
  },
  text: {
    paddingVertical: commonColor.contentPadding,
  }
});

export default OrderConfirmationScreen;

export const orderConfirmationScreenName = 'orderConfirmationScreen';
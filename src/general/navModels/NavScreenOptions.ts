import { RootState } from './../redux/RootState';
import { NavigationScreenProp, NavigationRoute } from 'react-navigation';

export interface NavScreenOptions {
  navigation: NavigationScreenProp<NavigationRoute>;
  screenProps: RootState;
}
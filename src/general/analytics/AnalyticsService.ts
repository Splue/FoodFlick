import { activeConfig } from './../../config';
import * as Amplitude from 'expo-analytics-amplitude';

class AnalyticsService {

  private static _instance: AnalyticsService;
  public events = {
    LOGIN_WITH_REFRESH: 'Logged in with refresh token',
    LOGIN_WITH_PASSWORD: 'Logged in with password',
    SIGN_UP: 'Signed up',
    SIGN_OUT: 'Signed out',
    UPLOADED_PHOTOS_TO_ACCOUNT: 'Uploaded photos to user account',
    CHANGED_EMAIL: 'Changed Email',
    ADD_CATEGORY: 'Added Category',
    ADD_ITEMS: 'Added Items',
    UPDATE_CATEGORY: 'Updated Category',
    UPDATE_CATEGORY_ORDER: 'Updated Category Order',
    MY_RESTAURANT_ITEM: 'Selected Restaurant',
    UPDATE_ITEM: 'Updated Item',
    UPDATE_ITEM_ORDER: "Updated Item Order",
    DELETE_ITEM: "Deleted Item",
    DELETE_CATEGORY: "Deleted Category",
  }
  private didInit: boolean = false;
  public static get Instance() {
    return this._instance || (this._instance = new this());
  }

  public async init(): Promise<void> {
    if (this.didInit) throw new Error('AnalyticsService aleady initialized');
    await Amplitude.initialize(activeConfig.analytics.key);
    this.didInit = true;
    return;
  }

  public async setUserId(userId: string): Promise<void> {
    this.throwIfNoInit();
    return Amplitude.setUserId(userId)
  }

  /**
   * 
   * @param properties custom map of properties
   */
  public async setUserProperties(properties: object): Promise<void> {
    this.throwIfNoInit();
    return Amplitude.setUserProperties(properties)
  }

  /**
   * @param eventName the event name
   * @param properties custom map of properties
   */
  public async trackEventWithProperties(eventName: string, properties: object): Promise<void> {
    this.throwIfNoInit();
    return Amplitude.logEventWithProperties(eventName, properties)
  }

  /**
   * @param eventName the event name
   */
  public async trackEvent(eventName: string): Promise<void> {
    this.throwIfNoInit();
    return Amplitude.logEvent(eventName)
  }

  // commented out since this is only available for enterprise amplitude
  // /**
  //  * @param groupType the type of group, ex: 'sports'
  //  * @param groupNames list of groups belonging to the groupType, ex: ['tennis', 'soccer']
  //  */
  // public async setGroup(groupType: string, groupNames: string[]): Promise<void> {
  //   this.throwIfNoInit();
  //   return Amplitude.setGroup(groupType, groupNames);
  // }

  private throwIfNoInit() {
    if (!this.didInit) throw new Error('AnalyiticsService not initialized. Initialize first with .init()');
  }
}

export default AnalyticsService.Instance;
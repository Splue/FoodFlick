import { Toast, Container, Content } from 'native-base';
import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet } from 'react-native';
import ValidatedForm from '../components/ValidatedForm';
import { NavigationRoute, NavigationScreenProp } from 'react-navigation';
import HeaderConfirmButton from '../components/header/HeaderConfirmButton';
import RestService from '../rest/restService';
import { NavScreenOptions } from '../navModels/NavScreenOptions';
import { RootState } from '../redux/RootState';
import ValidatedInput from '../components/ValidatedInput';
import commonColor from '../../../native-base-theme/variables/commonColor';

type props = {
  navigation: NavigationScreenProp<NavigationRoute>;
  onConfirm: (feedback: string) => void
}

type state = {
  feedback: string,
}

class GiveFeedbackScreen extends React.Component<props, state> {
  static navigationOptions = ({navigation: {state: {params = {}}}}: NavScreenOptions) => ({
    tabBarVisible: false,
    title: 'Give feedback',
    headerRight: <HeaderConfirmButton onConfirm={params.onConfirm} text={params.confirmText} />
  });

  state = {
    feedback: '',
  }

  render () {

    const inputs = {
      feedback: (
        <ValidatedInput
          floatingLabel
          required
          requiredLabel='Feedback'
          label='Share your thoughts'
          inputProps={{
            multiline: true,
            numberOfLines: 15,
          }}
          onChangeText={feedback => this.setState({ feedback })}
        />
      ) as unknown as ValidatedInput
    }

    return (
      <Container style={styles.background}>
        <Content>
          <ValidatedForm
            onConfirm={() => this.props.onConfirm(this.state.feedback)}
            originalInputs={inputs}
            render={newInputs => Object.values(newInputs)}
          />
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  }
});

const mapStateToProps = (state: RootState) => ({
  selectedRest: state.getTrending().getSelectedRest(),
})

const mergeProps = ({ selectedRest }, { dispatch }, props) => {
  const { navigation } = props;
  return {
    ...props,
    onConfirm: (feedback: string) => {
      RestService.giveRestFeedback(selectedRest._id, feedback);
      Toast.show({ text: 'Feedback posted', buttonText: 'Okay', position: 'bottom' });
      navigation.goBack();
    }
  }
}

export default connect(mapStateToProps, null, mergeProps)(GiveFeedbackScreen);

export const giveFeedbackScreenName = 'giveFeedbackScreen';
import { totalTipsFragment } from './totalTipsFragment';
import { OrderUserRef } from './OrderUserRef';
import { clonePrice, cloneOption } from './../menu/BaseMenu';
import { CartItem, CartItemSelector } from './../cart/CartItemModel';
import { Cart, CartSelector } from './../cart/CartModel';
import getApolloClient from '../../apolloClient';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import { orderFragment } from './orderFragment';
import { Order, OrderSelector } from './OrderModel';
import { reject } from 'lodash';

const completedOrdersQuery = gql`
  query completedOrders($restId: ID!) {
    completedOrders(restId: $restId) {
      ...orderFragment
    }
  }
  ${orderFragment}
`;

const openOrdersQuery = gql`
  query openOrders($restId: ID!) {
    openOrders(restId: $restId) {
      ...orderFragment
    }
  }
  ${orderFragment}
`;

const pendingTipOrdersQuery = gql`
  query pendingTipOrders($restId: ID!) {
    pendingTipOrders(restId: $restId) {
      ...orderFragment
    }
  }
  ${orderFragment}
`;

const myTotalTipsQuery = gql`
  query myTotalTips($restId: ID!, $since: Float!) {
    myTotalTips(restId: $restId, since: $since) {
      ...totalTipsFragment
    }
  }
  ${totalTipsFragment}
`;

const totalTipsQuery = gql`
  query totalTips($restId: ID!, $since: Float!) {
    totalTips(restId: $restId, since: $since) {
      ...totalTipsFragment
    }
  }
  ${totalTipsFragment}
`;

type props = {
  selectedRestId: string,
}

type completedOrdersresponse = {
  completedOrders: Order[]
}

type openOrdersresponse = {
  openOrders: Order[]
}

type pendingTipOrdersResponse = {
  pendingTipOrders: Order[]
}

type tipProps = {
  selectedRestId: string,
  since: number,
}

export interface tips {
  servers: OrderUserRef[]
  tips: number[]
}

type myTotalTipsResponse = {
  myTotalTips: tips
}

type totalTipsResponse = {
  totalTips: tips
}

export default {
  getCompletedOrdersInjector: graphql<props, completedOrdersresponse>(completedOrdersQuery, {
    options: ({ selectedRestId }) => ({
      variables: {
        restId: selectedRestId
      },
      fetchPolicy: 'no-cache',
    }),
    props: ({ data }) => ({
      data,
      loading: data.loading,
      refetch: data.refetch,
      completedOrders: data.completedOrders,
      error: data.error,
    })
  }),

  getOpenOrdersInjector: graphql<props, openOrdersresponse>(openOrdersQuery, {
    options: ({ selectedRestId }) => ({
      variables: {
        restId: selectedRestId
      },
      fetchPolicy: 'cache-and-network',
    }),
    props: ({ data }) => ({
      data,
      loading: data.loading,
      refetch: data.refetch,
      openOrders: data.openOrders,
      error: data.error,
    })
  }),

  getPendingTipOrdersInjector: graphql<props, pendingTipOrdersResponse>(pendingTipOrdersQuery, {
    options: ({ selectedRestId }) => ({
      variables: {
        restId: selectedRestId
      },
      fetchPolicy: 'cache-and-network',
    }),
    props: ({ data }) => ({
      data,
      loading: data.loading,
      refetch: data.refetch,
      pendingTipOrders: data.pendingTipOrders,
      error: data.error,
    })
  }),

  getMyTotalTipsInjector: graphql<tipProps, myTotalTipsResponse>(myTotalTipsQuery, {
    options: ({ selectedRestId, since }) => ({
      variables: {
        restId: selectedRestId,
        since,
      },
      fetchPolicy: 'cache-and-network',
    }),
    props: ({ data }) => ({
      data,
      loading: data.loading,
      refetch: data.refetch,
      tips: data.myTotalTips,
      error: data.error,
    })
  }),

  getTotalTipsInjector: graphql<tipProps, totalTipsResponse>(totalTipsQuery, {
    options: ({ selectedRestId, since }) => ({
      variables: {
        restId: selectedRestId,
        since,
      },
      fetchPolicy: 'cache-and-network',
    }),
    props: ({ data }) => ({
      data,
      loading: data.loading,
      refetch: data.refetch,
      tips: data.totalTips,
      error: data.error,
    })
  }),
  
  completeOrder: async (orderId, restId) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation completeOrder($orderId: ID!) {
          completeOrder(orderId: $orderId)
        }
      `,
      variables: {
        orderId,
      },
      update: cache => {
        const { pendingTipOrders } = cache.readQuery({
          query: pendingTipOrdersQuery,
          variables: {
            restId
          }
        });
        cache.writeQuery({
          query: pendingTipOrdersQuery,
          variables: {
            restId
          },
          data: { pendingTipOrders: reject(pendingTipOrders, (order: Order) => OrderSelector.getId(order) === orderId)}
        });
      },
    });

    return res.data.completeOrder;
  },

  setOrderPendingTip: async(orderId, restId) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation setOrderPendingTip($orderId: ID!) {
          setOrderPendingTip(orderId: $orderId)
        }
      `,
      variables: {
        orderId,
      },
      update: cache => {
        const { openOrders } = cache.readQuery({
          query: openOrdersQuery,
          variables: {
            restId
          }
        });
        cache.writeQuery({
          query: openOrdersQuery,
          variables: {
            restId
          },
          data: { openOrders: reject(openOrders, (order: Order) => OrderSelector.getId(order) === orderId)}
        });
      },
    });

    return res.data.setOrderPendingTip;
  },

  placeOrder: async (cart: Cart) => {
    const newCart = {
      restId: CartSelector.getRestId(cart),
      tableNumber: CartSelector.getTableNumber(cart),
      items: CartSelector.getItems(cart).map((item: CartItem) => ({
        name: CartItemSelector.getName(item),
        itemIndex: CartItemSelector.getItemIndex(item),
        categoryIndex: CartItemSelector.getCategoryIndex(item),
        // clone to remove typename
        selectedPrice: clonePrice(CartItemSelector.getSelectedPrice(item)),
        selectedOptions: CartItemSelector.getSelectedOptions(item) ? CartItemSelector.getSelectedOptions(item).map(cloneOption) : [],
        quantity: CartItemSelector.getQuantity(item),
        specialRequests: CartItemSelector.getSpecialRequests(item),
      }))
    }
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation placeOrder($cart: CartInput!) {
          placeOrder(cart: $cart)
        }
      `,
      variables: {
        cart: newCart
      },
    });

    return res.data.placeOrder;
  },

  printReceipts: async (orderId: string) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation printReceipts($orderId: ID!) {
          printReceipts(orderId: $orderId)
        }
      `,
      variables: {
        orderId,
      },
    });
    return res.data.printReceipts;
  },

  refundCompletedOrder: async (restId, orderId, stripeChargeId, amount) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation refundCompletedOrder($restId: ID!, $orderId: ID!, $stripeChargeId: ID!, $amount: Float!) {
          refundCompletedOrder(restId: $restId, orderId: $orderId, stripeChargeId: $stripeChargeId, amount: $amount) {
            ...orderFragment
          }
        }
        ${orderFragment}
      `,
      variables: {
        restId,
        orderId,
        stripeChargeId,
        amount
      },
    });

    return res.data.refundCompletedOrder;
  },

  refundPendingTipOrder: async (restId, orderId, amount) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation refundPendingTipOrder($restId: ID!, $orderId: ID!, $amount: Float!) {
          refundPendingTipOrder(restId: $restId, orderId: $orderId, amount: $amount) {
            ...orderFragment
          }
        }
        ${orderFragment}
      `,
      variables: {
        restId,
        orderId,
        amount
      },
    });

    return res.data.refundPendingTipOrder;
  },

  returnOrder: async (orderId, reason, restId) => {
    const res = await getApolloClient().mutate({
      mutation: gql`
        mutation returnOrder($orderId: ID!, $reason: String!) {
          returnOrder(orderId: $orderId, reason: $reason)
        }
      `,
      variables: {
        orderId,
        reason,
      },
      update: cache => {
        const { openOrders } = cache.readQuery({
          query: openOrdersQuery,
          variables: {
            restId
          }
        });
        cache.writeQuery({
          query: openOrdersQuery,
          variables: {
            restId
          },
          data: { openOrders: reject(openOrders, (order: Order) => OrderSelector.getId(order) === orderId)}
        });
      }
    });
    return res.data.returnOrder;
  }
};

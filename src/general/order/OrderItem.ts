import { Refund } from './RefundModel';
import { CartItem, CartItemSelector } from './../cart/CartItemModel';

export interface OrderItem extends CartItem {
  categoryIndex: undefined;
  itemIndex: undefined;
  prices: undefined;
  description: undefined;
  flick: undefined;
  likes: undefined;
  refund: Refund
}

export abstract class OrderItemSelector extends CartItemSelector {
  static getRefund = (item: OrderItem): Refund => item ? item.refund : undefined;
}
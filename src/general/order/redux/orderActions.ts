import { ACCOUNT_TAB } from './../../../account/redux/accountReducer';
import { RootState } from '../../redux/RootState';
import OrderService from '../orderService';
import { getNewCartWithTableNumber } from '../../cart/CartModel';
import { ManagerRestSelector } from '../../rest/ManagerRest';
import { Order, OrderSelector } from '../OrderModel';

export enum OrderActions {
  PLACE_ORDER_ACTION = 'PLACE_ORDER_ACTION',
  SELECT_ORDER_ACTION = 'SELECT_ORDER_ACTION',
}

export const placeOrderAction = tableNumber => async (dispatch, getState: () => RootState) => {
  await OrderService.placeOrder(getNewCartWithTableNumber(getState().getCart(), tableNumber));
  dispatch({ type: OrderActions.PLACE_ORDER_ACTION });
}

export const completeOrderAction = (
  orderId: string,
) => async (dispatch, getState: () => RootState) => {
  await OrderService.completeOrder(orderId, ManagerRestSelector.getId(getState().getAccount().getSelectedRest()));
  dispatch(selectOrderAction(null))
}

export const printReceiptsAction = () => async (dispatch, getState: () => RootState) => {
  return await OrderService.printReceipts(OrderSelector.getId(getState().getAccount().getSelectedOrder()));
}

export const setOrderPendingTipAction = (
  orderId: string,
) => async (dispatch, getState: () => RootState) => {
  await OrderService.setOrderPendingTip(orderId, ManagerRestSelector.getId(getState().getAccount().getSelectedRest()));
  dispatch(selectOrderAction(null))
}

export const refundOrderCompletedAction = (
  amount: number
) => async (dispatch, getState: () => RootState) => {
  const restId = ManagerRestSelector.getId(getState().getAccount().getSelectedRest());
  const orderId = OrderSelector.getId(getState().getAccount().getSelectedOrder());
  const stripeChargeId = OrderSelector.getStripeChargeId(getState().getAccount().getSelectedOrder());
  const order = await OrderService.refundCompletedOrder(restId, orderId, stripeChargeId, amount);
  dispatch(selectOrderAction(order));
}

export const refundOrderPendingTipAction = (
  amount: number
) => async (dispatch, getState: () => RootState) => {
  const restId = ManagerRestSelector.getId(getState().getAccount().getSelectedRest());
  const orderId = OrderSelector.getId(getState().getAccount().getSelectedOrder());
  const order = await OrderService.refundPendingTipOrder(restId, orderId, amount);
  dispatch(selectOrderAction(order));
}

export const returnOrderAction = (
  orderId: string,
  reason: string,
) => async (dispatch, getState: () => RootState) => {
  await OrderService.returnOrder(orderId, reason, ManagerRestSelector.getId(getState().getAccount().getSelectedRest()));
  dispatch(selectOrderAction(null))
}

export const selectOrderAction = (order: Order) => ({
   type: OrderActions.SELECT_ORDER_ACTION,
   tab: ACCOUNT_TAB,
   order,
})

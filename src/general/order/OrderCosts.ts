
export interface OrderCosts {
  itemTotal: number;
  tax: number;
  tip: number;
  percentFee: number; // ex: 2.9 = 2.9 percent
  flatRateFee: number; // ex: 0.30 = 30 cents
}
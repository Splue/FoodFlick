import gql from 'graphql-tag';

export const totalTipsFragment = gql`
  fragment totalTipsFragment on TotalTips {
    servers {
      firstName
      lastName
      userId
    }
    tips
  }
`

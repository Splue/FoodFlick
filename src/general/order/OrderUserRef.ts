export interface OrderUserRef {
  userId: string,
  firstName: string,
  lastName: string,
}
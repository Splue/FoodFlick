export enum OrderType {
  OPEN = 'OPEN',
  RETURNED = 'RETURNED',
  COMPLETED = 'COMPLETED',
}
import { OrderType } from "./OrderType";
import { OrderUserRef } from "./OrderUserRef";
import { OrderItem } from "./OrderItem";
import { OrderCosts } from "./OrderCosts";
import { Refund } from "./RefundModel";
import { round2 } from "../utils/math";

export interface Order {
  _id: string
  restId: string
  status: OrderType
  stripeChargeId: string
  customer: OrderUserRef
  cartUpdatedDate: number
  items: OrderItem[]
  costs: OrderCosts
  customRefunds: Refund[]
}

export abstract class OrderSelector {
  static getId = (order: Order): string => order ? order._id : undefined;
  
  static getStripeChargeId = (order: Order): string => order ? order.stripeChargeId : undefined;

  static getItems = (order: Order): OrderItem[] => order ? order.items : undefined;

  static getCustomerName = (order: Order): string => order ? `${order.customer.firstName} ${order.customer.lastName}` : undefined;

  static getCartUpdatedDate = (order: Order): number => order ? order.cartUpdatedDate : undefined;

  static getTax = (order: Order): number => order ? round2(order.costs.tax) : undefined

  static getTip = (order: Order): number => order ? round2(order.costs.tip) : undefined

  static getItemTotal = (order: Order): number => order ? round2(order.costs.itemTotal) : undefined

  static getCustomRefundsTotal = (order: Order): number => order ?
    order.customRefunds.reduce((sum, refund) => sum + refund.amount, 0)
    :
    undefined

  static getOrderTotal = (order: Order): number => order ?
    round2(order.costs.itemTotal + order.costs.tax + order.costs.tip)
    :
    undefined

}
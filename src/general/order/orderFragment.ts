import gql from 'graphql-tag';

export const orderFragment = gql`
  fragment orderFragment on Order {
    _id
    restId
    status
    customer {
      userId
      firstName
      lastName
    }
    cartUpdatedDate
    items {
      name
      quantity
      selectedAddons {
        label
        value
      }
      selectedPrice {
        label
        value
      }
      selectedOptions {
        name
        price
      }
      specialRequests
    }
    costs {
      itemTotal
      tip
      tax
      percentFee
      flatRateFee
    }
    customRefunds {
      stripeRefundId
      amount
    }
    stripeChargeId
  }
`


export interface Refund {
  stripeRefundId: string,
  amount: number,
}
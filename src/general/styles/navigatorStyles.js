import { StyleSheet, Platform } from 'react-native';
import commonColor from "../../../native-base-theme/variables/commonColor";

export const navigatorStyles = StyleSheet.create({
  header: {
    backgroundColor: commonColor.brandBase,
    elevation: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -Expo.Constants.statusBarHeight
  },
  title: {
    fontSize: 18,
    fontWeight: '500'
  },
});
import { cart } from '../cart/redux/cartReducer';
import { location } from './locationReducer';
import { nav } from './navReducer';
import { signedInUser } from '../user/signedInUserReducer';
import { firebaseStorageRef } from './firebaseStorageRefReducer'
import { accountReducer } from '../../account/redux/accountReducer';
import { trendingReducer } from '../../trending/redux/trendingReducer';
import { favoritesReducer } from '../../favorites/redux/favoritesReducer';
import { RootState } from './RootState';
import { ui } from './uiReducer';

// setting to undefined to corresponding reducers can choose default values
const initialState: RootState = new RootState(
  undefined,
  undefined,
  undefined,
  undefined,
  undefined, 
  undefined,
  undefined,
  undefined,
  undefined,
);

export const getRootReducer = () => (state: RootState = initialState, action) => new RootState(
  location(state.getLocation(), action),
  ui(state.getUi(), action),
  nav(state.getNav(), action),
  signedInUser(state.getSignedInUser(), action),
  firebaseStorageRef(state.getFirebaseStorageRef()),
  trendingReducer(state.getTrending(), action),
  favoritesReducer(state.getFavorites(), action),
  accountReducer(state.getAccount(), action),
  cart(state.getCart(), action),
);

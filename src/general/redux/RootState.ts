import { UiState } from './uiReducer';
import { Cart } from './../cart/CartModel';
import { LocationState } from './locationReducer';
import { FavoritesState } from './../../favorites/redux/FavoritesState';
import { AccountState } from "../../account/redux/AccountState";
import { TrendingState } from '../../trending/redux/TrendingState';
import { SignedInUser } from '../user/SignedInUser';

export class RootState {
  readonly location: LocationState
  readonly ui: UiState
  readonly nav: any
  readonly signedInUser: SignedInUser
  readonly firebaseStorageRef: any
  readonly trending: TrendingState
  readonly favorites: FavoritesState
  readonly account: AccountState
  readonly cart: Cart

  public constructor(location, ui, nav, signedInUser, firebaseStorageRef, trending, favorites, account, cart) {
    this.cart = cart;
    this.ui = ui;
    this.location = location;
    this.nav = nav;
    this.signedInUser = signedInUser;
    this.firebaseStorageRef = firebaseStorageRef;
    this.trending = trending;
    this.favorites = favorites;
    this.account = account;
  }

  getCart = () => this.cart;

  getUi = () => this.ui;

  getLocation = () => this.location;

  getNav = () => this.nav;

  getSignedInUser = () => this.signedInUser;

  getFirebaseStorageRef = () => this.firebaseStorageRef;
  
  getTrending = () => this.trending;
  
  getFavorites = () => this.favorites;
  
  getAccount = () => this.account;
}
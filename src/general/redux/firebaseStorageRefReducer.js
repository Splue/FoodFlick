import * as firebase from 'firebase/app';
import 'firebase/storage';
import { activeConfig } from '../../config';

//using firebase instead of aws because i could not restrict storage write access in aws. when using id_token in 
//aws, we would have to add a custom namespaced claim for write permission. however, the trust policy, which has access
//to the id_token, cannot read the custom claim because it aws doesn't allow multiple : in the policy. we woudl have
//multiple : because aws would have syntax similar to... https://food-flick.com: write-rest which has 2 colons.
firebase.initializeApp(activeConfig.firebase);

const storageRef = firebase.storage().ref();

export const firebaseStorageRef = (state = storageRef) => state;
import { GO_ORDER_CONFIRMATION_SCREEN } from './../cart/redux/cartActions';
import { NavigationActions } from 'react-navigation';
import { RawTabNavigator } from '../../AppNavigator';
import { GO_FLICK_PICKER_AFTER_CATEGORY_ADDITION } from '../menu/menuActions';
import { orderConfirmationScreenName } from '../cart/OrderConfirmationScreen';
import AnalyticsService from '../analytics/AnalyticsService';

const router = RawTabNavigator.router;

export function nav(state = null, action) {
  if (action.routeName) {
    AnalyticsService.trackEvent(`Viewed ${action.routeName}`);
  }
  switch (action.type) {
    case GO_FLICK_PICKER_AFTER_CATEGORY_ADDITION:
      // back to remove current category addition screen from nav stack
      state = router.getStateForAction(NavigationActions.back(), state);
      return router.getStateForAction(NavigationActions.navigate({
        routeName: 'flickPickerScreen',
        key: 'flickPickerScreen',
        params: { isSkippable: true, confirmText: 'Next' },
      }), state);
    case GO_ORDER_CONFIRMATION_SCREEN:
      // back to remove ReviewOrderScreen and CartScreen
      state = router.getStateForAction(NavigationActions.back(), state);
      state = router.getStateForAction(NavigationActions.back(), state);
      return router.getStateForAction(NavigationActions.navigate({
        routeName: orderConfirmationScreenName,
        key: orderConfirmationScreenName,
      }), state);
    default:
      // specify keys since stack navs auto generate keys. not for push action since keys aren't supported for push
      if (!action.key && action.type !== 'Navigation/PUSH') action.key = action.routeName;
      return router.getStateForAction(action, state) || state;
  }
}

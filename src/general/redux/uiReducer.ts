import { actionTypes } from './actions';
import { LoadingAction } from './uiActions';

export type LoadingState = {
  isLoading: boolean,
  msg?: string,
}

export interface UiState {
  loading: LoadingState
}

const defaultState: UiState = {
  loading: {
    isLoading: false,
  }
}

export const ui = (state = defaultState, action: LoadingAction): UiState => {
  switch (action.type) {
    case actionTypes.UPDATE_LOADING:
      return {
        loading: action.loading
      }
    default:
      return state
  }
}

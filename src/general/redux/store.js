import { createStore, applyMiddleware, combineReducers } from 'redux';
import { createReduxBoundAddListener, createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers';
import ReduxThunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import getRootReducer from './rootReducer';

const reactNav = createReactNavigationReduxMiddleware('root', state => state.nav);
const addNavReduxListener = createReduxBoundAddListener('root');

let store;

const initStore = (rootReducer = getRootReducer()) => {
  const logger = createLogger({
    collapsed: true,
    // diff: true, // can't turn on or dev mode is way too slow
  });

  // A middleware is needed so that any events that mutate the navigation state properly trigger the event listeners.
  const enhancer = process.env.NODE_ENV === 'development'
    // commented out for faster development
    // ? applyMiddleware(ReduxThunk, reactNav, logger)
    ? applyMiddleware(ReduxThunk, reactNav)
    : applyMiddleware(ReduxThunk, reactNav);

  store = createStore(rootReducer, enhancer);
  return store;
};

const getStore = () => store;

export { addNavReduxListener, initStore, getStore };

// const store = initStore();
// export default store;
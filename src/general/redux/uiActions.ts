import { LoadingState } from './uiReducer';
import { actionTypes } from './actions';

export type LoadingAction = {
  type: actionTypes,
  loading: LoadingState
}

export const showLoading = (msg?: string): LoadingAction => ({
  type: actionTypes.UPDATE_LOADING,
  loading: {
    isLoading: true,
    msg
  }
});

export const removeLoading = (): LoadingAction => ({
  type: actionTypes.UPDATE_LOADING,
  loading: {
    isLoading: false,
  }
});
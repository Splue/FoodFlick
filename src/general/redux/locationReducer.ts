import { actionTypes } from './actions';

export interface LocationState {
  lat: number,
  lon: number,
}

export const location = (state = null, action): LocationState => {
  switch (action.type) {
    case actionTypes.UPDATE_LOCATION:
      return action.location
    default:
      return state
  }  
}

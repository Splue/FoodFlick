export enum actionTypes {
  UPDATE_LOCATION = 'UPDATE_LOCATION',
  UPDATE_LOADING = 'UPDATE_LOADING'
}

export interface action {
  type: string,
  tab: string,
}
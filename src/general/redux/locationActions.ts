import { actionTypes } from './actions';
import { LocationState } from './locationReducer';

export interface LocationAction {
  type: string,
  location: LocationState,
}

export const updateCurrentLocation = (location: LocationState) => ({
  type: actionTypes.UPDATE_LOCATION,
  location,
});
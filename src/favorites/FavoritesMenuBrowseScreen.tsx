import React from 'react';
import { NavigationRoute, NavigationScreenComponent, NavigationScreenProp } from 'react-navigation';
import { connect } from 'react-redux';
import { CustomerCategory } from '../general/menu/CustomerMenu';
import { selectFavoritesCategoryAction, toggleFavoritesItemLikeAction } from '../general/menu/menuActions';
import MenuBrowser from '../general/components/MenuBrowser';
import { NavScreenOptions } from '../general/navModels/NavScreenOptions';
import { RootState } from '../general/redux/RootState';
import { CustomerRest } from '../general/rest/CustomerRest';
import { toggleFavoritesRestFavoriteAction } from '../general/rest/restActions';
import { Toast } from 'native-base';

type props = {
  navigation: NavigationScreenProp<NavigationRoute>;
  selectCategory: (category: CustomerCategory) => any;
  selectedRest: CustomerRest
  toggleItemLike: (selectedRest: CustomerRest, category: CustomerCategory, itemName: string) => void;
  toggleRestFavorite: (selectedRest: CustomerRest) => void;
}

const FavoritesMenuBrowseScreen: NavigationScreenComponent = (props: props) => (
  <MenuBrowser navigation={props.navigation} selectCategory={props.selectCategory} selectedRest={props.selectedRest}
  toggleItemLike={props.toggleItemLike} toggleRestFavorite={props.toggleRestFavorite} />
);

FavoritesMenuBrowseScreen.navigationOptions = (nav: NavScreenOptions) => {
  const newNav: NavScreenOptions = { ...nav };
  if (!newNav.navigation.state.params) {
    newNav.navigation.state.params = {};
  }
  newNav.navigation.state.params.selectedRest = newNav.screenProps.getFavorites().getSelectedRest();
  return MenuBrowser.navigationOptions(newNav);
};

const mapStateToProps = (state: RootState) => ({
  selectedRest: state.getFavorites().getSelectedRest()
});

const mapDispatchToProps = dispatch => ({
  toggleItemLike: async (selectedRest, category, itemName) => {
    await dispatch(toggleFavoritesItemLikeAction(selectedRest, category, itemName));
  },
  toggleRestFavorite: async selectedRest => {
    await dispatch(toggleFavoritesRestFavoriteAction(selectedRest));
  },
  selectCategory: category => dispatch(selectFavoritesCategoryAction(category)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FavoritesMenuBrowseScreen);

export const favoritesMenuBrowseScreenName = 'favoritesMenuBrowseScreen';
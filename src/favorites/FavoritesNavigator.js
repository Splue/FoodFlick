import { find } from 'lodash';
import FavoritesScreen, { favoritesScreenName } from './FavoritesScreen';
import StackNavigator from '../general/components/StackNavigator';
import FavoritesMenuBrowseScreen, { favoritesMenuBrowseScreenName } from './FavoritesMenuBrowseScreen';
import CartScreen, { cartScreenName } from '../general/cart/CartScreen';
import GiveFeedbackScreen, { giveFeedbackScreenName } from '../general/screens/GiveFeedbackScreen';
import ReviewOrderScreen, { reviewOrderScreenName } from '../general/cart/ReviewOrderScreen';
import OrderConfirmationScreen, { orderConfirmationScreenName } from '../general/cart/OrderConfirmationScreen';

const mapStateToProps = ({nav: {routes}}) => ({
  nav: find(routes, { key: favoritesNavigatorName }),
});

const FavoritesNavigator = StackNavigator(
  favoritesScreenName,
  mapStateToProps,
  {
    [cartScreenName]: { screen: CartScreen },
    [giveFeedbackScreenName]: { screen: GiveFeedbackScreen },
    [favoritesScreenName]: { screen: FavoritesScreen },
    [favoritesMenuBrowseScreenName]: { screen: FavoritesMenuBrowseScreen },
    [orderConfirmationScreenName]: { screen: OrderConfirmationScreen },
    [reviewOrderScreenName] : { screen: ReviewOrderScreen },
  }
);

export default FavoritesNavigator;

export const favoritesNavigatorName = 'favoritesNavigator';
import React from 'react';
import { connect } from 'react-redux';
import { Alert, StyleSheet } from 'react-native';
import { Container, Content, Button, Text } from 'native-base';
import { selectFavoritesRestAction, toggleFavoritesRestFavoriteAction } from '../general/rest/restActions';
import CenteredH3 from '../general/components/CenteredH3';
import commonColor from '../../native-base-theme/variables/commonColor';
import UndividedList from '../general/components/list/UndividedList';
import RestService from '../general/rest/restService';
import { favoritesMenuBrowseScreenName } from './FavoritesMenuBrowseScreen';
import HeaderCart from '../general/components/header/HeaderCart';

class FavoritesScreen extends React.Component<any, any> {
  static navigationOptions = {
    title: 'Favorites',
    headerRight: <HeaderCart />
  }

  render () {
    const { loading, signedInUser, favoriteRests, onPressRest, onDeleteRest, navigation } = this.props;
    if (!signedInUser) {
      return (
        <Container>
          <CenteredH3 text='Sign in to see your favorites'/>
          <Button block onPress={() => navigation.navigate('signInScreen')}>
            <Text>Sign in</Text>
          </Button>
        </Container>
      )
    }

    if (loading) {
      return <CenteredH3 text="Loading..." />
    }

    if (!favoriteRests || favoriteRests.length === 0) {
      return (
        <Container>
          <CenteredH3 text='You have no favorites'/>
        </Container>
      )
    }
    
    return (
      <Container style={styles.container}>
        <Content>
          <UndividedList items={favoriteRests.map(rest => ({
            text: rest.profile.name,
            rightIcon:'trash',
            onIconPress: () => onDeleteRest(rest),
            props: { onPress: () => onPressRest(rest) },
          }))} />
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = ({ signedInUser }) => ({
  signedInUser
});

const mapDispatchToProps = (dispatch, {navigation: { navigate }}) => ({
  onPressRest: rest => {
    dispatch(selectFavoritesRestAction(rest));
    navigate(favoritesMenuBrowseScreenName);
  },
  onDeleteRest: async rest => {
    Alert.alert(
      'Delete restaurant?',
      `Are you sure you want to delete ${rest.name} from your favorites?`,
      [
        { text: 'Cancel' },
        { text: 'Delete', onPress: async () => {
          await dispatch(toggleFavoritesRestFavoriteAction(rest));
        }},
      ],
      { cancelable: false }
    );
  },
})

const styles = StyleSheet.create({
  container: {
    backgroundColor: commonColor.brandCanvas,
    padding: commonColor.contentPadding,
  },
})

export default connect(mapStateToProps, mapDispatchToProps)(RestService.getMyFavoritesInjector(FavoritesScreen));

export const favoritesScreenName = 'favoritesScreen';

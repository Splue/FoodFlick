import { CustomerCategory } from '../../general/menu/CustomerMenu';
import { CustomerRest } from '../../general/rest/CustomerRest';

export class FavoritesState {

  readonly selectedRest: CustomerRest;
  readonly selectedCategory: CustomerCategory;

  public constructor(selectedRest: CustomerRest, selectedCategory: CustomerCategory) {
    this.selectedRest = selectedRest;
    this.selectedCategory = selectedCategory;
  }

  getSelectedRest = (): CustomerRest => this.selectedRest;
  
  getSelectedCategory = (): CustomerCategory => this.selectedCategory;
  
};
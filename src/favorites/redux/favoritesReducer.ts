import { FavoritesState } from './FavoritesState';
import { selectedCategory } from '../../general/menu/menuReducer';
import { selectedRest } from '../../general/rest/restReducer';

export const FAVORITES_TAB: string = 'FAVORITES_TAB';

export const favoritesReducer = (state = new FavoritesState(null, null), action: any = {}) => {
  switch(action.tab) {
    case FAVORITES_TAB:
      return new FavoritesState(
        selectedRest(state.getSelectedRest(), action),
        selectedCategory(state.getSelectedCategory(), action),
      );
    default:
      return state
  }
}

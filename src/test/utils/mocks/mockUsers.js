const mockSignedInRestManager = {
  accessToken: {
    token: 'access_token',
    type: 'Bearer',
  },
  email: 'manager@manager.com',
  firstName: null,
  fullName: null,
  perms: 'write:rests',
  _id: 'auth0|5acd251aa07bf228f6c3bb53'
}

const mockSignedInRestManagerIdToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlF6ZzBSVVkxT0VJd1FVRTFSakZGUkRJelFqaEVRakEyTVVKQlFrUkNORVV6TlVNMk4wWkRRdyJ9.eyJodHRwOi8vZm9vZGZsaWNrLmNvbS9maXJlYmFzZVRva2VuIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKU1V6STFOaUo5LmV5SnBjM01pT2lKbWFYSmxZbUZ6WlMxaFpHMXBibk5rYXkxaVltZHVNVUJtYjI5a0xXWnNhV05yTG1saGJTNW5jMlZ5ZG1salpXRmpZMjkxYm5RdVkyOXRJaXdpYzNWaUlqb2labWx5WldKaGMyVXRZV1J0YVc1elpHc3RZbUpuYmpGQVptOXZaQzFtYkdsamF5NXBZVzB1WjNObGNuWnBZMlZoWTJOdmRXNTBMbU52YlNJc0ltRjFaQ0k2SW1oMGRIQnpPaTh2YVdSbGJuUnBkSGwwYjI5c2EybDBMbWR2YjJkc1pXRndhWE11WTI5dEwyZHZiMmRzWlM1cFpHVnVkR2wwZVM1cFpHVnVkR2wwZVhSdmIyeHJhWFF1ZGpFdVNXUmxiblJwZEhsVWIyOXNhMmwwSWl3aWFXRjBJam94TlRJek16azBNREU1TENKbGVIQWlPakUxTWpNek9UYzJNVGtzSW5WcFpDSTZJbUYxZEdnd2ZEVmhZMlF5TlRGaFlUQTNZbVl5TWpobU5tTXpZbUkxTXlJc0luTmpiM0JsSWpvaWQzSnBkR1U2Y21WemRITWlmUS5HMnNfS0VEd3N0UDFFYU5BQ21Uc05DM0VfVTJrcHJ6Wnh6SDZTelgyUG14cFJQSjlYTWswT21fOURnNXBXcURUZ0xVTThRdHZISXVndDZTNHg0SGd0akZHalFPQUl1MVc0SnN6UnBRb1pRRFQ2aGk1RzdzWTVBcThrRGNNSXBfZ2dqa19UU2dzRzR1eGtRUllsQkIxdlZUUnllTWdoNURidjdLSC1LZmZYenJyTGNzVzB4UUVRY1E1M29KSWtvdlFSRjN3UGJ0YXIwNnZhRkljZXFxUWV3SWc2akM4cDQtSDdYUm9BWF9UaEg4ZjRGaGxMQXVNZUZ3eFRiR3dIa0ZnRUFRdm1JUDFKUEMzXzFocFZmVmVweERLdVZZTE9GNEhDeEtfWVBscFVlYllJNUJWLWVad3BhbEdfeDIwNFJlUnMwejFmUVdydjRiaWJsbElUYjJtNHciLCJuaWNrbmFtZSI6bnVsbCwibmFtZSI6bnVsbCwicGljdHVyZSI6Imh0dHBzOi8vcy5ncmF2YXRhci5jb20vYXZhdGFyL2RmZDZiMTAyMTVlMmU2MWI1YjU2Nzg5Mjk0ZDA2OGY1P3M9NDgwJnI9cGcmZD1odHRwcyUzQSUyRiUyRmNkbi5hdXRoMC5jb20lMkZhdmF0YXJzJTJGbWEucG5nIiwidXBkYXRlZF9hdCI6IjIwMTgtMDQtMTBUMjE6MDA6MTguNTIwWiIsImVtYWlsIjoibWFuYWdlckBtYW5hZ2VyLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiaXNzIjoiaHR0cHM6Ly9mb29kZmxpY2suYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhY2QyNTFhYTA3YmYyMjhmNmMzYmI1MyIsImF1ZCI6Im00Q2tnOUJYUzdQYkF1NlgySFZBVFJuN1E0Nng0VkJ0IiwiaWF0IjoxNTIzMzk0MDE4LCJleHAiOjE1MjM0MzAwMTh9.ruQBvfbGsliSdPJ7TFyXiF2ggoqtr0CUXuPB3r73anLeVAWKGiXO-gLb0sllh-eDlRrxYk1zqbiMoVAmxeGS4_TQ0xUIwXqMUYJmPAT7xlBcfa6SEGX7wP6uspLQcQT-k5_4Fx3VzbKkmxia7wdFTeRAdqX7UsUBtWQ8Cnr6zI2vyqYaDr-sOvFGPrFX0HwqAhTMYBljd2NFlLAJHUfuQGx8pEJsnMSr4q3oposzBqMCGWBSVa-wraOEpj8I9D1iiRCF3xBlqQ_KCaX5gqyDRr1J3qnIiBDsEHfzlf-FzJhTNX3wj5DjdhdgMGWPXIJSWegcaHj95zwgZX5wy-DVVw';

export const getMockSignedInRestManager = () => ({ ...mockSignedInRestManager });

export const getMockManagerSignInFetchRes = () => ({
  access_token: mockSignedInRestManager.accessToken.token,
  refresh_token: 'refresh_token',
  id_token: mockSignedInRestManagerIdToken,
  scope:'openid profile email address phone write:rests offline_access',
  expires_in: 86400,
  token_type: 'Bearer'
});

export const getMockManagerSignUpFetchRes = () => ({
  _id: mockSignedInRestManager._id.substring(6), //6 to remove auth0|
  email_verified: false,
  email: mockSignedInRestManager.email,
  app_metadata: {
    isRestManager: true
  },
  user_metadata: {
    firstName: null,
    lastName: null,
    alternateEmails: []
  }
});

export const getMockSignedInCustomer = () => ({
  accessToken: {
    token: 'abc',
    type: 'Bearer',
  },
  email: 'customer@customer.com',
  firstName: 'ned',
  fullName: 'stark',
  _id: 'auth0|5a78c1ccebf64a46ecdd0d9d'
});
const mockRest = {
  _id: '5acd6b4962fb952cb44ed678',
  profile: {
    name: 'magic wok',
    description: 'soup',
    phone: '123',
  },
  location: {
    address: {
      address1: 'wok road',
      city: 'chinatown',
      state: 'PA',
      zip: '123',
    }
  },
  managers: [
    { _id: '1', email: 'manager1@manager.com' },
    { _id: '2', email: 'manager2@manager.com' },
  ],
  menu: [],
}

export const getMockNewRest = () => ({
  profile: { ...mockRest.profile },
  location: { ...mockRest.location },
})

export const getMockSelectedRest = () => ({ ...mockRest });

export const getMockAddRestServiceRes = () => ({ ...mockRest });
import * as firebase from 'firebase/app';
import { initStore } from "../../../store";
import getRootReducers from '../../../rootReducer';

export const getMockNavigation = (options) => ({
  addListener: jest.fn(),
  dispatch: jest.fn(),
  goBack: jest.fn(),
  navigate: jest.fn(),
  pop: jest.fn(),
  popToTop: jest.fn(),
  push: jest.fn(),
  setParams: jest.fn(),
  state: {},
  reset: function () {
    for (const field in this) {
      if (field === 'reset') {
        continue;
      }
      
      if (field === 'state') {
        const params = this.state.params;
        if (params) {
          for (const key in params) {
            if (jest.isMockFunction(params[key])) params[key].mockReset();
          }
        }
        continue;
      }
      
      this[field].mockReset();
    }
  },
  ...options,
});

// necessary, otherwise async animations break tests. tried calling in setup.js. does not work
export const mockAnimated = () => (jest.mock('Animated', () => {
  const ActualAnimated = require.requireActual('Animated')
  return {
    ...ActualAnimated,
    timing: (value, config) => ({
      start: callback => {
        value.setValue(config.toValue)
        if (callback) {
          callback()
        }
      },
    }),
  }
}));

export const mockFirebase = () => {
  const realAuth = firebase.auth;

  firebase.auth = jest.fn(() => ({
    signInWithCustomToken: jest.fn(() => Promise.resolve({ user: 'kobe' }))
  }));

  firebase.mockReset = () => {
    firebase.auth.mockReset();
  }
};

export const getMockStore = (mockState) => {
  const reducer = getRootReducers();
  for (const key in mockState) {
    const original = reducer[key];
    reducer[key] = (state = mockState[key], action) => original(state, action)
  }
  return initStore(reducer)
}

// POC: broken poc for mounting and testing connected components with nav. see MockProvider.js and setup.js
// export const getSignInScreenNavState = () => ({
//   index: 2,
//   routes: [
//     {key: 'trendingScree', params: undefined, routeName: 'trendingScreen'},
//     {key: 'bookmarksScreen', params: undefined, routeName: 'bookmarksScreen'},
//     {
//       index: 1,
//       isTransitioning: true,
//       key: 'accountNavigator',
//       params: undefined,
//       routeName: 'accountNavigator',
//       routes: [
//         { key:'id-1522033251155-2', params: undefined, routeName:'accountScreen' },
//         { key: 'id-1522033251155-3', params: undefined, routeName: 'signInScreen' }
//       ]
//     }
//   ]
// })
// POC: see MockProvider.js

// import React from 'react';
// import { connect } from 'react-redux';
// import { TabNavigator, addNavigationHelpers } from "react-navigation";
// import { Container } from 'native-base';
// import { addNavReduxListener } from '../../store';
// import { RawTabNavigator } from '../../AppNavigator';

// //wrap TabNavigation so that we can pass it our own navigation prop. when we pass our own navigation
// //prop, TabNavigation "relinquishes control of its internal state" so that we can manage nav state
// //through redux. default navigators' navigation props only includes state and dispatch and we mimic
// //this behavior with addNavigationHelpers so nothing changes. to see redux manage nav state, go to
// //navReducer and note how it imports the TabNavigation component to manage its nav state.
// const MockAppNavigator = ({ dispatch, nav, reduxState }) => {
//   console.log(nav);
//   return (
//     <Container>
//       <RawTabNavigator screenProps={reduxState} navigation={addNavigationHelpers({
//         dispatch,
//         state: nav,
//         addListener: addNavReduxListener
//       })}/>
//     </Container>
//   )
// }

// const mapStateToProps = ({nav, ...remainingState}, props) => ({
//   nav: nav,
//   reduxState: remainingState
// });

// export default connect(mapStateToProps)(MockAppNavigator);
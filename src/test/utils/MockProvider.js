// POC: broken poc for mounting and testing connected components with nav. see signInScreen.test.js and setup.js

// import React from 'react';
// import { StyleProvider } from 'native-base';
// import getTheme from '../../../native-base-theme/components';
// import commonColor  from '../../../native-base-theme/variables/commonColor';
// import { Provider } from 'react-redux';
// import { initStore } from '../../store';
// import MockAppNavigator from './MockAppNavigator';
// import reducers from '../../rootReducer';
// import navReducer from '../../general/reducers/navReducer';

// export const connectProviderNavMock = (component, store, navState) => (
//   class ProviderMock extends React.Component {
//     componentDidCatch(error, info) {
//       console.error('GOT ERROR AND INFO IN APP.JS', error, info);
//     }
    
//     render() {
//       return (
//         <Provider store={store}>
//           <StyleProvider style={getTheme(commonColor)}>
//             <MockAppNavigator nav={navState} />
//           </StyleProvider>
//         </Provider>
//       );
//     }
//   }
// );

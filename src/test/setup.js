import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { mockFirebase } from '../test/utils/mocks/mockGeneral';

Enzyme.configure({ adapter: new Adapter() });
mockFirebase();

// to catch unhandled promise rejections, usually from mergeProps / mapDispatchToProps
// https://github.com/facebook/jest/issues/3251
// In Node v7 unhandled promise rejections will terminate the process
if (!process.env.LISTENING_TO_UNHANDLED_REJECTION) {
  process.on('unhandledRejection', reason => {
    throw reason
  });
  // Avoid memory leak by adding too many listeners
  process.env.LISTENING_TO_UNHANDLED_REJECTION = true
}

// POC: broken poc for mounting and testing connected components with nav. see signInScreen.test.js and MockProvider.js
// https://blog.joinroot.com/mounting-react-native-components-with-enzyme-and-jsdom/
// require('react-native-mock-render/mock');
// const { JSDOM } = require('jsdom');
// const jsdom = new JSDOM('<!doctype html><html><body></body></html>');
// const { window } = jsdom;
// function copyProps(src, target) {
//   const props = Object.getOwnPropertyNames(src)
//     .filter(prop => typeof target[prop] === 'undefined')
//     .reduce((result, prop) => ({
//       ...result,
//       [prop]: Object.getOwnPropertyDescriptor(src, prop),
//     }), {});
//   Object.defineProperties(target, props);
// }

// global.window = window;
// global.document = window.document;
// global.navigator = {
//   userAgent: 'node.js',
// };
// copyProps(window, global);
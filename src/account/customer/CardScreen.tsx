import React from 'react';
import { Alert, StyleSheet, Image } from 'react-native';
import { WebView } from 'react-native-webview';
import { connect } from 'react-redux';
import HeaderConfirmButton from '../../general/components/header/HeaderConfirmButton';
import { NavScreenOptions } from '../../general/navModels/NavScreenOptions';
import { confirmSignUpScreenName } from '../signUp/ConfirmSignUpScreen';
import { NavigationRoute, NavigationScreenProp } from 'react-navigation';
import { setNewCardTokenAction, updateCardAction } from '../../general/user/userActions';
import { HiddenCard } from '../../general/user/HiddenCard';
import UserService from '../../general/user/userService';
import SkipButton from '../../general/components/SkipButton';
import { Container } from 'native-base';
import commonColor from '../../../native-base-theme/variables/commonColor';
import { activeConfig } from '../../config';
import { updateRestSubscriptionCard } from '../../general/rest/restActions';
import { showLoading, removeLoading } from '../../general/redux/uiActions';

const onSkip = onAffirm => () => Alert.alert (
  'Skip card?',
  `Are you sure you want to skip card info? You need a card to make mobile payments from Foodflick.`,
  [
    { text: 'Cancel' },
    {
      text: 'Yes',
      onPress: onAffirm
    },
  ],
  { cancelable: false }
);

type props = {
  navigation: NavigationScreenProp<NavigationRoute>;
  onConfirm: (cardToken: string) => void
  showSpinner: () => void,
  hideSpinner: () => void,
  myCard?: HiddenCard,
  onSkip: () => void,
  refetchCard: () => void,
};

class CardScreen extends React.Component<props> {
  static navigationOptions = ({navigation: {state: {params ={}}}}: NavScreenOptions) => ({
    tabBarVisible: false,
    //next means i'm adding a credit card in sign up, otherwise just updating credit with confirmText = Save
    title: params.confirmText === 'Next' ? 'Credit or debit' : 'Update credit or debit',
    headerRight: <HeaderConfirmButton onConfirm={params.onConfirm} text={params.confirmText}/>
  });

  webView = null;

  componentDidMount() {
    this.props.showSpinner();
    this.props.navigation.setParams({
      ...this.props,
      onConfirm: this.submitForm,
    });
  }
  
  submitForm = () => {
    this.webView.injectJavaScript('submit()');
  }

  injectSkipButton = () => {
    if (this.props.onSkip) {
      return '';
    }

    return '';
  }

  render () {
    // might have to do this, https://medium.com/@filipedegrazia/embedding-a-local-website-on-your-expo-react-native-project-eea322738872
    // https://github.com/facebook/react-native/issues/505
    // const { localUri } = Asset.fromModule(require('./pathToFile.html'));
    // source={
    //   Platform
    //   .OS === 'android' ? {
    //     uri: localUri.includes('ExponentAsset') ? localUri : 'file:///android_asset/' + localUri.substr(9),
    //   }
    //   : require(‘./pathToFile.html’)
    // }
    // src\account\customer\card.html
    //submitButton.value = ${params.confirmText === 'Next' ? 'Credit or debit' : 'Update credit or debit'},

    // todo 1: enable default card values
    // let js;
    // if (this.props.myCard) {
    //   js = `
    //     var defaultCardNumber = document.createElement('div');
    //     defaultCardNumber.style.color = ${commonColor.textColor};
    //     defaultCardNumber.innerText = '**** **** **** ${this.props.myCard.last4}';
    //     defaultCardNumber.className = 'defaultNumber';
    //     var numberLabel = document.querySelector('[for=cardNumber]');
    //     numberLabel.style.position = 'static';
    //     numberLabel.insertAdjacentElement('afterend', defaultCardNumber);
    //   `
    // }
    
    // commented out instead of removed because we expect this code to be reused shortly.
    // console.log(require('./card.html'));
    // console.log(Asset.fromModule(require('./card.html')));
    // console.log(Image.resolveAssetSource(require('./card.html')));
    // const uri = Image.resolveAssetSource(require('./card.html')).uri
    // const { localUri, uri } = Asset.fromModule(require('./card.html'));
    return (
      <Container style={styles.background}>
        <WebView
          javaScriptEnabled
          style={{
            flex: 1
          }}
          scrollEnabled={false}
          injectedJavaScript={this.injectSkipButton()}
          ref={ref => this.webView = ref}
          /////////////////////////////////////////////////
          // THIS IS WHAT WE'RE SUPPOSED TO DO BUT THERES A BUG
          // source={{ uri: require('./card.html') }}
          // source={{ uri }}
          // this is hack because the above source props don't work!
          // https://github.com/react-native-community/react-native-webview/issues/630
          // https://github.com/react-native-community/react-native-webview/issues/656
          // https://github.com/react-native-community/react-native-webview/issues/549
          // https://github.com/react-native-community/react-native-webview/issues/392
          // https://github.com/react-native-community/react-native-webview/issues/428
          source={{
            uri: `${activeConfig.app.apiUrl}/card`,
          }}
          /////////////////////////////////////////////////
          onMessage={e => {
            this.props.onConfirm(e.nativeEvent.data);
          }}
          onLoad={() => this.props.hideSpinner()}
        />
        {this.props.onSkip && <SkipButton style={{ flex: 1 }}testId='skipButton' onPress={this.props.onSkip} />}
      </Container>
    )
  }
}

const mapDispatchToProps = (dispatch, props) => {
  const { navigation } = props;
  const { confirmText } = navigation.state.params;
  switch (confirmText) {
    case 'Next': //from signup
      return {
        ...props,
        onSkip: onSkip(() => navigation.navigate(confirmSignUpScreenName)),
        onConfirm: async cardToken => {
          dispatch(setNewCardTokenAction(cardToken));
          navigation.navigate(confirmSignUpScreenName);          
        }
      }
    case 'Save': // from updating rest subscription
      return {
        ...props,
        showSpinner: () => dispatch(showLoading()),
        hideSpinner: () => dispatch(removeLoading()),
        onConfirm: async cardToken => {
          await dispatch(updateRestSubscriptionCard(cardToken));
          navigation.goBack();
        }
      }
    default:
      //todo 1. throw errow. bad props.    
      return {}
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  }
});

export default connect(null, mapDispatchToProps)(UserService.getMyCardInjector(CardScreen));

export const cardScreenName = 'cardScreen';

const HTML = `
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://js.stripe.com/v3/"></script>

  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Source+Code+Pro" rel="stylesheet">

  <link rel="stylesheet" type="text/css" href="card.css" data-rel-css="" />
</head>
<body>
  <div class="formContainer">
    <form>
      <div class="row">
        <div class="field">
          <div id="cardNumber" class="input empty"></div>
          <label for="cardNumber">Card number</label>
          <div class="baseline"></div>
        </div>
      </div>
      <div class="row">
        <div class="field">
          <div id="cardExpiry" class="input empty"></div>
          <label for="cardExpiry">Expiration</label>
          <div class="baseline"></div>
        </div>
      </div>
      <div class="row">
        <div class="field">
          <div id="cardCvc" class="input empty"></div>
          <label for="cardCvc">CVC</label>
          <div class="baseline"></div>
        </div>
      </div>
      <div class="error" role="alert">
        <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17">
          <path class="base" fill="#000" d="M8.5,17 C3.80557963,17 0,13.1944204 0,8.5 C0,3.80557963 3.80557963,0 8.5,0 C13.1944204,0 17,3.80557963 17,8.5 C17,13.1944204 13.1944204,17 8.5,17 Z"></path>
          <path class="glyph" fill="#FFF" d="M8.5,7.29791847 L6.12604076,4.92395924 C5.79409512,4.59201359 5.25590488,4.59201359 4.92395924,4.92395924 C4.59201359,5.25590488 4.59201359,5.79409512 4.92395924,6.12604076 L7.29791847,8.5 L4.92395924,10.8739592 C4.59201359,11.2059049 4.59201359,11.7440951 4.92395924,12.0760408 C5.25590488,12.4079864 5.79409512,12.4079864 6.12604076,12.0760408 L8.5,9.70208153 L10.8739592,12.0760408 C11.2059049,12.4079864 11.7440951,12.4079864 12.0760408,12.0760408 C12.4079864,11.7440951 12.4079864,11.2059049 12.0760408,10.8739592 L9.70208153,8.5 L12.0760408,6.12604076 C12.4079864,5.79409512 12.4079864,5.25590488 12.0760408,4.92395924 C11.7440951,4.59201359 11.2059049,4.59201359 10.8739592,4.92395924 L8.5,7.29791847 L8.5,7.29791847 Z"></path>
        </svg>
        <span class="message"></span>
      </div>
    </form>
    <div class="success">
      <div class="icon">
        <svg width="84px" height="84px" viewBox="0 0 84 84" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <circle class="border" cx="42" cy="42" r="40" stroke-linecap="round" stroke-width="4" stroke="#000" fill="none"></circle>
        </svg>
      </div>
    </div>
  </div>

  <script src="card.js" data-rel-js></script>

</body>
</html>
`
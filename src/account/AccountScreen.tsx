import React from 'react';
import { StyleSheet, Dimensions, SafeAreaView } from 'react-native';
import { connect } from 'react-redux';
import { Container, Button, Content, Text, Icon } from 'native-base';
import UndividedList from '../general/components/list/UndividedList';
import commonColor from '../../native-base-theme/variables/commonColor';
import { myRestsScreenName } from './myRest/MyRestsScreen';
import { NavigationRoute, NavigationScreenProp } from 'react-navigation';
import { settingsScreenName } from './settings/SettingsScreen';
import { addMyFlicksScreenName } from './myRest/AddMyFlicksScreen';
import { RootState } from '../general/redux/RootState';
import { SignedInUser } from '../general/user/SignedInUser';
import { clearSignedInUser } from '../general/user/userActions';
import SignInScreen from './SignInScreen';

type props = {
  signedInUser: SignedInUser;
  navigation: NavigationScreenProp<NavigationRoute>;
  signOutUser: () => void;
}

class AccountScreen extends React.Component<props, any> {
  static navigationOptions = {
    header: null
  };

  render() {
    const { signOutUser, signedInUser, navigation } = this.props;
    const navigate = navigation.navigate;

    let items = [
      // {
      //   leftIcon: 'warning',
      //   text: 'Report a problem',
      //   props: {onPress: () => {}} //todo0 report a problem;
      // }
    ];

    // removed settings for now so we dont have to worry abotu updating email + email references
    // if (signedInUser) items.unshift({
    //   leftIcon: 'construct',
    //   text: 'Settings',
    //   props: { onPress: () => navigate(settingsScreenName) }
    // });

    let PersonIcon = null;
    let SignIn = null;
    let contentContainerStyle = null;
    let listStyle = null;

    if (signedInUser) {
      items.push({
        leftIcon: 'log-out',
        text: 'Sign out',
        props: { onPress: signOutUser }
      });
      const { perms } = signedInUser;
      if (perms && perms.includes('write:rests')) {
        items.unshift({
          leftIcon: 'images',
          text: 'Add photos',
          props: { onPress: () => navigate(addMyFlicksScreenName) }
        });
        items.unshift({
          leftIcon: 'restaurant',
          text: 'My restaurants',
          props: { onPress: () => navigate(myRestsScreenName) }
        });
      }
    } else {
      contentContainerStyle = styles.content;
      listStyle = styles.list;
      PersonIcon = <Icon name='person' style={styles.signInIcon} />;
      SignIn = (
        <Button block onPress={() => navigate('signInScreen')}>
          <Text>Sign in</Text>
        </Button>
      )
    }
    return (
      <Container>
        <SafeAreaView style={{ flex: 1 }}>
          {!signedInUser && <SignInScreen navigation={navigation} />}
          {signedInUser &&
            <Content contentContainerStyle={contentContainerStyle}>
              {PersonIcon}
              {SignIn}
              <UndividedList style={listStyle} items={items} />
            </Content>}
        </SafeAreaView>
      </Container>
    )
  }
};

const styles = StyleSheet.create({
  content: {
    flex: 1,
    justifyContent: 'space-between'
  },
  signInIcon: {
    fontSize: Dimensions.get("window").height / 2,
    color: '#c6c6c6',
    alignSelf: 'center',
  },
  list: {
    borderTopColor: commonColor.listBorderColor,
    borderTopWidth: 1
  }
});

const mapDispatchToProps = dispatch => ({
  signOutUser: () => dispatch(clearSignedInUser())
});

const mapStateToProps = (state: RootState) => ({
  signedInUser: state.getSignedInUser(),
});

export default connect(mapStateToProps, mapDispatchToProps)(AccountScreen);
const accountScreenName = 'accountScreen';
export { AccountScreen, accountScreenName };
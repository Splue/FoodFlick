import React from 'react';
import PropTypes from 'prop-types';
import { Image, StyleSheet, Animated, Dimensions } from 'react-native';
import { addKeyboardListeners, removeKeyboardListeners } from '../general/utils/keyboard';
import Divider from '../general/components/Divider';
import { Container, Text, Form, Item, Label, Input, Button, Toast } from 'native-base';
import { connect } from 'react-redux';
// import Hr from 'react-native-hr';
import { navigationPropType } from '../general/utils/propTypes/general';
import { signInWithBasicAction } from '../general/user/userActions';

//todo 2: figure out how to change the picture when keyboard shows.

const IMAGE_HEIGHT = 170;
const IMAGE_HEIGHT_SMALL = 50;

class SignInScreen extends React.Component {
  static propTypes = {
    signIn: PropTypes.func.isRequired,
    navigation: navigationPropType,
  }

  static navigationOptions = {
    tabBarVisible: false
  };
  
  keyboardHeight = new Animated.Value(0);
  imageHeight = new Animated.Value(IMAGE_HEIGHT);
  state = { 
    email: '',
    password: '',
  };

  componentWillMount () { addKeyboardListeners.bind(this)(this.keyboardDidShow, this.keyboardDidHide) }
  
  componentWillUnmount() { removeKeyboardListeners.bind(this)() }
  
  keyboardDidShow = (event) => {
    //animated.parallel starts X animations at the same time. in this case, 2.
    //the first animation slowly changes this.keyboardHeight to event.endCoordinates.height while
    //the second animation changes this.imageHeight to IMAGE_HEIGHT_SMALL
    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        toValue: event.endCoordinates.height,
      }),
      Animated.timing(this.imageHeight, {
        toValue: IMAGE_HEIGHT_SMALL,
      }),
    ]).start();
  };

  keyboardDidHide = () => {
    Animated.parallel([
      Animated.timing(this.keyboardHeight, {
        toValue: 0,
      }),
      Animated.timing(this.imageHeight, {
        toValue: IMAGE_HEIGHT,
      }),
    ]).start();
  };

  render () {
    // const AnimatedContainer = Animated.createAnimatedComponent(Container);
    const width = Dimensions.get("window").width;
    return (
      <Container>
        <Animated.Image source={require('../../assets/foodflick800.png')} resizeMode="contain" style={{height: this.imageHeight, width}} />
        <Form style={styles.marginHorizontal}>
          <Item floatingLabel>
            <Label>Email</Label>
            <Input value={this.state.email} onChangeText={email => this.setState({email})}/>
          </Item>
          <Item floatingLabel last>
            <Label>Password</Label>
            <Input value={this.state.password} secureTextEntry onChangeText={password => this.setState({password})}/>
          </Item>
          <Button testId='signIn' full style={styles.marginTop}
          onPress={() => this.props.signIn(this.state.email, this.state.password)}>
            <Text>Sign in</Text>
          </Button>
          {/* <Button full transparent>
            <Text style={styles.blue}>Forgot password?</Text>
          </Button> */}
        </Form>
        <Divider centerText='First time?' />
        {/* <Hr lineColor={commonColors.brandDivider} text='First time?' textColor={commonColors.brandDivider}/> */}
        <Button testId='signUp' block onPress={() => this.props.navigation.navigate('accountPickerScreen')}>
          <Text>Sign up</Text>
        </Button>
      </Container>
    )
  }
}

const MARGIN_SIZE = 10;
const styles = StyleSheet.create({
  marginHorizontal: {
    marginHorizontal: MARGIN_SIZE
  },
  marginTop: {
    marginTop: MARGIN_SIZE
  },
  blue: {
    color: '#6fa3f7'
  }
});

const mapDispatchToProps = (dispatch, { navigation }) => ({
  signIn: async (email, password) => {
    try {
      await dispatch(signInWithBasicAction(email, password));
      navigation.goBack();
    } catch(e) {
      Toast.show({
        text: e.error_description,
        type: 'warning',
        buttonText: 'Okay',
        position: 'bottom'
      });
    }
  }
})

export default connect(null, mapDispatchToProps)(SignInScreen);
const signInScreenName = 'signInScreen';
export { SignInScreen, signInScreenName };
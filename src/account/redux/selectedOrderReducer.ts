import { OrderActions } from "../../general/order/redux/orderActions";
import { Order } from "../../general/order/OrderModel";

type action = {
  type: OrderActions
  order: Order
}

export function selectedOrder(state = null, action: action) {
  switch(action.type) {
    case OrderActions.SELECT_ORDER_ACTION:
      return action.order
    default:
      return state
  }
}
import { ManagerRest } from '../../general/rest/ManagerRest';
import { ManagerCategory, ManagerItem } from "../../general/menu/ManagerMenu";
import { NewRest } from './NewRest';
import { Order } from '../../general/order/OrderModel';

export class AccountState {
  readonly newRest: NewRest;
  readonly selectedRest: ManagerRest;
  readonly selectedCategory: ManagerCategory;
  readonly selectedItems: ManagerItem[];
  readonly selectedOrder: Order;

  public constructor(
    newRest: NewRest,
    selectedRest: ManagerRest,
    selectedCategory: ManagerCategory,
    selectedItems: ManagerItem[],
    selectedOrder: Order) {

    this.newRest = newRest;
    this.selectedRest = selectedRest;
    this.selectedCategory = selectedCategory;
    this.selectedItems = selectedItems;
    this.selectedOrder = selectedOrder;
  }

  getNewRest = (): NewRest => this.newRest;
  
  getSelectedRest = (): ManagerRest => this.selectedRest;
  
  getSelectedCategory = (): ManagerCategory => this.selectedCategory;
  
  getSelectedItems = (): ManagerItem[] => this.selectedItems;

  getSelectedOrder = (): Order => this.selectedOrder;
};
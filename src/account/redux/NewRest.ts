import { Profile, Location } from '../../general/rest/BaseRest';

export class NewRest {
  readonly profile: Profile;
  readonly location: Location;
};
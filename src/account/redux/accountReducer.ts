import { AccountState } from './AccountState';
import { selectedRest } from '../../general/rest/restReducer';
import { selectedCategory, selectedItems } from '../../general/menu/menuReducer';
import { SELECT_REST } from '../../general/rest/restActions';
import { STORE_NEW_REST_LOCATION, STORE_NEW_REST_PROFILE, STORE_NEW_REST_BANKING } from '../../general/rest/restActions';
import { selectedOrder } from './selectedOrderReducer';

export const ACCOUNT_TAB: string = 'ACCOUNT_TAB';

const newRest = (state = null, action: any = {}) => {
  switch(action.type) {
    case STORE_NEW_REST_BANKING:
      return {...state, banking: { ...action.banking }}
    case STORE_NEW_REST_PROFILE:
      return {...state, profile: { ...action.profile }}
    case STORE_NEW_REST_LOCATION:
      return {...state, location: { ...action.location }}
    case SELECT_REST:
      return null;
    default:
      return state;
  }
}

const initialState: AccountState = new AccountState(null, null, null, null, null);

export const accountReducer = (state: AccountState = initialState, action: any = {}) => {
  switch(action.tab) {
    case ACCOUNT_TAB:
      return new AccountState(
        newRest(state.getNewRest(), action),
        selectedRest(state.getSelectedRest(), action),
        selectedCategory(state.getSelectedCategory(), action),
        selectedItems(state.getSelectedItems(), action),
        selectedOrder(state.getSelectedOrder(), action),
      );
    default:
      return state
  }
}

import React from 'react';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import HeaderConfirmButton from '../../../../general/components/header/HeaderConfirmButton';
import { RootState } from '../../../../general/redux/RootState';
import { addRestPrinter } from '../../../../general/rest/restActions';
import { Printer } from '../../../../general/rest/ManagerRest';
import { getCleanInputs } from '../../../../general/utils/formHelpers';
import PrinterDetailsScreen from './PrinterDetailsScreen';

type props = {
  onConfirm: (printer: any) => void,
};

const AddPrinterDetailsScreen: NavigationScreenComponent<any, any, props> = ({ onConfirm }) =>  (
  <PrinterDetailsScreen onConfirm={onConfirm} />
)

AddPrinterDetailsScreen.navigationOptions = ({ navigation }) => ({
  tabBarVisible: false,
  title: 'Add printer',
  headerRight: <HeaderConfirmButton onConfirm={navigation.getParam('onConfirm')} text="Add" />
});

const mapStateToProps = (state: RootState) => ({
  selectedRest: state.getAccount().getSelectedRest(),
});

const mergeProps = ({ selectedRest }, { dispatch }, props) => {
  const {navigation} = props;
  return {
    ...props,
    onConfirm: async (printer: Printer) => {
      const cleanPrinter = getCleanInputs(printer) as Printer;
      await dispatch(addRestPrinter(selectedRest._id, cleanPrinter));
      navigation.goBack();
    }
  }
}

export default connect(mapStateToProps, null, mergeProps)(AddPrinterDetailsScreen);

export const addPrinterDetailsScreenName = 'addPrinterDetailsScreen';
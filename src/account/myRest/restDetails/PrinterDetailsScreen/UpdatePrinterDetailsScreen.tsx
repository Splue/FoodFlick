import React from 'react';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import HeaderConfirmButton from '../../../../general/components/header/HeaderConfirmButton';
import { RootState } from '../../../../general/redux/RootState';
import { updateRestPrinter } from '../../../../general/rest/restActions';
import { Printer, UpdatePrinter } from '../../../../general/rest/ManagerRest';
import { getCleanInputs } from '../../../../general/utils/formHelpers';
import PrinterDetailsScreen from './PrinterDetailsScreen';
import { showLoading, removeLoading } from '../../../../general/redux/uiActions';

type navParams = {
  printer: UpdatePrinter;
}

type props = {
  onConfirm: (printer: any) => void,
};

const UpdatePrinterDetailsScreen: NavigationScreenComponent<navParams, any, props> = ({ navigation, onConfirm }) => {
  const originalPrinter: UpdatePrinter = navigation.getParam('printer');
  const onConfirmFinal = (printer: Printer) => onConfirm({
    index: originalPrinter.index,
    printer,
  });
  return (
    <PrinterDetailsScreen defaultPrinter={originalPrinter.printer} onConfirm={onConfirmFinal} />
  )
}

UpdatePrinterDetailsScreen.navigationOptions = ({ navigation }) => ({
  tabBarVisible: false,
  title: 'Update printer',
  headerRight: <HeaderConfirmButton onConfirm={navigation.getParam('onConfirm')} text="Save" />
});

const mapStateToProps = (state: RootState) => ({
  selectedRest: state.getAccount().getSelectedRest(),
});

const mergeProps = ({ selectedRest }, { dispatch }, props) => {
  const {navigation} = props;
  return {
    ...props,
    onConfirm: async (printer: UpdatePrinter) => {
      dispatch(showLoading());
      const cleanPrinter = getCleanInputs(printer) as UpdatePrinter;
      await dispatch(updateRestPrinter(selectedRest._id, cleanPrinter));
      navigation.goBack();
      dispatch(removeLoading());
    }
  }
}

export default connect(mapStateToProps, null, mergeProps)(UpdatePrinterDetailsScreen);

export const updatePrinterDetailsScreenName = 'updatePrinterDetailsScreen';
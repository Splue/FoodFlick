import { Item, Label, Text, Button, Container, Content, Segment, CheckBox } from 'native-base';
import React, { useState, useRef } from 'react';
import { StyleSheet, View } from 'react-native';
import ValidatedForm from '../../../../general/components/ValidatedForm';
import commonColor from '../../../../../native-base-theme/variables/commonColor';
import { Printer, PrinterType } from '../../../../general/rest/ManagerRest';
import ValidatedInput from '../../../../general/components/ValidatedInput';

type props = {
  onConfirm: (printer: Printer) => void,
  defaultPrinter?: Printer
};

const PrinterDetailsScreen: React.FunctionComponent<props> = ({ defaultPrinter = {} as Printer, onConfirm }) => {
  const [name, setName] = useState(defaultPrinter.name);
  const [ip, setIp] = useState(defaultPrinter.ip);
  const [port, setPort] = useState(defaultPrinter.port);
  const [type, setType] = useState(defaultPrinter.type || PrinterType.EPSON);
  const [isReceipt, setReceipt] = useState(defaultPrinter.isReceipt || false);
  const inputs = {
    name: (
      <ValidatedInput
        floatingLabel
        required
        label='Name'
        value={name}
        onChangeText={name => setName(name)}
      />
    ) as unknown as ValidatedInput,
    ip: (
      <ValidatedInput
        floatingLabel
        required
        inputProps={{
          keyboardType: 'numeric',
        }}
        label='Ip'
        value={ip}
        onChangeText={ip => setIp(ip)}
      />
    ) as unknown as ValidatedInput,
    port: (
      <ValidatedInput
        floatingLabel
        required
        inputProps={{
          keyboardType: 'numeric',
        }}
        label='Port'
        value={port}
        onChangeText={port => setPort(port)}
      />
    ) as unknown as ValidatedInput
  }
  // https://github.com/facebook/react/issues/14010
  const refPrinter = useRef({ name, ip, port, type, isReceipt });
  refPrinter.current = { name, ip, port, type, isReceipt }
  return (
    <Container style={styles.background}>
      <Content>
        <ValidatedForm onConfirm={() => onConfirm(refPrinter.current)} originalInputs={inputs} render={newInputs => (
          <React.Fragment>
            {newInputs.name}
            {newInputs.ip}
            {newInputs.port}
            <Item fixedLabel style={styles.type}>
              <Label>Type</Label>
              <Segment>
                <Button first active={type === PrinterType.EPSON} onPress={() => setType(PrinterType.EPSON)}>
                  <Text>Epson</Text>
                </Button>
                <Button last active={type === PrinterType.STAR} onPress={() => setType(PrinterType.STAR)}>
                  <Text>Star</Text>
                </Button>
              </Segment>
            </Item>
            <Item fixedLabel style={styles.receipt}>
              <View>
                <Label>Receipt</Label>
                <Text note>Make all orders print here</Text>
              </View>
              <CheckBox checked={isReceipt} onPress={() => setReceipt(!isReceipt)} style={styles.checkbox}/>
            </Item>
          </React.Fragment>
        )} />
      </Content>
    </Container>
  )
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  },
  checkbox: {
    left: -commonColor.formInputMarginLeft
  },
  receipt: {
    marginTop: commonColor.listItemPadding,
    justifyContent: 'space-between',
    borderBottomWidth: 0,
  },
  type: {
    marginTop: commonColor.listItemPadding,
    flexDirection: 'column',
    alignItems: 'flex-start',
    borderBottomWidth: 0
  },
});

export default PrinterDetailsScreen;
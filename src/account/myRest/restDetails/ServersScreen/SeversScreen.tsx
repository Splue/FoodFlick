import React from 'react';
import { connect } from 'react-redux';
import { deleteRestServerAction, addRestServerAction } from '../../../../general/rest/restActions';
import EmailListEditor from '../../../../general/components/list/EmailListEditor';
import { NavigationScreenComponent } from 'react-navigation';
import { RootState } from '../../../../general/redux/RootState';
import { ManagerRestSelector, User } from '../../../../general/rest/ManagerRest';
import { showLoading, removeLoading } from '../../../../general/redux/uiActions';

type props = {
  servers: User[],
  onDelete: (email: string) => void,
  onAdd: () => void
}

const ServersScreen: NavigationScreenComponent<any, any, props> = ({ servers, onDelete, onAdd }) => (
  <EmailListEditor
    addButtonText='Add servers'
    users={servers}
    onAdd={onAdd}
    onDelete={onDelete}
    emptyEmailsText='No servers. Start by adding some servers'
  />
);

ServersScreen.navigationOptions = () => ({
  title: 'Edit servers'
});

const mapStateToProps = (state: RootState) => ({
  restId: ManagerRestSelector.getId(state.getAccount().getSelectedRest()),
  servers: ManagerRestSelector.getServers(state.getAccount().getSelectedRest())
});

const mergeProps = ({ restId, servers }, { dispatch }, { navigation }) => ({
  servers,
  onDelete: async userId => await dispatch(deleteRestServerAction(restId, userId)),
  onAdd: () => {
    navigation.navigate('createEmailScreen', {
      shouldNotCheckIfUserExists: true,
      confirmText: 'Add',
      onConfirm: async serverEmail => {
        try {
          dispatch(showLoading());
          await dispatch(addRestServerAction(restId, serverEmail));
          navigation.goBack();
          dispatch(removeLoading());
        } catch (e) {} // empty catch to prevent error crashing
      }
    })
  },
})

export default connect(mapStateToProps, null, mergeProps)(ServersScreen);

export const serversScreenName = 'serversScreen';
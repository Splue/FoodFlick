import React, { useState, useRef } from 'react';
import { StyleSheet } from 'react-native';
import ValidatedInput from '../../../../general/components/ValidatedInput';
import { Container, Content } from 'native-base';
import ValidatedForm from '../../../../general/components/ValidatedForm';
import commonColor from '../../../../../native-base-theme/variables/commonColor';

type props = {
  onConfirm: (string: string) => void,
  defaultTableId?: string
};

const TableInput: React.FunctionComponent<props> = ({ defaultTableId, onConfirm }) => {
  const [tableId, setTableId] = useState(defaultTableId);
  const inputs = {
    table: (
      <ValidatedInput
        floatingLabel
        required
        label='Table #'
        value={tableId}
        onChangeText={tableId => setTableId(tableId)}
      />
    ) as unknown as ValidatedInput,
  };
  // https://github.com/facebook/react/issues/14010
  const refTableId = useRef(tableId);
  refTableId.current = tableId;
  return (
    <Container style={styles.background}>
      <Content>
        <ValidatedForm onConfirm={() => onConfirm(refTableId.current)} originalInputs={inputs} render={newInputs => (
          <React.Fragment>
            {newInputs.table}
          </React.Fragment>
        )} />
      </Content>
    </Container>
  )
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  },
});

export default TableInput;
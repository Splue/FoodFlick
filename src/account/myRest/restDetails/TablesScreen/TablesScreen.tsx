import { Text, Button, Container, ListItem, Left, Toast } from 'native-base';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import { RootState } from '../../../../general/redux/RootState';
import { ManagerRestSelector, Table, User } from '../../../../general/rest/ManagerRest';
import commonColor from '../../../../../native-base-theme/variables/commonColor';
import CenteredH3 from '../../../../general/components/CenteredH3';
import { addTableScreenName } from './AddTableScreen';
import { showLoading, removeLoading } from '../../../../general/redux/uiActions';
import { updateRestTableCheckInAction, deleteRestTable } from '../../../../general/rest/restActions';
import EditableList from '../../../../general/components/list/EditableList';
import { updateTableScreenName } from './UpdateTableScreen';

type props = {
  currTables: Table[];
  availableServers: User[],
  availableManagers: User[],
  isManager: boolean,
  owner: User,
  onCheckIn: (tableId: string) => void,
  onDeleteTable: (tableId: string) => void,
};

const TablesScreen: NavigationScreenComponent<any, any, props> = ({
  availableManagers,
  availableServers,
  onCheckIn,
  onDeleteTable,
  owner,
  currTables,
  isManager,
  navigation,
}) => {
  const onPressAdd = () => navigation.navigate(addTableScreenName);
  const getEmailFromId = userId => {
    let targetUser = availableServers.find(server => server.userId === userId);
    if (targetUser) return targetUser.email;
    targetUser = availableManagers.find(manager => manager.userId === userId);
    if (targetUser) return targetUser.email;
    if (owner.userId === userId) return owner.email;
    return null;
  }
  
  const editableItems = [{
    icon: 'checkmark-circle-outline',
    text: 'Check me in',
    onPress: (table: Table, index, clearActiveItem) => {
      onCheckIn(table._id);
      clearActiveItem();
    }
  }]

  if (isManager) {
    editableItems.unshift({
      icon: 'create',
      text: 'Update table',
      onPress: (table: Table, index, clearActiveItem) => {
        clearActiveItem();
        navigation.navigate(updateTableScreenName, {
          prevTableId: table._id
        });
      }
    })
    editableItems.push({
      icon: 'trash',
      text: 'Delete',
      onPress: (table, index, clearActiveItem) => {
        onDeleteTable(table._id);
        clearActiveItem();
      }
    });
  }

  return (currTables.length === 0 ?
    <Container>
      <CenteredH3 text={`No tables. ${isManager ? 'Start by adding a table' : 'Ask a manager to add tables'}`} />
      {isManager && 
      <Button primary block style={styles.button} onPress={onPressAdd}>
        <Text>Add table</Text>
      </Button>}
    </Container>
    :
    <Container>
      {isManager &&
      <Button transparent primary style={styles.button} onPress={onPressAdd}>
        <Text>Add table</Text>
      </Button>}
      <EditableList
        list={currTables}
        editableItems={editableItems}
        renderItem={(table: Table, index, setActive) => (
          <ListItem key={table._id} button onPress={setActive}>
            <Left>
              <View style={styles.column}>
                <Text style={styles.left}>{table._id}</Text>
                <Text note style={styles.left}>{getEmailFromId(table.userId)}</Text>
              </View>
            </Left>
          </ListItem>
        )}
      />
    </Container>
  )
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: commonColor.brandCanvas,
  },
  button: {
    alignSelf: 'center',
    marginVertical: 8,
  },
  column: {
    flexDirection: 'column',
  },
  left: {
    marginLeft: 0,
    alignSelf: 'flex-start',
  },
});

TablesScreen.navigationOptions = ({ navigation }) => ({
  tabBarVisible: false,
  title: 'Table check-in',
});

const mapStateToProps = (state: RootState) => ({
  currTables: ManagerRestSelector.getTables(state.getAccount().getSelectedRest()),
  availableServers: ManagerRestSelector.getServers(state.getAccount().getSelectedRest()),
  availableManagers: ManagerRestSelector.getManagers(state.getAccount().getSelectedRest()),
  isManager: ManagerRestSelector.isManager(state.getAccount().getSelectedRest(), state.getSignedInUser()),
  owner: ManagerRestSelector.getOwner(state.getAccount().getSelectedRest()),
});

const mapDispatchToProps = dispatch => ({
  onCheckIn: async tableId => {
    dispatch(showLoading());
    await dispatch(updateRestTableCheckInAction(tableId));
    dispatch(removeLoading());
    Toast.show({
      text: 'Checked-in',
      buttonText: 'Okay',
      position: 'bottom',
      duration: 5000,
    });
  },
  onDeleteTable: async tableId => {
    dispatch(showLoading());
    await dispatch(deleteRestTable(tableId));
    dispatch(removeLoading());
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(TablesScreen);

export const tablesScreenName = 'tablesScreen';
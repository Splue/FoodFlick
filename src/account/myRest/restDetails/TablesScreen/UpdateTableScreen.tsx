import React from 'react';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import HeaderConfirmButton from '../../../../general/components/header/HeaderConfirmButton';
import { updateRestTableAction } from '../../../../general/rest/restActions';
import { getCleanInput } from '../../../../general/utils/formHelpers';
import TableInputScreen from './TableInputScreen';
import { showLoading, removeLoading } from '../../../../general/redux/uiActions';

type props = {
  onConfirm: (receiverId: string) => void,
  tableId: string,
};

type navParams = {
  prevTableId: string
}

const UpdateTableScreen: NavigationScreenComponent<navParams, any, props> = ({ onConfirm, navigation }) =>  (
  <TableInputScreen onConfirm={onConfirm} defaultTableId={navigation.getParam('prevTableId')} />
);

UpdateTableScreen.navigationOptions = ({ navigation }) => ({
  tabBarVisible: false,
  title: 'Update table',
  headerRight: <HeaderConfirmButton onConfirm={navigation.getParam('onConfirm')} text='Save' />
});


const mapDispatchToProps = (dispatch, props) => {
  const {navigation} = props;
  return {
    onConfirm: async (newTableId: string) => {
      dispatch(showLoading());
      const cleanTableId = getCleanInput(newTableId)
      await dispatch(updateRestTableAction(navigation.getParam('prevTableId'), cleanTableId));
      navigation.goBack();
      dispatch(removeLoading());
    }
  }
}

export default connect(null, mapDispatchToProps)(UpdateTableScreen);

export const updateTableScreenName = 'updateTableScreen';
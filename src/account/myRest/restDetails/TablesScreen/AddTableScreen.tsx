import React from 'react';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import HeaderConfirmButton from '../../../../general/components/header/HeaderConfirmButton';
import { RootState } from '../../../../general/redux/RootState';
import { addRestTableAction } from '../../../../general/rest/restActions';
import { getCleanInput } from '../../../../general/utils/formHelpers';
import TableInputScreen from './TableInputScreen';
import { showLoading, removeLoading } from '../../../../general/redux/uiActions';

type props = {
  onConfirm: (receiverId: string) => void,
};

const AddTableScreen: NavigationScreenComponent<any, any, props> = ({ onConfirm }) =>  (
  <TableInputScreen onConfirm={onConfirm} />
)

AddTableScreen.navigationOptions = ({ navigation }) => ({
  tabBarVisible: false,
  title: 'Add table',
  headerRight: <HeaderConfirmButton onConfirm={navigation.getParam('onConfirm')} text='Add' />
});

const mapStateToProps = (state: RootState) => ({
  selectedRest: state.getAccount().getSelectedRest(),
});

const mergeProps = ({ selectedRest }, { dispatch }, props) => {
  const {navigation} = props;
  return {
    ...props,
    onConfirm: async (tableId: string) => {
      dispatch(showLoading());
      const cleanTableId = getCleanInput(tableId)
      await dispatch(addRestTableAction(selectedRest._id, cleanTableId));
      navigation.goBack();
      dispatch(removeLoading());
    }
  }
}

export default connect(mapStateToProps, null, mergeProps)(AddTableScreen);

export const addTableScreenName = 'addTableScreen';
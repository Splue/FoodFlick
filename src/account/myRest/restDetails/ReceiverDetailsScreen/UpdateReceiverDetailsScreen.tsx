import React from 'react';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import HeaderConfirmButton from '../../../../general/components/header/HeaderConfirmButton';
import { RootState } from '../../../../general/redux/RootState';
import { updateRestReceiver } from '../../../../general/rest/restActions';
import { getCleanInput } from '../../../../general/utils/formHelpers';
import ReceiverDetails from './ReceiverDetailsScreen';
import { ManagerRestSelector } from '../../../../general/rest/ManagerRest';
import { showLoading, removeLoading } from '../../../../general/redux/uiActions';

type props = {
  onConfirm: (receiverId: string) => void,
  receiverId: string,
};

const UpdateReceiverDetailsScreen: NavigationScreenComponent<any, any, props> = ({ onConfirm, receiverId }) =>  (
  <ReceiverDetails onConfirm={onConfirm} defaultReceiver={receiverId} />
)

UpdateReceiverDetailsScreen.navigationOptions = ({ navigation }) => ({
  tabBarVisible: false,
  title: 'Update receiver',
  headerRight: <HeaderConfirmButton onConfirm={navigation.getParam('onConfirm')} text="Save" />
});

const mapStateToProps = (state: RootState) => ({
  selectedRest: state.getAccount().getSelectedRest(),
});

const mergeProps = ({ selectedRest }, { dispatch }, props) => {
  const {navigation} = props;
  return {
    ...props,
    receiverId: ManagerRestSelector.getReceiverId(selectedRest),
    onConfirm: async (receiverId: string) => {
      dispatch(showLoading());
      const cleanReceiverId = getCleanInput(receiverId)
      await dispatch(updateRestReceiver(selectedRest._id, cleanReceiverId));
      navigation.goBack();
      dispatch(removeLoading());
    }
  }
}

export default connect(mapStateToProps, null, mergeProps)(UpdateReceiverDetailsScreen);

export const updateReceiverDetailsScreenName = 'updateReceiverDetailsScreen';
import React from 'react';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import HeaderConfirmButton from '../../../../general/components/header/HeaderConfirmButton';
import { RootState } from '../../../../general/redux/RootState';
import { addRestReceiver } from '../../../../general/rest/restActions';
import { getCleanInput } from '../../../../general/utils/formHelpers';
import ReceiverDetails from './ReceiverDetailsScreen';
import { showLoading, removeLoading } from '../../../../general/redux/uiActions';

type props = {
  onConfirm: (receiverId: string) => void,
};

const AddReceiverDetailsScreen: NavigationScreenComponent<any, any, props> = ({ onConfirm }) =>  (
  <ReceiverDetails onConfirm={onConfirm} />
)

AddReceiverDetailsScreen.navigationOptions = ({ navigation }) => ({
  tabBarVisible: false,
  title: 'Add receiver',
  headerRight: <HeaderConfirmButton onConfirm={navigation.getParam('onConfirm')} text="Add" />
});

const mapStateToProps = (state: RootState) => ({
  selectedRest: state.getAccount().getSelectedRest(),
});

const mergeProps = ({ selectedRest }, { dispatch }, props) => {
  const {navigation} = props;
  return {
    ...props,
    onConfirm: async (receiverId: string) => {
      dispatch(showLoading());
      const cleanReceiverId = getCleanInput(receiverId)
      await dispatch(addRestReceiver(selectedRest._id, cleanReceiverId));
      navigation.goBack();
      dispatch(removeLoading());
    }
  }
}

export default connect(mapStateToProps, null, mergeProps)(AddReceiverDetailsScreen);

export const addReceiverDetailsScreenName = 'addReceiverDetailsScreen';
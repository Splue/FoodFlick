import React, { useState, useRef } from 'react';
import { StyleSheet } from 'react-native';
import ValidatedInput from '../../../../general/components/ValidatedInput';
import { Container, Content } from 'native-base';
import ValidatedForm from '../../../../general/components/ValidatedForm';
import commonColor from '../../../../../native-base-theme/variables/commonColor';

type props = {
  onConfirm: (string: string) => void,
  defaultReceiver?: string
};

const ReceiverDetails: React.FunctionComponent<props> = ({ defaultReceiver, onConfirm }) => {
  const [receiverId, setReceiverId] = useState(defaultReceiver);
  const inputs = {
    receiverId: (
      <ValidatedInput
        floatingLabel
        required
        label='Receiver id'
        value={receiverId}
        onChangeText={receiverId => setReceiverId(receiverId)}
      />
    ) as unknown as ValidatedInput,
  };
  // https://github.com/facebook/react/issues/14010
  const refReceiverId = useRef(receiverId);
  refReceiverId.current = receiverId;
  return (
    <Container style={styles.background}>
      <Content>
        <ValidatedForm onConfirm={() => onConfirm(refReceiverId.current)} originalInputs={inputs} render={newInputs => (
          <React.Fragment>
            {newInputs.receiverId}
          </React.Fragment>
        )} />
      </Content>
    </Container>
  )
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  },
});

export default ReceiverDetails;
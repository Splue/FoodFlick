import { differenceWith, without } from 'lodash';
import { Input, Item, Label, Text, Icon, Button, Container, Content } from 'native-base';
import React from 'react';
import { StyleSheet, View, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import { NavigationRoute, NavigationScreenProp } from 'react-navigation';
import ValidatedForm from '../../../general/components/ValidatedForm';
import HeaderConfirmButton from '../../../general/components/header/HeaderConfirmButton';
import { NavScreenOptions } from '../../../general/navModels/NavScreenOptions';
import { RootState } from '../../../general/redux/RootState';
import { storeNewRestProfileAction, updateRestProfileAction } from '../../../general/rest/restActions';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import { TagService } from '../../../general/tags/tagService';
import SuggestionInput from '../../../general/components/SuggestionInput';
import { ManagerRest, ManagerRestSelector } from '../../../general/rest/ManagerRest';
import { Tag } from '../../../general/tags/Tag';
import { doEvent } from '../../../general/utils/componentEventHandler';
import ValidatedInput from '../../../general/components/ValidatedInput';
import { getCleanInput, getCleanInputs } from '../../../general/utils/formHelpers';
import { restBankingScreenName } from './RestBankingScreen';
import { showLoading, removeLoading } from '../../../general/redux/uiActions';

//todo 1: add input masking for phone and address
// validation https://www.smashingmagazine.com/2018/08/best-practices-for-mobile-form-design/

type props = {
  navigation: NavigationScreenProp<NavigationRoute>;
  onConfirm: (state: state) => void
  selectedRest?: ManagerRest,
};

type state = {
  name: string,
  description: string,
  phone: string,
  tagsQuery: string,
  tagSuggestions: Tag[],
  tags: Tag['name'][],
};

const MAX_TAGS = 5;

class RestProfileScreen extends React.Component<props, state> {
  static navigationOptions = ({ navigation: {state: {params = {}}}}: NavScreenOptions ) => ({
    tabBarVisible: false,
    title: params.confirmText === 'Next' ? "What's the restaurant?" : 'Restaurant profile',
    headerRight: <HeaderConfirmButton onConfirm={params.onConfirm} text={params.confirmText}/>
  });

  state = this.props.navigation.state.params.confirmText === 'Save' ?
  {
    name: ManagerRestSelector.getName(this.props.selectedRest),
    description: ManagerRestSelector.getDescription(this.props.selectedRest),
    phone: ManagerRestSelector.getPhone(this.props.selectedRest),
    tagsQuery: '',
    tagSuggestions: [] as Tag[],
    tags: ManagerRestSelector.getTags(this.props.selectedRest) || [] as Tag['name'][],
  } : {
    name: '',
    description: '',
    phone: '',
    tagsQuery: '',
    tagSuggestions: [] as Tag[],
    tags: [] as Tag['name'][],
  }

  updateTagSuggestions = tagsQuery => {
    if (this.state.tags.length >= MAX_TAGS) return;

    TagService.getTagSearchSuggestions(tagsQuery)
      .then(suggestions => {
        // depending on network speed, it is possible to receive suggestions out of the order they were made.
        // ex: suggestions for 'foo' may come after 'food' so we only set state when suggestions correspond to the
        // most recent query.
        if (this.state.tagsQuery === tagsQuery) {
          this.setState({
            tagSuggestions: differenceWith(suggestions, this.state.tags, (suggestion, existingTag) => suggestion.name === existingTag)
          });
        }
      });
  }

  validateOnChangeQuery = (query: string) => {
    const message = `Will only use 5 tags`;
    return getCleanInput(query).length > 0 && this.state.tags.length >= MAX_TAGS ? message : ''
  }

  renderSuggestionInput = controledSuggestionFetch => (
    <ValidatedInput
      floatingLabel
      label={`Tags (max ${MAX_TAGS})`}
      value={this.state.tagsQuery}
      onChangeText={newQuery => {
        this.setState({ tagsQuery: newQuery }, controledSuggestionFetch(newQuery));
      }}
      validateTextChange={this.validateOnChangeQuery}
    />
  )

  onPressSuggestion = suggestionName => {
    if (this.state.tags.length < MAX_TAGS) {
      this.setState({
        tagsQuery: '',
        tagSuggestions: [],
        tags: [...this.state.tags, suggestionName]
      });
    }
  }

  onConfirm = () => {
    const { name, description, phone, tags } = this.state;
    doEvent(this, this.props.onConfirm, { name, description, phone, tags });
  }

  validateOnEnd = (text, field) => text.length === 0 ? `${field} cannot be empty` : '';

  render () {
    const inputs = {
      name: (
        <ValidatedInput
          floatingLabel
          required
          label='Name'
          value={this.state.name}
          onChangeText={name => this.setState({ name })}
        />
      ) as unknown as ValidatedInput,
      phone: (
        <ValidatedInput
          floatingLabel
          required
          label='Phone'
          value={this.state.phone}
          onChangeText={phone => this.setState({ phone })}
        />
      ) as unknown as ValidatedInput
    }

    return (
      <Container style={styles.background}>
        <Content>
          <ValidatedForm onConfirm={this.onConfirm} originalInputs={inputs} render={newInputs => (
            <React.Fragment>
              {newInputs.name}
              <SuggestionInput renderInput={this.renderSuggestionInput} updateSearchSuggestions={this.updateTagSuggestions} />
              <View style={styles.suggestions}>
                {this.state.tagSuggestions.map(({ name, count }) => (
                  <TouchableWithoutFeedback key={name} onPress={() => this.onPressSuggestion(name)}>
                    <View style={styles.suggestion}>
                      <Text>{name}</Text>
                      <Text style={styles.count}> x {count}</Text>
                    </View>
                  </TouchableWithoutFeedback >
                ))}
              </View>
              <View style={styles.suggestions}>
                {this.state.tags.map(name => (
                  <Button key={name} rounded small style={styles.tag}>
                    <Text>{name}</Text>
                    <Icon name='trash' style={styles.trashcan} onPress={() => {
                      this.setState({ tags: without(this.state.tags, name)});
                    }} />
                  </Button>
                ))}
              </View>
              {newInputs.phone}
              <Item floatingLabel>
                <Label>Description</Label>
                <Input value={this.state.description}
                onChangeText={description => this.setState({description})}/>
              </Item>
            </React.Fragment>
          )} />
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  },
  suggestionName: {
    fontSize: commonColor.inputFontSize,
  },
  count: {
    fontSize: commonColor.inputFontSize,
    color: commonColor.brandFaint,
  },
  suggestions: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  suggestion: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    marginVertical: 10,
  },
  tag: {
    margin: 5,
  },
  trashcan: {
    marginLeft: 5,
    fontSize: commonColor.inputFontSize,
  }
})

const mapStateToProps = (state: RootState) => ({
  newRest: state.getAccount().getNewRest(),
  selectedRest: state.getAccount().getSelectedRest(),
  signedInUser: state.getSignedInUser(),
});

//need to use mergeProps so that 'Add' case has access to newRest. if not using mergeProps, then would have to repeat
//this switch logic in the component because comp would need to know when to pass in an entire newRest for add vs
//when to just pass profile. given this decision, we could also just always passa newRest, but then that would not be
//a good argument for the save condition.
const mergeProps = ({ selectedRest }, {dispatch}, props) => {
  const {navigation} = props;
  switch (navigation.state.params.confirmText) {
    case 'Next': //from signup or from restLocation which came from myRest's add rest button
      return {
        ...props,
        onConfirm: async profile => {
          dispatch(showLoading());
          const cleanProfile = getCleanInputs(profile);
          await dispatch(storeNewRestProfileAction(cleanProfile));
          navigation.navigate(restBankingScreenName, {
            confirmText: navigation.state.params.forSignUp ? 'Next' : 'Add',
            isSkippable: true,
          });
          dispatch(removeLoading());
        }
      }
    case 'Save': //updating profile
      return {
        ...props,
        selectedRest,     
        onConfirm: async newProfile => {
          dispatch(showLoading());
          const cleanProfile = getCleanInputs(newProfile);
          await dispatch(updateRestProfileAction(selectedRest._id, cleanProfile));
          navigation.goBack();
          dispatch(removeLoading());
        }
      }
    default:
      //todo 1. throw errow. bad props.    
      return {}
  }
}

export default connect(mapStateToProps, null, mergeProps)(RestProfileScreen);

export const restProfileScreenName = 'restProfileScreen';
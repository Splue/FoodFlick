import React from 'react';
import { shallow } from 'enzyme';
import { getMockStore, getMockNavigation } from '../../../test/utils/mocks/mockGeneral';
import { getMockSelectedRest } from '../../../test/utils/mocks/mockRests';
import RestDetailsScreen from './RestDetailsScreen';

describe('RestDetailsScreen', () => {
  const selectedRest = getMockSelectedRest();
  let store;
  const navigation = getMockNavigation();
  const getShallow = () => shallow(<RestDetailsScreen navigation={navigation} />, {context: {store}}).dive();

  beforeEach(() => {
    store = getMockStore({ selectedRest });
  });

  it('renders with no managers', () => {
    const restWithoutManagers = { ...selectedRest };
    restWithoutManagers.managers = [];
    store = getMockStore({ selectedRest: restWithoutManagers });
    expect(getShallow()).toMatchSnapshot();
  });

  it('renders with managers', () => {
    expect(getShallow()).toMatchSnapshot();
  });

  it.skip('on trash icon press calls deleteRestManagerAction for selected manager', () => {
  });
  
  it.skip('on add goes to CreateEmailScreen with onConfirm that calls addRestManager', () => {
  });
});

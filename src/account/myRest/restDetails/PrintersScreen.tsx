import { Text, Button, Container } from 'native-base';
import React from 'react';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import { RootState } from '../../../general/redux/RootState';
import { ManagerRestSelector, Printer } from '../../../general/rest/ManagerRest';
import { updatePrinterDetailsScreenName } from './PrinterDetailsScreen/UpdatePrinterDetailsScreen';
import { addPrinterDetailsScreenName } from './PrinterDetailsScreen/AddPrinterDetailsScreen';
import { deleteRestPrinter, testPrinterAction } from '../../../general/rest/restActions';
import CenteredH3 from '../../../general/components/CenteredH3';
import EditableList from '../../../general/components/list/EditableList';
import LeftRightListItem from '../../../general/components/list/LeftRightListItem';

type props = {
  printers: Printer[];
  onDelete: (printerName: string) => void,
  testPrinter: (printer: Printer) => void,
};

const PrintersScreen: NavigationScreenComponent<any, any, props> = ({ printers, navigation, onDelete, testPrinter }) => {
  const onPressAdd = () => {
    navigation.navigate(addPrinterDetailsScreenName)
  };
  const onPressUpdate = (printer, index, clearActiveItem) => {
    clearActiveItem();
    navigation.navigate(updatePrinterDetailsScreenName, {
      printer: {
        index,
        printer,
      }
    });
  };
  const onPressDelete = (printerName, clearActiveItem) => {
    onDelete(printerName);
    clearActiveItem();
  };

  const editableItems = [
    {
      icon: 'create',
      text: 'Update printer',
      onPress: (printer, index, clearActiveItem) => onPressUpdate(printer, index, clearActiveItem)
    },
    {
      icon: 'print',
      text: 'Test printer',
      onPress: testPrinter
    },
    {
      icon: 'trash',
      text: 'Delete',
      onPress: (printer, index, clearActiveItem) => onPressDelete(printer.name, clearActiveItem)
    }
  ]

  return (printers.length === 0 ?
    <Container>
      <CenteredH3 text='You have no printers. Start by adding a printer' />
      <Button primary block style={styles.button} onPress={onPressAdd}>
        <Text>Add printer</Text>
      </Button>
    </Container>
    :
    <Container>
      <Button transparent primary style={styles.button} onPress={onPressAdd}>
        <Text>Add printer</Text>
      </Button>
      <EditableList
        list={printers}
        editableItems={editableItems}
        renderItem={(printer, index, setActive) => (
          <LeftRightListItem
            key={printer.name}
            leftText={printer.name}
            leftNote={printer.isReceipt ? 'receipt' : null}
            right={<Text note>{printer.ip}</Text>}
            onPress={setActive}
          />
        )}
      />
    </Container>
  )
}

const styles = StyleSheet.create({
  button: {
    alignSelf: 'center',
    marginVertical: 8,
  }
});

PrintersScreen.navigationOptions = ({
  title: 'Printers'
});

const mapStateToProps = (state: RootState) => ({
  selectedRestId: ManagerRestSelector.getId(state.getAccount().getSelectedRest()),
  printers: ManagerRestSelector.getPrinters(state.getAccount().getSelectedRest()),
});

const mergeProps = ({ selectedRestId, printers }, { dispatch }, props) => ({
  ...props,
  printers,
  onDelete: (printerName: string) => {
    dispatch(deleteRestPrinter(selectedRestId, printerName));
  },
  testPrinter: printer => dispatch(testPrinterAction(selectedRestId, printer))
})

export default connect(mapStateToProps, null, mergeProps)(PrintersScreen);

export const printersScreenName = 'printersScreen';
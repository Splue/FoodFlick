import React from 'react';
import { shallow } from 'enzyme';
import { getMockStore, getMockNavigation } from '../../../test/utils/mocks/mockGeneral';
import { getMockSelectedRest } from '../../../test/utils/mocks/mockRests';
import RestManagersScreen from './RestManagersScreen';
import RestService from '../myRest/services/restService';

describe('RestManagersScreen', () => {
  const selectedRest = getMockSelectedRest();
  let store;
  const navigation = getMockNavigation();
  const getShallow = () => shallow(<RestManagersScreen navigation={navigation} />, {context: {store}}).dive();

  beforeEach(() => {
    store = getMockStore({ selectedRest });
  });

  it('renders', () => {
    expect(getShallow()).toMatchSnapshot();
  });

  it('on delete calls deleteRestManagerAction for selected manager', () => {
    const restWithDeletedManager = getMockSelectedRest();
    const deletedManager = restWithDeletedManager.managers.pop();
    const mockDeleteRestManagerService = jest.spyOn(RestService, 'deleteRestManager').mockImplementation(() => restWithDeletedManager);
    getShallow().prop('onDelete')(deletedManager.email);
    setImmediate(() => {
      expect(mockDeleteRestManagerService).toBeCalledWith(restWithDeletedManager._id, deletedManager.email);
      expect(store.getState().selectedRest).toEqual(restWithDeletedManager);
      mockDeleteRestManagerService.mockRestore();
    });
  });
  
  it('on add goes to CreateEmailScreen with onConfirm that calls addRestManager', () => {
    getShallow().prop('onAdd')('somenewemail.@email.com');
    expect(navigation.navigate).toBeCalled();
  });
});

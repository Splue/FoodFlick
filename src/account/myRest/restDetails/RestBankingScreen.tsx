import React from 'react';
import { Alert, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { storeNewRestBankingAction, updateRestBankingAction, addNewRestAction } from '../../../general/rest/restActions';
import HeaderConfirmButton from '../../../general/components/header/HeaderConfirmButton';
import ValidatedForm from '../../../general/components/ValidatedForm';
import SkipButton from '../../../general/components/SkipButton';
import { NavScreenOptions } from '../../../general/navModels/NavScreenOptions';
import { RootState } from '../../../general/redux/RootState';
import { confirmSignUpScreenName } from '../../signUp/ConfirmSignUpScreen';
import ValidatedInput from '../../../general/components/ValidatedInput';
import { getCleanInputs, getCleanInput } from '../../../general/utils/formHelpers';
import { NavigationRoute, NavigationScreenProp } from 'react-navigation';
import { ManagerRest, ManagerRestSelector } from '../../../general/rest/ManagerRest';
import { Banking } from '../../../general/rest/ManagerRest';
import { restLocationScreenName } from './RestLocationScreen';
import { doEvent } from '../../../general/utils/componentEventHandler';
import { Container, Content } from 'native-base';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import { showLoading, removeLoading } from '../../../general/redux/uiActions';

const onSkip = onAffirm => () => Alert.alert (
  'Skip banking?',
  `Are you sure you want to skip banking? You need banking to accept mobile payments from Foodflick.`,
  [
    { text: 'Cancel' },
    {
      text: 'Yes',
      onPress: onAffirm
    },
  ],
  { cancelable: false }
);

const routingNumberRegex = /^\d{9}$/; // 9 digit string

type props = {
  navigation: NavigationScreenProp<NavigationRoute>;
  selectedRest: ManagerRest,
  onConfirm: (state: Banking) => void
  onSkip: () => void,
};

type state = {
  accountNumber: string,
  routingNumber: string,
  routingNumberValidation: string,
};

class RestBankingScreen extends React.Component<props, state> {
  static navigationOptions = ({navigation: {state: {params ={}}}}: NavScreenOptions) => ({
    tabBarVisible: false,
    //next means i'm adding a rest, otherwise just updating banking with confirmText = Save
    title: params.confirmText === 'Next' ? 'Banking' : 'Update banking',
    headerRight: <HeaderConfirmButton onConfirm={params.onConfirm} text={params.confirmText}/>
  });
  
  state = this.props.navigation.state.params.confirmText === 'Next' || !this.props.selectedRest ? { 
    accountNumber:  '',
    routingNumber: '',
    routingNumberValidation: '',
  } : {
    accountNumber: ManagerRestSelector.getBankAccountNumber(this.props.selectedRest),
    routingNumber: ManagerRestSelector.getRoutingNumber(this.props.selectedRest),
    routingNumberValidation: '',
  }

  validateOnEndRouting = (routingNumber: string) => {
    const cleanedRoutingNumber = getCleanInput(routingNumber);
    return routingNumberRegex.test(cleanedRoutingNumber) ? '' : 'Please enter a 9 digit routing number';
  }

  onConfirm = () => doEvent(this, this.props.onConfirm, {
    accountNumber: this.state.accountNumber,
    routingNumber: this.state.routingNumber,
  });

  render () {
    const inputs = {
      accountNumber: (
        <ValidatedInput
          floatingLabel
          label='Account number'
          value={this.state.accountNumber}
          onChangeText={accountNumber => this.setState({ accountNumber })}
          required
        />
      ) as unknown as ValidatedInput,
      routingNumber: (
        <ValidatedInput
          floatingLabel
          label='Routing number'
          value={this.state.routingNumber}
          onChangeText={routingNumber => this.setState({ routingNumber })}
          validateEndEdit={this.validateOnEndRouting}
          required
        />
      ) as unknown as ValidatedInput
    }

    const {navigation, onSkip} = this.props;

    return (
      <Container style={styles.background}>
        <Content>
          <ValidatedForm onConfirm={this.onConfirm} originalInputs={inputs} 
          render={newInputs => (
            <React.Fragment>
              {Object.values(newInputs)}
              {navigation.state.params.isSkippable && <SkipButton testId='skipButton' onPress={onSkip} />}
            </React.Fragment>
          )} />
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  }
});


const mapStateToProps = (state: RootState) => ({
  newRest: state.getAccount().getNewRest(),
  selectedRest: state.getAccount().getSelectedRest(),
});

const mergeProps = ({ newRest, selectedRest }, { dispatch }, props) => {
  const { navigation } = props;
  switch (navigation.state.params.confirmText) {
    case 'Next': //from signup
      return {
        ...props,
        onSkip: onSkip(() => navigation.navigate(confirmSignUpScreenName)),
        onConfirm: async banking => {
          dispatch(showLoading());
          const cleanBanking = getCleanInputs(banking);
          await dispatch(storeNewRestBankingAction(cleanBanking));
          navigation.navigate(confirmSignUpScreenName);    
          dispatch(removeLoading());      
        }
      }
    case 'Add': // from restProfile which came from myRest's add rest button
      return {
        ...props,
        onSkip: onSkip(async () => {
          dispatch(showLoading());
          await dispatch(addNewRestAction(newRest));
          // go back from restLocationScreen because that's the first screen for adding a rest
          navigation.goBack(restLocationScreenName);
          dispatch(removeLoading());
        }),
        onConfirm: async banking => {
          dispatch(showLoading());
          const cleanBanking = getCleanInputs(banking);
          await dispatch(addNewRestAction({
            ...newRest,
            banking: cleanBanking,
          }));
          // go back from restLocationScreen because that's the first screen for adding a rest
          navigation.goBack(restLocationScreenName);
          dispatch(removeLoading());
        }
      }
    case 'Save': // from updating rest
      return {
        ...props,
        selectedRest,     
        onConfirm: async banking => {
          dispatch(showLoading());
          const cleanBanking = getCleanInputs(banking);
          await dispatch(updateRestBankingAction(selectedRest._id, cleanBanking));
          navigation.goBack();
          dispatch(removeLoading());
        }
      }
    default:
      //todo 1. throw errow. bad props.    
      return {}
  }
}

export default connect(mapStateToProps, null, mergeProps)(RestBankingScreen);

export const restBankingScreenName = 'restBankingScreen';
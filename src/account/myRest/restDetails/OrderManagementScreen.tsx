import React, { useState, useRef } from 'react';
import { StyleSheet, View } from 'react-native';
import ValidatedInput from '../../../general/components/ValidatedInput';
import { Container, Content, Text } from 'native-base';
import ValidatedForm from '../../../general/components/ValidatedForm';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import { NavigationScreenComponent } from 'react-navigation';
import { connect } from 'react-redux';
import HeaderConfirmButton from '../../../general/components/header/HeaderConfirmButton';
import { RootState } from '../../../general/redux/RootState';
import { ManagerRestSelector } from '../../../general/rest/ManagerRest';
import { getCleanInput } from '../../../general/utils/formHelpers';
import { updateMinsToUpdateCart } from '../../../general/rest/restActions';
import { showLoading, removeLoading } from '../../../general/redux/uiActions';

type props = {
  onConfirm: (string: string) => void,
  defaultMins: number
};

const OrderManagementScreen:  NavigationScreenComponent<any, any, props> = ({ defaultMins, onConfirm }) => {
  const [minsTillAutoOrderCompletion, setMinsTillAutoOrderCompletion] = useState(defaultMins.toString());
  const validateOnEnd = minStr => {
    const regex = /^[0-9]+$/
    const cleanMins = getCleanInput(minStr);
    if (cleanMins.length === 0) return `Required`;
    if (!regex.test(cleanMins)) return 'Please only use whole numbers';
    const mins = parseFloat(cleanMins)
    if (mins <= 0) return 'Minutes must be greater than 0';
    return '';
  }

  const inputs = {
    mins: (
      <ValidatedInput
        floatingLabel
        inputProps={{
          keyboardType: 'numeric',
        }}
        label='How long (mins) can customers update orders?'
        value={minsTillAutoOrderCompletion}
        onChangeText={mins => setMinsTillAutoOrderCompletion(mins)}
        validateEndEdit={validateOnEnd}
      />
    ) as unknown as ValidatedInput,
  };
  // https://github.com/facebook/react/issues/14010
  const refMins = useRef(minsTillAutoOrderCompletion);
  refMins.current = minsTillAutoOrderCompletion;
  return (
    <Container style={styles.background}>
      <Content>
        <ValidatedForm onConfirm={() => onConfirm(refMins.current)} originalInputs={inputs} render={newInputs => newInputs.mins} />
        <View style={styles.texts}>
          <Text style={styles.text}>
            Every customer order starts as an open order. You can think of open orders as a customer's open tab. While the
            order is open, customers can keep adding items to it. If the customer changes phone number, credit card, order
            type, or table number a new open order is created.
          </Text>
          <Text style={styles.text}>
            After a number of minutes you choose, the sit-down orders will automatically change to PENDING_TIP. Orders will stay
            here for 3 hours so the customer can change the tip if necessary. If you do nothing, foodflick will automatically
            process the payment after 3 hours and move the order to completed. In rare cases, an order can get stuck in
            Open or Pending Tip. If this hapens, push the order manually.
          </Text>
        </View>
      </Content>
    </Container>
  )
}

OrderManagementScreen.navigationOptions = ({ navigation }) => ({
  tabBarVisible: false,
  title: 'Update order management',
  headerRight: <HeaderConfirmButton onConfirm={navigation.getParam('onConfirm')} text="Save" />
});


const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  },
  text: {
    paddingTop: commonColor.formInputMarginLeft
  },
  texts: {
    paddingHorizontal: commonColor.formInputMarginLeft,
  }
});

const mapStateToProps = (state: RootState) => ({
  selectedRest: state.getAccount().getSelectedRest(),
});

const mergeProps = ({ selectedRest }, { dispatch }, props) => {
  const {navigation} = props;
  return {
    ...props,
    defaultMins: ManagerRestSelector.getMinsToUpdateCart(selectedRest),
    onConfirm: async (mins: string) => {
      dispatch(showLoading());
      await dispatch(updateMinsToUpdateCart(selectedRest._id, parseFloat(mins)));
      navigation.goBack();
      dispatch(removeLoading());
    }
  }
}


export default connect(mapStateToProps, null, mergeProps)(OrderManagementScreen);

export const orderManagementScreenName = 'orderManagementScreen';
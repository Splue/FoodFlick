import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteRestManagerAction, addRestManagerAction } from '../../../general/rest/restActions';
import EmailListEditor from '../../../general/components/list/EmailListEditor';
import { getReferencedUserPropType } from '../../../general/utils/propTypes/user';
import { NavigationScreenComponent } from 'react-navigation';
import { Root, Toast } from 'native-base';
import { RootState } from '../../../general/redux/RootState';
import { ManagerRest } from '../../../general/rest/ManagerRest';
import { showLoading, removeLoading } from '../../../general/redux/uiActions';

const RestManagersScreen: NavigationScreenComponent = ({ managers, onDelete, onAdd }: any) => (
  <EmailListEditor
    addButtonText='Add managers'
    users={managers}
    onAdd={onAdd}
    onDelete={onDelete}
    emptyEmailsText='No managers. Start by adding some managers'
  />
);

// RestManagersScreen.propTypes = {
//   managers: PropTypes.arrayOf(getReferencedUserPropType({ _id: PropTypes.string })),
//   onAdd: PropTypes.func.isRequired,
//   onDelete: PropTypes.func.isRequired,
// }

RestManagersScreen.navigationOptions = () => ({
  title: 'Edit managers'
});

const mapStateToProps = (state: RootState) => ({
  selectedRest: state.getAccount().getSelectedRest(),
});

type StateProps = {
  selectedRest: ManagerRest;
}

const mergeProps = ({selectedRest: {_id: restId, managers}}: StateProps, {dispatch}, {navigation}) => ({
  managers,
  onDelete: async userId => await dispatch(deleteRestManagerAction(restId, userId)),
  onAdd: () => {
    navigation.navigate('createEmailScreen', {
      shouldNotCheckIfUserExists: true,
      confirmText: 'Add',
      onConfirm: async managerEmail => {
        try {
          dispatch(showLoading());
          await dispatch(addRestManagerAction(restId, managerEmail));
          navigation.goBack();
          dispatch(removeLoading());
        } catch (e) {
          Toast.show({
            text: e.message,
            type: 'warning',
            buttonText: 'Okay',
            position: 'bottom'
          });
        }
      }
    })
  },
})

export default connect(mapStateToProps, null, mergeProps)(RestManagersScreen);

export const restManagersScreenName = 'restManagersScreen';
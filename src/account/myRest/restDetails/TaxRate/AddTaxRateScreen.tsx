import React from 'react';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import HeaderConfirmButton from '../../../../general/components/header/HeaderConfirmButton';
import { RootState } from '../../../../general/redux/RootState';
import { addRestTaxRate } from '../../../../general/rest/restActions';
import { getCleanInput } from '../../../../general/utils/formHelpers';
import TaxRateInput from './TaxRateInput';
import { showLoading, removeLoading } from '../../../../general/redux/uiActions';

type props = {
  onConfirm: (receiverId: string) => void,
};

const AddTaxRateScreen: NavigationScreenComponent<any, any, props> = ({ onConfirm }) =>  (
  <TaxRateInput onConfirm={onConfirm} />
)

AddTaxRateScreen.navigationOptions = ({ navigation }) => ({
  tabBarVisible: false,
  title: 'Add sales tax  %',
  headerRight: <HeaderConfirmButton onConfirm={navigation.getParam('onConfirm')} text='Add' />
});

const mapStateToProps = (state: RootState) => ({
  selectedRest: state.getAccount().getSelectedRest(),
});

const mergeProps = ({ selectedRest }, { dispatch }, props) => {
  const {navigation} = props;
  return {
    ...props,
  onConfirm: async (taxRate: string) => {
      dispatch(showLoading());
      const cleanTaxRate = getCleanInput(taxRate)
      await dispatch(addRestTaxRate(selectedRest._id, parseFloat(cleanTaxRate) / 100));
      navigation.goBack();
      dispatch(removeLoading());
    }
  }
}

export default connect(mapStateToProps, null, mergeProps)(AddTaxRateScreen);

export const addTaxRateScreenName = 'addTaxRateScreen';
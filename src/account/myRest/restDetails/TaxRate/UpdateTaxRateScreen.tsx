import React from 'react';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import HeaderConfirmButton from '../../../../general/components/header/HeaderConfirmButton';
import { RootState } from '../../../../general/redux/RootState';
import { updateRestTaxRate } from '../../../../general/rest/restActions';
import { getCleanInput } from '../../../../general/utils/formHelpers';
import TaxRateInput from './TaxRateInput';
import { ManagerRestSelector } from '../../../../general/rest/ManagerRest';
import { showLoading, removeLoading } from '../../../../general/redux/uiActions';
import { displayPercentageStr } from '../../../../general/utils/math';

type props = {
  onConfirm: (receiverId: string) => void,
  taxRate: string,
};

const UpdateTaxRateScreen: NavigationScreenComponent<any, any, props> = ({ onConfirm, taxRate }) =>  (
  <TaxRateInput onConfirm={onConfirm} defaultTaxRate={taxRate} />
)

UpdateTaxRateScreen.navigationOptions = ({ navigation }) => ({
  tabBarVisible: false,
  title: 'Update sales tax %',
  headerRight: <HeaderConfirmButton onConfirm={navigation.getParam('onConfirm')} text="Save" />
});

const mapStateToProps = (state: RootState) => ({
  selectedRest: state.getAccount().getSelectedRest(),
});

const mergeProps = ({ selectedRest }, { dispatch }, props) => {
  const {navigation} = props;
  return {
    ...props,
    taxRate: displayPercentageStr(ManagerRestSelector.getTaxRate(selectedRest)),
    onConfirm: async (taxRate: string) => {
      dispatch(showLoading());
      const cleanTaxRate = getCleanInput(taxRate)
      await dispatch(updateRestTaxRate(selectedRest._id, parseFloat(cleanTaxRate) / 100));
      navigation.goBack();
      dispatch(removeLoading());
    }
  }
}

export default connect(mapStateToProps, null, mergeProps)(UpdateTaxRateScreen);

export const updateTaxRateScreenName = 'updateTaxRateScreen';
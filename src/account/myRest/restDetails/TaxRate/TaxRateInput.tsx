import React, { useState, useRef } from 'react';
import { StyleSheet } from 'react-native';
import ValidatedInput from '../../../../general/components/ValidatedInput';
import { Container, Content } from 'native-base';
import ValidatedForm from '../../../../general/components/ValidatedForm';
import commonColor from '../../../../../native-base-theme/variables/commonColor';

type props = {
  onConfirm: (string: string) => void,
  defaultTaxRate?: string
};

const TaxRateInput: React.FunctionComponent<props> = ({ defaultTaxRate, onConfirm }) => {
  const [taxRate, setTaxRate] = useState(defaultTaxRate);
  const inputs = {
    taxRate: (
      <ValidatedInput
        floatingLabel
        required
        inputProps={{
          keyboardType: 'numeric',
        }}
        label='Sales tax %'
        value={taxRate}
        onChangeText={taxRate => setTaxRate(taxRate)}
      />
    ) as unknown as ValidatedInput,
  };
  // https://github.com/facebook/react/issues/14010
  const refTaxRate = useRef(taxRate);
  refTaxRate.current = taxRate;
  return (
    <Container style={styles.background}>
      <Content>
        <ValidatedForm onConfirm={() => onConfirm(refTaxRate.current)} originalInputs={inputs} render={newInputs => (
          <React.Fragment>
            {newInputs.taxRate}
          </React.Fragment>
        )} />
      </Content>
    </Container>
  )
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  },
});

export default TaxRateInput;
import React from 'react';
import { shallow } from 'enzyme';
import RestProfileScreen from './RestProfileScreen';
import { getMockSignedInRestManager } from '../../../test/utils/mocks/mockUsers';
import { getMockNavigation, mockAnimated, getMockStore } from '../../../test/utils/mocks/mockGeneral';
import { getMockNewRest, getMockSelectedRest } from '../../../test/utils/mocks/mockRests';
import * as myRestActions from '../../../general/rest/restActions';
import RestService from '../myRest/services/restService';

describe('RestProfileScreen', () => {
  mockAnimated();
  const navigation = getMockNavigation({
    state: {
      params: {}
    }
  });

  let store;
  const newRest = getMockNewRest();
  delete newRest.profile;

  const getShallow = () => shallow(<RestProfileScreen navigation={navigation} />, {context: {store}}).dive();

  const newProfile = {
    name: 'kobe',
    description: 'black mamba',
    phone: '555',
  };

  afterEach(() => {
    navigation.reset();
  });

  describe('Sign up', () => {
    beforeAll(() => {
      navigation.state.params.confirmText = 'Next';
      store = getMockStore({ newRest });
    });

    it('renders with empty state', () => {
      expect(getShallow()).toMatchSnapshot();
    });
      
    it('on confirm stores rest profile and routes to ConfirmSignUpScreen', done => {
      const screen = getShallow();
      screen.instance().setState({ ...newProfile });
      screen.prop('onConfirm')();
      setImmediate(() => {
        expect(store.getState().newRest).toEqual({
          ...newRest,
          profile: newProfile,
        });
        expect(navigation.navigate).toHaveBeenCalledWith('confirmSignUpScreen');
        done();
      });
    });
  });

  describe('Rest addition confirmation', () => {
    const signedInUser = getMockSignedInRestManager();
    beforeAll(() => {
      navigation.state.params.confirmText = 'Add';
      store = getMockStore({
        newRest,
        signedInUser,
      });
    });

    it('renders with empty state', () => {
      expect(getShallow()).toMatchSnapshot();
    });

    it('on confirm calls addRestNewAction, routes RestOverviewScreen, and prevents back button from returning', done => {
      const addedRest = getMockSelectedRest();

      const mockAddNewRestService = jest.spyOn(RestService, 'addNewRest').mockImplementation(() => addedRest);
      const mockAddNewRestAction = jest.spyOn(myRestActions, 'addNewRestAction');
      const mockGoRestOverviewAfterRestAddAction = jest.spyOn(myRestActions, 'goRestOverviewAfterRestAddAction').mockImplementation(() => ({}));
      
      const screen = getShallow();
      screen.instance().setState({ ...newProfile });
      screen.prop('onConfirm')();
      setImmediate(() => {
        expect(mockAddNewRestAction).toHaveBeenCalledWith({
          ...newRest,
          profile: newProfile,
          owner: { _id: signedInUser._id, email: signedInUser.email },
        });
        expect(mockGoRestOverviewAfterRestAddAction).toHaveBeenCalled();
        expect(store.getState().selectedRest).toEqual(addedRest);
        mockAddNewRestService.mockRestore();
        mockAddNewRestAction.mockRestore();
        mockGoRestOverviewAfterRestAddAction.mockRestore();
        done();
      });
    });
  });

  describe('Rest edit confirmation', () => {
    const selectedRest = getMockSelectedRest();
    beforeEach(() => {
      navigation.state.params.confirmText = 'Save';
      store = getMockStore({
        selectedRest,
      });
    });

    it('renders with default state', () => {
      expect(getShallow()).toMatchSnapshot();
    });

    it('on confirm calls updateRestProfileAction and routes to previous screen', done => {
      const updatedRest = { ...selectedRest };
      updatedRest.profile = { ...newProfile };

      const mockUpdateRestService = jest.spyOn(RestService, 'updateRestProfile').mockImplementation(() => updatedRest);
      const mockUpdateRestProfileAction = jest.spyOn(myRestActions, 'updateRestProfileAction');
      
      const screen = getShallow();
      screen.instance().setState({ ...newProfile });
      screen.prop('onConfirm')();
      setImmediate(() => {
        expect(mockUpdateRestProfileAction).toHaveBeenCalledWith(selectedRest._id, newProfile);
        expect(navigation.goBack).toHaveBeenCalled();
        expect(store.getState().selectedRest).toEqual(updatedRest);

        mockUpdateRestProfileAction.mockRestore();
        mockUpdateRestService.mockRestore();
        done();
      });
    });
  });
});

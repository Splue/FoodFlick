import React, { useState, useRef } from 'react';
import { StyleSheet } from 'react-native';
import ValidatedInput from '../../../general/components/ValidatedInput';
import { Container, Content } from 'native-base';
import ValidatedForm from '../../../general/components/ValidatedForm';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import { NavigationScreenComponent } from 'react-navigation';
import { connect } from 'react-redux';
import HeaderConfirmButton from '../../../general/components/header/HeaderConfirmButton';
import { RootState } from '../../../general/redux/RootState';
import { updateRestUrl } from '../../../general/rest/restActions';
import { ManagerRestSelector } from '../../../general/rest/ManagerRest';
import { showLoading, removeLoading } from '../../../general/redux/uiActions';

type props = {
  onConfirm: (string: string) => void,
  defaultUrl?: string
};

const UpdateUrlScreen:  NavigationScreenComponent<any, any, props> = ({ defaultUrl, onConfirm }) => {
  const [url, setUrl] = useState(defaultUrl);
  const validateOnEnd = url => {
    if (url.length === 0) return `URL cannot be empty`;
    const regex = /^[a-zA-Z0-9_-]*$/
    if (!regex.test(url)) return 'Please use only letters, numbers, -, or _';
    return '';
  }

  const inputs = {
    url: (
      <ValidatedInput
        floatingLabel
        label='URL'
        value={url}
        onChangeText={url => setUrl(url)}
        validateEndEdit={validateOnEnd}
      />
    ) as unknown as ValidatedInput,
  };
  // https://github.com/facebook/react/issues/14010
  const refUrl = useRef(url);
  refUrl.current = url;
  return (
    <Container style={styles.background}>
      <Content>
        <ValidatedForm onConfirm={() => onConfirm(refUrl.current)} originalInputs={inputs} render={newInputs => (
          <React.Fragment>
            {newInputs.url}
          </React.Fragment>
        )} />
      </Content>
    </Container>
  )
}

UpdateUrlScreen.navigationOptions = ({ navigation }) => ({
  tabBarVisible: false,
  title: 'Update URL',
  headerRight: <HeaderConfirmButton onConfirm={navigation.getParam('onConfirm')} text="Save" />
});


const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  },
});

const mapStateToProps = (state: RootState) => ({
  selectedRest: state.getAccount().getSelectedRest(),
});

const mergeProps = ({ selectedRest }, { dispatch }, props) => {
  const {navigation} = props;
  return {
    ...props,
    defaultUrl: ManagerRestSelector.getUrl(selectedRest),
    onConfirm: async (url: string) => {
      dispatch(showLoading());
      await dispatch(updateRestUrl(selectedRest._id, url));
      navigation.goBack();
      dispatch(removeLoading());
    }
  }
}


export default connect(mapStateToProps, null, mergeProps)(UpdateUrlScreen);

export const updateUrlScreenName = 'updateUrlScreen';
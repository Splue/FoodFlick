import React from 'react';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { storeNewRestLocationAction, updateRestLocationAction, selectAccountRestAction } from '../../../general/rest/restActions';
import HeaderConfirmButton from '../../../general/components/header/HeaderConfirmButton';
import ValidatedForm from '../../../general/components/ValidatedForm';
import SkipButton from '../../../general/components/SkipButton';
import { NavScreenOptions } from '../../../general/navModels/NavScreenOptions';
import { RootState } from '../../../general/redux/RootState';
import { confirmSignUpScreenName } from '../../signUp/ConfirmSignUpScreen';
import ValidatedInput from '../../../general/components/ValidatedInput';
import { states } from '../../../general/utils/consts';
import { getCleanInputs, getCleanInput } from '../../../general/utils/formHelpers';
import { NavigationRoute, NavigationScreenProp } from 'react-navigation';
import { ManagerRest, ManagerRestSelector } from '../../../general/rest/ManagerRest';
import { Address } from '../../../general/rest/BaseRest';
import { Content, Container } from 'native-base';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import { showLoading, removeLoading } from '../../../general/redux/uiActions';

type props = {
  navigation: NavigationScreenProp<NavigationRoute>;
  selectedRest: ManagerRest,
  onConfirm: (state: Address) => void
  onSkip: () => void,
};

type state = {
  address1: string,
  city: string,
  state: string,
  zip: string,
  stateValidation: string,
};

class RestLocationScreen extends React.Component<props, state> {
  static navigationOptions = ({navigation: {state: {params ={}}}}: NavScreenOptions) => ({
    tabBarVisible: false,
    //next means i'm adding a rest, otherwise just updating location with confirmText = Save
    title: params.confirmText === 'Next' ? 'Restaurant address' : 'Update address',
    headerRight: <HeaderConfirmButton onConfirm={params.onConfirm} text={params.confirmText}/>
  });
  
  state = this.props.navigation.state.params.confirmText === 'Next' || !this.props.selectedRest ? { 
    address1:  '',
    city: '',
    state: '',
    zip: '',
    stateValidation: '',
  } : {
    address1: ManagerRestSelector.getAddress1(this.props.selectedRest),
    city: ManagerRestSelector.getCity(this.props.selectedRest),
    state: ManagerRestSelector.getState(this.props.selectedRest),
    zip: ManagerRestSelector.getZip(this.props.selectedRest),
    stateValidation: ''
  }

  validateOnChangeState = (state: string) => {
    const cleanedState = getCleanInput(state);
    const message = 'Use 2-letter state abbreviations, e.g. MA';
    if (cleanedState.length > 2) return message;

    if (cleanedState.length === 2 && !states.includes(state.toUpperCase().trim())) return message;

    return '';
  }

  validateOnEnd = (text, field) => text.length === 0 ? `${field} cannot be empty` : '';

  validateOnEndState = (state: string) => {
    const cleanedState = getCleanInput(state);
    const errorMessage = 'Use 2-letter state abbreviations, e.g. MA';
    let message = '';

    if (cleanedState.length < 2 || cleanedState.length > 2) {
      message = errorMessage;
      this.setState({
        stateValidation: message
      });
    }

    if (cleanedState.length === 2 && !states.includes(cleanedState.toUpperCase())) {
      message = errorMessage;
      this.setState({
        stateValidation: message
      });
    }

    return message;
  }

  onConfirm = () => {
    const { stateValidation, ...location } =this.state;
    this.props.onConfirm(location)
  }

  render () {
    const inputs = {
      address: (
        <ValidatedInput
          floatingLabel
          required
          label='Address'
          value={this.state.address1}
          onChangeText={address1 => this.setState({ address1 })}
        />
      ) as unknown as ValidatedInput,
      city: (
        <ValidatedInput
          floatingLabel
          label='City'
          value={this.state.city}
          onChangeText={city => this.setState({ city })}
          required
        />
      ) as unknown as ValidatedInput,
      state: (
        <ValidatedInput
          floatingLabel
          label='State'
          value={this.state.state}
          onChangeText={state => this.setState({ state })}
          validateTextChange={this.validateOnChangeState}
          validateEndEdit={this.validateOnEndState}
        />
      ) as unknown as ValidatedInput,
      zip: (
        <ValidatedInput
          floatingLabel
          required
          label='Zip'
          value={this.state.zip}
          onChangeText={zip => this.setState({ zip })}
        />
      ) as unknown as ValidatedInput
    }

    const {navigation, onSkip} = this.props;
    return (
      <Container style={styles.background}>
        <Content>
          <ValidatedForm onConfirm={this.onConfirm} originalInputs={inputs} 
          render={newInputs => (
            <React.Fragment>
              {Object.values(newInputs)}
              {navigation.state.params.isSkippable && <SkipButton testId='skipButton' onPress={onSkip} />}
            </React.Fragment>
          )} />
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  }
});

const mapStateToProps = (state: RootState) => ({
  selectedRest: state.getAccount().getSelectedRest(),
})

const mergeProps = ({ selectedRest }, {dispatch}, props) => {
  const { navigation } = props;
  return ({
    ...props,
    selectedRest,
    onConfirm: navigation.state.params && navigation.state.params.confirmText === 'Next' ? 
      async (address: Address) => {
        address = getCleanInputs(address) as Address;
        address.state = address.state.toLocaleUpperCase(); 
        await dispatch(storeNewRestLocationAction({ address }));
        navigation.navigate('restProfileScreen', {
          confirmText: 'Next',
          forSignUp: navigation.state.params.forSignUp,
        });
      } :
      async (newAddress: Address) => { //otherwise, confirmText is save, so we save as the user is updating location
        dispatch(showLoading());
        newAddress = getCleanInputs(newAddress) as Address;
        newAddress.state = newAddress.state.toLocaleUpperCase(); 
        await dispatch(updateRestLocationAction(selectedRest._id, { address: newAddress }));
        navigation.goBack();
        dispatch(removeLoading());
      },
    onSkip: () => {
      dispatch(selectAccountRestAction(null));
      navigation.navigate(confirmSignUpScreenName)
    }
  })
}

export default connect(mapStateToProps, null, mergeProps)(RestLocationScreen);

export const restLocationScreenName = 'restLocationScreen';
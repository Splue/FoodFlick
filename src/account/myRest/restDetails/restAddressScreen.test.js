import React from 'react';
import { shallow } from 'enzyme';
import RestLocationScreen from './RestLocationScreen';
import { getMockNavigation, mockAnimated, getMockStore } from '../../../test/utils/mocks/mockGeneral';
import { getMockSelectedRest } from '../../../test/utils/mocks/mockRests';
import * as myRestActions from '../../../general/rest/restActions';
import RestService from '../myRest/services/restService';

describe('RestLocationScreen', () => {
  mockAnimated();
  const navigation = getMockNavigation({
    state: {
      params: {}
    }
  });

  let store;
  const newLocation = {
    address: {
      address1: 'winterfell',
      city: 'the north',
      state: 'WE',
      zip: '123'
    }
  };
  const getShallow = () => shallow(<RestLocationScreen navigation={navigation} />, {context: {store}}).dive();

  afterEach(() => {
    navigation.reset();
  });

  it('renders skip button only if it receives the skip route param', () => {
    store = getMockStore();
    navigation.state.params.confirmText = 'Next';
    navigation.state.params.isSkippable = true;
    expect(getShallow()).toMatchSnapshot();
    navigation.state.params.isSkippable = false;
    expect(getShallow()).toMatchSnapshot();
  });

  it('routes to ConfirmSignupScreen onSkip', () => {
    store = getMockStore();
    navigation.state.params.confirmText = 'Next';
    navigation.state.params.isSkippable = true;
    const screen = getShallow();
    screen.find({testId: 'skipButton'}).simulate('press');
    expect(navigation.navigate).toBeCalledWith('confirmSignUpScreen');
  });

  describe('rest update', () => {
    const selectedRest = getMockSelectedRest();
    beforeAll(() => {
      store = getMockStore({ selectedRest });
      navigation.state.params.isSkippable = false;
      navigation.state.params.confirmText = 'Save';
    });

    it('renders with populated rest', () => {
      expect(getShallow()).toMatchSnapshot();
    });

    it('onConfirm calls updateRestAction and routes back', done => {
      const updatedRest = { ...selectedRest };
      updatedRest.location = { ...newLocation };

      const mockUpdateRestLocationsService = jest.spyOn(RestService, 'updateRestLocation').mockImplementation(() => updatedRest);
      const mockUpdateRestLocationAction = jest.spyOn(myRestActions, 'updateRestLocationAction');
      const screen = getShallow();
      screen.instance().setState({ ...newAddress });
      screen.prop('onConfirm')();
      setImmediate(() => {
        expect(mockUpdateRestLocationAction).toHaveBeenCalledWith(selectedRest._id, newLocation);
        expect(mockUpdateRestLocationService).toHaveBeenCalledWith(selectedRest._id, newLocation);
        expect(navigation.goBack).toHaveBeenCalled();
        expect(store.getState().selectedRest).toEqual(updatedRest);

        mockUpdateRestLocationService.mockRestore();
        mockUpdateRestLocationAction.mockRestore();
        done();
      });
    });
  });

  describe('rest addition', () => {
    const selectedRest = getMockSelectedRest();
    beforeAll(() => {
      store = getMockStore({ selectedRest });
      navigation.state.params.isSkippable = false;
      navigation.state.params.confirmText = 'Next';
    });

    it('renders without data', () => {
      expect(getShallow()).toMatchSnapshot();
    });

    it('onConfirm calls storeNewRestLocationAction and routes to restProfileScreen', done => {
      const mockStoreNewRestLocationAction = jest.spyOn(myRestActions, 'storeNewRestLocationAction');
      const screen = getShallow();
      screen.instance().setState({ ...newLocation });
      screen.prop('onConfirm')();
      setImmediate(() => {
        expect(mockStoreNewRestLocationAction).toHaveBeenCalledWith(newLocation);
        expect(store.getState().newRest).toEqual({ location: newLocation });
        expect(navigation.navigate).toHaveBeenCalled();
        mockStoreNewRestLocationAction.mockRestore();
        done();
      });
    });

    it('sets RestProfileScreen to "Next" during sign up ', done => {
      navigation.state.params.forSignUp = true;
      const screen = getShallow();
      screen.instance().setState({ ...newLocation });
      screen.prop('onConfirm')();
      setImmediate(() => {
        expect(navigation.navigate).toHaveBeenCalledWith('restProfileScreen', { confirmText: 'Next' });
        done();
      });
    });

    it('sets RestProfileScreen to "Add" during non sign up ', done => {
      navigation.state.params.forSignUp = false;
      const screen = getShallow();
      screen.instance().setState({ ...newLocation });
      screen.prop('onConfirm')();
      setImmediate(() => {
        expect(navigation.navigate).toHaveBeenCalledWith('restProfileScreen', { confirmText: 'Add' });
        done();
      });
    });
  });
});

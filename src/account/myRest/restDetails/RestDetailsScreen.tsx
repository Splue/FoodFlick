import React from 'react';
import { connect } from 'react-redux';
import { Container, Content } from 'native-base';
import SummaryCard from '../../../general/components/SummaryCard';
import { NavigationScreenComponent } from 'react-navigation';
import { ManagerRestSelector } from '../../../general/rest/ManagerRest';
import { NavScreenOptions } from '../../../general/navModels/NavScreenOptions';
import { RootState } from '../../../general/redux/RootState';
import { restBankingScreenName } from './RestBankingScreen';
import RestService from '../../../general/rest/restService';
import { selectAccountRestAction } from '../../../general/rest/restActions';
import { printersScreenName } from './PrintersScreen';
import { addReceiverDetailsScreenName } from './ReceiverDetailsScreen/AddReceiverDetailsScreen';
import { updateReceiverDetailsScreenName } from './ReceiverDetailsScreen/UpdateReceiverDetailsScreen';
import { updateUrlScreenName } from './UpdateUrlScreen';
import { orderManagementScreenName } from './OrderManagementScreen';
import { serversScreenName } from './ServersScreen/SeversScreen';
import { tablesScreenName } from './TablesScreen/TablesScreen';
import { updateTaxRateScreenName } from './TaxRate/UpdateTaxRateScreen';
import { addTaxRateScreenName } from './TaxRate/AddTaxRateScreen';
import { displayPercentageStr } from '../../../general/utils/math';


const RestDetails: NavigationScreenComponent = (props: any) => {
  // todo 1: replace this destructure with ManagerRest.selectors
  const {
    //extract out specific fields because apollo data adds __typename field when using fragments  
    selectedRest: {
      owner,
      managers,
      minsToUpdateCart,
      banking: {
        accountNumberLast4,
        routingNumber,
      },
      profile: {
        name,
        description,
        phone,
        tags,
      },
      location: {
        address: {
          address1,
          address2,
          city,
          state,
          zip
        }
      },
      receiver: {
        receiverId,
        printers,
      },
      servers,
      tables,
      taxRate,
      url,
    },
    isManager,
    signedInUserId,
    navigation: {
      navigate
    },
    bankingLoading,
    selectRest,
  } = props

  tags.toString = function() {
    return this.join(', ');
  };

  const canEditBanking = signedInUserId === owner.userId;
  const profileItems = { name, description, phone, tags };
  const addressItems = { address1, address2, city, state, zip };
  let loadedAccountNumberLast4;
  let loadedRoutingNumber;
  if (props.restBanking) {
    loadedAccountNumberLast4 = props.restBanking.accountNumberLast4;
    loadedRoutingNumber = props.restBanking.routingNumber;
  }
  const bankingItems: any = {};
  let shouldUpdateSelectedRest;
  if (accountNumberLast4 && routingNumber) {
    bankingItems.accountNumberLast4 = `**** **** **** ${accountNumberLast4}`;
    bankingItems.routingNumber = routingNumber;
  } else if (loadedAccountNumberLast4 && loadedRoutingNumber) {
    bankingItems.accountNumberLast4 = `**** **** **** ${loadedAccountNumberLast4}`;
    bankingItems.routingNumber = loadedRoutingNumber;
    shouldUpdateSelectedRest = true;
  }

  const tableCard = (
    <SummaryCard
      title='Tables'
      icon='create'
      emptyItemsText='None'
      items={tables.map(({ _id }) => _id)}
      onIconPress={() => navigate(tablesScreenName, { confirmText: 'Save' })}
    />
  )

  if (!isManager) {
    return (
      <Container>
        <Content>
          {tableCard}
        </Content>
      </Container>
    )
  }

  return (
    <Container>
      <Content>
        <SummaryCard
          title='Profile'
          icon='create'
          emptyItemsText='None'
          items={Object.values(profileItems)}
          onIconPress={() => navigate('restProfileScreen', {confirmText: 'Save'})}
        />
        <SummaryCard
          title='Address'
          icon='create'
          emptyItemsText='None'
          items={Object.values(addressItems)}
          onIconPress={() => navigate('restLocationScreen', {confirmText: 'Save', isSkippable: false})}
        />
        <SummaryCard
          title='Managers'
          icon='create'
          emptyItemsText='None'
          items={managers.map(({ email }) => email)} 
          onIconPress={() => navigate('restManagersScreen')}
        />
        <SummaryCard
          title='Servers'
          icon='create'
          emptyItemsText='None'
          items={servers.map(({ email }) => email)} 
          onIconPress={() => navigate(serversScreenName)}
        />
        {tableCard}
        <SummaryCard
          title='Printers'
          icon='create'
          emptyItemsText='None'
          items={printers.map(({ name }) => name)} 
          onIconPress={() => navigate(printersScreenName)}
        />
        <SummaryCard
          title='Receiver'
          icon='create'
          emptyItemsText='None'
          items={receiverId ? [receiverId] : []} 
          onIconPress={receiverId ?
            () => navigate(updateReceiverDetailsScreenName)
            :
            () => navigate(addReceiverDetailsScreenName)
          }
        />
        <SummaryCard
          title='URL'
          icon='create'
          emptyItemsText='None'
          items={[url]} 
          onIconPress={() => navigate(updateUrlScreenName)}
        />
        <SummaryCard
          title='Order management'
          icon='create'
          emptyItemsText='None'
          items={[`${minsToUpdateCart} mins`]} 
          onIconPress={() => navigate(orderManagementScreenName)}
        />
        <SummaryCard
          title='Sales tax'
          icon='create'
          emptyItemsText='None'
          items={[`${displayPercentageStr(taxRate)}%`]} 
          onIconPress={taxRate ?
            () => navigate(updateTaxRateScreenName)
            :
            () => navigate(addTaxRateScreenName)
          }
        />
        {canEditBanking ?
          <SummaryCard
            title='Banking'
            icon='create'
            items={Object.values(bankingItems)}
            emptyItemsText={bankingLoading ? 'Loading...' : 'No banking data'}
            onIconPress={() => {
              if (shouldUpdateSelectedRest) {
                selectRest({
                  ...props.selectedRest,
                  banking: props.restBanking
                });
              }
              navigate(restBankingScreenName, { confirmText: 'Save', isSkippable: false });
            }}
          />
        : null}
      </Content>
    </Container>
  )
}
RestDetails.navigationOptions = ({ screenProps }: NavScreenOptions) => ({
  title: ManagerRestSelector.getName(screenProps.getAccount().getSelectedRest()) + ' details',
});

const mapStateToProps = (state: RootState) => ({
  isManager: ManagerRestSelector.isManager(state.getAccount().getSelectedRest(), state.getSignedInUser()),
  signedInUserId: state.getSignedInUser()._id,
  selectedRest: state.getAccount().getSelectedRest(),
});

const mapDispatchToProps = dispatch => ({
  selectRest: rest => dispatch(selectAccountRestAction(rest)),
})

export default connect(mapStateToProps, mapDispatchToProps)(RestService.getRestBankingInjector(RestDetails));

export const restDetailsScreenName = 'restDetailsScreen';
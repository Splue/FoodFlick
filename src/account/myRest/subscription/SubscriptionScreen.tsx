import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet } from 'react-native';
import { Container, Content, Text, Card, Body, CardItem, H3 } from 'native-base';
import { NavScreenOptions } from '../../../general/navModels/NavScreenOptions';
import { PlanNames, Plan, PlanSelector } from '../../../general/plan/PlanModel';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import HeaderConfirmButton from '../../../general/components/header/HeaderConfirmButton';
import { NavigationScreenComponent } from 'react-navigation';
import PlanService from '../../../general/plan/planService';
import { ManagerRestSelector, Banking, ActiveSubscription } from '../../../general/rest/ManagerRest';
import { RootState } from '../../../general/redux/RootState';
import { connect } from 'react-redux';
import CenteredH3 from '../../../general/components/CenteredH3';
import { updateRestSubscription } from '../../../general/rest/restActions';
import { showLoading, removeLoading } from '../../../general/redux/uiActions';

type props = {
  activePlans: Plan[],
  currRestPlan: ActiveSubscription,
  hasCard: boolean,
  restBanking: Banking,
  plansLoading: boolean,
  bankingLoading: boolean,
  save: (planId: string) => void,
}

const getNote = (sub: Plan) => PlanSelector.getMonthlyOrders(sub) < 0 ?
  <Text note>Unlimited orders a month</Text>
  :
  <Text note>{PlanSelector.getMonthlyOrders(sub)} orders a month, then {PlanSelector.getOveragePercentageFee(sub)}% per order</Text>

const SubscriptionScreen: NavigationScreenComponent<any, any, props> = ({
  plansLoading,
  bankingLoading,
  activePlans,
  hasCard,
  currRestPlan,
  save,
  navigation
}) => {
  const [selectedPlan, setSelectedPlan] = useState<Plan>(currRestPlan);
  const selectedPlanRef = useRef(selectedPlan);
  selectedPlanRef.current = selectedPlan;

  useEffect(() => {
    if (!plansLoading && !bankingLoading && hasCard) {
      navigation.setParams({
        onConfirm: async () => {
          if (!PlanSelector.isEqual(selectedPlanRef.current, currRestPlan)) {
            await save(PlanSelector.getId(selectedPlanRef.current));
          }
          navigation.goBack();
        }
      });
    }
  }, [plansLoading, bankingLoading, hasCard]);

  if (plansLoading || bankingLoading) return <CenteredH3 text='Loading...' />

  if (!selectedPlan) return <CenteredH3 text='Loading...' />
  const proation = PlanSelector.getProration(selectedPlan);
  const isActiveSelected = PlanSelector.isEqual(selectedPlan, currRestPlan);
  const isActiveCustom = PlanSelector.getName(currRestPlan) === PlanNames.Custom;

  let description;
  if (isActiveSelected) {
    description = `This is your current plan${isActiveCustom ? '. If you update your plan, you cannot get back your custom plan.' : ''}`
  } else if (!hasCard) {
    description = 'You cannot update your plan until you add a card for payment';
  } else if (PlanSelector.getMonthlyRate(currRestPlan) === 0) {
    description = `Your billing period will start today`;
  } else {
    description = "Next month's bill will include a 1-time "
    + (proation > 0 ? `additional charge of` : `refund of`) + ` $${Math.abs(proation)} `
    + `for changing plans during a billing-cycle.${'\n\n'} The exact ${proation > 0 ? 'charge' : 'refund'} may vary `
    + 'slightly depending on when the plan is updated.'
  }

  return (
    <Container style={styles.background}>
      <Content>
        {activePlans.sort((sub1, sub2) =>
          PlanSelector.getName(sub1).localeCompare(PlanSelector.getName(sub2))).map(sub => {
            const subName = PlanSelector.getName(sub);
            if (subName === PlanNames.Custom) return null;
            return (
              <Card
                key={subName}
                style={PlanSelector.isEqual(selectedPlan, sub) ? styles.card : {}}
              >
                <CardItem button onPress={() => setSelectedPlan(sub)}>
                  <Body>
                    <Text>{subName}: ${PlanSelector.getMonthlyRate(sub)} a month</Text>
                    {getNote(sub)}
                  </Body>
                </CardItem>
              </Card>
            )
        })}
        {isActiveCustom &&
          <Card style={isActiveSelected ? styles.card : {}}>
            <CardItem button onPress={() => setSelectedPlan(currRestPlan)}>
              <Body>
                <Text>{PlanSelector.getName(currRestPlan)}: ${PlanSelector.getMonthlyRate(currRestPlan)} a month</Text>
                {getNote(currRestPlan)}
              </Body>
            </CardItem>
          </Card>
        }
        <H3 style={styles.description}>
          {description}
        </H3>
      </Content>
    </Container>
  );
}

SubscriptionScreen.navigationOptions = ({ navigation }: NavScreenOptions) => ({
  title: 'Subscription plans',
  headerRight: navigation.getParam('onConfirm') && <HeaderConfirmButton onConfirm={navigation.getParam('onConfirm')} text='Save' />
});

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
    paddingHorizontal: commonColor.listItemPadding,
    paddingTop: commonColor.listItemPadding
  },
  card: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: commonColor.brandInfo,
  },
  description: {
    textAlign: 'center',
    paddingTop: 10,
  },
});

const mapStateToProps = (state: RootState) => ({
  // necessary getActivePlansInjector
  selectedRest: state.getAccount().getSelectedRest(),
  hasCard: ManagerRestSelector.getSubscriptionCard(state.getAccount().getSelectedRest()),
  currRestPlan: ManagerRestSelector.getActiveSubscription(state.getAccount().getSelectedRest()),
});

const mapDispatchToProps = dispatch => ({
  save: async planId => {
    dispatch(showLoading());
    await dispatch(updateRestSubscription(planId));
    dispatch(removeLoading());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(
  PlanService.getActivePlansInjector(SubscriptionScreen)
);

export const subscriptionScreenName = 'subscriptionScreen';
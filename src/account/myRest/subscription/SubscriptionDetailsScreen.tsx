import React from 'react';
import { connect } from 'react-redux';
import { Container, Content } from 'native-base';
import SummaryCard from '../../../general/components/SummaryCard';
import { NavigationScreenComponent } from 'react-navigation';
import { ManagerRestSelector, ManagerRest } from '../../../general/rest/ManagerRest';
import { RootState } from '../../../general/redux/RootState';
import { PlanSelector } from '../../../general/plan/PlanModel';
import { subscriptionScreenName } from './SubscriptionScreen';
import CenteredH3 from '../../../general/components/CenteredH3';
import { cardScreenName } from '../../customer/CardScreen';

type props = {
  selectedRest: ManagerRest,
  signedInUserId: string,
}

const SubscriptionDetailsScreen: NavigationScreenComponent<any, any, props> = ({
  selectedRest,
  signedInUserId,
  navigation,
}) => {

  const canEditSubscription = signedInUserId === ManagerRestSelector.getOwner(selectedRest).userId;
  if (!canEditSubscription) {
    return <CenteredH3 text='Only the restaurant owner can change the subscription plan' />
  }

  const card = ManagerRestSelector.getSubscriptionCard(selectedRest);

  return (
    <Container>
      <Content>
        <SummaryCard
          title='Plans'
          icon='create'
          emptyItemsText=''
          items={[`Current plan: ${PlanSelector.getName(ManagerRestSelector.getActiveSubscription(selectedRest))}`]} 
          onIconPress={() => navigation.navigate(subscriptionScreenName)}
        />
        <SummaryCard
          title='Payment'
          icon='create'
          items={card ? [card] : []}
          emptyItemsText='No card'
          onIconPress={() => navigation.navigate(cardScreenName, { confirmText: 'Save' })}
        />
      </Content>
    </Container>
  )
}

SubscriptionDetailsScreen.navigationOptions = {
  title: 'Subscription settings',
};

const mapStateToProps = (state: RootState) => ({
  signedInUserId: state.getSignedInUser()._id,
  selectedRest: state.getAccount().getSelectedRest(),
});

export default connect(mapStateToProps)(SubscriptionDetailsScreen);

export const subscriptionDetailsScreenName = 'subscriptionDetailsScreen';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, ScrollView, PanResponder } from 'react-native';
import { Container, Text, Button, Icon, Fab, Content } from 'native-base';
import CenteredH3 from '../../../general/components/CenteredH3';
import { selectItemsAction, selectAccountCategoryAction } from '../../../general/menu/menuActions';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import MenuOverview from '../../../general/menu/MenuOverview';
import { NavScreenOptions } from '../../../general/navModels/NavScreenOptions';
import { RootState } from '../../../general/redux/RootState';
import { ManagerRestSelector } from '../../../general/rest/ManagerRest';
import { itemDetailsScreenName } from './ItemDetailsScreen';
import { updateItemsScreenName } from './UpdateItemsScreen';
import { itemFlickPickerScreenName } from './ItemFlickPickerScreen';
import { updateCategoriesScreenName } from './UpdateCategoriesScreen';
import { categoryDetailsScreenName } from './CategoryDetailScreen';

class MenuUpdateScreen extends React.Component<any, any> {
  static navigationOptions = ({ navigation: { navigate }, screenProps }: NavScreenOptions) => ({
    title: ManagerRestSelector.getName(screenProps.getAccount().getSelectedRest()) + ' menu',
  });

  containerScroll = undefined;

  state = {
    scrollHeight: null,
  }

  onPressAddItems = () => this.props.navigation.navigate(itemFlickPickerScreenName, { confirmText: 'Next' })

  emptyCategoryView = (
    <React.Fragment>
      <CenteredH3 text="This category won't be public until you add some items." />
      <Button block onPress={this.onPressAddItems}>
        <Text>Add items</Text>
      </Button>
    </React.Fragment>
  )

  renderCategory = category => (
    <React.Fragment>
      <View style={styles.buttons}>
        <Button style={styles.updateButton} bordered onPress={() => this.props.navigation.navigate(updateItemsScreenName)}>
          <Text>Update items</Text>
        </Button>
        <Button style={styles.addButton} bordered onPress={this.onPressAddItems}>
          <Text>Add items</Text>
        </Button>
      </View>
      {category}
    </React.Fragment>
  )
  
  renderMenu = tabs => (
    <React.Fragment>
      {tabs}
    </React.Fragment>
  )

  render () {
    return (this.props.menu.length === 0 ?
      <Container>
        <CenteredH3 text="This restaurant won't be public until you add a menu. Start by adding a category"/>
        <Button block onPress={() => this.props.navigation.navigate(categoryDetailsScreenName,  { confirmText: 'Add' })}>
          <Text>Create category</Text>
        </Button>
      </Container>
      :
      <Container>
        <ScrollView
          ref={ref => this.containerScroll = ref}
          contentContainerStyle={{
            height: this.state.scrollHeight,
          }} 
        >
          <View style={styles.buttons}>
            <Button style={styles.updateButton} bordered onPress={() => this.props.navigation.navigate(updateCategoriesScreenName)}>
              <Text>Update categories</Text>
            </Button>
            <Button style={styles.addButton} bordered
            onPress={() => this.props.navigation.navigate(categoryDetailsScreenName,  { confirmText: 'Add' })}>
              <Text>Add category</Text>
            </Button>
          </View>
          {/* wrap renderCategory in fn otherwise, it ONLY gets called on mount and we need it called/rendered on scroll */}
          <MenuOverview
            emptyCategoryView={this.emptyCategoryView}
            renderMenu={this.renderMenu}
            menu={this.props.menu}
            renderCategory={category => this.renderCategory(category)}
            onPressItem={this.props.onPressItem}
            onChangeTab={height => {
              if (this.containerScroll) this.containerScroll.scrollTo({ x: 0, y: 0 });
              // 180 is the height of the category buttons. determined by inspection
              this.setState({ scrollHeight: height + 180 });
            }}
            selectCategory={this.props.selectCategory}
            profile={this.props.profile}
            location={this.props.location}
          />
        </ScrollView>
        <Fab style={styles.fab}>
          <Icon name='arrow-round-up' style={styles.icon} onPress={() => this.containerScroll.scrollTo({ x: 0, y: 0 })} />
        </Fab>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  addButton: {
    flex: 1,
    margin: 10,
    justifyContent: 'center',
  },
  buttons: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  updateButton: {
    flex: 1,
    margin: 10,
    justifyContent: 'center',
  },
  icon: {
    color: commonColor.brandInfo
  },
  fab: {
    backgroundColor: commonColor.brandCanvas,
  },
  emptyCategoryButton: {
    alignSelf: 'center',
    margin: 10
  }
})

const mapStateToProps = (state: RootState) => ({
  menu: ManagerRestSelector.getMenu(state.getAccount().getSelectedRest()),
  profile: ManagerRestSelector.getProfile(state.getAccount().getSelectedRest()),
  location: ManagerRestSelector.getLocation(state.getAccount().getSelectedRest()),
});

const mapDispatchToProps = (dispatch, {navigation}) => ({
  onPressItem: (item, index) => {
    dispatch(selectItemsAction([{ index, item }]));
    navigation.navigate(itemDetailsScreenName, { confirmText: 'Save' });
  },
  selectCategory: category => dispatch(selectAccountCategoryAction(category)),
})

export default connect(mapStateToProps, mapDispatchToProps)(MenuUpdateScreen);

export const menuUpdateScreenName = 'menuUpdateScreen';
import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet } from 'react-native';
import { Item, Label, Input, Container, Content } from 'native-base';
import { goFlickPickerScreenAfterCategoryAddition, addCategoryAction, updateCategoryAction } from '../../../general/menu/menuActions';
import ValidatedForm from '../../../general/components/ValidatedForm';
import HeaderConfirmButton from '../../../general/components/header/HeaderConfirmButton';
import { RootState } from '../../../general/redux/RootState';
import { NavScreenOptions } from '../../../general/navModels/NavScreenOptions';
import { ManagerRestSelector } from '../../../general/rest/ManagerRest';
import ValidatedInput from '../../../general/components/ValidatedInput';
import { getCleanInputs } from '../../../general/utils/formHelpers';
import { ManagerCategory } from '../../../general/menu/ManagerMenu';
import { NavigationRoute, NavigationScreenProp } from 'react-navigation';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import { showLoading, removeLoading } from '../../../general/redux/uiActions';

type props = {
  navigation: NavigationScreenProp<NavigationRoute>,
  selectedCategory: ManagerCategory,
  onConfirm: (categoryDetail: state) => void
};

type state = {
  name: string,
  description: string,
};

class CategoryDetailScreen extends React.Component<props, state> {
  //default to emtpy object otherwise, params is undefined if there are no params, causing null pointer 
  static navigationOptions = ({navigation: {navigate, state: {params = {}}}}: NavScreenOptions) => ({
    tabBarVisible: false,
    title: params.confirmText === 'Add' ? 'Add category' : 'Update category',
    headerRight: <HeaderConfirmButton onConfirm={params.onConfirm} text={params.confirmText}/>
  });

  state = this.props.navigation.state.params.confirmText === 'Save' ?
  {
    name: this.props.selectedCategory.name,
    description: this.props.selectedCategory.description,
  }
  // need {} with ternary because i tried to do state = expression && { stuff } which made state = false when expression 
  // was false
  :
  {
    name: '',
    description: '',
  }

  render () {
    const { navigation, onConfirm } = this.props;

    const inputs = {
      name: (
        <ValidatedInput 
          floatingLabel
          required
          label='Name'
          value={this.state.name}
          onChangeText={name => this.setState({ name })}
        />
      ) as unknown as ValidatedInput
    }

    return (
      <Container style={styles.background}>
        <Content>
          <ValidatedForm onConfirm={() => onConfirm(this.state)} originalInputs={inputs} render={newInputs => (
            <React.Fragment>
              {Object.values(newInputs)}
              <Item floatingLabel>
                <Label>Description</Label>
                <Input value={this.state.description} onChangeText={(description) => this.setState({description})}/>
              </Item>
            </React.Fragment>
          )} />
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  }
});

const mapStateToProps = (state: RootState) => ({
  selectedRestId: ManagerRestSelector.getId(state.getAccount().getSelectedRest()),
  selectedCategory: state.getAccount().getSelectedCategory(),
});

const mergeProps = ({ selectedRestId, selectedCategory }, { dispatch }, props) => {
  const { navigation } = props;
  return {
    ...props,
    selectedCategory,
    onConfirm: navigation.state.params && navigation.state.params.confirmText === 'Add' ? 
    async category => {
      dispatch(showLoading());
      const cleanCategory = getCleanInputs(category);
      await dispatch(addCategoryAction(selectedRestId, cleanCategory))
      dispatch(goFlickPickerScreenAfterCategoryAddition());
      dispatch(removeLoading());
    } : 
    async newCategory => { //otherwise we have 'Save' for updating
      const cleanCategory = getCleanInputs(newCategory);
      dispatch(showLoading());
      await dispatch(updateCategoryAction(selectedRestId, selectedCategory, cleanCategory))
      navigation.goBack();
      dispatch(removeLoading());
    } 
  }
};

export default connect(mapStateToProps, null, mergeProps)(CategoryDetailScreen);

export const categoryDetailsScreenName = 'categoryDetailScreen';
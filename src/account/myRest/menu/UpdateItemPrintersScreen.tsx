import { Text, Button, Container, Content, ListItem, Left } from 'native-base';
import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import { RootState } from '../../../general/redux/RootState';
import { ManagerRestSelector, Printer } from '../../../general/rest/ManagerRest';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import CenteredH3 from '../../../general/components/CenteredH3';
import { addPrinterDetailsScreenName } from '../restDetails/PrinterDetailsScreen/AddPrinterDetailsScreen';
import HeaderConfirmButton from '../../../general/components/header/HeaderConfirmButton';
import { ItemPrinter } from '../../../general/menu/BaseMenu';

type navParams = {
  itemName?: string,
  itemPrinters?: ItemPrinter[],
  onConfirm: (printers: ItemPrinter[]) => void,
  preventAdd?: boolean,
  privateNames: string[]
}

type props = {
  availablePrinters: Printer[];
};

const UpdateItemPrintersScreen: NavigationScreenComponent<navParams, any, props> = ({ availablePrinters, navigation }) => {
  const preventAdd = navigation.getParam('preventAdd');
  const publicName = navigation.getParam('itemName');
  const privateNames = navigation.getParam('privateNames');
  const currPrinterSelections = navigation.getParam('itemPrinters')
  let dupelicatePrivateName = '';
  let hasEmptyName = !!publicName ? false : true;
  for (let i = 0; i < privateNames.length; i++) {
    if (!privateNames[i]) hasEmptyName = true;
    for (let j = 0; j < privateNames.length; j++) {
      if (i === j) continue;
      if (privateNames[i] === privateNames[j]) dupelicatePrivateName = privateNames[i];
    }
  }
  const onPressAdd = () => navigation.navigate(addPrinterDetailsScreenName);

  const [printerToItemName, setPrinterToItemName] = useState(currPrinterSelections.reduce((map, printer) => {
    map[printer.name] = printer.itemName;
    return map;
  }, {}));
  // https://github.com/facebook/react/issues/14010
  const printerToNamesRef = useRef(printerToItemName);
  printerToNamesRef.current = printerToItemName;
  useEffect(() => {
    navigation.setParams({
      onConfirm: hasEmptyName || dupelicatePrivateName ?
        null
        :
        () => {
          navigation.getParam('onConfirm')(Object.keys(printerToNamesRef.current).reduce((itemPrinters, itemPrinterName) => {
            const {
              name,
              ip,
              port,
              type
            } = ManagerRestSelector.getPrinter(availablePrinters, itemPrinterName)
            if (printerToNamesRef.current[itemPrinterName]) {
              itemPrinters.push({
                name,
                itemName: printerToNamesRef.current[itemPrinterName],
                ip,
                port,
                type
              });
            }
            return itemPrinters;
          }, [] as ItemPrinter[])),
          navigation.goBack();
        },
      privateNames,
    })
  }, []);

  const getListItemForItemName = (printerName: string, itemName: string) => (
    <ListItem
      key={itemName}
      button
      selected={printerToItemName[printerName] === itemName}
      onPress={() => setPrinterToItemName({
        ...printerToItemName,
        [printerName]: printerToItemName[printerName] === itemName ? null : itemName
      })}
    >
      <Left>
        <Text>
          {itemName}
        </Text>
      </Left>
    </ListItem>
  )


  if (hasEmptyName) {
    return <CenteredH3 text={`Found an empty name. Please go back and enter a name`} />
  }

  if (dupelicatePrivateName) {
    return <CenteredH3 text={`Found duplicate private name ${dupelicatePrivateName}. Please go back and remove it`} />
  }

  return (availablePrinters.length === 0 ?
    <Container>
      <CenteredH3 text='You have no printers. Start by adding a printer' />
      {!preventAdd && 
      <Button primary block style={styles.button} onPress={onPressAdd}>
        <Text>Add printer</Text>
      </Button>}
    </Container>
    :
    <Container>
      {!preventAdd &&
      <Button transparent primary style={styles.button} onPress={onPressAdd}>
        <Text>Add printer</Text>
      </Button>}
      <Content style={styles.content}>
        {availablePrinters.map((printer, index) => (
          <React.Fragment key={index}>
            <ListItem itemDivider>
              <Text>{printer.name}</Text>
            </ListItem>
            {getListItemForItemName(printer.name, publicName)}
            {privateNames.map(name => getListItemForItemName(printer.name, name))}
          </React.Fragment>
        ))}
      </Content>
    </Container>
  )
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: commonColor.brandCanvas,
  },
  button: {
    alignSelf: 'center',
    marginVertical: 8,
  }
});

UpdateItemPrintersScreen.navigationOptions = ({ navigation }) => ({
  tabBarVisible: false,
  title: 'Assign names to printers',
  headerRight: navigation.getParam('onConfirm') === null ? null : <HeaderConfirmButton onConfirm={navigation.getParam('onConfirm')} text='Done' />
});

const mapStateToProps = (state: RootState) => ({
  availablePrinters: ManagerRestSelector.getPrinters(state.getAccount().getSelectedRest()),
});

export default connect(mapStateToProps)(UpdateItemPrintersScreen);

export const updateItemPrintersScreenName = 'updateItemPrintersScreen';
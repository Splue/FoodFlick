import { Container, Content } from 'native-base';
import React from 'react';
import { StyleSheet } from 'react-native';
import ValidatedForm from '../../../general/components/ValidatedForm';
import { NavigationRoute, NavigationScreenProp } from 'react-navigation';
import HeaderConfirmButton from '../../../general/components/header/HeaderConfirmButton';
import { NavScreenOptions } from '../../../general/navModels/NavScreenOptions';
import ValidatedInput from '../../../general/components/ValidatedInput';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import { option } from '../../../general/menu/BaseMenu';

type props = {
  navigation: NavigationScreenProp<NavigationRoute>;
  choice: option,
}

type state = {
  choice: string,
}

class ChoiceScreen extends React.Component<props, state> {
  static navigationOptions = ({navigation: {state: {params = {}}}}: NavScreenOptions) => ({
    tabBarVisible: false,
    title: params.confirmText === 'Add' ? 'Add choice' : 'Update choice',
    headerRight: <HeaderConfirmButton onConfirm={params.onConfirm} text={params.confirmText} />
  });

  state = {
    choice: this.props.navigation.state.params.option || '',
  }

  render () {
    const inputs = {
      choice: (
        <ValidatedInput
          floatingLabel
          required
          label='A custom choice with this order'
          value={this.state.choice}
          onChangeText={choice => this.setState({ choice })}
        />
      ) as unknown as ValidatedInput
    }
    return (
      <Container style={styles.background}>
        <Content>
          <ValidatedForm
            onConfirm={() => this.props.navigation.state.params.onConfirmUnvalidated(this.state.choice)}
            originalInputs={inputs}
            render={newInputs => Object.values(newInputs)}
          />
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  }
});

export default ChoiceScreen;

export const choiceScreenName = 'choiceScreen';
import { Container, Tab, Tabs } from 'native-base';
import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { NavigationRoute, NavigationScreenProp } from 'react-navigation';
import { connect } from 'react-redux';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import CameraRollPicker from '../../../general/components/flick/CameraRollPicker';
import HeaderConfirmButton from '../../../general/components/header/HeaderConfirmButton';
import { BaseCategorySelector } from '../../../general/menu/BaseMenu';
import { addSelectedItemUrisAction, updateItemUriAction, addEmtpyItemToSelectedAction } from '../../../general/menu/menuActions';
import { NavScreenOptions } from '../../../general/navModels/NavScreenOptions';
import { itemDetailsScreenName } from './ItemDetailsScreen';
import MyFlicksPicker from '../../MyFlicksPicker';


type flickSelection = {
  // key is the index of the photo in the camera roll and value is the photo's uri
  [key: number]: string
}

type state = {
  cameraRollSelected: flickSelection;
  myFlicksSelected: flickSelection;
}

type props = {
  navigation: NavigationScreenProp<NavigationRoute>;
  onConfirm: (flicks: string[]) => void;
  onSkip: () => void;
}

class ItemFlickPickerScreen extends React.Component<props, state> {
  static navigationOptions = ({ navigation: { state: { params = {} } }, screenProps }: NavScreenOptions) => {
    const name = BaseCategorySelector.getName(screenProps.getAccount().getSelectedCategory());
    return {
      tabBarVisible: false,
      title: name ? `Flicks of ${name}` : 'Flicks',
      headerRight: <HeaderConfirmButton onConfirm={params.onConfirm} text={params.confirmText} />
    }
  };

  state = {
    cameraRollSelected: {},
    myFlicksSelected: {},
  }

  onCameraPermissionGranted = () => {
    this.props.navigation.setParams({
      onConfirm: () => this.props.onConfirm([
        ...Object.values(this.state.cameraRollSelected) as string[],
        ...Object.values(this.state.myFlicksSelected) as string[],
      ])
    });
  }

  updateCameraRollSelected = (newSelected: flickSelection) => {
    this.setState({ cameraRollSelected: newSelected });
  }

  updateMyFlicksSelected = (newSelected: flickSelection) => {
    this.setState({ myFlicksSelected: newSelected });
  }

  render() {
    // if there is an index, that means i came from itemDetailScreen, and im updating an item picture and thus can
    // only have 1 selected at a time
    const multiple = this.props.navigation.state.params.itemIndex ? false : true;
    const onSkip = (this.props.navigation.state.params || {}).isSkippable ? this.props.onSkip : null;
    return (
      <Container>
        <Tabs initialPage={0} tabBarBackgroundColor={commonColor.brandCanvas} style={styles.tabs} >
          <Tab heading='Camera Roll' textStyle={styles.tabText} activeTextStyle={styles.tabActiveText}>
            <CameraRollPicker canSelectMultiple={multiple} selected={this.state.cameraRollSelected} numColumns={4}
              updateSelected={this.updateCameraRollSelected} onCameraPermissionGranted={this.onCameraPermissionGranted}
              onSkip={onSkip} />
          </Tab>
          <Tab heading='Uploaded flicks' textStyle={styles.tabText} activeTextStyle={styles.tabActiveText}>
            <MyFlicksPicker canSelectMultiple={multiple} selected={this.state.myFlicksSelected} numColumns={4}
              updateSelected={this.updateMyFlicksSelected} />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

// todo 1: 1) add items. 2) edit photo for item in itemDetailScreen. 3) click back in flickPickerScreen. note that it
// takes me back to menu overview, but should take me back to itemDetail
const mapDispatchToProps = (dispatch, { navigation }) => ({
  onConfirm: navigation.state.params && navigation.state.params.confirmText === 'Next' ?
    (uris: string[]) => { // next means im adding items
      uris.length === 0 ? dispatch(addEmtpyItemToSelectedAction()) : dispatch(addSelectedItemUrisAction(uris));
      navigation.navigate(itemDetailsScreenName, { confirmText: 'Add' });
    } :
    async (uris: string[]) => { // otherwise we have 'Save' for updating, coming from itemDetailsScreen
      if (uris.length === 1) { // only update the flick if there was a flick selected
        // for now only use the first item, but uris is an array for future multi item update support
        await dispatch(updateItemUriAction(navigation.state.params.itemIndex, uris[0]));
      }
      navigation.goBack();
    },
  onSkip: () => navigation.goBack()
});

export default connect(null, mapDispatchToProps)(ItemFlickPickerScreen);

export const itemFlickPickerScreenName = 'itemflickPickerScreen';

const styles = StyleSheet.create({
  tabs: {
    backgroundColor: commonColor.brandCanvas,
    flex: 1
  },
  tabText: {
    color: commonColor.brandText,
  },
  tabActiveText: {
    color: commonColor.brandText,
  }
});
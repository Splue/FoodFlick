import React from 'react';
import { connect } from 'react-redux';
import { Dimensions, Image, StyleSheet, View, Platform } from 'react-native';
import { Card, CardItem, Item, Label, Input, Button, Icon, Text, Toast, H3, Container, Right, List, Body, Content } from 'native-base';
import ValidatedForm from '../../../general/components/ValidatedForm';
import HeaderBackButton from '../../../general/components/header/HeaderBackButton';
import HeaderConfirmButton from '../../../general/components/header/HeaderConfirmButton';
import { clearItemsAction, addEmtpyItemToSelectedAction, deleteItemLocallyAction } from '../../../general/menu/menuActions';
import { addItemsAction, updateItemsAction } from '../../../general/menu/menuActions';
import { BaseCategorySelector, BaseItemSelector, cloneOptionGroups, option, getCleanItems } from '../../../general/menu/BaseMenu';
import { NavScreenOptions } from '../../../general/navModels/NavScreenOptions';
import { RootState } from '../../../general/redux/RootState';
import { ManagerCategory, ManagerItem, ItemUpdateState, ManagerItemSelector } from '../../../general/menu/ManagerMenu';
import { ManagerRest, Printer } from '../../../general/rest/ManagerRest';
import { itemFlickPickerScreenName } from './ItemFlickPickerScreen';
import ValidatedInput from '../../../general/components/ValidatedInput';
import { NavigationScreenProp, NavigationRoute } from 'react-navigation';
import { getCleanInput } from '../../../general/utils/formHelpers';
import MovableList from '../../../general/components/list/MovableList';
import MovableItem from '../../../general/components/list/MovableItem';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import PopupMenu from '../../../general/components/PopupMenu';
import { choiceScreenName } from './ChoiceScreen';
import MovableRow from '../../../general/components/list/MovableRow';
import { updateItemPrintersScreenName } from './UpdateItemPrintersScreen';
import { showLoading, removeLoading } from '../../../general/redux/uiActions';

//todo 2: move typename logic into apollolink
//https://github.com/apollographql/apollo-client/issues/1564#issuecomment-357492659
const getStateItems = (items: ItemUpdateState[]) => items.map(({ index, item }) => {
  // item's index in a category. only exists when updating an item. otherwise it's undefined since unadded items
  // can't have index in category
  const newItem = {
    index,
    item: {
      ...item,
      privateNames: (item.privateNames || []).map(name => ( // remap to discard __typename
        name
      )),
      // item.prices is undefined if we're adding items
      prices: (item.prices || []).map(({ value, label }) => ({ // remap to discard __typename
        value,
        label,
      })),
      // item.addons is undefined if we're adding items
      addons: (item.addons || []).map(({ value, label }) => ({ // remap to discard __typename
        value,
        label,
      })),
      likes: { ...item.likes }, // need to destructure again otherwise the likes object is referenced
      optionGroups: (item.optionGroups || []).map(({ options }) => ({
        options: options.map(({ name, price }) => ({
          name,
          price,
        }))
      })),
      printers: (item.printers || []).map(({ name, ip, itemName, port, type }) => ({
        name,
        ip,
        itemName,
        port,
        type
      }))
    }
  };
  // @ts-ignore
  delete newItem.item.__typename; //__typename is added by apollo
  delete newItem.item.likes; // itemDetails cannot update likes
  return newItem;
});

type state = {
  items: ItemUpdateState[],
  contentScrollEnabled: boolean,
  //necessary so that item details rerenders and forces recalculation of option groups height
  doesNeedRefresh: boolean,
}

type props = {
  items: ItemUpdateState[],
  addEmptyItem: () => void,
  onConfirm: (state: state) => void,
  onBack: () => void,
  deleteItemLocally: (index: number) => void,
  navigation: NavigationScreenProp<NavigationRoute>,
}

const priceMessage = 'Ex: 10.00';
const priceRegex = /^\d+([.]\d{0,2})?$/;

class ItemDetails extends React.Component <props, state> {
  static navigationOptions = ({navigation: {navigate, state: {params = {}}}, screenProps}: NavScreenOptions) => ({
    tabBarVisible: false,
    // there's no selectedCategory when it gets temporarily cleared by selectRest
    title: `Item details for ${BaseCategorySelector.getName(screenProps.getAccount().getSelectedCategory())}`,
    headerRight: <HeaderConfirmButton onConfirm={params.onConfirm} text={params.confirmText}/>,
    headerLeft: <HeaderBackButton onPress={params.onBack} />
  });

  sortableListRef = null;

  state = {
    items: getStateItems(this.props.items),
    contentScrollEnabled: true,
    doesNeedRefresh: false,
  }

  componentDidUpdate() {
    if (this.state.items.length === 0) this.props.navigation.goBack();
  }

  static getDerivedStateFromProps (nextProps: props, state: state) {
    // only add new items to the end of state. if item is arleady in state, leave as is. don't worry abotu deleting as
    // that's handled immediately from event handler as the handler has index deleted
    const nextItems = nextProps.items;
    const currItems = state.items;
    if (nextItems.length > currItems.length) {
      return { items: [...currItems, ...getStateItems(nextItems.slice(currItems.length))]};
    } else if (nextItems.length === currItems.length) {
      for (let i = 0; i < nextItems.length; i++) {
        if (nextItems[i].item.uri !== currItems[i].item.uri) {
          const newItems: ItemUpdateState[] = [...currItems];
          newItems[i].item.uri = nextItems[i].item.uri;
          return { items: newItems };
        }
      }
    }
    return null;
  }

  updatePrinterItemNames = (targetItem: ManagerItem, prevName, newName) => {
    targetItem.printers.forEach((printer, index) => {
      if (printer.itemName === prevName) {
        targetItem.printers[index].itemName = newName;
      }
    });
  }

  updateItem = (indexInSelectedItems, field, value) => {
    const items = [...this.state.items];
    //.item because items is a list of { index, item } where index is the  index of the item within the category
    items[indexInSelectedItems].item[field] = value;
    this.setState({ items });
  }

  updateItemName = (indexInSelectedItems, field, value) => {
    const items = [...this.state.items];
    const targetItem = items[indexInSelectedItems].item;
    const prevName = targetItem.name;
    this.updatePrinterItemNames(targetItem, prevName, value);
    //.item because items is a list of { index, item } where index is the  index of the item within the category
    targetItem[field] = value;
    this.setState({ items });
  }

  addOptionGroup = indexInSelectedItems => {
    this.setState(prevState => {
      const items = [...prevState.items];
      // create new groups, otherwise MovableList won't recognize change and recalculate size
      const newOptionGroups = cloneOptionGroups(items[indexInSelectedItems].item.optionGroups);
      newOptionGroups.unshift({ options: [] });
      items[indexInSelectedItems].item.optionGroups = newOptionGroups;
      return { items };
    });
  }

  addPrice = (indexInSelectedItems: number) => {
    const items = [...this.state.items];
    items[indexInSelectedItems].item.prices.push({
      value: null,
      label: null,
    });
    this.setState({ items });
  }

  deletePrice = (indexInSelectedItems: number, targetPriceIndex: number) => {
    const items = [...this.state.items];
    const prices = items[indexInSelectedItems].item.prices.filter((price, priceIndex) => priceIndex !== targetPriceIndex);
    items[indexInSelectedItems].item.prices = prices;
    this.setState({ items });
  }

  addAddon = (indexInSelectedItems: number) => {
    const items = [...this.state.items];
    items[indexInSelectedItems].item.addons.push({
      value: null,
      label: null,
    });
    this.setState({ items });
  }

  addPrivateName = (indexInSelectedItems: number) => {
    const items = [...this.state.items];
    items[indexInSelectedItems].item.privateNames.push(null);
    this.setState({ items });
  }

  deleteAddon = (indexInSelectedItems: number, targetAddonIndex: number) => {
    const items = [...this.state.items];
    const addons = items[indexInSelectedItems].item.addons.filter((addon, addonIndex) => addonIndex !== targetAddonIndex);
    items[indexInSelectedItems].item.addons = addons;
    this.setState({ items });
  }

  deletePrivateName = (indexInSelectedItems: number, targetNameIndex: number) => {
    const items = [...this.state.items];
    const targetItem = items[indexInSelectedItems].item;
    const deletedPrivateName = targetItem.privateNames[targetNameIndex];
    const printersWithoutPrivateName = targetItem.printers.filter(({ itemName }) => itemName !== deletedPrivateName);
    const privateNames = targetItem.privateNames.filter((name, nameIndex) => nameIndex !== targetNameIndex);
    targetItem.privateNames = privateNames;
    targetItem.printers = printersWithoutPrivateName;
    this.setState({ items });
  }

  updatePrices = (indexInSelectedItems, index, field, value) => {
    const items = [...this.state.items];
    //.item because items is a list of { index, item } where index is the  index of the item within the category
    items[indexInSelectedItems].item.prices[index][field] = value;
    this.setState({ items });
  }

  updateAddons = (indexInSelectedItems, index, field, value) => {
    const items = [...this.state.items];
    //.item because items is a list of { index, item } where index is the  index of the item within the category
    items[indexInSelectedItems].item.addons[index][field] = value;
    this.setState({ items });
  }

  updatePrivateNames = (indexInSelectedItems, index, value) => {
    const items = [...this.state.items];
    const targetItem = items[indexInSelectedItems].item;
    const prevPrivateName = targetItem.privateNames[index];
    this.updatePrinterItemNames(targetItem, prevPrivateName, value);
    //.item because items is a list of { index, item } where index is the  index of the item within the category
    targetItem.privateNames[index] = value;
    this.setState({ items });
  }

  _getPriceValueStr = (price: number) => {
    if (!price) return '';
    return price.toString();
  }

  validateOnEnd = price => {
    if (price.length === 0) return `Required`;
    return priceRegex.test(getCleanInput(price)) ? '' : priceMessage
  }

  validatePrivateNamesOnEnd = (name, index, privateNames) => {
    if (name.length === 0) return `Required`;
    for (let i = 0; i < privateNames.length; i++) {
      if (i === index) continue;
      if (name === privateNames[i]) return 'Private name already exists';
    }
    return '';
  }

  /**
   * Validates the given price string isaany number of digits, followed by a . and 0 - 2 digits. 
   * We allow 0 - 2 digits because we don't want to display error as user is typing the price. EX: we don't want to
   * display error when user types "10." because they are about to type "10.00". If the validation is successful, return
   * empty string, otherwise return the error validation message describing why the price string failed validation
   * 
   * @param {price} the price string
   * @returns {string} the validation message otherwise, an empty string to indicate no error validation message
   * necessary
   */
  validatePriceChange = (price: string): string => {
    const cleanedPrice = getCleanInput(price);
    return priceRegex.test(cleanedPrice) ? '' : priceMessage;
  }

  deleteLocalItem = targetIndex => {
    this.props.deleteItemLocally(targetIndex);
    const before = this.state.items.slice(0, targetIndex);
    const after = this.state.items.slice(targetIndex+1);
    const remainingItems = [...before, ...after];

    remainingItems.length > 0 ?
      this.setState({
        items: [...before, ...after],
      })
      :
      this.props.navigation.goBack();
  }

  onPressOption = (itemIndex, optionGroupIndex, optionIndex, option) => this.props.navigation.navigate(choiceScreenName, {
    confirmText: 'Save',
    option,
    onConfirmUnvalidated: (choice: string) => {
      this.setState(prevState => {
        const items = [...prevState.items];
        const clonedOptionGroups = cloneOptionGroups(items[itemIndex].item.optionGroups);
        clonedOptionGroups[optionGroupIndex].options[optionIndex].name = choice;
        items[itemIndex].item.optionGroups = clonedOptionGroups;
        return { items };
      });
      // pop instead of goBack() because otherwise we'd go back to before ItemDetailsScreen as this
      // navigation prop is the ItemDetailsScreen navigation prop
      this.props.navigation.pop();
    }
  })

  onAddOption = (itemIndex, optionGroupIndex) => {
    this.setState({ doesNeedRefresh: true })
    this.props.navigation.navigate(choiceScreenName, {
      confirmText: 'Add',
      // onConfirmUnvalidated instead of onConfirm otherwise naming colision with re-setting onConfirm
      // causes infinite loop
      onConfirmUnvalidated: (choice: string) => {
        // pop instead of goBack() because otherwise we'd go back to before ItemDetailsScreen as this
        // navigation prop is the ItemDetailsScreen navigation prop
        this.props.navigation.pop();
        this.setState(prevState => {
          const items = [...prevState.items];
          const clonedOptionGroups = cloneOptionGroups(items[itemIndex].item.optionGroups);
          clonedOptionGroups[optionGroupIndex].options.push({
            name: choice,
          });
          items[itemIndex].item.optionGroups = clonedOptionGroups;
          return {
            items,
            doesNeedRefresh: false,
          };
        });
      }
    });
  }

  onDeleteOptionGroup = (itemIndex, optionGroupIndex) => {
    this.setState(prevState => {
      const items = [...prevState.items];
      const clonedOptionGroups = cloneOptionGroups(items[itemIndex].item.optionGroups);
      clonedOptionGroups.splice(optionGroupIndex, 1);
      items[itemIndex].item.optionGroups = clonedOptionGroups;
      return { items };
    });
  }

  onPressPopupMenu = (actionIndex, itemIndex, optionGroupIndex) => {
    if (actionIndex === 0) {
      this.onAddOption(itemIndex, optionGroupIndex);
    // explicitly check for 1 because clicking out of menu also calls onPress
    } else if (actionIndex === 1) {
      this.onDeleteOptionGroup(itemIndex, optionGroupIndex);
    }
  }

  onDeleteOptionChoice = (itemIndex, optionGroupIndex, optionIndex) => {
    this.setState(prevState => {
      const items = [...prevState.items];
      const clonedOptionGroups = cloneOptionGroups(items[itemIndex].item.optionGroups);
      clonedOptionGroups[optionGroupIndex].options.splice(optionIndex, 1);
      items[itemIndex].item.optionGroups = clonedOptionGroups;
      return { items };
    });
  }

  renderOption = (itemIndex: number, optionGroupIndex: number, optionIndex: string,  option: option) => (
    <MovableRow
      noRightBottomBorder
      bodyStyle={styles.choiceOption}
      onPressData={() => this.onPressOption(itemIndex, optionGroupIndex, optionIndex, option.name)}
      onDeleteData={() => this.onDeleteOptionChoice(itemIndex, optionGroupIndex, optionIndex)}
    >
      <Text>{option.name}</Text>
    </MovableRow>
  )

  renderOptionsGroup = (itemIndex, { options }, currOptionGroupIndex) => (
    <MovableItem renderItem={swapper => (
      <Card transparent style={{ borderLeftWidth: 0, borderRightWidth: 0, borderTopWidth: 0}}>
        <CardItem>
          {swapper}
          <H3>Option group</H3>
          <Right>
            <PopupMenu
              actions={['Add choice', 'Delete group']}
              onPress={actionIndex => this.onPressPopupMenu(actionIndex, itemIndex, currOptionGroupIndex)}
            />
          </Right>
        </CardItem>
        {options.length === 0 &&
        (<CardItem>
          <Text>No options. Customers won't see this option group until you add some options.</Text>
        </CardItem>)}
        {options.length > 0 &&
        // manual setting of height, otherwise MovableList of optionGroups cannot calculate proper height
        <List style={{ height: commonColor.listItemHeight * options.length }}>
          <MovableList
            list={options}
            scrollEnabled={false}
            onActivateRow={() => {
              // need to turn off other scrolls so options's scrollview can take precedence and allow reorder
              // this is a hack! must setState for internal for sortableList because its implementation does not update
              // inner state of scrollEnabled even if the props changed. so if we flip scrollEnabled for the outer
              // MovableList, then the flips dont affect scrollEnabed as prop is only used for initial state.
              this.sortableListRef.setState({
                scrollEnabled: false,
              });
              this.toggleContentScroll();
            }}
            onReleaseRow={() => {
              this.sortableListRef.setState({
                scrollEnabled: true,
              });
              this.toggleContentScroll();
            }}
            updateOrder={newOptionOrder => this.updateOptionOrder(itemIndex, currOptionGroupIndex, newOptionOrder)}
            renderItem={(option, optionIndex) => this.renderOption(itemIndex, currOptionGroupIndex, optionIndex, option)}
          />
        </List>}
      </Card>
    )} />
  );

  /**
   * @param itemIndex the index of the item whose optionGroup order is being updated
   * @param newOrderGroupOrder an array of numbers where each value represents the current optionGroup index and each
   * position represents the new optionGroup's position. ex: [2,0,1] means the optionGroup previously at 2 is now at 0
   */
  updateOrderGroupOrder = (itemIndex, newOrderGroupOrder: number[]) => {
    this.setState(prevState => {
      const items = [...prevState.items]
      const currOptionGroups = items[itemIndex].item.optionGroups;
      const newOptionGroups = newOrderGroupOrder.reduce((acc, currOptionGroupIndex) => {
        acc.push(currOptionGroups[currOptionGroupIndex]);
        return acc;
      }, []);
      items[itemIndex].item.optionGroups = newOptionGroups;
      return { items }
    });
  }

  updateOptionOrder = (itemIndex, groupIndex: number, newOptionOrder: number[]) => {
    this.setState(prevState => {
      const items = [...prevState.items];
      const newOptionGroups = cloneOptionGroups(items[itemIndex].item.optionGroups);
      const newOptions = newOptionOrder.reduce((acc, currOptionIndex) => {
        acc.push(newOptionGroups[groupIndex].options[currOptionIndex]);
        return acc;
      }, []);
      newOptionGroups[groupIndex].options = newOptions;
      items[itemIndex].item.optionGroups = newOptionGroups;
      return { items }
    });
  }

  toggleContentScroll = () => {
    this.setState(prevState => ({ contentScrollEnabled: !prevState.contentScrollEnabled}));
  }

  render () {
    const {
      onConfirm,
      navigation: { navigate, push, state: { params } },
      onBack,
      addEmptyItem,
    } = this.props;

    // must default to {} otherwise, when backing out of this screen, items turn to undefined and we end up passing
    // undefined to originalInputs prop of ValidatedForm
    const allValidatedInputs = {};
    const priceLabels = {};
    this.state.items.forEach(({ item }, indexInSelectedItems) => {
      const name = BaseItemSelector.getName(item);
      const prices = BaseItemSelector.getPrices(item);
      const privateNames = ManagerItemSelector.getPrivateNames(item);
      const addons = BaseItemSelector.getAddons(item);
      allValidatedInputs[`${indexInSelectedItems}name`] = (
        <ValidatedInput
          floatingLabel
          required
          label='Name'
          value={name}
          onChangeText={name => this.updateItemName(indexInSelectedItems, 'name', name)}
        />
      );

      privateNames.forEach((privateName, index) => {
        allValidatedInputs[`${indexInSelectedItems}-${index}privateName`] = (
          <ValidatedInput
            floatingLabel
            required
            label='Private name'
            validateEndEdit={name => this.validatePrivateNamesOnEnd(name, index, privateNames)}
            style={styles.label}
            value={privateName}
            onChangeText={name => this.updatePrivateNames(indexInSelectedItems, index, name)}
          />
        );
      });
      
      if (prices.length === 0) {
        prices.push({
          value: null
        });
      }

      prices.forEach(({ value, label }, index) => {
        allValidatedInputs[`${indexInSelectedItems}-${index}price`] = (
          <ValidatedInput
            style={styles.price}
            fixedLabel
            labelProps={{
              style: styles.priceLabel,
            }}
            inputProps={{
              keyboardType: 'numeric',
              style: styles.priceInput
            }}
            label='$'
            value={this._getPriceValueStr(value)}
            onChangeText={value => {
              this.updatePrices(indexInSelectedItems, index, 'value', value);
            }}
            validateEndEdit={this.validateOnEnd}
            validateTextChange={this.validatePriceChange}
          />
        );
        priceLabels[`${indexInSelectedItems}-${index}`] = (
          <Item floatingLabel style={styles.label}>
            <Label>Price label</Label>
            <Input onChangeText={label => this.updatePrices(indexInSelectedItems, index, 'label', label)} value={label} />
          </Item>
        );
      });

      addons.forEach(({ value, label }, index) => {
        allValidatedInputs[`${indexInSelectedItems}-${index}addonPrice`] = (
          <ValidatedInput
            style={styles.price}
            fixedLabel
            labelProps={{
              style: styles.priceLabel,
            }}
            inputProps={{
              keyboardType: 'numeric',
              style: styles.priceInput
            }}
            label='$'
            value={this._getPriceValueStr(value)}
            onChangeText={value =>
              this.updateAddons(indexInSelectedItems, index, 'value', value)}
            validateEndEdit={this.validateOnEnd}
            validateTextChange={this.validatePriceChange}
          />
        );
        allValidatedInputs[`${indexInSelectedItems}-${index}addonLabel`] = (
          <ValidatedInput
            style={styles.label}
            required
            floatingLabel
            label='Addon label'
            value={label}
            onChangeText={label => this.updateAddons(indexInSelectedItems, index, 'label', label)}
          />
        );
      });
    });

    const isAdding = params && params.confirmText === 'Add';
    /*
    * pass in entire state for onConfirm instead of this.state.items to avoid updating issues. ex: if we update
    * item detail image, redux updates selectedItems and this component updates with the new selected items. however,
    * if we did onConfirm{() => onConfirm(this.state.items)}, ValidatedForm would strangely use the OLD state.items
    * instead of the updated one 
    * 
    * indexInSelectedItems is the index of the item in the list of selectedItems. Available for both update and add items
    * as both require a list of items. This is NOT the index of the item in the menu, but rather just an item's index in
    * the list of selectedItems
    */
    return (
      <React.Fragment>
        <Container style={styles.background}>
          <Content scrollEnabled={this.state.contentScrollEnabled}>
            <ValidatedForm
              onConfirm={() =>  onConfirm(this.state)}
              onBack={onBack}
              originalInputs={allValidatedInputs}
              render={newInputs => this.state.items.map(({ item: {
                name,
                privateNames,
                flick,
                uri,
                description,
                prices,
                addons,
                printers,
                optionGroups
              }}, indexInSelectedItems) => {
              const itemDetails = (
                <React.Fragment>
                  <CardItem cardBody>
                    <Button light small iconLeft style={styles.imageButton}
                    // push instead of navigate because if we came from ItemFlickPickerScreen's adding, then navigating to
                    // itemFlickPickerScreenName would actually be the same as goBack(). thus if ItemFlickPickerScreen called
                    // goBack, it would go back even further into the stack instead of going back to this screen
                    onPress={() => push(itemFlickPickerScreenName, { confirmText: 'Save', itemIndex: indexInSelectedItems })}>
                      <Icon name='camera' style={Platform.OS === 'ios' ? styles.imageButtonIcon : undefined} />
                      <Text>Edit</Text>
                    </Button>
                    {isAdding &&
                    <Button light small iconLeft style={[ styles.imageButton, styles.removeButton ]}
                    onPress={() => this.deleteLocalItem(indexInSelectedItems)}>
                      <Icon name='close' style={Platform.OS === 'ios' ? styles.imageButtonIcon : undefined} />
                      <Text>Remove</Text>
                    </Button>}
                    {/* flick is the firebase url. uri is the phone path/user's images. prioritize uri over flick,
                    because if uri exists that means user has chosen a new image for this item */}
                    {(!!uri || !!flick) ?
                    <Image style={styles.image} source={{ uri: uri || flick }} />
                    :
                    <H3 style={[ styles.image, styles.text ]}>No selected image</H3>}
                  </CardItem>
                  <CardItem style={styles.cardForm}>
                    {newInputs[`${indexInSelectedItems}name`]}
                    <Button
                      transparent
                      primary
                      small
                      style={styles.centerButton}
                      onPress={() => this.addPrivateName(indexInSelectedItems)}
                    >
                      <Text>Add a private name</Text>
                    </Button>
                    {privateNames.map((name, nameIndex) => (
                      <View key={`${indexInSelectedItems}-${nameIndex}privateName`} style={styles.priceView}>
                        {newInputs[`${indexInSelectedItems}-${nameIndex}privateName`]}
                        <Icon name="trash" style={styles.rightIcon} onPress={() => this.deletePrivateName(indexInSelectedItems, nameIndex)} />
                      </View>
                    ))}
                    {prices.map((price, priceIndex) => (
                      <View key={`${indexInSelectedItems}-${priceIndex}`} style={styles.priceView}>
                        {newInputs[`${indexInSelectedItems}-${priceIndex}price`]}
                        {priceLabels[`${indexInSelectedItems}-${priceIndex}`]}
                        {priceIndex === 0 ?
                          <Icon name="md-add-circle" style={styles.rightIcon} onPress={() => this.addPrice(indexInSelectedItems)} />
                          :
                          <Icon name="trash" style={styles.rightIcon} onPress={() => this.deletePrice(indexInSelectedItems, priceIndex)} />
                        }
                      </View>
                    ))}
                    <Button
                      transparent
                      primary
                      small
                      style={styles.centerButton}
                      onPress={() => this.addAddon(indexInSelectedItems)}
                    >
                      <Text>Add an addon</Text>
                    </Button>
                    {addons.map((price, addonIndex) => (
                      <View key={`${indexInSelectedItems}-${addonIndex}`} style={styles.priceView}>
                        {newInputs[`${indexInSelectedItems}-${addonIndex}addonPrice`]}
                        {newInputs[`${indexInSelectedItems}-${addonIndex}addonLabel`]}
                        <Icon name="trash" style={styles.rightIcon} onPress={() => this.deleteAddon(indexInSelectedItems, addonIndex)} />
                      </View>
                    ))}
                    <Item floatingLabel>
                      <Label>Description</Label>
                      <Input onChangeText={description => this.updateItem(indexInSelectedItems, 'description', description)}
                      value={description} />
                    </Item>
                    <Item fixedLabel style={styles.printers}>
                      <Label>Printers</Label>
                      <Icon name="create" style={styles.rightIcon} onPress={() => navigate(updateItemPrintersScreenName, {
                        preventAdd: true,
                        itemName: name,
                        privateNames,
                        itemPrinters: printers,
                        onConfirm: (printers: Printer[]) => this.updateItem(indexInSelectedItems, 'printers', printers)
                      })} />
                    </Item>
                    {(printers || []).length === 0 &&  <Text note>This item has no printers</Text>}
                    <Text note>
                      {(printers || []).map((printer, printerIndex) => `${printerIndex === 0 ? '' : ', '}${printer.name}`)}
                    </Text>
                  </CardItem>
                  <CardItem style={styles.addOptions}>
                    <Right>
                      <Button transparent primary small onPress={() => this.addOptionGroup(indexInSelectedItems)}>
                        <Text>Add ordering options group</Text>
                      </Button>
                    </Right>  
                  </CardItem>
                </React.Fragment>
              );
              return (
                <Card key={indexInSelectedItems}>
                  {/* only render MovableList if there are items, otherwise there's height is too small
                  https://github.com/gitim/react-native-sortable-list/issues/107 */}
                  {optionGroups.length === 0 ?
                  itemDetails :
                  !this.state.doesNeedRefresh &&
                  <MovableList
                    list={optionGroups}
                    listRef={ref => this.sortableListRef = ref}
                    updateOrder={newOrder => this.updateOrderGroupOrder(indexInSelectedItems, newOrder)}
                    renderItem={(optionGroup, originalIndex, newIndex) => this.renderOptionsGroup(indexInSelectedItems, optionGroup, newIndex)}
                    onReleaseRow={() => {
                      this.toggleContentScroll();
                      this.setState({ doesNeedRefresh: true }, () => this.setState({ doesNeedRefresh: false }))
                    }}
                    onActivateRow={this.toggleContentScroll}
                    renderHeader={() => itemDetails}
                  />}
                </Card>
              );
            })} />
          </Content>
        </Container>
        {
          !!params.onConfirm &&
          <View style={styles.buttons}>
            {isAdding &&
            <React.Fragment>
              {/* push instead of navigate because if we came from ItemFlickPickerScreen's adding, then navigating to
              itemFlickPickerScreenName would actually be the same as goBack(). thus if ItemFlickPickerScreen called
              goBack, it would go back even further into the stack instead of going back to this screen */}
              <Button transparent primary onPress={() => push(itemFlickPickerScreenName, { confirmText: 'Next' })}>
                <Text>More flicks</Text>
              </Button>
              <Button transparent primary onPress={addEmptyItem}>
                <Text>More items</Text>
              </Button>
            </React.Fragment>}
          </View>
        }
      </React.Fragment>
    )
  }
}

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  },
  centerButton: {
    marginVertical: commonColor.listItemPadding,
    alignSelf: 'center',
  },
  cardForm: {
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  imageButton: {
    position: 'absolute',
    zIndex: 1,
    opacity: 0.75,
  },
  imageButtonIcon: {
    paddingTop: 0,
  },
  removeButton: {
    right: 0,
  },
  image: {
    height: width/1.5,
    width,
  },
  priceView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 1,
  },
  printerList: {
    flexDirection: 'row',
    flex: 1,
  },
  price: {
    flex: .50,
  },
  priceInput: {
    // height and top are to match styles of floating label
    height: 60,
    top: 8,
    paddingLeft: 0,
    left : -15, // determined by inspection
  },
  priceLabel: {
    paddingRight: Platform.OS === 'ios' ? 10 : 0,
    top: 8 // 8 to match price input
  },
  printers: {
    marginTop: commonColor.listItemPadding,
    justifyContent: 'space-between',
    borderBottomWidth: 0,
  },
  label: {
    flex: 1,
  },
  rightIcon: {
    alignSelf: 'center',
    textAlign: 'center',
  },
  text: {
    textAlign: 'center',
    textAlignVertical: 'center',
    padding: 20,
  },
  buttons: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  choiceOption: {
    borderBottomWidth: 0,
  },
  addOptions: {
    paddingTop: 0,
  },
})

type mapState = {
  selectedCategory: ManagerCategory,
  selectedItems: ManagerItem[],
  selectedRest: ManagerRest,
}

const mapStateToProps = (state: RootState) => ({
  selectedCategory: state.getAccount().getSelectedCategory(),
  selectedItems: state.getAccount().getSelectedItems(),
  selectedRest: state.getAccount().getSelectedRest(),
});

const mergeProps = ({ selectedCategory, selectedItems, selectedRest }: mapState, { dispatch }, props) => ({
  ...props,
  items: selectedItems,
  onConfirm: props.navigation.state.params && props.navigation.state.params.confirmText === 'Add' ? 
    async ({ items }: state) => {
      dispatch(showLoading('Saving. This may take a second...'))
      const cleanItems = items.map(({ index, item }) => ({
        index,
        item: getCleanItems(item),
      }));
      await dispatch(addItemsAction(selectedRest, selectedCategory.name, cleanItems)); //todo 0: make this way faster
      //pop2 to remove this screen and flickPickerScreen
      props.navigation.pop(2);
      dispatch(removeLoading());
    } : //otherwise it's save for update
    async ({ items }: state) => {
      dispatch(showLoading('Saving. This may take a second...'))
      const cleanItems = items.map(({ index, item }) => ({
        index,
        item: getCleanItems(item),
      }));
      await dispatch(updateItemsAction(selectedRest, selectedCategory.name, cleanItems));
      props.navigation.goBack();
      dispatch(removeLoading());
    },
  addEmptyItem: () => {
    dispatch(addEmtpyItemToSelectedAction());
    // originally tried to do auto scroll to end  with ref, but could only scroll to second to last item. this is the
    // easy alternative
    Toast.show({
      text: 'Scroll down to see added item',
      buttonText: 'Okay',
      position: 'bottom',
      duration: 5000,
    })
  },
  deleteItemLocally: index => {
    dispatch(deleteItemLocallyAction(index));
  },
  onBack: () => {
    dispatch(clearItemsAction());
    props.navigation.goBack();
  }
})

export default connect(mapStateToProps, null, mergeProps)(ItemDetails);

export const itemDetailsScreenName = 'itemDetailsScreen';
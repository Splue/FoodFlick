import React from 'react';
import { shallow } from 'enzyme';
import { getMockStore, getMockNavigation } from '../../../test/utils/mocks/mockGeneral';
import { getMockSelectedRest } from '../../../test/utils/mocks/mockRests';
import MenuOverviewScreen from './MenuOverviewScreen';

let store;
const navigation = getMockNavigation();
const getShallow = () => shallow(<MenuOverviewScreen navigation={navigation} />, {context: {store}}).dive();

describe('MenuOverviewScreen', () => {
  it('renders add button with title when no menu exists', () => {
    const selectedRest = getMockSelectedRest();
    store = getMockStore({selectedRest});
    expect(getShallow()).toMatchSnapshot();
  });

  it.skip('renders existing menu', () => {

  });
});

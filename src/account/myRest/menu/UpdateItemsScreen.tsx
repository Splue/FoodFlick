import React from 'react';
import { connect } from 'react-redux';
import { Alert, StyleSheet } from 'react-native';
import { findIndex, isEqual } from 'lodash';
import { Text, Body, Toast, Container, List } from 'native-base';
import { selectItemsAction, deleteItemAction, updateItemOrderAction } from '../../../general/menu/menuActions';
import MovableList from '../../../general/components/list/MovableList';
import { RootState } from '../../../general/redux/RootState';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import MovableRow from '../../../general/components/list/MovableRow';

class UpdateItemsScreen extends React.Component<any, any> {
  static navigationOptions = {
    title: 'Update items',
  }


  lastUpdatedItem = null;


  onPressItem = item => {
    const itemIndex = findIndex(this.props.selectedCategory.items, { name: item.name });
    const items = [{ index: itemIndex, item }];
    this.props.onPressItem(items);
  }

  renderItem = item => (
    <MovableRow onPressData={this.onPressItem} onDeleteData={this.props.onDeleteItem}>
      <Text>{item.name}</Text>
    </MovableRow>
  )

  render () {
    const { updateItemOrder, items } = this.props;
    if (!items) return null;
    return (
      <Container>
        <List style={styles.list}>
          <MovableList
            list={items}
            updateOrder={updateItemOrder}
            renderItem={this.renderItem} 
          />
        </List>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  list: {
    flex: 1,
    backgroundColor: commonColor.brandCanvas,
  },
})

const mapStateToProps = (state: RootState) => ({
  selectedCategory: state.getAccount().getSelectedCategory(),
  selectedRest: state.getAccount().getSelectedRest(),
});

const mergeProps = ({ selectedCategory, selectedRest }, { dispatch }, props) => ({
  ...props,
  selectedCategory,
  // if we update category order, we unselectCategory before reselected the new ordered category,
  // leading to {}.items which is undefined
  items: (selectedCategory || {}).items,
  onPressItem: async items => {
      await dispatch(selectItemsAction(items));
      props.navigation.navigate('itemDetailsScreen', { confirmText: 'Save' });
  },
  onDeleteItem: async item => {
    Alert.alert(
      'Delete item?',
      `Are you sure you want to delete ${item.name}?`,
      [
        {text: 'Cancel' },
        {text: 'Delete', onPress: async () => {
          const newRest = await props.navigation.dispatch(deleteItemAction(selectedRest._id, selectedCategory.name, item));
          const categoryIndex = findIndex(newRest.menu, { name: selectedCategory.name });
          if (newRest.menu[categoryIndex] === 0) {
            props.navigation.goBack();
          }
        }},
      ],
      { cancelable: false }
    );
  },
  updateItemOrder: async (newOrder) => {
    // no await so the toast feels faster
    dispatch(updateItemOrderAction(selectedRest._id, selectedCategory.name, newOrder));
    Toast.show({ text: 'Order saved. Refreshing...', buttonText: 'Okay', position: 'bottom' })
  }
})

export default connect(mapStateToProps, null, mergeProps)(UpdateItemsScreen);

export const updateItemsScreenName = 'updateItemsScreen';
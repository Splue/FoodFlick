import React from 'react';
import { connect } from 'react-redux';
import { Alert, StyleSheet } from 'react-native';
import { Text, Body, Toast, Container, List } from 'native-base';
import { selectAccountCategoryAction, updateCategoryOrderAction, deleteCategoryAction } from '../../../general/menu/menuActions';
import MovableList from '../../../general/components/list/MovableList';
import { ManagerRestSelector } from '../../../general/rest/ManagerRest';
import { RootState } from '../../../general/redux/RootState';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import MovableRow from '../../../general/components/list/MovableRow';

class UpdateCategoriesScreen extends React.Component<any, any> {
  static navigationOptions = {
    title: 'Update categories',
  }

  state = {
    updating: false,
  }

  renderItem = category => (
    <MovableRow onPressData={this.props.onPressCategory} onDeleteData={this.props.onDeleteCategory}>
      <Text>{category.name}</Text>
      <Text note>{category.items.length} items</Text>
    </MovableRow>
  )

  render () {
    const { menu, updateCategoryOrder } = this. props;
    // necessary to refresh the movablelist. otherwise consecutive reorders make dragging jumpy
    if (this.state.updating) return null;
    return (
      <Container>
        <List style={styles.list}>
          <MovableList
            list={menu}
            updateOrder={async newOrder => {
              this.setState({ updating: true })
              await updateCategoryOrder(newOrder);
              this.setState({ updating: false });
            }}
            renderItem={this.renderItem}
          />
        </List>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  list: {
    flex: 1,
    backgroundColor: commonColor.brandCanvas,
  },
})

const mapStateToProps = (state: RootState) => ({
  menu: ManagerRestSelector.getMenu(state.getAccount().getSelectedRest()),
  selectedRest: state.getAccount().getSelectedRest(),
});

const mergeProps = ({ menu, selectedRest }, { dispatch }, props) => ({
  ...props,
  menu,
  onPressCategory: async category => {
    await dispatch(selectAccountCategoryAction(category));
    props.navigation.navigate('categoryDetailScreen', { confirmText: 'Save' });
  },
  onDeleteCategory: async category => {
    Alert.alert(
      'Delete category?',
      `Are you sure you want to delete ${category.name}? Items in this category will also be deleted`,
      [
        {text: 'Cancel' },
        {text: 'Delete', onPress: async () => {
          const newRest = await props.navigation.dispatch(deleteCategoryAction(selectedRest._id, category.name));
          if (newRest.menu.length === 0) {
            props.navigation.goBack();
          }
        }},
      ],
      { cancelable: false }
    );
  },
  updateCategoryOrder: newOrder => {
    Toast.show({ text: 'Order saved. Refreshing...', buttonText: 'Okay', position: 'bottom' });
    return dispatch(updateCategoryOrderAction(selectedRest._id, newOrder));
  }
})

export default connect(mapStateToProps, null, mergeProps)(UpdateCategoriesScreen);

export const updateCategoriesScreenName = 'updateCategoriesScreen';
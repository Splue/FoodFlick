import React from 'react';
import { shallow } from 'enzyme';
import { getMockStore, getMockNavigation } from '../../test/utils/mocks/mockGeneral';
import { getMockSelectedRest } from '../../test/utils/mocks/mockRests';
import { EjectedMyRestsScreen } from './MyRestsScreen';

describe('MyRestsScreen', () => {
  const navigation = getMockNavigation();
  let store;
  const selectedRest = getMockSelectedRest();
  const getShallow = props => shallow(<EjectedMyRestsScreen navigation={navigation} {...props} />, {context: {store}}).dive();

  beforeEach(() => {
    store = getMockStore();
  });

  it('renders add button with title when no rests exist', () => {
    expect(getShallow()).toMatchSnapshot();
  });

  it('renders 1 rest', () => {
    expect(getShallow({ myRests: [selectedRest]})).toMatchSnapshot();
  });

  it('renders multiple rests', () => {
    expect(getShallow({ myRests: [selectedRest, selectedRest]})).toMatchSnapshot();
  });

  it('on pressing rest, sets selectedRest and routes to RestOverviewScreen', () => {
    const screen = getShallow({ myRests: [selectedRest]});
    screen.find({testId: selectedRest._id}).simulate('press');
    setImmediate(() => {
      expect(store.getState().selectedRest).toEqual(selectedRest);
      expect(navigation.navigate).toBeCalledWith('restOverviewScreen');
    });
  });
});

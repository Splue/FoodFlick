import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { Container, Content, Card, CardItem, Text, Body } from 'native-base';
import { NavScreenOptions } from '../../general/navModels/NavScreenOptions';
import { ManagerRestSelector } from '../../general/rest/ManagerRest';
import { RootState } from '../../general/redux/RootState';
import CenteredH3 from '../../general/components/CenteredH3';

class RestFeedbackScreen extends React.Component<any, any> {

  static navigationOptions = ({ screenProps }: NavScreenOptions) => ({
    title: 'Feedback - ' + ManagerRestSelector.getName(screenProps.getAccount().getSelectedRest()),
  });

  render() {
    const { feedback } = this.props;
    return (
      <Container>
        <Content>
          {feedback.length == 0 ?
          <CenteredH3 text='No feedback' /> :
          feedback.map((feed, index) =>
            <Card key={index}>
              <CardItem header>
                <Text>{moment(feed.createdDate).format('MM/DD/YY')}</Text>
              </CardItem>
              <CardItem>
                <Body>
                  <Text>{feed.feedback}</Text>
                </Body>
              </CardItem>
            </Card>
          )}
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  feedback: ManagerRestSelector.getFeedback(state.getAccount().getSelectedRest()),
});

export default connect(mapStateToProps)(RestFeedbackScreen);

export const restFeedbackScreenName = 'restFeedbackScreen';
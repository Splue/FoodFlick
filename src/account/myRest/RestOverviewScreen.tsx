import React from 'react';
import { connect } from 'react-redux';
import { Container, Content } from 'native-base';
import UndividedList from '../../general/components/list/UndividedList';
import { menuUpdateScreenName } from './menu/MenuUpdateScreen';
import { restDetailsScreenName } from './restDetails/RestDetailsScreen';
import { ManagerRestSelector } from '../../general/rest/ManagerRest';
import { NavScreenOptions } from '../../general/navModels/NavScreenOptions';
import { completedOrdersScreenName } from './orders/CompletedOrdersScreen';
import { openOrdersScreenName } from './orders/OpenOrdersScreen';
import { pendingTipOrdersScreenName } from './orders/PendingTipOrdersScreen';
import { subscriptionDetailsScreenName } from './subscription/SubscriptionDetailsScreen';
import { RootState } from '../../general/redux/RootState';
import { NavigationScreenProps } from 'react-navigation';
import { totalTipsScreenName } from './tips/TotalTipsScreen';

type props = {
  isManager: boolean
}
class RestOverviewScreen extends React.Component<props & NavigationScreenProps, any> {
  static navigationOptions = ({ screenProps }: NavScreenOptions) => ({
    title: ManagerRestSelector.getName(screenProps.getAccount().getSelectedRest()),
  });

  render () {
    const {
      navigation: { navigate },
      isManager
    } = this.props;

    const items = [
      // {
      //   leftIcon: 'text',
      //   text: 'View feedback',
      //   props: { onPress: () => navigate(restFeedbackScreenName) }
      // },
      {
        leftIcon: 'clipboard',
        text: 'Restaurant details',
        props: { onPress: () => navigate(restDetailsScreenName) }
      },
      {
        leftIcon: 'filing',
        text: 'Open orders',
        props: { onPress: () => navigate(openOrdersScreenName) }
      },
      {
        leftIcon: 'alarm',
        text: 'Pending tip orders',
        props: { onPress: () => navigate(pendingTipOrdersScreenName) }
      },
      {
        leftIcon: 'paper',
        text: 'Completed orders',
        props: { onPress: () => navigate(completedOrdersScreenName) }
      },
      {
        leftIcon: 'gift',
        text: 'Total tips',
        props: { onPress: () => navigate(totalTipsScreenName) }
      }
    ];

    if (isManager) {
      items.unshift({
        leftIcon: 'restaurant',
        text: 'Update menu',
        props: { onPress: () => navigate(menuUpdateScreenName) }
      })
      items.push({
        leftIcon: 'business',
        text: 'Subscription',
        props: { onPress: () => navigate(subscriptionDetailsScreenName) }
      })
    }
    
    return (
      <Container>
        <Content>
          <UndividedList items={items}/>
        </Content>
      </Container>
    )
  }
}

const mapStateToProps = (state: RootState) => ({
  isManager: ManagerRestSelector.isManager(state.getAccount().getSelectedRest(), state.getSignedInUser()),
});

export default connect(mapStateToProps)(RestOverviewScreen);

export const restOverviewScreenName = 'restOverviewScreen';
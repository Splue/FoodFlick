import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import RestService from '../../general/rest/restService';
import { Container, Button, List, Text, ListItem, Content } from 'native-base';
import CenteredH3 from '../../general/components/CenteredH3';
import commonColor from '../../../native-base-theme/variables/commonColor';
import { navigationPropType } from '../../general/utils/propTypes/general';
import { selectedRestPropType } from '../../general/utils/propTypes/rest';
import { selectAccountRestAction } from '../../general/rest/restActions';
import { restOverviewScreenName } from './RestOverviewScreen';
import AnalyticsService from '../../general/analytics/AnalyticsService';

class MyRestsScreen extends React.Component {
  static navigationOptions = {
    title: 'My Restaurants'
  };

  static propTypes = {
    myRests: PropTypes.arrayOf(selectedRestPropType),
    navigation: navigationPropType
  }

  shouldComponentUpdate(nextProps) {
    if (nextProps.myRests !== this.props.myRests) {
      return true;
    }
    return false;
  }

  render() {
    let { myRests, loading } = this.props
    myRests = myRests && myRests.map(rest => (
      <ListItem testId={rest._id} key={rest._id} onPress={() => {
        AnalyticsService.trackEventWithProperties(AnalyticsService.events.MY_RESTAURANT_ITEM,
          {
            'restaurantName': rest.profile.name,
            'restaurantId': rest._id
          });
        this.props.onPressListItem(rest)
      }}>
        <Text>{rest.profile.name}</Text>
      </ListItem>
    ));

    const AddRestButton = (
      <Button transparent primary style={styles.button}
        onPress={() => this.props.navigation.navigate('restLocationScreen', { confirmText: 'Next', forSignUp: false })}>
        <Text>Add restaurant</Text>
      </Button>
    )

    if (loading) return <CenteredH3 text='Loading...' />

    return (myRests && myRests.length === 0 ?
      <Container>
        <CenteredH3 text='You have no restaurants. Start by adding a restaurant' />
        {AddRestButton}
      </Container>
      :
      <Container>
        {AddRestButton}
        <Content style={styles.content}>
          <List style={styles.list}>
            {myRests}
          </List>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  content: {
    backgroundColor: commonColor.brandCanvas,
  },
  list: {
    flex: 1,
    backgroundColor: commonColor.brandCanvas,
  },
  button: {
    alignSelf: 'center',
    marginVertical: 8,
  }
})

const mapDispatchToProps = (dispatch, { navigation: { navigate } }) => ({
  onPressListItem: rest => {
    dispatch(selectAccountRestAction(rest));
    navigate(restOverviewScreenName);
  }
});

export default connect(null, mapDispatchToProps)(RestService.getMyRestsInjector(MyRestsScreen));

export const EjectedMyRestsScreen = connect(null, mapDispatchToProps)(MyRestsScreen);

export const myRestsScreenName = 'myRestsScreen';

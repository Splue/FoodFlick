import { Container, Content, List, Text } from 'native-base';
import { StyleSheet, View } from 'react-native';
import React from 'react';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import { RootState } from '../../../general/redux/RootState';
import { ManagerRestSelector } from '../../../general/rest/ManagerRest';
import CenteredH3 from '../../../general/components/CenteredH3';
import { Order, OrderSelector } from '../../../general/order/OrderModel';
import OrderService from '../../../general/order/orderService';
import { displayDateTime } from '../../../general/utils/dateTime';
import LeftRightListItem from '../../../general/components/list/LeftRightListItem';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import { selectOrderAction } from '../../../general/order/redux/orderActions';
import { openOrderOverviewScreenName } from './OpenOrderOverviewScreen';

type props = {
  openOrders: Order[];
  loading: boolean,
  selectOrder: (order: Order) => void,
};

const OpenOrdersScreen: NavigationScreenComponent<any, any, props> = ({
  openOrders,
  loading,
  navigation,
  selectOrder,
}) => {
  if (loading) {
    return <CenteredH3 text="Loading..." />
  }

  if (!openOrders || openOrders.length === 0) {
    return (
      <Container>
        <CenteredH3 text='You have no open orders.' />
      </Container>
    )
  }
  return (
    <Container>
      <Text style={styles.description}>Open orders are new orders where customers can still add items</Text>
      <Content style={styles.content}>
        <List>
          {openOrders.map((order, index) => {
            return (
              <LeftRightListItem
                key={index}
                leftText={OrderSelector.getCustomerName(order)}
                leftNote={displayDateTime(OrderSelector.getCartUpdatedDate(order))}
                right={
                  <View style={styles.right}>
                    <Text note>
                      ${OrderSelector.getOrderTotal(order)}
                    </Text>
                  </View>
                }
                onPress={() => {
                  selectOrder(order);
                  navigation.navigate(openOrderOverviewScreenName);
                }}
              />
            )
          })}
        </List>
      </Content>
    </Container>
  );
}

OpenOrdersScreen.navigationOptions = ({
  title: 'Open orders'
});

const mapStateToProps = (state: RootState) => ({
  // necessary so that getCompletedOrdersInjector receives restId
  selectedRestId: ManagerRestSelector.getId(state.getAccount().getSelectedRest()),
});

const mapDispatchToProps = dispatch => ({
  selectOrder: order => dispatch(selectOrderAction(order)),
});

const styles = StyleSheet.create({
  content: {
    backgroundColor: commonColor.brandCanvas,
  },
  description: {
    padding:  commonColor.contentPadding,
  },
  right: {
    alignItems: 'flex-end',
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderService.getOpenOrdersInjector(OpenOrdersScreen));

export const openOrdersScreenName = 'openOrdersScreen';
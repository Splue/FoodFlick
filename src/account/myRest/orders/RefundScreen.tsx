import { Container, Text, Button, Content, Toast } from 'native-base';
import React, { useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import { Order, OrderSelector } from '../../../general/order/OrderModel';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import { refundOrderCompletedAction } from '../../../general/order/redux/orderActions';
import ValidatedForm from '../../../general/components/ValidatedForm';
import ValidatedInput from '../../../general/components/ValidatedInput';
import { round2 } from '../../../general/utils/math';
import { getCleanInput } from '../../../general/utils/formHelpers';
import { RootState } from '../../../general/redux/RootState';
import { showLoading, removeLoading } from '../../../general/redux/uiActions';

type props = {
  selectedOrder: Order,
};
type params = {
  refundOrder: (amount: number) => Promise<Order>,
};

const amountMessage = 'Ex: 10.00';
const amountRegex = /^\d+([.]\d{2})?$/;

const getRefundFromInputString = value => {
  if (value.length >= 1) return getCleanInput(value)
  return value
}

const getStringFromRefund = (amount: string) => {
  if (!amount || !amount.trim()) return '';
  return amount;
};

const RefundScreen: NavigationScreenComponent<params, any, props> = ({ navigation, selectedOrder }) => {
  const [amount, setAmount] = useState('');
  const orderTotal = OrderSelector.getOrderTotal(selectedOrder);
  const remaining = round2(round2(orderTotal) - round2(OrderSelector.getCustomRefundsTotal(selectedOrder)));
  const validateOnEnd = amount => {
    const cleanAmount = getCleanInput(amount);
    if (cleanAmount.length === 0) return `Required`;
    if (cleanAmount === '0') return 'Cannot refund 0';
    if (cleanAmount === '0.00') return 'Cannot refund 0';
    if (parseFloat(cleanAmount) > remaining) return 'Cannot refund greater than the remaining amount';
    return amountRegex.test(cleanAmount) ? '' : amountMessage
  }
  const inputs = {
    amount: (
      <ValidatedInput
        style={styles.amount}
        inlineLabel
        inputProps={{
          keyboardType: 'numeric',
        }}
        label='Refund $'
        value={getStringFromRefund(amount)}
        onChangeText={value => setAmount(getRefundFromInputString(value))}
        validateEndEdit={validateOnEnd}
      />
    ) as unknown as ValidatedInput
  };
  
  const onPressConfirm = async () => {
    const cleanAmount = getCleanInput(amount);
    const validation = validateOnEnd(cleanAmount);
    if (validation) {
      Toast.show({
        text: validation,
        type: 'warning',
        buttonText: 'Okay',
        position: 'bottom'
      });
      return;
    }
    navigation.getParam('refundOrder')(parseFloat(amount));
  }

  return (
    <Container style={styles.background}>
      <Content>
        <ValidatedForm
          originalInputs={inputs}
          render={newInputs => (
            <React.Fragment>
              <View style={styles.amountView}>
                {newInputs['amount']}
                <Text note style={styles.remaining}>of remaining {remaining}</Text>
              </View>
            </React.Fragment>
          )}
        />
        <Text style={styles.warningText}>Are you sure you want issue a permanant refund? This cannot be undone</Text>
        <Button block onPress={onPressConfirm}>
          <Text>Refund {getStringFromRefund(amount)}</Text>
        </Button>
      </Content>
    </Container>
  );
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  },
  warningText: {
    paddingHorizontal: commonColor.formInputMarginLeft,
    paddingVertical: 20,
  },
  amountView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingRight: commonColor.formInputMarginLeft,
    flex: 1,
  },
  remaining: {
    alignSelf: 'center'
  },
  amount: {
    flex: 1,
  },
});

RefundScreen.navigationOptions = {
  title: 'Refund payment'
};

const mapStateToProps = (state: RootState) => ({
  selectedOrder: state.getAccount().getSelectedOrder(),
});

const mapDispatchToProps = dispatch => ({
  refundOrder: async (amount) => {
    dispatch(showLoading());
    const order = await dispatch(refundOrderCompletedAction(amount));
    dispatch(removeLoading());
    return order;
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(RefundScreen);

export const refundScreenName = 'refundScreen';
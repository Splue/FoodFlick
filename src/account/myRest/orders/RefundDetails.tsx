import { Text, Icon } from 'native-base';
import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenProp, NavigationRoute } from 'react-navigation';
import { Order, OrderSelector } from '../../../general/order/OrderModel';
import { RootState } from '../../../general/redux/RootState';
import { refundScreenName } from './RefundScreen';
import { round2 } from '../../../general/utils/math';
import { ManagerRestSelector } from '../../../general/rest/ManagerRest';

type props = {
  isManager: boolean,
  selectedOrder: Order,
  refundOrder: (amount: number) => void,
  navigation: NavigationScreenProp<NavigationRoute<params>, params>
}
type params = {
  refetchOrders: () => void,
}

const RefundDetails: React.FC<props> = ({
  isManager,
  navigation,
  refundOrder,
  selectedOrder
}) => {
  if (!selectedOrder) return null;
  return (
    <React.Fragment>
      {isManager &&
      <TouchableOpacity style={styles.refund} onPress={() => navigation.navigate(refundScreenName, {
        refundOrder,
      })}>
        <Icon name='undo' style={styles.editText} />
        <Text style={styles.editText}>Refund</Text>
        <Text note style={styles.editText}>refunded: ${round2(OrderSelector.getCustomRefundsTotal(selectedOrder))}</Text>
      </TouchableOpacity>}
      {!isManager &&
      <View style={styles.refund}>
        <Text note style={styles.editText}>
          refunded: ${round2(OrderSelector.getCustomRefundsTotal(selectedOrder))}
        </Text>
      </View>}
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  editText: {
    textAlign: 'center',
  },
  refund: {
    flex: 0.50,
    alignSelf: 'center',
  },
});

const mapStateToProps = (state: RootState) => ({
  selectedOrder: state.getAccount().getSelectedOrder(),
  isManager: ManagerRestSelector.isManager(state.getAccount().getSelectedRest(), state.getSignedInUser()),
});

export default connect(mapStateToProps)(RefundDetails);

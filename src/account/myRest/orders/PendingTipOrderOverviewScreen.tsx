import { Container, Text, Content, List, Button } from 'native-base';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import { Order, OrderSelector } from '../../../general/order/OrderModel';
import { OrderItem } from '../../../general/order/OrderItem';
import CartListItem from '../../../general/cart/components/CartListItem';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import { displayDateTime } from '../../../general/utils/dateTime';
import { RootState } from '../../../general/redux/RootState';
import { completeOrderAction, refundOrderPendingTipAction } from '../../../general/order/redux/orderActions';
import { showLoading, removeLoading } from '../../../general/redux/uiActions';
import { ManagerRestSelector } from '../../../general/rest/ManagerRest';
import RefundDetails from './RefundDetails';
import PrintOrderButton from './PrintOrderButton';

type props = {
  isManager: boolean,
  refundOrder: (amount: number) => void
  selectedOrder: Order
  completeOrder: (orderId: string) => void,
}
type params = {
  refetchOrders: () => void,
}
const PendingTipOrderOverviewScreen: NavigationScreenComponent<params, any, props> = ({
  isManager,
  navigation,
  selectedOrder,
  refundOrder,
  completeOrder
}) => {
  if (!selectedOrder) return null;
  const itemTotal = OrderSelector.getItemTotal(selectedOrder);
  const tax = OrderSelector.getTax(selectedOrder);
  const tip = OrderSelector.getTip(selectedOrder);
  const orderTotal = OrderSelector.getOrderTotal(selectedOrder);
  const items = OrderSelector.getItems(selectedOrder);
  const onCompleteOrder = async () => {
    await completeOrder(selectedOrder._id);
    navigation.goBack();
  }
  return (
    <Container>
      <Content>
        <List>
          {items.map((item: OrderItem, index) => <CartListItem key={index} item={item} />)}
        </List>
      </Content>
      <View style={styles.costs}>
        <View>
          <Text>Items:</Text>
          <Text>Tax:</Text>
          <Text>Tip:</Text>
          <Text style={styles.bold}>Total:</Text>
        </View>
        <View>
          <Text>{itemTotal.toFixed(2)}</Text>
          <Text>{tax.toFixed(2)}</Text>
          <Text>{tip.toFixed(2)}</Text>
          <Text style={styles.totalPrice}>{orderTotal.toFixed(2)}</Text>
        </View>
        <RefundDetails navigation={navigation} refundOrder={refundOrder} />
      </View>
      <PrintOrderButton />
      {isManager &&
      <Button bordered block onPress={onCompleteOrder}>
        <Text>Complete order</Text>
      </Button>}
    </Container>
  );
}

const styles = StyleSheet.create({
  costs: {
    paddingHorizontal: commonColor.listItemPadding,
    marginTop: commonColor.listItemPadding,
    paddingBottom: commonColor.listItemPadding,
    paddingTop: commonColor.listItemThinPadding,
    borderTopWidth: commonColor.borderWidth,
    borderTopColor: commonColor.cardBorderColor,
    backgroundColor: commonColor.brandCanvas,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  bold: {
    fontWeight: 'bold',
  },
  totalPrice: {
    fontWeight: 'bold',
    color: commonColor.brandPrimary,
  },
});

PendingTipOrderOverviewScreen.navigationOptions = ({ screenProps }) => ({
  title: OrderSelector.getCustomerName(screenProps.getAccount().getSelectedOrder())
    + ' '
    + displayDateTime(OrderSelector.getCartUpdatedDate(screenProps.getAccount().getSelectedOrder()))
});

const mapStateToProps = (state: RootState) => ({
  selectedOrder: state.getAccount().getSelectedOrder(),
  isManager: ManagerRestSelector.isManager(state.getAccount().getSelectedRest(), state.getSignedInUser()),
});

const mapDispatchToProps = (dispatch, props) => ({
  completeOrder: async orderId => {
    dispatch(showLoading());
    await dispatch(completeOrderAction(orderId));
    dispatch(removeLoading());
  },
  refundOrder: async (amount: number) => {
    dispatch(showLoading());
    await dispatch(refundOrderPendingTipAction(amount));
    props.navigation.getParam('refetchOrders')();
    props.navigation.goBack();
    dispatch(removeLoading());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(PendingTipOrderOverviewScreen);

export const pendingTipOrderOverviewScreenName = 'pendingTipOrderOverviewScreen';
import { Text, Button, Toast } from 'native-base';
import React from 'react';
import { connect } from 'react-redux';
import { printReceiptsAction } from '../../../general/order/redux/orderActions';
import { showLoading, removeLoading } from '../../../general/redux/uiActions';

type props = {
  printReceipt: () => void,
}

const PrintReceiptButton: React.FC<props> = ({
  printReceipt
}) => (
  <Button block onPress={printReceipt}>
    <Text>Print receipt</Text>
  </Button>
);

const mapDispatchToProps = dispatch => ({
  printReceipt: async () => {
    dispatch(showLoading());
    await dispatch(printReceiptsAction());
    Toast.show({
      text: 'Printed',
      buttonText: 'Okay',
      position: 'bottom',
      duration: 5000,
    })
    dispatch(removeLoading());
  }
});

export default connect(null, mapDispatchToProps)(PrintReceiptButton);

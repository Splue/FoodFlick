import { Container, Text, Button, Content, Toast } from 'native-base';
import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import { Order, OrderSelector } from '../../../general/order/OrderModel';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import { returnOrderAction } from '../../../general/order/redux/orderActions';
import ValidatedForm from '../../../general/components/ValidatedForm';
import ValidatedInput from '../../../general/components/ValidatedInput';
import { RootState } from '../../../general/redux/RootState';
import { showLoading, removeLoading } from '../../../general/redux/uiActions';

type props = {
  returnOrder: (orderId, reason) => void,
  selectedOrder: Order,
};
type params = {
  refetchOrders: () => void
};

const ReturnOrderScreen: NavigationScreenComponent<params, any, props> = ({ returnOrder, navigation, selectedOrder }) => {
  const [reason, setReason] = useState('');
  const inputs = {
    reason: (
      <ValidatedInput
        required
        floatingLabel
        label='Why return the order?'
        value={reason}
        onChangeText={value => setReason(value)}
      />
    ) as unknown as ValidatedInput
  };
  
  const onPressConfirm = async () => {
    if (!reason) {
      Toast.show({
        text: 'Must provide a reason when returning an order',
        type: 'warning',
        buttonText: 'Okay',
        position: 'bottom'
      });
      return;
    }
    await returnOrder(OrderSelector.getId(selectedOrder), reason);
    navigation.pop(2);
  }

  return (
    <Container style={styles.background}>
      <Content>
        <ValidatedForm
          originalInputs={inputs}
          render={newInputs => newInputs['reason']}
        />
        <Text style={styles.warningText}>
          Are you sure you want to permanently return this order? This will send it back to the customer for changes
        </Text>
        <Button block onPress={onPressConfirm}>
          <Text>Return</Text>
        </Button>
      </Content>
    </Container>
  );
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  },
  warningText: {
    paddingHorizontal: commonColor.formInputMarginLeft,
    paddingVertical: 20,
  },
});

ReturnOrderScreen.navigationOptions = {
  title: 'Return order'
};

const mapStateToProps = (state: RootState) => ({
  selectedOrder: state.getAccount().getSelectedOrder(),
});

const mapDispatchToProps = dispatch => ({
  returnOrder: async (orderId: string, reason: string) => {
    dispatch(showLoading());
    await dispatch(returnOrderAction(orderId, reason));
    dispatch(removeLoading());
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(ReturnOrderScreen);

export const returnOrderScreenName = 'returnOrderScreen';
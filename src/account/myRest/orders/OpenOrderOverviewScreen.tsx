import { Container, Text, Content, List, Button } from 'native-base';
import React from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import { Order, OrderSelector } from '../../../general/order/OrderModel';
import { OrderItem } from '../../../general/order/OrderItem';
import CartListItem from '../../../general/cart/components/CartListItem';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import { displayDateTime } from '../../../general/utils/dateTime';
import { RootState } from '../../../general/redux/RootState';
import { returnOrderScreenName } from './ReturnOrderScreen';
import { setOrderPendingTipAction } from '../../../general/order/redux/orderActions';
import { showLoading, removeLoading } from '../../../general/redux/uiActions';
import { ManagerRestSelector } from '../../../general/rest/ManagerRest';
import PrintOrderButton from './PrintOrderButton';

type props = {
  isManager: boolean,
  selectedOrder: Order
  setOrderPendingTip: (orderId: string) => void,
}
type params = {
  refetchOrders: () => void,
}
const OpenOrderOverviewScreen: NavigationScreenComponent<params, any, props> = ({
  isManager,
  navigation,
  selectedOrder,
  setOrderPendingTip
}) => {
  if (!selectedOrder) return null;
  const itemTotal = OrderSelector.getItemTotal(selectedOrder);
  const tax = OrderSelector.getTax(selectedOrder);
  const tip = OrderSelector.getTip(selectedOrder);
  const orderTotal = OrderSelector.getOrderTotal(selectedOrder);
  const items = OrderSelector.getItems(selectedOrder);
  const onSetOrderPendingTip = async () => {
    await setOrderPendingTip(selectedOrder._id);
    navigation.goBack();
  }
  return (
    <Container>
      <Content>
        <List>
          {items.map((item: OrderItem, index) => <CartListItem key={index} item={item} />)}
        </List>
      </Content>
      <View style={styles.costs}>
        <View>
          <Text>Items:</Text>
          <Text>Tax:</Text>
          <Text>Tip:</Text>
          <Text style={styles.bold}>Total:</Text>
        </View>
        <View>
          <Text>{itemTotal.toFixed(2)}</Text>
          <Text>{tax.toFixed(2)}</Text>
          <Text>{tip.toFixed(2)}</Text>
          <Text style={styles.totalPrice}>{orderTotal.toFixed(2)}</Text>
        </View>
      </View>
      <PrintOrderButton />
      <Button block onPress={() => navigation.navigate(returnOrderScreenName)}>
        <Text>Return order</Text>
      </Button>
      {isManager &&
      <Button bordered block onPress={onSetOrderPendingTip}>
        <Text>Move order to Pending Tip</Text>
      </Button>}
    </Container>
  );
}

const styles = StyleSheet.create({
  costs: {
    paddingHorizontal: commonColor.listItemPadding,
    marginTop: commonColor.listItemPadding,
    paddingTop: commonColor.listItemThinPadding,
    borderTopWidth: commonColor.borderWidth,
    borderTopColor: commonColor.cardBorderColor,
    backgroundColor: commonColor.brandCanvas,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  bold: {
    fontWeight: 'bold',
  },
  totalPrice: {
    fontWeight: 'bold',
    color: commonColor.brandPrimary,
  },
});

OpenOrderOverviewScreen.navigationOptions = ({ screenProps }) => ({
  title: OrderSelector.getCustomerName(screenProps.getAccount().getSelectedOrder())
    + ' '
    + displayDateTime(OrderSelector.getCartUpdatedDate(screenProps.getAccount().getSelectedOrder()))
});

const mapStateToProps = (state: RootState) => ({
  selectedOrder: state.getAccount().getSelectedOrder(),
  isManager: ManagerRestSelector.isManager(state.getAccount().getSelectedRest(), state.getSignedInUser()),
});

const mapDispatchToProps = dispatch => ({
  setOrderPendingTip: async orderId => {
    dispatch(showLoading());
    await dispatch(setOrderPendingTipAction(orderId));
    dispatch(removeLoading());
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(OpenOrderOverviewScreen);

export const openOrderOverviewScreenName = 'openOrderOverviewScreen';
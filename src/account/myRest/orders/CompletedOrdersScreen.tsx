import { Container, Content, List, Text } from 'native-base';
import { StyleSheet, View } from 'react-native';
import React from 'react';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import { RootState } from '../../../general/redux/RootState';
import { ManagerRestSelector } from '../../../general/rest/ManagerRest';
import CenteredH3 from '../../../general/components/CenteredH3';
import { Order, OrderSelector } from '../../../general/order/OrderModel';
import OrderService from '../../../general/order/orderService';
import { displayDateTime } from '../../../general/utils/dateTime';
import LeftRightListItem from '../../../general/components/list/LeftRightListItem';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import { selectOrderAction } from '../../../general/order/redux/orderActions';
import { round2 } from '../../../general/utils/math';
import { completedOrderOverviewScreenName } from './CompletedOrderOverviewScreen';

type props = {
  completedOrders: Order[];
  loading: boolean,
  selectOrder: (order: Order) => void,
  refetch: () => void,
};

const CompletedOrdersScreen: NavigationScreenComponent<any, any, props> = ({
  completedOrders,
  loading,
  navigation,
  selectOrder,
  refetch
}) => {
  if (loading) {
    return <CenteredH3 text="Loading..." />
  }

  if (!completedOrders || completedOrders.length === 0) {
    return (
      <Container>
        <CenteredH3 text='You have no completed orders.' />
      </Container>
    )
  }
  return (
    <Container>
      <Text style={styles.description}>Completed orders are orders with completed transactions</Text>
      <Content style={styles.content}>
        <List>
          {completedOrders.map((order, index) => {
            const refunded = round2(OrderSelector.getCustomRefundsTotal(order));
            return (
              <LeftRightListItem
                key={index}
                leftText={OrderSelector.getCustomerName(order)}
                leftNote={displayDateTime(OrderSelector.getCartUpdatedDate(order))}
                right={
                  <View style={styles.right}>
                    <Text note>
                      ${OrderSelector.getOrderTotal(order)}
                    </Text>
                    {!!refunded && 
                      <Text note>
                        refunded ${refunded}
                      </Text>}
                  </View>
                }
                onPress={() => {
                  selectOrder(order);
                  navigation.navigate(completedOrderOverviewScreenName, {
                    refetchOrders: refetch,
                  });
                }}
              />
            )
          })}
        </List>
      </Content>
    </Container>
  );
}

CompletedOrdersScreen.navigationOptions = ({
  title: 'Completed orders'
});

const mapStateToProps = (state: RootState) => ({
  // necessary so that getCompletedOrdersInjector receives restId
  selectedRestId: ManagerRestSelector.getId(state.getAccount().getSelectedRest()),
});

const mapDispatchToProps = dispatch => ({
  selectOrder: order => dispatch(selectOrderAction(order)),
});

const styles = StyleSheet.create({
  content: {
    backgroundColor: commonColor.brandCanvas,
  },
  description: {
    padding:  commonColor.contentPadding,
  },
  right: {
    alignItems: 'flex-end',
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderService.getCompletedOrdersInjector(CompletedOrdersScreen));

export const completedOrdersScreenName = 'completedOrdersScreen';
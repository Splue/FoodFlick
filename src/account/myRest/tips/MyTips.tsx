import React from 'react';
import { connect } from 'react-redux';
import { RootState } from '../../../general/redux/RootState';
import { ManagerRestSelector } from '../../../general/rest/ManagerRest';
import CenteredH3 from '../../../general/components/CenteredH3';
import OrderService, { tips } from '../../../general/order/orderService';
import TipList from './TipList';

type props = {
  since: number,
  loading: boolean,
  tips: tips,
};

const MyTips: React.FC<props> = ({
  tips,
  loading,
}) => {
  if (loading) return <CenteredH3 text='Loading...' />
  if (tips && tips.servers.length === 0) return <CenteredH3 text='No tips. Try a different date' />
  return <TipList tips={tips} />
}

const mapStateToProps = (state: RootState) => ({
  selectedRestId: ManagerRestSelector.getId(state.getAccount().getSelectedRest()),
});

export default connect(mapStateToProps)(OrderService.getMyTotalTipsInjector(MyTips));

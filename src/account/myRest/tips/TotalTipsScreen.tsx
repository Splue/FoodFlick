import { Container, Text, DatePicker, Switch } from 'native-base';
import { StyleSheet, View } from 'react-native';
import React, { useState } from 'react';
import { connect } from 'react-redux';
import { NavigationScreenComponent } from 'react-navigation';
import { RootState } from '../../../general/redux/RootState';
import { ManagerRestSelector } from '../../../general/rest/ManagerRest';
import moment from 'moment-timezone';
import AllTips from './AllTips';
import MyTips from './MyTips';

type props = {
  timezone: string;
  isManager: boolean;
};

const TotalTipsScreen: NavigationScreenComponent<any, any, props> = ({
  timezone,
  isManager,
}) => {
  // get the miliseconds of the timezone's day start and convert that to date object
  const startOfDay = new Date(moment().tz(timezone).startOf('day').valueOf());
  const [date, setDate] = useState(startOfDay);
  // const [isAllServers, setIsAllServers] = useState(false);
  return (
    <Container>
      <View style={styles.header}>
        <DatePicker
          defaultDate={startOfDay}
          maximumDate={startOfDay}
          modalTransparent={false}
          formatChosenDate={date => `Since: ${moment(date).tz(timezone).format('l')}`}
          onDateChange={setDate}
        />
        {/* <View style={styles.toggle}>
          <Text>All servers</Text>
          <Switch
            value={isAllServers}
            onValueChange={v => setIsAllServers(v)}
          />
        </View> */}
      </View>
      {isManager ?
        <AllTips since={date.valueOf()} />
        :
        <MyTips since={date.valueOf()} />
      }
    </Container>
  );
}

TotalTipsScreen.navigationOptions = ({
  title: 'Tips'
});

const mapStateToProps = (state: RootState) => ({
  timezone: ManagerRestSelector.getTimezone(state.getAccount().getSelectedRest()),
  isManager: ManagerRestSelector.isManager(state.getAccount().getSelectedRest(), state.getSignedInUser()),
});

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  toggle: {
    flexDirection: 'row',
  },
});

export default connect(mapStateToProps)(TotalTipsScreen);

export const totalTipsScreenName = 'totalTipsScreen';
import { Content, List, Text } from 'native-base';
import { StyleSheet, View } from 'react-native';
import React from 'react';
import { NavigationScreenComponent } from 'react-navigation';
import LeftRightListItem from '../../../general/components/list/LeftRightListItem';
import commonColor from '../../../../native-base-theme/variables/commonColor';
import { tips } from '../../../general/order/orderService';

type props = {
  tips: tips
};

const TipList: NavigationScreenComponent<any, any, props> = ({
  tips,
}) => (
  <Content style={styles.content}>
    <List>
      {tips.servers.map((server, index) => (
        <LeftRightListItem
          key={server.userId}
          leftText={`${server.firstName} ${server.lastName}`}
          right={
            <View style={styles.right}>
              <Text note>
                ${tips.tips[index]}
              </Text>
            </View>
          }
        />
      ))}
    </List>
  </Content>
);

const styles = StyleSheet.create({
  content: {
    backgroundColor: commonColor.brandCanvas,
  },
  right: {
    alignItems: 'flex-end',
  },
});

export default TipList;

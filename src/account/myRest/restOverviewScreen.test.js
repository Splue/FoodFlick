import React from 'react';
import { shallow } from 'enzyme';
import { getMockStore, getMockNavigation } from '../../test/utils/mocks/mockGeneral';
import { getMockSelectedRest } from '../../test/utils/mocks/mockRests';
import RestOverviewScreen from './RestOverviewScreen';

describe('RestOverviewScreen', () => {
  const selectedRest = getMockSelectedRest();
  const store = getMockStore({ selectedRest });
  const navigation = getMockNavigation();
  const getShallow = () => shallow(<RestOverviewScreen navigation={navigation} />);

  it('renders', () => {
    expect(getShallow()).toMatchSnapshot();
  });

  it.skip('on menu press routes to MenuOverviewScreen', () => {
  });

  it.skip('on restaurant details press routes to RestDetailsScreen', () => {
  });

});

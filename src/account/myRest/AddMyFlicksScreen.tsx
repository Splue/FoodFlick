import React from 'react';
import { connect } from 'react-redux';
import { NavScreenOptions } from '../../general/navModels/NavScreenOptions';
import HeaderConfirmButton from '../../general/components/header/HeaderConfirmButton';
import { NavigationRoute, NavigationScreenProp } from 'react-navigation';
import CameraRollPicker from '../../general/components/flick/CameraRollPicker';
import { Container } from 'native-base';
import { addFlicksAction } from '../../general/user/userActions';
import { showLoading, removeLoading } from '../../general/redux/uiActions';

type flickSelection = {
  // key is the index of the photo in the camera roll and value is the photo's uri
  [key: number]: string
}

type state = {
  selected: flickSelection;
}

type props = {
  navigation: NavigationScreenProp<NavigationRoute>;
  onConfirm: (flicks: string[]) => void;
}

class AddMyFlicksScreen extends React.Component<props, state> {
  static navigationOptions = ({ navigation: { state: {params = {}} } }: NavScreenOptions) => ({
    tabBarVisible: false,
    title: 'Camera roll',
    headerRight: <HeaderConfirmButton onConfirm={params.onConfirm} text='Add' />
  });

  state = {
    selected: {},
  }

  onCameraPermissionGranted = () => {
    this.props.navigation.setParams({
      onConfirm: () => this.props.onConfirm(Object.values(this.state.selected))
    });
  }

  updateSelected = (newSelected: flickSelection) => {
    this.setState({ selected: newSelected });
  }

  render () {
    return (
      <Container>
        <CameraRollPicker canSelectMultiple selected={this.state.selected} updateSelected={this.updateSelected}
        numColumns={4} onCameraPermissionGranted={this.onCameraPermissionGranted}/>
      </Container>
    )
  }
}

const mapDispatchToProps = (dispatch, { navigation }: props) => ({
  onConfirm: async (flicks: string[]) => {
    if (flicks.length === 0) {
      navigation.goBack();
      return;
    }
    dispatch(showLoading());
    await dispatch(addFlicksAction(flicks));
    navigation.goBack();
    dispatch(removeLoading());
  }
});

export default connect(null, mapDispatchToProps)(AddMyFlicksScreen);

export const addMyFlicksScreenName = 'addMyFlicksScreen';
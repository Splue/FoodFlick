import { find } from 'lodash';
import AccountPickerScreen, { accountPickerScreenName } from './signUp/AccountPickerScreen';
import AccountScreen, { accountScreenName } from './AccountScreen';
import SignInScreen, { signInScreenName } from './SignInScreen';
import SettingsScreen, { settingsScreenName } from './settings/SettingsScreen';
import CategoryDetailScreen, { categoryDetailsScreenName } from '../account/myRest/menu/CategoryDetailScreen';
import ConfirmSignUpScreen, { confirmSignUpScreenName } from '../account/signUp/ConfirmSignUpScreen';
import CreateEmailScreen, { createEmailScreenName } from './CreateEmailScreen';
import CreatePasswordScreen, { createPasswordScreenName } from './signUp/CreatePasswordScreen';
import ItemDetailsScreen, { itemDetailsScreenName } from './myRest/menu/ItemDetailsScreen';
import ItemFlickPickerScreen, { itemFlickPickerScreenName } from './myRest/menu/ItemFlickPickerScreen';
import MenuUpdateScreen, { menuUpdateScreenName } from './myRest/menu/MenuUpdateScreen';
import UpdateCategoriesScreen, { updateCategoriesScreenName } from './myRest/menu/UpdateCategoriesScreen';
import RestLocationScreen, { restLocationScreenName } from './myRest/restDetails/RestLocationScreen';
import RestDetailsScreen, { restDetailsScreenName } from './myRest/restDetails/RestDetailsScreen';
import RestManagersScreen, { restManagersScreenName } from './myRest/restDetails/RestManagersScreen';
import MyRestsScreen, { myRestsScreenName } from './myRest/MyRestsScreen';
import RestProfileScreen, { restProfileScreenName } from './myRest/restDetails/RestProfileScreen';
import RestOverviewScreen, { restOverviewScreenName } from './myRest/RestOverviewScreen';
import UpdateItemsScreen, { updateItemsScreenName } from './myRest/menu/UpdateItemsScreen';
import StackNavigator from '../general/components/StackNavigator';
import RestFeedbackScreen, { restFeedbackScreenName } from './myRest/RestFeedbackScreen';
import AddFlicksScreen, { addMyFlicksScreenName } from './myRest/AddMyFlicksScreen';
import RestBankingScreen, { restBankingScreenName } from './myRest/restDetails/RestBankingScreen';
import NameScreen, { nameScreenName } from './signUp/customer/NameScreen';
import CardScreen, { cardScreenName } from './customer/CardScreen';
import ChoiceScreen, { choiceScreenName } from './myRest/menu/ChoiceScreen';
import PrintersScreen, { printersScreenName } from './myRest/restDetails/PrintersScreen';
import UpdatePrinterDetailsScreen, { updatePrinterDetailsScreenName } from './myRest/restDetails/PrinterDetailsScreen/UpdatePrinterDetailsScreen';
import AddPrinterDetailsScreen, { addPrinterDetailsScreenName } from './myRest/restDetails/PrinterDetailsScreen/AddPrinterDetailsScreen';
import UpdateItemPrintersScreen, { updateItemPrintersScreenName } from './myRest/menu/UpdateItemPrintersScreen';
import UpdateReceiverDetailsScreen, { updateReceiverDetailsScreenName } from './myRest/restDetails/ReceiverDetailsScreen/UpdateReceiverDetailsScreen';
import AddReceiverDetailsScreen, { addReceiverDetailsScreenName } from './myRest/restDetails/ReceiverDetailsScreen/AddReceiverDetailsScreen';
import CompletedOrdersScreen, { completedOrdersScreenName } from './myRest/orders/CompletedOrdersScreen';
import RefundScreen, { refundScreenName } from './myRest/orders/RefundScreen';
import UpdateUrlScreen, { updateUrlScreenName } from './myRest/restDetails/UpdateUrlScreen';
import OpenOrdersScreen, { openOrdersScreenName } from './myRest/orders/OpenOrdersScreen';
import ReturnOrderScreen, { returnOrderScreenName } from './myRest/orders/ReturnOrderScreen';
import OrderManagementScreen, { orderManagementScreenName } from './myRest/restDetails/OrderManagementScreen';
import OpenOrderOverviewScreen, { openOrderOverviewScreenName } from './myRest/orders/OpenOrderOverviewScreen';
import CompletedOrderOverview, { completedOrderOverviewScreenName } from './myRest/orders/CompletedOrderOverviewScreen';
import PendingTipOrdersScreen, { pendingTipOrdersScreenName } from './myRest/orders/PendingTipOrdersScreen';
import PendingTipOrderOverviewScreen, { pendingTipOrderOverviewScreenName } from './myRest/orders/PendingTipOrderOverviewScreen';
import SubscriptionScreen, { subscriptionScreenName } from './myRest/subscription/SubscriptionScreen';
import SubscriptionDetailsScreen, { subscriptionDetailsScreenName } from './myRest/subscription/SubscriptionDetailsScreen';
import ServersScren, { serversScreenName } from './myRest/restDetails/ServersScreen/SeversScreen';
import TablesScreen, { tablesScreenName } from './myRest/restDetails/TablesScreen/TablesScreen';
import AddTableScreen, { addTableScreenName } from './myRest/restDetails/TablesScreen/AddTableScreen';
import UpdateTableScreen, { updateTableScreenName } from './myRest/restDetails/TablesScreen/UpdateTableScreen';
import UpdateTaxRateScreen, { updateTaxRateScreenName } from './myRest/restDetails/TaxRate/UpdateTaxRateScreen';
import AddTaxRateScreen, { addTaxRateScreenName } from './myRest/restDetails/TaxRate/AddTaxRateScreen';
import TotalTipsScreen, { totalTipsScreenName } from './myRest/tips/TotalTipsScreen';

const mapStateToProps = ({ nav: { routes }}) => ({
  nav: find(routes, { key: accountNavigatorName }),
});

const AccountNavigator = StackNavigator(
  accountScreenName,
  mapStateToProps,
  {
    [accountPickerScreenName]: { screen: AccountPickerScreen },
    [accountScreenName]: { screen: AccountScreen },
    [addPrinterDetailsScreenName]: { screen: AddPrinterDetailsScreen },
    [addTaxRateScreenName]: { screen: AddTaxRateScreen },
    [restBankingScreenName]: { screen: RestBankingScreen },
    [addMyFlicksScreenName]: { screen: AddFlicksScreen },
    [addReceiverDetailsScreenName]: { screen: AddReceiverDetailsScreen },
    [addTableScreenName]: { screen: AddTableScreen },
    [categoryDetailsScreenName]: { screen: CategoryDetailScreen },
    [cardScreenName]: { screen: CardScreen },
    [choiceScreenName]: { screen: ChoiceScreen },
    [completedOrdersScreenName]: { screen: CompletedOrdersScreen },
    [completedOrderOverviewScreenName]: { screen: CompletedOrderOverview },
    [confirmSignUpScreenName]: { screen: ConfirmSignUpScreen },
    [createEmailScreenName]: { screen: CreateEmailScreen },
    [createPasswordScreenName]: { screen: CreatePasswordScreen },
    [itemFlickPickerScreenName]: { screen: ItemFlickPickerScreen },
    [itemDetailsScreenName]: { screen: ItemDetailsScreen },
    [menuUpdateScreenName]: { screen: MenuUpdateScreen },
    [myRestsScreenName]: { screen: MyRestsScreen },
    [nameScreenName]: { screen: NameScreen },
    [orderManagementScreenName]: { screen: OrderManagementScreen },
    [openOrdersScreenName]: { screen: OpenOrdersScreen },
    [openOrderOverviewScreenName]: { screen: OpenOrderOverviewScreen },
    [pendingTipOrdersScreenName]: { screen: PendingTipOrdersScreen },
    [pendingTipOrderOverviewScreenName]: { screen: PendingTipOrderOverviewScreen },
    [printersScreenName]: { screen: PrintersScreen },
    [refundScreenName]: { screen: RefundScreen },
    [returnOrderScreenName]: { screen: ReturnOrderScreen },
    [restDetailsScreenName]: { screen: RestDetailsScreen },
    [restLocationScreenName]: { screen: RestLocationScreen },
    [restFeedbackScreenName]: { screen: RestFeedbackScreen },
    [restProfileScreenName]: { screen: RestProfileScreen },
    [restManagersScreenName]: { screen: RestManagersScreen },
    [restOverviewScreenName]: { screen: RestOverviewScreen },
    [settingsScreenName]: { screen: SettingsScreen },
    [signInScreenName]: { screen: SignInScreen },
    [serversScreenName]: { screen: ServersScren },
    [subscriptionScreenName]: { screen: SubscriptionScreen },
    [subscriptionDetailsScreenName]: { screen: SubscriptionDetailsScreen },
    [tablesScreenName]: { screen: TablesScreen },
    [totalTipsScreenName]: { screen: TotalTipsScreen },
    [updateCategoriesScreenName]: { screen: UpdateCategoriesScreen },
    [updateItemsScreenName]: { screen: UpdateItemsScreen },
    [updatePrinterDetailsScreenName]: { screen: UpdatePrinterDetailsScreen },
    [updateItemPrintersScreenName]: { screen: UpdateItemPrintersScreen },
    [updateReceiverDetailsScreenName]: { screen: UpdateReceiverDetailsScreen },
    [updateTableScreenName]: { screen: UpdateTableScreen },
    [updateUrlScreenName]: { screen: UpdateUrlScreen },
    [updateTaxRateScreenName]: { screen: UpdateTaxRateScreen },
  }
);

export default AccountNavigator;

export const accountNavigatorName = 'accountNavigator';
import React from 'react';
import { shallow } from 'enzyme';
import AccountPickerScreen from './AccountPickerScreen';
import { getMockNavigation, getMockStore } from '../../test/utils/mocks/mockGeneral';

describe('AccountPickerScreen', () => {

  const navigation = getMockNavigation({
    state: {
      params: {
        onBack: jest.fn(),
      }
    }
  });

  let store;
  const getShallow = () => shallow(<AccountPickerScreen navigation={navigation} />, {context: {store}}).dive()

  afterEach(() => {
    navigation.reset();
  });

  it('renders', () => {
    store  = getMockStore();
    expect(getShallow()).toMatchSnapshot();
  });

  it.skip('routes to ??? on pressing customer', () => {
  });

  it('pressing restaurant routes to CreateEmailScreen', () => {
    store  = getMockStore();
    const screen = getShallow();
    screen.find({testId: 'rest'}).simulate('press');
    expect(navigation.navigate).toHaveBeenCalledWith('createEmailScreen', {
      confirmText: 'Next',
      onConfirm: screen.instance().props.onEmailConfirm,
    });
    expect(store.getState().signedInUser).toEqual({isRestManager: true});
  });

  it('pressing back routes to SignUpScreen and clears all sign up data', async () => {
    const newRestManager = {
      email: 'test@test.com',
      password: 'kobe',
      isRestManager: true,
    };

    store = getMockStore({signedInUser: newRestManager});
    const screen = getShallow();

    expect(store.getState().signedInUser).toEqual(newRestManager);

    await screen.instance().props.onBack();
    
    expect(store.getState().signedInUser).toEqual(null);
    expect(navigation.goBack).toHaveBeenCalled();
  });
});

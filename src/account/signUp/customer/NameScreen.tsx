import React from 'react';
import { NavigationRoute, NavigationScreenProp } from 'react-navigation';
import { connect } from 'react-redux';
import { StyleSheet } from 'react-native';
import { RootState } from '../../../general/redux/RootState';
import { SignedInUser, SignedInUserSelector } from '../../../general/user/SignedInUser';
import { NavScreenOptions } from '../../../general/navModels/NavScreenOptions';
import ValidatedForm from '../../../general/components/ValidatedForm';
import HeaderConfirmButton from '../../../general/components/header/HeaderConfirmButton';
import ValidatedInput from '../../../general/components/ValidatedInput';
import { getCleanInputs } from '../../../general/utils/formHelpers';
import { setNameAction, setEmailAction } from '../../../general/user/userActions';
import { createEmailScreenName } from '../../CreateEmailScreen';
import { createPasswordScreenName } from '../CreatePasswordScreen';
import { Container, Content } from 'native-base';
import commonColor from '../../../../native-base-theme/variables/commonColor';

type props = {
  signedInUser: SignedInUser
  navigation: NavigationScreenProp<NavigationRoute>
  onConfirm: (name: state) => void;
}

type state = {
  firstName: string,
  lastName: string,
}

class NameScreen extends React.Component<props, state> {
  static navigationOptions = ({ navigation: {state: {params = {}}}}: NavScreenOptions ) => ({
    tabBarVisible: false,
    title: params.confirmText === 'Next' ? "What's your name?" : 'Your name',
    headerRight: <HeaderConfirmButton onConfirm={params.onConfirm} text={params.confirmText}/>
  });

  state = { 
    firstName: (this.props.navigation.state.params.confirmText === 'Save' &&
        SignedInUserSelector.getFirstName(this.props.signedInUser)) || '',
    lastName: (this.props.navigation.state.params.confirmText === 'Save' &&
        SignedInUserSelector.getLastName(this.props.signedInUser)) || '',
  };

  render () {
    const { onConfirm } = this.props;
    
    const inputs = {
      firstName: (
        <ValidatedInput
          floatingLabel
          required
          label='First name'
          value={this.state.firstName}
          onChangeText={firstName => this.setState({ firstName })}
        />
      ) as unknown as ValidatedInput,
      lastName: (
        <ValidatedInput
          floatingLabel
          required
          label='Last name'
          value={this.state.lastName}
          onChangeText={lastName => this.setState({ lastName })}
        />
      ) as unknown as ValidatedInput
    }

    return (
      <Container style={styles.background}>
        <Content>
          <ValidatedForm
            onConfirm={() => onConfirm(getCleanInputs(this.state) as state)}
            originalInputs={inputs}
            render={newInputs => Object.values(newInputs)}
          />
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  }
});

const mapStateToProps = (state: RootState) => ({
  firstName: SignedInUserSelector.getFirstName(state.getSignedInUser()),
  lastName: SignedInUserSelector.getLastName(state.getSignedInUser())
});

const mergeProps = ({ firstName, lastName, }, { dispatch }, props) => {
  const { navigation } = props;
  switch (navigation.state.params.confirmText) {
    case 'Next': // from signup
      return {
        ...props,
        firstName,
        lastName,
        onConfirm: async name => {
          const cleanName = getCleanInputs(name) as state;
          await dispatch(setNameAction(cleanName));
          navigation.navigate(createEmailScreenName, {
            confirmText: 'Next',
            onConfirm: async email => {
              await dispatch(setEmailAction(email));
              navigation.navigate(createPasswordScreenName, { confirmText: 'Next' });
            }
          });
        }
      }
    case 'Save': //updating name
      return {
        ...props,
        firstName,
        lastName,
        onConfirm: async name => {
        }
      }
    default:
      //todo 1. throw errow. bad props.    
      return {}
  }
}

export default connect(mapStateToProps, null, mergeProps)(NameScreen);
export const nameScreenName = 'nameScreen';

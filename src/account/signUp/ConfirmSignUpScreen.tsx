import React from 'react';
import { StyleSheet, LinkingIOS, Linking } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { signUpAction, signInWithBasicAction, navigateAndResetAccountNavAction } from '../../general/user/userActions';
import { addNewRestAction } from '../../general/rest/restActions';
import { Container, Button, Text } from 'native-base';
import CenteredH3 from '../../general/components/CenteredH3';
import { RootState } from '../../general/redux/RootState';
import { NavigationScreenComponent } from 'react-navigation';
import { restOverviewScreenName } from '../myRest/RestOverviewScreen';
import { myRestsScreenName } from '../myRest/MyRestsScreen';
import commonColor from '../../../native-base-theme/variables/commonColor';

const ServicesAgreement = () => (
  <Text style={styles.link} onPress={() => Linking.openURL('https://stripe.com/connect-account/legal')}>services agreement</Text>
)
// todo 0: add error if signup fails
const ConfirmSignupScreen: NavigationScreenComponent = ({ signUp }: any) => (
  <Container>
    <CenteredH3 text='Finish Signing Up'/>
    <Text style={styles.body}>By registering your account, you agree to our <ServicesAgreement />.</Text>
    <Button {...{ testId: 'signUp' }} block onPress={signUp}>
      <Text>Sign up</Text>
    </Button>
  </Container>  
);

const styles = StyleSheet.create({
  link: {
    color: 'blue',
  },
  body: {
    marginHorizontal: commonColor.contentPadding,
  }
});


// ConfirmSignupScreen.propTypes = {
//   signUp: PropTypes.func.isRequired
// }

ConfirmSignupScreen.navigationOptions = {
  tabBarVisible: false,
  title: 'Sign up'
}

const mapStateToProps = (state: RootState) => ({
  newUser: state.getSignedInUser(),
  newRest: state.getAccount().getNewRest(),
})

const mergeProps = ({ newUser, newRest }, { dispatch }) => ({
  signUp: async () => {
    await dispatch(signUpAction(newUser));
    await dispatch(signInWithBasicAction(newUser.email, newUser.password));
    if (newUser.isRestManager) {
      if (newRest) {//there is no newRest if user skips adding a rest
        await dispatch(addNewRestAction(newRest));
        await dispatch(navigateAndResetAccountNavAction(restOverviewScreenName));
      } else {
        await dispatch(navigateAndResetAccountNavAction(myRestsScreenName));
      }
    } else {
      await dispatch(navigateAndResetAccountNavAction());
    }
  }
})

export default connect(mapStateToProps, null, mergeProps)(ConfirmSignupScreen);

export const confirmSignUpScreenName ='confirmSignUpScreen';
import React from 'react';
import { shallow } from 'enzyme';
import ConfirmSignUpScreen from './ConfirmSignUpScreen';
import { getMockSelectedRest } from '../../test/utils/mocks/mockRests';
import {
  getMockManagerSignUpFetchRes,
  getMockManagerSignInFetchRes,
  getMockSignedInRestManager,
} from '../../test/utils/mocks/mockUsers';
import { getMockNewRest } from '../../test/utils/mocks/mockRests';
import { getMockNavigation, getMockStore } from '../../test/utils/mocks/mockGeneral';
import * as accountActions from '../accountActions';
import * as myRestActions from '../../general/rest/restActions';
import RestService from '../myRest/services/restService';
import fetchMock from 'fetch-mock';

describe('ConfirmSignUpScreen', () => {
  const navigation = getMockNavigation({
    state: {
      params: {
        confirmText: 'Sign up'
      }
    }
  });

  const mockSignedInRestManager = getMockSignedInRestManager();

  const newCustomer = {
    email: mockSignedInRestManager.email,
    password: 'kobe',
  };

  fetchMock.post(accountActions.auth0Domain + 'dbconnections/signup', getMockManagerSignUpFetchRes());
  fetchMock.post(accountActions.auth0Domain + 'oauth/token', getMockManagerSignInFetchRes());
  const signUpAction = jest.spyOn(accountActions, 'signUpAction');
  const signInAction = jest.spyOn(accountActions, 'signInAction');
  const navigateAndResetAccountNavAction = jest.spyOn(accountActions, 'navigateAndResetAccountNavAction');

  let store;
  const getShallow = () => shallow(<ConfirmSignUpScreen navigation={navigation} />, {context: {store}}).dive()

  beforeEach(() => {
    signUpAction.mockClear();
    signInAction.mockClear();
    navigateAndResetAccountNavAction.mockClear();
  });

  afterAll(() => {
    signInAction.mockRestore();
    signUpAction.mockRestore();
    navigateAndResetAccountNavAction.mockRestore();
    fetchMock.restore();
  })

  it('renders', () => {
    store = getMockStore({signedInUser: newCustomer});
    expect(getShallow()).toMatchSnapshot();
  });

  it('on confirm signs up then signs in', done => {
    store = getMockStore({signedInUser: newCustomer});
    getShallow().find({testId: 'signUp'}).simulate('press');
    setImmediate(() => {
      expect(signUpAction).toHaveBeenCalledWith(newCustomer);
      expect(signInAction).toHaveBeenCalledWith(newCustomer.email, newCustomer.password);
      expect(store.getState().signedInUser).toEqual({ ...mockSignedInRestManager });
      done();
    })
  });
  
  describe('customer', () => {
    it.skip('on confirm, resets AccountNavigator', () => {

    });
  });

  describe('rest manager', () => {
    const newRestManager = {
      ...newCustomer,
      isRestManager: true,
    };

    it('on confirm with rest, dispatches addNewRestAction, resets AccountNavigator, and routes to RestOverview', done => {
      const addNewRestAction = jest.spyOn(myRestActions, 'addNewRestAction');
      const newRest = getMockNewRest();
      const addedRest = getMockSelectedRest();
      const addNewRest = jest.spyOn(RestService, 'addNewRest').mockImplementation(() => addedRest);
      store = getMockStore({signedInUser: newRestManager, newRest});
      getShallow().find({testId: 'signUp'}).simulate('press');
      const mockSignedInRestManager = getMockSignedInRestManager();
      setImmediate(() => {
        expect(myRestActions.addNewRestAction).toHaveBeenCalledWith({
          ...newRest,
          owner: {
            _id: mockSignedInRestManager._id,
            email: mockSignedInRestManager.email,
          }
        });
        expect(store.getState().selectedRest).toEqual(addedRest);
        expect(accountActions.navigateAndResetAccountNavAction).toHaveBeenCalledWith('restOverviewScreen');
        addNewRestAction.mockRestore();
        done();
      })
    });

    it('on confirm without rest, resets AccountNavigator and routes to MyRestScreen', done => {
      store = getMockStore({signedInUser: newRestManager});
      getShallow().find({testId: 'signUp'}).simulate('press');
      setImmediate(() => {
        expect(accountActions.navigateAndResetAccountNavAction).toHaveBeenCalledWith('myRestsScreen');
        done();
      });
    });
  });
});

import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import HeaderBackButton from '../../general/components/header/HeaderBackButton';
import { Container, Text, H3, Icon, Button } from 'native-base';
import { updateIsRestManagerAction, clearSignedInUser, setEmailAction } from '../../general/user/userActions';
import { connect } from 'react-redux';
import { navigationPropType } from '../../general/utils/propTypes/general';
import { nameScreenName } from './customer/NameScreen';

class AccountPickerScreen extends React.Component {

  static navigationOptions = ({navigation}) => {
    //default to emtpy object otherwise, params is undefined if there are no params
    const { params = {} } = navigation.state;
    return {
      tabBarVisible: false,
      title: 'Who are you?',
      headerLeft: <HeaderBackButton onPress={params.onBack} />
    }
  };

  static propTypes = {
    navigation: navigationPropType,
    updateIsRestManager: PropTypes.func.isRequired,
    onBack: PropTypes.func.isRequired,
  }

  componentDidMount () {
    this.props.navigation.setParams({ 
      onBack: this.props.onBack,
    });
  }

  render () {
    const { navigation, updateIsRestManager } = this.props;

    return (
      <Container style={styles.container}>
        <H3 style={styles.h3}></H3>
        <Container style={styles.buttonContainer}>
          {/* <Button testId='customer' transparent large vertical onPress={() => navigation.navigate(nameScreenName, {
            confirmText: 'Next',
          })}>
            <Icon name='person' />
            <Text>Customer</Text>
          </Button> */}
          <Button testId='rest' transparent large vertical onPress={() => {
            updateIsRestManager(true);
            navigation.navigate(nameScreenName, {
              confirmText: 'Next',
            });
          }}>
            <Icon name='restaurant' />
            <Text>I'm a restaurant</Text>
          </Button>
        </Container>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  h3: {
    flex: 1,
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  buttonContainer: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'space-around',
  }
});

const mapDispatchToProps = (dispatch, { navigation }) => ({
  onBack: async () => {
    await dispatch(clearSignedInUser());
    navigation.goBack();      
  },
  updateIsRestManager: async isRestManager => await dispatch(updateIsRestManagerAction(isRestManager)),
})

export default connect(null, mapDispatchToProps)(AccountPickerScreen);

export const accountPickerScreenName ='accountPickerScreen';

import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet } from 'react-native';
import ValidatedForm from '../../general/components/ValidatedForm';
import { NavigationRoute, NavigationScreenProp } from 'react-navigation';
import { NavScreenOptions } from '../../general/navModels/NavScreenOptions';
import HeaderConfirmButton from '../../general/components/header/HeaderConfirmButton';
import { setPasswordAction } from '../../general/user/userActions';
import ValidatedInput from '../../general/components/ValidatedInput';
import { getCleanInput } from '../../general/utils/formHelpers';
import { cardScreenName } from '../customer/CardScreen';
import { Container, Content } from 'native-base';
import commonColor from '../../../native-base-theme/variables/commonColor';

type props = {
  navigation: NavigationScreenProp<NavigationRoute>;
  onConfirm: (password: string) => void
};

type state = {
  password: string,
}

class CreatePasswordScreen extends React.Component<props, state> {
  // static propTypes = {
  //   navigation: getNavigationFormPropType(),
  //   onConfirm: PropTypes.func.isRequired,
  // }

  static navigationOptions = ({navigation: {state: {params ={}}}}: NavScreenOptions) => ({
    tabBarVisible: false,
    title: "What's your password?",
    headerRight: <HeaderConfirmButton onConfirm={params.onConfirm} text={params.confirmText}/>
  });

  state = { 
    password: '',
  };

  validateOnEnd = password => {
    return password.length < 6 ? 'Password must be at least 6 characters' : '';
  }

  render () {
    const inputs = {
      password: (
        <ValidatedInput
          floatingLabel
          inputProps={{
            secureTextEntry: true
          }}
          value={this.state.password}
          label='Password'
          validateEndEdit={this.validateOnEnd}
          onChangeText={password => this.setState({ password })}
        />
      ) as unknown as ValidatedInput
    }

    return (
      <Container style={styles.background}>
        <Content>
          <ValidatedForm
            onConfirm={() => this.props.onConfirm(this.state.password)}
            originalInputs={inputs}
            render={newInputs => Object.values(newInputs)}
          />
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  }
});

const mapStateToProps = ({ signedInUser }) => ({
  signedInUser,
});

const mergeProps = ({ signedInUser }, {dispatch}, props) => ({
  ...props,
  onConfirm: props.navigation.state.params.confirmText === 'Next' ?
    async password => { //coming from sign up as only signup sets confirmText === next. thus isRestManger is for the new user
      const cleanPassword = getCleanInput(password);
      await dispatch(setPasswordAction(cleanPassword));
      if (signedInUser.isRestManager) {
        props.navigation.navigate('restLocationScreen', {confirmText: 'Next', isSkippable: true, forSignUp: true});
      } else {
        props.navigation.navigate(cardScreenName, { confirmText: 'Next', isSkippable: true });
      }
    } :
    password => { //coming from updating password
      // savePassword(password);
      // props.navigation.goBack();
    }
});

export default connect(mapStateToProps, null, mergeProps)(CreatePasswordScreen);

export const createPasswordScreenName = 'createPasswordScreen';
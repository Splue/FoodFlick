import React from 'react';
import { shallow } from 'enzyme';
import CreatePasswordScreen from './CreatePasswordScreen';
import { mockAnimated, getMockNavigation, getMockStore } from '../../test/utils/mocks/mockGeneral';

describe('CreatePasswordScreen', () => {
  mockAnimated();
  const navigation = getMockNavigation({
    state: {
      params: {
        confirmText: 'Next'
      }
    }
  });

  const getState = isRestManager => ({
    signedInUser: {
      email: 'test@test.com',
      isRestManager,
    },
  })

  const getShallow = () => shallow(<CreatePasswordScreen navigation={navigation} />, {context: {store}}).dive()
  
  let store; 

  afterEach(() => {
    navigation.reset();
  });

  it('renders', () => {
    store = getMockStore(getState());
    expect(getShallow()).toMatchSnapshot();
  });

  describe('customer sign up', () => {
    it.skip('on confirm routes to ConfirmSignupScreen and stores password in redux', done => {
      store = getMockStore(getState(false));

    });
  });

  describe('manager sign up', () => {
    it('on confirm routes to RestLocationScreen and stores password in redux', done => {
      store = getMockStore(getState(true));
      const screen = getShallow();
      const password = 'kobe'
      screen.instance().setState({password});
      screen.prop('onConfirm')();
      setImmediate(() => {
        expect(navigation.navigate).toHaveBeenCalledWith('restLocationScreen', {confirmText: 'Next', isSkippable: true, forSignUp: true});
        expect(store.getState().signedInUser.password).toEqual(password);
        done();
      })
    });
  });

  describe('user password edit', () => {
    it.skip('on confirm updates user password and routes back to prev screen', () => {

    });
  });
});

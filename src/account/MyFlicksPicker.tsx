import React from 'react';
import UserService from '../general/user/userService';
import FlickPicker from '../general/components/flick/FlickPicker';
import { Flick } from '../general/redux/Flick';

type flickSelection = {
  // key is the index of the photo in the camera roll and value is the photo's uri
  [key: number]: string
}

type props = {
  selected: flickSelection;
  updateSelected: (newSelected: flickSelection) => void;
  canSelectMultiple: boolean;
  numColumns: number;
  myFlicks: Flick[]; // string of photo urls belonging to signedInUser
  loading: boolean;
}

const MyFlicksPicker = (props: props) => (
  <FlickPicker {...props} listEmptyText='You have no uploaded flicks in your account'
  flicks={props.myFlicks && props.myFlicks.map(({ flick }) => ({ uri: flick }))} />
);

export default UserService.getMyFlicksInjector(MyFlicksPicker);
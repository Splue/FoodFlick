import React from 'react';
import { shallow } from 'enzyme';
import SignInScreen, { SignInScreen as RawSignInScreen } from './SignInScreen';
import { getMockSignedInRestManager, getMockManagerSignInFetchRes } from '../test/utils/mocks/mockUsers';
import { getMockNavigation, mockAnimated, getMockStore } from '../test/utils/mocks/mockGeneral';
import * as accountActions from './accountActions';
import fetchMock from 'fetch-mock';

describe('SignInScreen', () => {
  mockAnimated();
  const navigation = getMockNavigation({
    state: {
      params: {
        signIn: jest.fn(),
      }
    }
  });
  const store = getMockStore();
  const getShallow = () => shallow(<SignInScreen navigation={navigation} />, {context: {store}}).dive()

  afterEach(() => {
    navigation.reset(); 
  });

  it('renders', () => {
    expect(getShallow()).toMatchSnapshot();
  });

  it('sign in routes to AccountScreen and prevents user from going back to sign in screen', done => {
    const mockSignedInRestManager = getMockSignedInRestManager();
    fetchMock.postOnce(accountActions.auth0Domain + 'oauth/token', getMockManagerSignInFetchRes());
    
    const signInScreen = getShallow();
    const state = {
      email: mockSignedInRestManager.email,
      password: 'a',
    };
    
    signInScreen.instance().setState({ ...state });

    const signInAction = jest.spyOn(accountActions, 'signInAction');
    signInScreen.find({testId: 'signIn'}).simulate('press');
    setImmediate(() => {
      expect(signInAction).toHaveBeenCalledWith(state.email, state.password);
      expect(store.getState().signedInUser).toEqual(mockSignedInRestManager);
      expect(navigation.goBack).toHaveBeenCalled();
      signInAction.mockRestore();
      fetchMock.reset();
      done();
    })
  });

  it('sign up takes you to AccountPickerScreen', () => {
    const wrapper = getShallow();
    wrapper.find({testId: 'signUp'}).simulate('press');
    expect(navigation.navigate).toHaveBeenCalledWith('accountPickerScreen');
  });

  // POC: broken poc for mounting and testing connected components with nav. see MockProvider.js and setup.js
  // it('sign up takes you to AccountPickerScreen', () => {
  //   
  //   const store = initStore({...reducers, nav: (state = getSignInScreenNavState(), action) => {
  //     return nav(state, action);
  //   }});

  //   const SignInScreen = connectProviderNavMock(ReduxSignInScreen, store);

  //   // const renderer = TestRenderer.create(<SignInScreen />);

  //   const wrapper = mount(<SignInScreen />);
  //   console.log('WRAPPER', wrapper);

  //   console.log('SIGNIN SCREEN', SignInScreen);
  //   const signUpButton = wrapper.find({ testId: 'signUp' });
  //   console.log(signUpButton);
  //   signUpButton.simulate('press');
  // });
});

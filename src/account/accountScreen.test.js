import React from 'react';
import { shallow } from 'enzyme';
import { initStore } from '../store';
import AccountScreen from './AccountScreen';
import rootReducer from '../rootReducer';
import { getMockSignedInRestManager } from '../test/utils/mocks/mockUsers';
import { getMockNavigation, getMockStore, mockAnimated } from '../test/utils/mocks/mockGeneral';
import { signedInUser } from '../account/accountReducer';

describe('AccountScreen', () => {
  mockAnimated();
  const navigation = getMockNavigation();
  const signedInRestManager = getMockSignedInRestManager();
  const getShallow = () => shallow(<AccountScreen navigation={navigation} />, {context: {store}}).dive();

  let store;  
  beforeEach(() => {
    store = initStore();
  })

  afterEach(() => {
    navigation.reset();
  });

  describe('signed out', () => {
    it('renders sign in button', () => {
      expect(getShallow()).toMatchSnapshot();
    });
  });

  describe('manager', () => {
    it('renders rest manager options', () => {
      store = getMockStore({
        signedInUser: getMockSignedInRestManager(),
      })
      expect(getShallow()).toMatchSnapshot();
    });
  });

  describe('customer', () => {
    it.skip('renders customer options', () => {
      expect(getShallow()).toMatchSnapshot();
    });
  });
});

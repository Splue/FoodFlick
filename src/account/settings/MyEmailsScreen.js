// import React from 'react';
// import { compose } from 'react-apollo';
// import { StyleSheet, View } from 'react-native';
// import { connect } from 'react-redux';
// import { Button, Container, Content, Text, H3 } from 'native-base';
// import { selectRestAction } from '../../general/rest/restActions';
// import { deleteRestManagerService, addRestManagerService } from './services/restServices';
// import UndividedList from '../../general/components/UndividedList';
// import EmailListEditor from '../../general/components/EmailListEditor';
// import commonColor from '../../../native-base-theme/variables/commonColor';

// const RestManagersScreen = ({managers, onDelete, onAdd}) => (
//   <EmailListEditor
//     addButtonText='Add managers'
//     users={managers}
//     onAdd={onAdd}
//     onDelete={onDelete}
//   />
// );

// RestManagersScreen.navigationOptions = () => ({
//   title: 'Edit managers'
// });

// const mapStateToProps = ({selectedRest}) => ({
//   selectedRest
// });

// const mergeProps = ({selectedRest: {_id: restId, managers}}, {dispatch}, {deleteRestManager, addRestManager, navigation}) => ({
//   managers,
//   onDelete: managerId => dispatch(deleteRestManager(restId, managerId)),
//   onAdd: () => {
//     navigation.navigate('createEmailScreen', {
//       confirmText: 'Add', 
//       onConfirm: async managerEmail => {
//         dispatch(selectRestAction(await addRestManager(restId, managerEmail)));
//         navigation.goBack();
//       }
//     })
//   },
// })

// export default compose(
//   deleteRestManagerService,
//   addRestManagerService,
//   connect(mapStateToProps, null, mergeProps)
// )(RestManagersScreen);

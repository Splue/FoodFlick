import React from 'react';
import { connect } from 'react-redux';
import { Container, Content } from 'native-base';
import { updateEmailAction } from '../../general/user/userActions';
import SummaryCard from '../../general/components/SummaryCard';
import { RootState } from '../../general/redux/RootState';
import { SignedInUserSelector } from '../../general/user/SignedInUser';
import { cardScreenName } from '../customer/CardScreen';
import UserService from '../../general/user/userService';

/*
just have primary email (login email) and recovery email. if they forget username then they can send username to either
the recover number or email
*/

const SettingsScreen = ({ signedInUserEmail, cardLoading, myCard, onPressEmailIcon, onPressCardIcon }) => {
  let card;
  if (cardLoading) {
    card = ['Loading...'];
  } else {
    card = myCard ? [`**** **** **** **** ${myCard.last4}`] : []
  }
  return (
    <Container>
      <Content>
        <SummaryCard
          title='Email'
          icon='create'
          items={[signedInUserEmail]}
          onIconPress={onPressEmailIcon}
        />
        {/* todo victor: make the screen to edit password. should look like amazon's password edit */}
        {/* <SummaryCard
          title='Password'
          icon='create'
          items={['**********']}
          onIconPress={() => {}}
        />
        <SummaryCard
          title='Card'
          icon='create'
          items={card}
          emptyItemsText='No saved card'
          onIconPress={onPressCardIcon}
        /> */}
        {/* <SummaryCard title='Location' items={Object.values(addressItems)}
          onIconPress={() => navigate('restLocationScreen', {confirmText: 'Save', isSkippable: false})} /> */}
      </Content>
    </Container>
  )
}

SettingsScreen.navigationOptions = {
  title: 'Settings',
};

const mapStateToProps = (state: RootState) => ({
  signedInUserEmail: SignedInUserSelector.getEmail(state.getSignedInUser())
});

const mapDispatchToProps = (dispatch, { navigation }) => ({
  //todo victor: route to new screen to match mock 'settings - email edit'
  onPressEmailIcon: () => navigation.navigate('createEmailScreen', {
    confirmText: 'Save',
    onConfirm: async newEmail => {
      await dispatch(updateEmailAction(newEmail));
      navigation.goBack(null); //null, otherwise it goes back from settings screen instead of createEmailScreen
    }
  }),
  onPressCardIcon: () => navigation.navigate(cardScreenName, { confirmText: 'Save' }),
})

export default connect(mapStateToProps, mapDispatchToProps)(UserService.getMyCardInjector(SettingsScreen));

export const settingsScreenName = 'settingsScreen';
import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet } from 'react-native';
import { NavigationRoute, NavigationScreenProp } from 'react-navigation';
import HeaderConfirmButton from '../general/components/header/HeaderConfirmButton';
import ValidatedForm from '../general/components/ValidatedForm';
import { doEvent } from '../general/utils/componentEventHandler';
import ValidatedInput from '../general/components/ValidatedInput';
import { getCleanInput } from '../general/utils/formHelpers';
import { doesUserExist } from '../general/user/userActions';
import { Container, Content } from 'native-base';
import commonColor from '../../native-base-theme/variables/commonColor';

type props = {
  signedInUserEmail?: string;
  navigation: NavigationScreenProp<NavigationRoute>
}

type state = {
  email: string,
}

class CreateEmailScreen extends React.Component<props, state> {
  // static propTypes = {
  //   signedInUserEmail: PropTypes.string,
  //   navigation: getNavigationFormPropType({ onConfirm: PropTypes.func.isRequired })
  // }

  static navigationOptions = ({ navigation }) => {
    //default to emtpy object otherwise, params is undefined if there are no params
    const { params = {} } = navigation.state;
    return {
      tabBarVisible: false,
      title: "Email?",
      headerRight: <HeaderConfirmButton onConfirm={params.onConfirm} text={params.confirmText}/>
    }
  };

  state = { 
    email: (this.props.navigation.state.params.confirmText === 'Save' && this.props.signedInUserEmail) || '',
  };

  validateOnEnd = async email => {
    if (email.length === 0) return `Email cannot be empty`;
    const regex = /\S+@\S+\.\S+/; // simple regex to cover majority of cases
    if (!regex.test(email)) return 'Please enter an email like example@example.com';
    const { params = {} } = this.props.navigation.state;
    if (params.shouldNotCheckIfUserExists) {
      return '';
    } else {
      const usersExists = await doesUserExist(email);
      return usersExists ? 'This user already exists with Foodflick. Please use a different email': ''
    }
  }

  render () {
    const {navigation} = this.props;
    
    const inputs = {
      email: (
        <ValidatedInput
          floatingLabel
          label='Email'
          value={this.state.email}
          onChangeText={email => this.setState({ email })}
          validateEndEditAsync={this.validateOnEnd}
        />
      ) as unknown as ValidatedInput
    }

    /**
    * @TODO: 1
    * separate all forms such that for form x, there is a separate update and separate create form that share
    * the same base code of x. that way we dont need big switches based on onConfirmText
    * 
    * below is an explanation of why this screen id different than the other form screens...
    * 
    * currently receive onConfirm as a route prop because otherwise theres too much logic here. createEmailScreen would
    * need to have logic to determine and execute 4 different scenarios. but again, this needs to change
    * 1) add new account email. 2) signup as user 3) signup as rest 4) adding rest manager
    **/
    return (
      <Container style={styles.background}>
        <Content>
          <ValidatedForm
            onConfirm={() => doEvent(this, navigation.state.params.onConfirm, getCleanInput(this.state.email))}
            originalInputs={inputs}
            render={newInputs => Object.values(newInputs)}
          />
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: commonColor.brandCanvas,
  }
});

const mapStateToProps = ({ signedInUser }) => ({
  signedInUserEmail: signedInUser && signedInUser.email
});

export default connect(mapStateToProps)(CreateEmailScreen);
const createEmailScreenName = 'createEmailScreen';

export {
  CreateEmailScreen,
  createEmailScreenName
}

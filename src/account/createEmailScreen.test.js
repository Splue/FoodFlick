import React from 'react';
import { shallow } from 'enzyme';
import CreateEmailScreen from './CreateEmailScreen';
import { getMockSignedInRestManager } from '../test/utils/mocks/mockUsers';
import { getMockNavigation, mockAnimated, getMockStore } from '../test/utils/mocks/mockGeneral';
import { signedInUser } from '../account/accountReducer';
import rootReducer from '../rootReducer';

describe('CreateEmailScreen', () => {
  mockAnimated();
  const navigation = getMockNavigation({
    state: {
      params: {
        confirmText: 'Save',
        onConfirm: jest.fn(),
      }
    }
  });
  const getShallow = () => shallow(<CreateEmailScreen navigation={navigation} />, {context: {store}}).dive();

  let store; 
  beforeEach(() => {
    store = getMockStore();
  });

  afterEach(() => {
    navigation.reset();
  });

  it('renders with confirm text', () => {
    expect(getShallow()).toMatchSnapshot();
  });
  
  it('calls the onConfirm callback', () => {
    const createEmailScreen = getShallow();
    createEmailScreen.instance().setState({email: 'kobe'});
    createEmailScreen.prop('onConfirm')();
    expect(navigation.state.params.onConfirm).toHaveBeenCalledWith('kobe');
  });

  describe('user email edit', () => {
    it('renders with populated data', () => {
      const mockUser = getMockSignedInRestManager();
      store = getMockStore({
        signedInUser: mockUser,
      })
      const createEmailScreen = getShallow();
      expect(createEmailScreen.state('email')).toEqual(mockUser.email);
    });
  });
});

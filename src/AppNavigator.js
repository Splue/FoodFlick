import React from 'react';
import { connect } from 'react-redux';
import { TabNavigator, addNavigationHelpers } from "react-navigation";
import { Container } from 'native-base';
import { addNavReduxListener } from './general/redux/store';
import FooterTabNav from './general/FooterTabNav';
import AccountNavigator, { accountNavigatorName } from './account/AccountNavigator';
import TrendingNavigator, { trendingNavigatorName } from './trending/TrendingNavigator';
import FavoritesNavigator, { favoritesNavigatorName } from './favorites/FavoritesNavigator';

export const RawTabNavigator = TabNavigator(
  {
    [trendingNavigatorName]: { screen: TrendingNavigator },
    [favoritesNavigatorName]: { screen: FavoritesNavigator },
    [accountNavigatorName]: { screen: AccountNavigator }
  },
  {
    initialRouteName: accountNavigatorName,
    tabBarPosition: 'bottom',
    swipeEnabled: false,
    tabBarComponent: () => null,
    // tabBarComponent: (props) => <FooterTabNav {...props} />
  }
);  

//wrap TabNavigation so that we can pass it our own navigation prop. when we pass our own navigation
//prop, TabNavigation "relinquishes control of its internal state" so that we can manage nav state
//through redux. default navigators' navigation props only includes state and dispatch and we mimic
//this behavior with addNavigationHelpers so nothing changes. to see redux manage nav state, go to
//navReducer and note how it imports the TabNavigation component to manage its nav state.
const AppNavigator = ({ dispatch, nav, reduxState }) => {
  // const addListener = createReduxBoundAddListener("root");
  return (
    <Container>
      <RawTabNavigator screenProps={reduxState} navigation={addNavigationHelpers({dispatch, state: nav, addListener: addNavReduxListener})} />
    </Container>
  )
}

const mapStateToProps = ({nav, ...remainingState}) => ({
  nav,
  reduxState: remainingState
});

//since we have no mapDispatchToProps argument, connect default passes the dispatch prop
export default connect(mapStateToProps)(AppNavigator);
import { TrendingState } from './TrendingState';
import { selectedCategory } from '../../general/menu/menuReducer';
import { selectedRest } from '../../general/rest/restReducer';

export const TRENDING_TAB: string = 'TRENDING_TAB';

export const trendingReducer = (state = new TrendingState(null, null), action: any = {}) => {
  switch(action.tab) {
    case TRENDING_TAB:
      return new TrendingState(
        selectedRest(state.getSelectedRest(), action),
        selectedCategory(state.getSelectedCategory(), action),
      );
    default:
      return state
  }
}

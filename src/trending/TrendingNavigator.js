import { find } from 'lodash';
import TrendingScreen, { trendingScreenName } from './TrendingScreen';
import SearchScreen, { searchScreenName } from './SearchScreen';
import StackNavigator from '../general/components/StackNavigator';
import GiveFeedbackScreen, { giveFeedbackScreenName } from '../general/screens/GiveFeedbackScreen';
import TrendingMenuBrowseScreen, { trendingMenuBrowseScreenName } from './TrendingMenuBrowseScreen';
import CartScreen, { cartScreenName } from '../general/cart/CartScreen';
import ReviewOrderScreen, { reviewOrderScreenName } from '../general/cart/ReviewOrderScreen';
import OrderConfirmationScreen, { orderConfirmationScreenName } from '../general/cart/OrderConfirmationScreen';

const mapStateToProps = ({nav: {routes}}) => ({
  nav: find(routes, { key: trendingNavigatorName }),
});

const TrendingNavigator = StackNavigator(
  trendingScreenName,
  mapStateToProps,
  {
    [cartScreenName]: { screen: CartScreen },
    [giveFeedbackScreenName]: { screen: GiveFeedbackScreen },
    [orderConfirmationScreenName]: { screen: OrderConfirmationScreen },
    [reviewOrderScreenName]: { screen: ReviewOrderScreen },
    [searchScreenName]: { screen: SearchScreen },
    [trendingScreenName]: { screen: TrendingScreen },
    [trendingMenuBrowseScreenName]: { screen: TrendingMenuBrowseScreen }
  }
);

export default TrendingNavigator;

export const trendingNavigatorName = 'trendingNavigator';
import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Platform, ImageBackground, View, TouchableOpacity, Image, Dimensions } from 'react-native';
import { Container, Text, Icon, Button, Input, Item, Content } from 'native-base';
import commonColor from '../../native-base-theme/variables/commonColor';
import { Permissions, Location, IntentLauncherAndroid } from 'expo';
import CenteredH3 from '../general/components/CenteredH3';
import { RootState } from '../general/redux/RootState';
import { updateCurrentLocation } from '../general/redux/locationActions';
import { LocationState } from '../general/redux/locationReducer';
import HeaderCart from '../general/components/header/HeaderCart';
import { NavigationInjectedProps } from 'react-navigation';

type state = {
  isLocationGranted: boolean,
  isLocationLoading: boolean,
}

type props = {
  location: LocationState
  setLocation: (location: LocationState) => void
}

class TrendingScreen extends React.Component<props & NavigationInjectedProps, state> {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: <Image source={require('../../assets/foodflick800white.png')} resizeMode="contain" style={styles.image}/>,
    headerRight: (
      <React.Fragment>
        {/* <Icon style={styles.headerSearch} name='search' onPress={() => navigation.navigate('searchScreen')} /> */}
        <HeaderCart />
      </React.Fragment>
    )
  });

  state = {
    isLocationGranted: false,
    isLocationLoading: true,
  }
  // CODE IS COMMENTED OUT BECAUSE WE DONT NEED LOCATION AND THERE IS NO TRENDING SCREEN IN V1. TURN THIS BACK ON
  // IF WE HAVE ACTUAL TRENDING SCREEN

  // componentWillMount() {
  //   const isGranted = this.getLocationPermission();
  //   if (isGranted) {
  //     this.getLocation(false);
  //   }
  // }

  // getLocationPermission = async () => {
  //   if (this.state.isLocationGranted) {
  //     return true;
  //   }

  //   const { status } = await Permissions.askAsync(Permissions.LOCATION);
  //   const isGranted = status === 'granted';
  //   this.setState({ isLocationGranted: isGranted });
  //   return isGranted;
  // }

  // getLocation = async (shouldGoToLocationSettings = false) => {
  //   console.log('getting location');
  //   // todo 1: move this logic out of the component and into an action or something. problem is that actions aren't
  //   // getters. thought about putting it in reducer, but then it's not a pure function.
  //   try {
  //     if (this.getLocationPermission()) {
  //       this.setState({ isLocationLoading: true });
  //       // need high accuracy because https://github.com/expo/expo/issues/946
  //       const location: Location.LocationData = await Location.getCurrentPositionAsync({ enableHighAccuracy: true });
  //       console.log('location', location);
  //       this.setState({ isLocationLoading: false });
  //       this.props.setLocation({
  //         lat: location.coords.latitude,
  //         lon: location.coords.longitude,
  //       });
  //     }
  //   } catch (e) {
  //     console.log('error', JSON.stringify(e));
  //     this.setState({ isLocationLoading: false });
  //     if (e.code === 'E_LOCATION_SERVICES_DISABLED' && Platform.OS === 'android' && shouldGoToLocationSettings) {
  //       const hasReturnedToApp = await IntentLauncherAndroid.startActivityAsync(
  //         IntentLauncherAndroid.ACTION_LOCATION_SOURCE_SETTINGS
  //       );
  //       if (hasReturnedToApp) this.getLocation();
  //     }
  //   }
  // }

  render() {
    // if (!this.state.isLocationGranted) {
    //   return (
    //     <Container>
    //       <CenteredH3 text="Location access denied. Please allow location access then try again" />
    //       <Button block onPress={() => this.getLocation()}>
    //         <Text>Try again</Text>
    //       </Button>
    //     </Container>
    //   )
    // }

    // if (this.state.isLocationLoading) return <CenteredH3 text='Finding your location...' />

    // if (!this.props.location) {
    //   return (
    //     <Container>
    //       <CenteredH3 text="Can't get your location." />
    //       <Button block onPress={() => this.getLocation(true)}>
    //         <Text>Turn on high-accuracy location and try again</Text>
    //       </Button>
    //     </Container>
    //   )
    // }

    return <Text>Popular restaurants in Boston</Text>
    // return (
    //   <ImageBackground source={require('../../assets/homeBackground.jpg')} style={styles.background}>
    //     <CenteredH3 text='Welcome' style={styles.header} />
    //     <Item style={styles.search} onPress={() => this.props.navigation.navigate('searchScreen')}>
    //       <Input placeholder='What are you hungry for?' disabled/>
    //       <Icon active name='search' />
    //     </Item>
    //   </ImageBackground>
    // )
  }
}

const styles = StyleSheet.create({
  image: {
    width: Dimensions.get("window").width / 3,
    marginLeft: 10,
  },
  background: {
    flex: 1,
  },
  header: {
    fontWeight: 'bold',
  },
  headerSearch: {
    color: commonColor.brandCanvas,
    marginRight: commonColor.contentPadding,
  },
  search: {
    elevation: 4, // android only
    shadowColor: '#000', // ios
    shadowOffset: { width: 0, height: 1 }, // ios
    shadowOpacity: 0.8, // ios
    shadowRadius: 2, // ios
    backgroundColor: commonColor.brandCanvas,
    marginLeft: commonColor.contentPadding,
    marginRight: commonColor.contentPadding
  }
});

const mapStateToProps = (state: RootState) => ({
  location: state.getLocation(),
});

const mapDispatchToProps = (dispatch) => ({
  setLocation: (location: LocationState): void => dispatch(updateCurrentLocation(location))
});

export default connect(mapStateToProps, mapDispatchToProps)(TrendingScreen);

export const trendingScreenName = 'trendingScreen';

import React from 'react';
import { NavigationRoute, NavigationScreenComponent, NavigationScreenProp } from 'react-navigation';
import { connect } from 'react-redux';
import { CustomerCategory, CustomerItem } from '../general/menu/CustomerMenu';
import { selectTrendingCategoryAction, toggleTrendingItemLikeAction } from '../general/menu/menuActions';
import MenuBrowser from '../general/components/MenuBrowser';
import { NavScreenOptions } from '../general/navModels/NavScreenOptions';
import { RootState } from '../general/redux/RootState';
import { CustomerRest } from '../general/rest/CustomerRest';
import { toggleTrendingRestFavoriteAction } from '../general/rest/restActions';
import { Toast } from 'native-base';

type props = {
  navigation: NavigationScreenProp<NavigationRoute>;
  selectCategory: (category: CustomerCategory) => any;
  selectedRest: CustomerRest
  toggleItemLike: (selectedRest: CustomerRest, category: CustomerCategory, itemName: string) => void;
  toggleRestFavorite: (selectedRest: CustomerRest) => void;
}

const TrendingMenuBrowseScreen: NavigationScreenComponent = (props: props) => (
  <MenuBrowser
    navigation={props.navigation}
    selectCategory={props.selectCategory}
    selectedRest={props.selectedRest}
    toggleItemLike={props.toggleItemLike}
    toggleRestFavorite={props.toggleRestFavorite}
  />
);

TrendingMenuBrowseScreen.navigationOptions = (nav: NavScreenOptions) => {
  const newNav: NavScreenOptions = { ...nav };
  if (!newNav.navigation.state.params) {
    newNav.navigation.state.params = {};
  }
  newNav.navigation.state.params.selectedRest = newNav.screenProps.getTrending().getSelectedRest();
  return MenuBrowser.navigationOptions(newNav);
};

const mapStateToProps = (state: RootState) => ({
  selectedRest: state.getTrending().getSelectedRest()
});

const mapDispatchToProps = dispatch => ({
  toggleItemLike: async (selectedRest: CustomerRest, category: CustomerCategory, itemName: string) => {
    await dispatch(toggleTrendingItemLikeAction(selectedRest, category, itemName));
  },
  toggleRestFavorite: async selectedRest => {
    await dispatch(toggleTrendingRestFavoriteAction(selectedRest));
  },
  selectCategory: category => dispatch(selectTrendingCategoryAction(category)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TrendingMenuBrowseScreen);

export const trendingMenuBrowseScreenName = 'trendingMenuBrowseScreen';
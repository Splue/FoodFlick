import React from 'react';
import { connect } from 'react-redux';
import { View, StyleSheet } from 'react-native';
import { Container, Text, List, ListItem, Item, Icon, Input } from 'native-base';
import { NavigationRoute, NavigationScreenProp } from 'react-navigation';
import RestService from '../general/rest/restService';
import commonColors from '../../native-base-theme/variables/commonColor';
import { selectTrendingRestAction } from '../general/rest/restActions';
import { trendingMenuBrowseScreenName } from './TrendingMenuBrowseScreen';
import { RootState } from '../general/redux/RootState';
import { CustomerRest } from '../general/rest/CustomerRest';
import { LocationState } from '../general/redux/locationReducer';
import SuggestionInput from '../general/components/SuggestionInput';

type props = {
  navigation: NavigationScreenProp<NavigationRoute>;
  currentLocation: LocationState,
  onPressSuggestion: (rest: CustomerRest) => void,
}

type state = {
  searchQuery: string,
  restSuggestions: CustomerRest[],
  selectedLocation: LocationState,
}

class SearchScreen extends React.Component<props, state> {
  static navigationOptions = () => ({
    header: null,
    tabBarVisible: false,
  });

  state = {
    searchQuery: '',
    restSuggestions: [],
    selectedLocation: this.props.currentLocation,
  }

  hasSelectedCurrentLocation = (): boolean => {
    if (!this.state.selectedLocation) return false;
    const { selectedLocation } = this.state;
    const { currentLocation } = this.props;
    return selectedLocation.lat === currentLocation.lat && selectedLocation.lon === currentLocation.lon;
  }

  updateSearchSuggestions = searchQuery => {
    RestService.getRestSearchSuggestions(searchQuery, this.state.selectedLocation)
      .then(restSuggestions => {
        // depending on network speed, it is possible to receive suggestions out of the order they were made.
        // ex: suggestions for 'foo' may come after 'food' so we only set state when suggestions correspond to the
        // most recent query.
        if (this.state.searchQuery === searchQuery) {
          this.setState({ restSuggestions });
        }
      });
  }

  renderSuggestionInput = controledSuggestionFetch => (
    <Searchbar placeholder='What are you hungry for?' icon='arrow-back' autoFocus
    onChangeText={searchQuery => this.setState({ searchQuery }, controledSuggestionFetch(searchQuery))}
    onIconPress={() => this.props.navigation.goBack()}/>
  )

  render() {
    const { onPressSuggestion } = this.props;
    return (
      <Container>
        <View>
          <SuggestionInput renderInput={this.renderSuggestionInput} updateSearchSuggestions={this.updateSearchSuggestions} />
          <Searchbar placeholder={this.hasSelectedCurrentLocation() ? 'Current location' : 'Boston, MA'}
          icon='pin' disabled/>
        </View>
        <List dataArray={this.state.restSuggestions} style={styles.list} renderRow={restSuggestion =>
          <ListItem onPress={() => onPressSuggestion(restSuggestion)}>
            <Text>{restSuggestion.profile.name}</Text>
          </ListItem>}>
        </List>
      </Container>
    );
  }
}

type searchBarProps = {
  disabled?: boolean,
  autoFocus?: boolean,
  placeholder: string,
  icon: string,
  onChangeText: (query: string) => void,
  onIconPress: () => void,
}

const Searchbar = ({ disabled, autoFocus, placeholder, icon, onChangeText, onIconPress }: searchBarProps) => (
  <Item regular style={styles.search}>
    <Icon name={icon} onPress={onIconPress} />
    <Input disabled={disabled} placeholder={placeholder} autoFocus={autoFocus} style={styles.input}
    onChangeText={onChangeText}/>
  </Item>
);

const styles = StyleSheet.create({
  list: {
    flex: 1,
    backgroundColor: commonColors.brandCanvas,
  },
  search: {
    backgroundColor: commonColors.brandCanvas,
    marginRight: 5
  },
  input: {
    margin: -5
  }
})

const mapStateToProps = (state: RootState) => ({
  currentLocation: state.getLocation(),
});

const mapDispatchToProps = (dispatch, {navigation: { navigate }}) => ({
  onPressSuggestion: rest => {
    dispatch(selectTrendingRestAction(rest));
    navigate(trendingMenuBrowseScreenName);
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchScreen);
export const searchScreenName = 'searchScreen';
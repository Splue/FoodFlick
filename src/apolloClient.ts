import { activeConfig } from './config';
import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { onError } from 'apollo-link-error';
import { InMemoryCache, defaultDataIdFromObject, NormalizedCacheObject  } from 'apollo-cache-inmemory';
import { Toast } from 'native-base';
import { captureException } from 'sentry-expo';
import { removeLoading } from './general/redux/uiActions';

let apolloClient: ApolloClient<NormalizedCacheObject>;
const initApolloClient = store => {
  const httpLink = new HttpLink({uri: `${activeConfig.app.apiUrl}/graphql`})
  const errorLink = onError(e => {
    const { graphQLErrors, networkError, operation } = e;
    const { variables, operationName } = operation;
    let toastMsg = '';
    let captureMessage = '';
    if (networkError) {
      console.log('network error', networkError)
      toastMsg = networkError.message;
      captureMessage = `[Network error]: '${networkError}', Operation: '${operationName}', variables: '${JSON.stringify(variables)}`;
      captureException(new Error(captureMessage));
    }

    if (graphQLErrors) {
      graphQLErrors.forEach(({ message, path }) => {
        console.log('graphql error', message);
        toastMsg = toastMsg.length === 0 ? message : toastMsg + '\n' + message;
        const str = `[GraphQL error]: Message: '${message}', Path: '${path}', variables: '${JSON.stringify(variables)}'`;
        captureMessage = captureMessage.length === 0 ? str : captureMessage + '\n' + str;
        captureException(new Error(captureMessage));
      });
    }

    store.dispatch(removeLoading());
    Toast.show({
      text: toastMsg,
      type: 'warning',
      buttonText: 'Okay',
      position: 'bottom',
      duration: 30000,
    });
  });

  const authLink = setContext((_, { headers }) => {
    const signedInUser = store.getState().signedInUser || {};
    const {
      accessToken: {
        type = null,
        token = null,
      } = {}
    } = signedInUser;
    return {
      headers: {
        ...headers,
        //undefined, otherwise server will receive a null authorization header which is wrong
        authorization: type && token ? type + ' ' + token : undefined
      }
    }
  });

  const link = ApolloLink.from([
    authLink,
    errorLink,
    httpLink,
  ]);
  
  apolloClient =  new ApolloClient({
    link,
    cache: new InMemoryCache({
      //why? see https://stackoverflow.com/questions/48840223/apollo-duplicates-first-result-to-every-node-in-array-of-edges/49249163#49249163
      dataIdFromObject: o => (o._id ? `${o.__typename}:${o._id}`: null),
    }),
    // defaultOptions: {
    //   query: {
    //     // fetchPolicy: 'network-only',
    //     errorPolicy: 'all',
    //   },
    //   mutate: {
    //     errorPolicy: 'all'
    //   }
    // }
  });

  return apolloClient;
}

// exporting function to ensure we can only import and use a valid apolloClient. otherwise, if we simply export default
// apolloClient, we are subject to file load order which is hard to debug
export default () => apolloClient;

export { initApolloClient };

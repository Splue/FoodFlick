import variable from "./../variables/platform";
import commonColor from '../variables/commonColor';

export default (variables = variable) => {
  const platform = variables.platform;

  const theme = {
    "NativeBase.Item": {
      ".fixedLabel": {
        "NativeBase.Label": {
          paddingLeft: null
        },
        marginLeft: commonColor.formInputMarginLeft
      },
      ".inlineLabel": {
        "NativeBase.Label": {
          paddingLeft: null
        },
        marginLeft: commonColor.formInputMarginLeft
      },
      ".placeholderLabel": {
        "NativeBase.Input": {}
      },
      ".stackedLabel": {
        "NativeBase.Label": {
          top: 5,
          paddingLeft: null
        },
        "NativeBase.Input": {
          paddingLeft: null,
          marginLeft: null
        },
        "NativeBase.Icon": {
          marginTop: 36
        },
        marginLeft: commonColor.formInputMarginLeft
      },
      ".floatingLabel": {
        "NativeBase.Input": {
          paddingLeft: null,
          top: 10,
          marginLeft: null
        },
        "NativeBase.Label": {
          left: 0,
          top: 8
        },
        "NativeBase.Icon": {
          top: 6
        },
        marginTop: 15,
        marginLeft: commonColor.formInputMarginLeft
      },
      ".regular": {
        "NativeBase.Label": {
          left: 0
        },
        marginLeft: 0
      },
      ".rounded": {
        "NativeBase.Label": {
          left: 0
        },
        marginLeft: 0
      },
      ".underline": {
        "NativeBase.Label": {
          left: 0,
          top: 0,
          position: "relative"
        },
        "NativeBase.Input": {
          left: -15
        },
        marginLeft: commonColor.formInputMarginLeft
      },
      ".last": {
        marginLeft: 0,
        paddingLeft: 15
      },
      "NativeBase.Label": {
        paddingRight: 5
      },
      marginLeft: commonColor.formInputMarginLeft
    }
  };

  return theme;
};

import variable from "./../variables/platform";
import commonColors from './../variables/commonColor';

export default (variables = variable) => {
  const iconTheme = {
    fontSize: variables.iconFontSize,
    color: commonColors.brandInfo
  };

  return iconTheme;
};

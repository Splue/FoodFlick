import { Platform, Dimensions } from "react-native";

import variable from "./../variables/platform";
import commonColor from './../variables/commonColor';

const deviceHeight = Dimensions.get("window").height;
export default (variables = variable) => {
  const theme = {
    flex: 1,
    height: commonColor.containerHeight,
    backgroundColor: commonColor.brandNeutral
  };

  return theme;
};

import color from "color";

import { Platform, Dimensions, PixelRatio } from "react-native";

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const platform = Platform.OS;
const platformStyle = undefined;

const scarlet = "#632822"; // old = #85362e
const blush = "#9d5458"; // old = #945d60
const lightGray = '#f8f8f8';
const charcoal = '#3c3c3c';
const nickelGray = '#757575';
const elephantGray = '#c9c9c9';
const leafGreen = '#536a4f'; //old color = #626e60, #586356

const brandBase = leafGreen;
const brandPrimary = leafGreen; // used to be scarlet
const brandInfo = blush;
const brandInfoLighten = x => color(brandInfo).lighten(x);
const brandDark = charcoal;
const brandFaintAccent = nickelGray;
const brandText = charcoal;
const brandNeutral = lightGray;
const brandFaint = elephantGray;
const brandSuccess = "#5cb85c";
const brandDanger = "#d9534f";
const brandWarning = "#f0ad4e";
const brandSidebar = "#252932";
const brandCanvas = "#ffffff";

export default {
  platformStyle,
  platform,
  // AndroidRipple
  androidRipple: true,
  // androidRippleColor: "rgba(256, 256, 256, 0.3)",
  androidRippleColor: "rgba(0, 0, 0, 0.15)",
  androidRippleColorDark: "rgba(0, 0, 0, 0.15)",

  // Badge
  badgeBg: "#ED1727",
  badgeColor: "#fff",
  // New Variable
  badgePadding: platform === "ios" ? 3 : 0,

  // Button
  btnFontFamily: platform === "ios" ? "System" : "Roboto_medium",
  btnDisabledBg: "#b5b5b5",
  btnDisabledClr: "#f1f1f1",

  // CheckBox
  CheckboxRadius: platform === "ios" ? 13 : 0,
  CheckboxBorderWidth: platform === "ios" ? 1 : 2,
  CheckboxPaddingLeft: platform === "ios" ? 4 : 2,
  CheckboxPaddingBottom: platform === "ios" ? 0 : 5,
  CheckboxIconSize: platform === "ios" ? 21 : 14,
  CheckboxIconMarginTop: platform === "ios" ? undefined : 1,
  CheckboxFontSize: platform === "ios" ? 23 / 0.9 : 18,
  DefaultFontSize: 17,
  checkboxBgColor: brandInfo,
  checkboxSize: 20,
  checkboxTickColor: "#fff",
  checkboxDefaultColor: 'transparent',
  checkboxTextShadowRadius: 0,

  // Segment
  segmentBackgroundColor: 'transparent',
  segmentActiveBackgroundColor: brandBase,
  segmentTextColor: brandText,
  segmentActiveTextColor: brandCanvas,
  segmentBorderColor: brandText,
  segmentBorderColorMain: brandBase,

  // New Variable
  get defaultTextColor() {
    return this.textColor;
  },

  get btnPrimaryBg() {
    return this.brandPrimary;
  },
  get btnPrimaryColor() {
    return this.inverseTextColor;
  },
  get btnInfoBg() {
    return this.brandInfo;
  },
  get btnInfoColor() {
    return this.inverseTextColor;
  },
  get btnSuccessBg() {
    return this.brandSuccess;
  },
  get btnSuccessColor() {
    return this.inverseTextColor;
  },
  get btnDangerBg() {
    return this.brandDanger;
  },
  get btnDangerColor() {
    return this.inverseTextColor;
  },
  get btnWarningBg() {
    return this.brandWarning;
  },
  get btnWarningColor() {
    return this.inverseTextColor;
  },
  get btnTextSize() {
    return platform === "ios" ? this.fontSizeBase * 1.1 : this.fontSizeBase - 1;
  },
  get btnTextSizeLarge() {
    return this.fontSizeBase * 1.5;
  },
  get btnTextSizeSmall() {
    return this.fontSizeBase * 0.8;
  },
  get borderRadiusLarge() {
    return this.fontSizeBase * 3.8;
  },

  buttonPadding: 6,

  get iconSizeLarge() {
    return this.iconFontSize * 1.5;
  },

  get iconSize() {
    return this.iconFontSize;
  },

  get iconSizeSmall() {
    return this.iconFontSize * 0.6;
  },

  // Color
  brandBase,
  brandCanvas,
  brandPrimary,
  brandInfo,
  brandInfoLighten,
  brandSuccess,
  brandDanger,
  brandWarning,
  brandSidebar,
  brandDark,
  brandNeutral,
  brandFaint,
  brandText,
  brandFaintAccent,

  // Font
  fontFamily: platform === "ios" ? "System" : "Roboto",
  fontSizeBase: 15,

  get fontSizeH1() {
    return this.fontSizeBase * 1.8;
  },
  get fontSizeH2() {
    return this.fontSizeBase * 1.6;
  },
  get fontSizeH3() {
    return this.fontSizeBase * 1.4;
  },

  // Footer
  footerHeight: 55,
  footerDefaultBg: "#ffffff",

  // FooterTab
  tabBarTextColor: brandBase,
  tabBarTextSize: platform === "ios" ? 14 : 11,
  activeTab: platform === "ios" ? "#007aff" : "#fff",
  sTabBarActiveTextColor: "#007aff",
  tabBarActiveTextColor: "#fff",
  tabActiveBgColor: platform === "ios" ? "#1569f4" : undefined,

  // Form
  formInputMarginLeft: 15,

  // Tab
  tabDefaultBg: "#fff",
  topTabBarTextColor: brandText, // doesn't actually do anything
  topTabBarActiveTextColor: brandText,
  topTabActiveBgColor: "#fff", // doesn't actually do anything...
  topTabBarBorderColor: "#fff",
  topTabBarActiveBorderColor: brandPrimary,

  // Header
  toolbarBtnColor: "#fff",
  toolbarDefaultBg: "#2874F0",
  toolbarHeight: platform === "ios" ? 64 : 56,
  toolbarIconSize: platform === "ios" ? 20 : 22,
  toolbarSearchIconSize: platform === "ios" ? 20 : 23,
  toolbarInputColor: platform === "ios" ? "#CECDD2" : "#fff",
  searchBarHeight: platform === "ios" ? 30 : 40,
  toolbarInverseBg: "#222",
  toolbarTextColor: brandCanvas,
  iosStatusbar: "light-content",
  toolbarDefaultBorder: "#2874F0",
  get statusBarColor() {
    return color(this.toolbarDefaultBg).darken(0.2).hex();
  },

  // Icon
  iconFamily: "Ionicons",
  iconFontSize: platform === "ios" ? 30 : 28,
  iconMargin: 7,
  iconHeaderSize: platform === "ios" ? 33 : 24,

  // InputGroup
  inputFontSize: 17,
  inputBorderColor: "#D9D5DC",
  inputSuccessBorderColor: "#2b8339",
  inputErrorBorderColor: "#ed2f2f",

  get inputColor() {
    return this.textColor;
  },
  get inputColorPlaceholder() {
    return "#575757";
  },

  inputGroupMarginBottom: 10,
  inputHeightBase: 50,
  inputPaddingLeft: 5,

  get inputPaddingLeftIcon() {
    return this.inputPaddingLeft * 8;
  },

  // Line Height
  btnLineHeight: platform === 'ios' ? 17 : 19, // 17 is manually determined via inspection
  lineHeightH1: 34,
  lineHeightH2: 29,
  lineHeightH3: 24,
  iconLineHeight: platform === "ios" ? 37 : 30,
  lineHeight: platform === "ios" ? 20 : 24,

  // List
  listBg: "#fff",
  listBorderColor: elephantGray,
  listDividerBg: "#f4f4f4",
  listItemHeight: 45,
  listBtnUnderlayColor: "#DDD",

  // Card
  cardBorderColor: "#ccc",

  // Changed Variable
  listItemPadding: platform === "ios" ? 10 : 12,
  listItemThinPadding: platform === "ios" ? 4 : 6,

  listNoteColor: "#808080",
  listNoteSize: 13,

  // Container
  containerHeight: Platform.OS === "ios" ? deviceHeight : deviceHeight - 20,

  // Progress Bar
  defaultProgressColor: "#E4202D",
  inverseProgressColor: "#1A191B",

  // Radio Button
  radioBtnSize: platform === "ios" ? 25 : 23,
  radioSelectedColorAndroid: brandInfo,

  // New Variable
  radioBtnLineHeight: platform === "ios" ? 29 : 24,

  radioColor: "#7e7e7e",

  get radioSelectedColor() {
    return color(this.radioColor).darken(0.2).hex();
  },

  // Spinner
  defaultSpinnerColor: "#45D56E",
  inverseSpinnerColor: "#1A191B",

  // Tabs
  tabBgColor: "#F8F8F8",
  tabFontSize: 15,
  tabTextColor: "#222222",

  // Text
  textColor: brandText,
  inverseTextColor: "#fff",
  noteFontSize: 14,

  // Title
  titleFontfamily: platform === "ios" ? "System" : "Roboto_medium",
  titleFontSize: platform === "ios" ? 17 : 19,
  subTitleFontSize: platform === "ios" ? 12 : 14,
  subtitleColor: "#FFF",

  // New Variable
  titleFontColor: "#FFF",

  // Other
  borderRadiusBase: platform === "ios" ? 5 : 2,
  borderWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
  contentPadding: 10,

  get darkenHeader() {
    return color(this.tabBgColor).darken(0.03).hex();
  },

  dropdownBg: "#000",
  dropdownLinkColor: "#414142",
  inputLineHeight: platform === "ios" ? 21 : 24, // 21 is manually determined via inspection
  jumbotronBg: "#C9C9CE",
  jumbotronPadding: 30,
  deviceWidth,
  deviceHeight,

  // New Variable
  inputGroupRoundedBorderRadius: 30
};

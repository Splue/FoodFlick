# Demo
![Foodflick demo](/docs/demo.gif)

video of demo - https://www.youtube.com/watch?v=gOXhAjrQZm8

# Arch Overview
see more flow explanations in /docs/flows
![Foodflick demo](/docs/arch.png)

# Set Up Development Environment
* Node 8.5
* [Android Emulator](https://facebook.github.io/react-native/docs/getting-started.html#android-development-environment)
* [Expo](https://docs.expo.io/versions/latest/introduction/installation.html) React Native runner (like jdk) (no need for Genymotion - Android studio virtual machine is enough)
* [React Native Debugger](https://github.com/jhen0409/react-native-debugger/releases) - debugger guide [here](https://medium.com/@jimgbest/react-native-debugger-expo-awesome-d7a00da51460) - when you open the debugger, do ctrl + shift + t to open a new window with custom port
* Elasticsearch + Kibana

## Running the app

https://github.com/react-community/create-react-native-app/issues/270 to make sure that expo
grabs the correct lan address. see comment by freddy c and ahmetkakici
change priority with https://www.ghacks.net/2016/12/02/change-network-adapter-priorities-in-windows-10/.
this is really only a problem if you use genymotion with virtual box

install globally
npm install -g typescript



1) modify the ip inside apolloClient.ts to match your ip. In windows, find your ip with `ipconfig` (windows) or `ifconfig` (mac) in terminal
2) run your emulator. See the fb docs on setting this up. Once you create the emulate and give it a name, you can run it by seeing the "Some annoying things" section.
3) `npm install -g expo-cli`
4) `npm install`
5) open the react native debugger to port 19001 (or whatever Metro Bundler is running on)
6) `expo start` -- this brings up a website with to manage expo settings fo FoodFlick. Set connection to lan on the left.
7) in terminal, restart expo by pressing `r`. Make sure to see the following output

```
[02:06:29] Restarting Metro Bundler...
[02:06:33] Starting Metro Bundler on port 19003.
[02:06:34] Metro Bundler ready.
[02:06:35] Successfully ran `adb reverse`. Localhost URLs should work on the connected Android device.
[02:06:35] Tunnel ready.
```
8) in terminal press `a` to open in android. You should see

>Building JavaScript bundle 

9) you can now log in as `owner1.foodflick@gmail.com / owner1.ff`. The actual gmail password is `owner1.ff`

## To run with dev mode
Do the same thing but click on the emulator and do ctrl+m to bring up dev menu. Click dev mode
and make sure react debugger is listening on the right port


## Some annoying things

If app needs to totally rebuild, sometimes it wont start building if you have debug mode on. Turn off debug mode
and then hit `r` to rebuild. Once build is finished, turn on debug mode again

### Windows

To add an emulator shortcut to your taskbar...
write a .bat file in C:\Users\simon\AppData\Local\Android\sdk\emulator

to include 

`%ANDROID_HOME%\emulator\emulator.exe -avd <your device name here>`

### Mac

`emulator -avd <device name here>` (make sure emulator is in your path)

Notes

content component in native base
        {/* the content here uses scrollview internally. so if i wanted to style to content, then i
        need to use contentContainerStyle. see the reactdocs for scrollview*/}
        {/* <Content contentContainerStyle={styles.center}> */}


  container component

        //the entire phone is the screen. and it's using flex layout. so if we have 1 container, namely
      //the container here and we do flex: 1 for this container then it takes up the whole screen
      //but if we do flex .5 (doesn't actually exist) then this container will only take up half the
      //screen. the only exception is that we are using a footer and somehow magically the nav
      //package removed the footer as available space of the phone.

component files will only navigate via navigate('somescreen'); they will not do any more than that such as go back or reseting any router.

all the more complicated nav stuff goes in actions as they are usually effects that occur after an async action is complete


put async infront of all dispatches in connect fn to treat them all the same. acutal async fns can throw errors and we want to bubble them up. if no await in front of actual async then...

1) next line will run too early
2) promises will be uncaught and thrown to no where



download elasticsearch and kibana.

set elastic repo path following directions here
https://jee-appy.blogspot.com/2016/11/elasticsearch-backup-and-restore.html

for windows... actually change path.repo at
C:\ProgramData\Elastic\Elasticsearch\config\elasticsearch.yml
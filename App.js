import App from './src/App'
import { init, setRelease } from 'sentry-expo';
import Constants from 'expo-constants';

init({
  dsn: 'https://06c7e3c157d14758918f35277f875448@sentry.io/1777199',
});
setRelease(Constants.manifest.revisionId);

// this file is expo's entry point. place logical App.js into src to put all source into same folder for typescript
export default App